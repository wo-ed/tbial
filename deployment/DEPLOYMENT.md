# Praktikum Software Engineering für Fortgeschrittene: The Bug is a Lie

## Prerequistes
- *Install Git*
- *Install Maven*
- *Install Tomcat*
- *Set your Tomcat Home directory in the CATALINA_HOME environment variable* (Example ```export CATALINA_HOME=/usr/local/apache-tomcat-9.0.0.M21```)

## Deployment
- *Place DB-Config file (context.xml) in ~/.tbial/* (Example [here](https://gitlab.cip.ifi.lmu.de/beckmo/TBIAL-2017/raw/master/de.lmu.ifi.sosy.tbial/src/main/webapp/META-INF/context.xml))
- *Place Email-Config file (emailConfig.txt) in ~/.tbial/* (Example [here](https://gitlab.cip.ifi.lmu.de/beckmo/TBIAL-2017/raw/master/de.lmu.ifi.sosy.tbial/src/main/java/de/lmu/ifi/sosy/tbial/util/emailConfig.txt))
- *Create folder for download and compilation of the application*
- *Download deployment-script ([here](https://gitlab.cip.ifi.lmu.de/beckmo/TBIAL-2017/raw/master/deployment/deploy.sh)) and put it into the installation folder*
- *Run deployment-script*: Open terminal and execute ```./deploy.sh```
- *Open http://localhost:8080/tbial-1.0/*
