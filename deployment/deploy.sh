# install git
# install maven
# install tomcat
# place config files in ~/.tbial/


git clone git@gitlab.cip.ifi.lmu.de:beckmo/TBIAL-2017.git

cd TBIAL-2017/de.lmu.ifi.sosy.tbial/

# copy db config
cp ~/.tbial/context.xml src/main/webapp/META-INF
# copy email config
cp ~/.tbial/emailconfig.txt src/main/java/de/lmu/ifi/sosy/tbial/util/

mvn install

cd target

# export CATALINA_HOME=/usr/local/apache-tomcat-9.0.0.M21

cp tbial-1.0.war $CATALINA_HOME/webapps

cd $CATALINA_HOME/bin

./catalina.sh start
