alter table users (
	add active boolean default false not null,
	add gameswon int default 0 not null,
	add gameslost int default 0 not null,
	add gamesparticipated int default 0 not null,
	add bugsplayed int default 0 not null,
	add systemintegrationparticipated int default 0 not null,
	add primary key (id)
);

create table tokens (
	tid int generated always as identity,
	token varchar(255)  unique not null,
	uid int references users(id));



 UPDATE users
        SET active = true;