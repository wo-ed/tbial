alter table users 
add active boolean default false not null,
add gameswon int default 0 not null,
add gameslost int default 0 not null,
add gamesparticipated int default 0 not null,
add bugsplayed int default 0 not null,
add systemintegrationparticipated int default 0 not null,
add PRIMARY KEY (ID);

CREATE SEQUENCE TOKENS_seq;
	
CREATE TABLE TOKENS (
	TID INT PRIMARY KEY NOT NULL DEFAULT NEXTVAL ('TOKENS_seq'),
	TOKEN VARCHAR(255) NOT NULL,
	UID INT REFERENCES USERS(ID),
	CONSTRAINT TOKEN_IS_UNIQUE UNIQUE (TOKEN));

 update users
        SET active = true;