drop table healthpoints;
drop table tokens;
drop table users;
drop table games;


create table users (
	id int generated always as identity,
	name varchar(255) unique not null,
	salt varchar(255) not null,
	hash varchar(255) not null,
	active boolean default false not null,
	gameswon int default 0 not null,
	gameslost int default 0 not null,
	gamesparticipated int default 0 not null,
	bugsplayed int default 0 not null,
	systemintegrationparticipated int default 0 not null,
	admin boolean default false not null,
	primary key (id));
	
create table tokens (
	tid int generated always as identity,
	token varchar(255)  unique not null,
	uid int references users(id));
	
create table games (
	gid int generated always as identity,
	name varchar(255) not null,
	bugsplayed int default 0 not null,
	excusesplayed int default 0 not null,
	solutionsplayed int default 0 not null,
	specialcardsplayed int default 0 not null,
	abilitycardsplayed int default 0 not null,
	stumblingcardsplayed int default 0 not null,
	durationintime varchar(255) default '00:00' not null,
	durationinturns int default 0 not null,
	primary key (gid));
	
delete from USERS where (NAME = 'Adam');

insert into USERS (NAME, SALT, HASH, ACTIVE, ADMIN) VALUES ('Adam', '4cpu2Km2XId3T+WXyXhr4StEvIlxOhVNune02BYfp+4=', 'F51C7CBD0BD436928E5306D7FA39DB255F39C67763C82B6C66E3BDFB9F6E5A08', true, true);

	
create table healthpoints (
	gid int references games(gid),
	uid int references users(id),
	healthpoints varchar(255) default '[]' not null,
	primary key (gid, uid));
