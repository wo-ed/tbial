package de.lmu.ifi.sosy.tbial.db;

import static de.lmu.ifi.sosy.tbial.TestUtil.hasNameAndPassword;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.util.VerificationToken;

public abstract class AbstractDatabaseTest {

	protected Database database;

	protected String name;

	protected String password;

	protected User user;

	@Before
	public void initGeneral() {
		name = "name";
		password = "pass";
		user = new User(name, password);
		user.setActive(true);

	}

	protected void addUser() {
		addUser(user);
	}

	protected abstract void addUser(User user);

	@Test(expected = NullPointerException.class)
	public void registerUserWhenNullNameGivenThrowsException() {
		database.register(null, password);
	}

	@Test(expected = NullPointerException.class)
	public void registerUserWhenNullPasswordGivenThrowsException() {
		database.register(name, null);
	}

	@Test
	public void hasUserWithNameWhenUserNotRegisteredReturnsFalse() {
		assertThat(database.nameTaken(name), is(false));
	}

	@Test
	public void hasUserWithNameWhenUserRegisteredReturnsTrue() {
		addUser();
		assertThat(database.nameTaken(name), is(true));
	}

	@Test
	public void registerUserWhenUserExistsReturnsNull() {
		addUser();
		User user = database.register(name, password);
		assertThat(user, is(nullValue()));
	}

	@Test
	public void registerUserWhenUserDoesNotExistReturnsUser() {
		User user = database.register(name, password);

		assertThat(user, hasNameAndPassword(name, password));
	}

	@Test(expected = NullPointerException.class)
	public void getUserWhenNullGivenThrowsException() {
		database.getUser(null);
	}

	@Test
	public void getUserWhenUserDoesNotExistReturnsNull() {
		addUser(new User("someoneelse", "withsomepassword"));
		User user = database.getUser(name);

		assertThat(user, is(nullValue()));
	}

	@Test
	public void getUserWhenNoUserExistsReturnsNull() {
		User user = database.getUser(name);

		assertThat(user, is(nullValue()));
	}

	@Test
	public void getUserWhenUserExistsReturnsUser() {
		addUser();

		User user = database.getUser(name);

		assertThat(user, hasNameAndPassword(name, password));
	}

	@Test
	public void getUserWhenMultipleUsersExistsReturnsCorrectUser() {
		addUser();
		addUser(new User("AnotherName", "AnotherPass"));

		User user = database.getUser(name);

		assertThat(user, hasNameAndPassword(name, password));
	}

	@Test
	public void checkStatInitialization() {
		User user = database.register(name, password);
		User dbUser = database.getUser(name);

		assertThat(user.getGamesWon(), is(0));
		assertThat(user.getGamesLost(), is(0));
		assertThat(user.getGamesParticipated(), is(0));
		assertThat(user.getBugsPlayed(), is(0));
		assertThat(user.getSystemIntegrationParticipated(), is(0));

		assertThat(dbUser.getGamesWon(), is(0));
		assertThat(dbUser.getGamesLost(), is(0));
		assertThat(dbUser.getGamesParticipated(), is(0));
		assertThat(dbUser.getBugsPlayed(), is(0));
		assertThat(dbUser.getSystemIntegrationParticipated(), is(0));
	}

	@Test
	public void checkGamesWon() {
		User user = database.register(name, password);
		user.incrementGamesWon(database);

		User dbUser = database.getUser(name);

		assertThat(user.getGamesWon(), is(1));
		assertThat(dbUser.getGamesWon(), is(1));
	}

	@Test
	public void checkGamesLost() {
		User user = database.register(name, password);
		user.incrementGamesLost(database);

		User dbUser = database.getUser(name);

		assertThat(user.getGamesLost(), is(1));
		assertThat(dbUser.getGamesLost(), is(1));
	}

	@Test
	public void checkGamesParticipated() {
		User user = database.register(name, password);
		user.incrementGamesParticipated(database);

		User dbUser = database.getUser(name);

		assertThat(user.getGamesParticipated(), is(1));
		assertThat(dbUser.getGamesParticipated(), is(1));
	}

	@Test
	public void checkBugsPlayed() {
		User user = database.register(name, password);
		user.incrementBugsPlayed(database);

		User dbUser = database.getUser(name);

		assertThat(user.getBugsPlayed(), is(1));
		assertThat(dbUser.getBugsPlayed(), is(1));
	}

	@Test
	public void checkSystemIntegrationParticipated() {
		User user = database.register(name, password);
		user.incrementSystemIntegrationParticipated(database);

		User dbUser = database.getUser(name);

		assertThat(user.getSystemIntegrationParticipated(), is(1));
		assertThat(dbUser.getSystemIntegrationParticipated(), is(1));
	}

	@Test
	public void checkTokenStored() {
		User user = database.register(name, password);
		VerificationToken vt = new VerificationToken(user);
		database.storeToken(user.getId(), vt.getToken());

		int i = 0;
		List<VerificationToken> tokens = database.getVerificationTokens();
		for (VerificationToken t : tokens) {
			if (user.getId() == t.getUser().getId()) {
				i++;
			}
		}
		assertThat(i, is(1));

	}

	@Test
	public void getVerificationTokensTest() {
		User user1 = database.register("u1", password);
		User user2 = database.register("u2", password);
		User user3 = database.register("u3", password);
		VerificationToken vt1 = new VerificationToken(user1);
		VerificationToken vt2 = new VerificationToken(user2);
		VerificationToken vt3 = new VerificationToken(user3);
		database.storeToken(user1.getId(), vt1.getToken());
		database.storeToken(user2.getId(), vt2.getToken());
		database.storeToken(user3.getId(), vt3.getToken());

		boolean containsVT1 = false;
		boolean containsVT2 = false;
		boolean containsVT3 = false;

		List<VerificationToken> tokens = database.getVerificationTokens();
		for (VerificationToken t : tokens) {
			if (vt1.getToken().equals(t.getToken())) {
				containsVT1 = true;
			} else if (vt2.getToken().equals(t.getToken())) {
				containsVT2 = true;
			} else if (vt3.getToken().equals(t.getToken())) {
				containsVT3 = true;
			}
		}

		assertThat(tokens.size(), is(3));
		assertTrue(containsVT1);
		assertTrue(containsVT2);
		assertTrue(containsVT3);
	}

	@Test(expected = NullPointerException.class)
	public void storeNullGame() {
		database.storeGameStats(null);
	}

	@Test(expected = NullPointerException.class)
	public void updateNullGame() {
		database.updateGameStats(null);
	}

	// @Test
	// public void checkGameStatsStoredAndUpdatedWithCorrectValues() {
	//
	// user = database.register(name, password);
	// User user2 = database.register("u2", "pass");
	// User user3 = database.register("u3", "pass");
	// User user4 = database.register("u4", "pass");
	//
	// Game game = new MockGame("TestGame", (short) 4, false, user, true);
	//
	// game.join(user2);
	// game.join(user3);
	// game.join(user4);
	//
	// Player p1 = game.getPlayer(user);
	// Player p2 = game.getPlayer(user2);
	// Player p3 = game.getPlayer(user3);
	// Player p4 = game.getPlayer(user4);
	//
	// game.init();
	//
	// game.initHealthPoints();
	//
	// // game is stored successfully
	// assertThat(database.storeGameStats(game), is(true));
	//
	// GameStat insertedGameStat = database.getGameStat(game.getId());
	//
	// // game was stored with correct default values
	// assertThat(insertedGameStat.getGameId(), is(game.getId()));
	// assertThat(insertedGameStat.getName(), is(game.getName()));
	// assertThat(insertedGameStat.getBugsPlayed(), is(0));
	// assertThat(insertedGameStat.getExcusesPlayed(), is(0));
	// assertThat(insertedGameStat.getSolutionsPlayed(), is(0));
	// assertThat(insertedGameStat.getSpecialCardsPlayed(), is(0));
	// assertThat(insertedGameStat.getAbilityCardsPlayed(), is(0));
	// assertThat(insertedGameStat.getStumblingCardsPlayed(), is(0));
	// assertThat(insertedGameStat.getDurationInTime(), is("00:00"));
	// assertThat(insertedGameStat.getDurationInTurns(), is(0));
	//
	// // existing game (game with the same id) cannot be stored again
	// assertThat(database.storeGameStats(game), is(false));
	//
	// game.setBugsPlayed(6);
	// game.setExcusesPlayed(5);
	// game.setSolutionsPlayed(4);
	// game.setSpecialCardsPlayed(3);
	// game.setSpecialAbilityPlayed(2);
	// game.setStumblingCardsPlayed(1);
	//
	// game.setStartTime(new Date());
	// long miliSec = game.getStartTime().getTime();
	// // set end time 5 min later
	// game.setEndTime(new Date(miliSec + 300000));
	// game.setTurnsCounter(10);
	//
	// while (p4.isAlive()) {
	// p4.getCharacter().decHp();
	// }
	//
	// game.updateHealthPoints();
	//
	// // game is updated successfully (healthpoints are stored successfully)
	// assertThat(database.updateGameStats(game), is(true));
	//
	// insertedGameStat = database.getGameStat(game.getId());
	//
	// assertThat(insertedGameStat.getGameId(), is(game.getId()));
	// assertThat(insertedGameStat.getName(), is(game.getName()));
	// assertThat(insertedGameStat.getBugsPlayed(), is(6));
	// assertThat(insertedGameStat.getExcusesPlayed(), is(5));
	// assertThat(insertedGameStat.getSolutionsPlayed(), is(4));
	// assertThat(insertedGameStat.getSpecialCardsPlayed(), is(3));
	// assertThat(insertedGameStat.getAbilityCardsPlayed(), is(2));
	// assertThat(insertedGameStat.getStumblingCardsPlayed(), is(1));
	// assertThat(insertedGameStat.getDurationInTime(), is("00:05"));
	// assertThat(insertedGameStat.getDurationInTurns(), is(10));
	//
	// // check healthpoints
	//
	// HealthPoints dbHealthPoints;
	// String gameHealthPoints;
	//
	// for (Player p : game.getPlayers()) {
	//
	// dbHealthPoints = database.getHealthPoints(game.getId(),
	// game.getUser(p).getId());
	// gameHealthPoints = game.getPlayerHealthPoints().get(p).toString();
	// assertThat(dbHealthPoints.getHealthpoints(), is(gameHealthPoints));
	//
	// }
	//
	// }

}
