package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.MockGame;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.actioncards.LANParty;

public class GameTest {
	Game game;
	User user;
	Player player1;
	Player player2;

	@Before
	public void init() {
		user = new User("testUser", "");
		game = new MockGame("Testgame", (short) 4, true, user, true);

		game.join(new User("0", "0"));
		game.join(new User("0", "0"));
		game.join(new User("0", "0"));
		game.init();
		player1 = game.getCurrentPlayer();
		player2 = game.getNextPlayer(player1);
	}

	@Test
	public void beginTurnCardsTest() {
		int handCardsSize = game.getCurrentPlayer().getHandcards().size();
		game.beginTurn();
		assertEquals(game.getCurrentPlayer().getHandcards().size(), handCardsSize + 2);
	}

	@Test
	public void endTurnTest() {

		// Only the current player should be able to end the turn or drop cards.
		player2.getHandcards().clear();
		assertFalse(game.tryEndTurn(player2));

		// Reset hand cards
		player1.getHandcards().clear();
		player1.getHandcards().addAll(
				IntStream.range(0, player1.getHp() + 2).mapToObj(i -> new LANParty()).collect(Collectors.toList()));
		player1.getHandcards().forEach(c -> c.setPlayer(player1));

		// Hand cards count = hp + 2
		assertFalse(game.tryEndTurn(player1));
		assertEquals(player1.getState(), PlayerState.ON_TURN);

		game.throwCardToHeap(player1.getHandcards().get(0));

		// Hand cards count = hp + 1
		assertFalse(game.tryEndTurn(player1));
		assertEquals(player1.getState(), PlayerState.ON_TURN);

		game.throwCardToHeap(player1.getHandcards().get(0));

		// Hand cards count = hp
		assertTrue(game.tryEndTurn(player1));
		assertEquals(player1.getState(), PlayerState.WAIT);

		assertNotEquals(game.getCurrentPlayer(), player1);
		assertEquals(game.canPlayBug(), true);
	}
}