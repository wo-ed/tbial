package de.lmu.ifi.sosy.tbial.db;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.util.Utils;

public class UserTest {

	private String password;

	private String name;

	private User user;

	private int id;

	@Before
	public void init() {
		password = "pass";
		name = "name";
		id = 42;
		user = new User("", "");
	}
	
	@Test
	public void hashIsSalted() {
		User userToCompare = new User("", "");
		assertThat(user.getHash(), not(userToCompare.getHash()));
	}

	@Test
	public void hashIsSHA256() {
		assertThat(Utils.hashPasswordWithSalt("test", ""),
				is("9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08"));
	}

	@Test(expected= NullPointerException.class)
	public void constructor_whenNullNameGiven_throwsException() {
		new User(null, password);
	}

	@Test(expected= NullPointerException.class)
	public void constructor_whenNullPasswordGiven_throwsException() {
		new User(name, null);
	}

	@Test
	public void getName_returnsName() {
		user.setName(name);
		assertThat(user.getName(), is(name));
	}

	@Test
	public void getId_returnsId() {
		user.setId(id);
		assertThat(user.getId(), is(id));
	}

	@Test(expected= NullPointerException.class)
	public void setName_whenNullNameGiven_throwsException() {
		user.setName(null);
	}
}
