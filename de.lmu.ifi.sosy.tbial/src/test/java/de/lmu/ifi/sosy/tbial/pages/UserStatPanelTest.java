package de.lmu.ifi.sosy.tbial.pages;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.util.tester.TagTester;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.PageTestBase;
import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.TBIALSession;
import de.lmu.ifi.sosy.tbial.cards.MockGame;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.RoleCard;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.actioncards.SystemIntegration;
import de.lmu.ifi.sosy.tbial.model.rolecards.Consultant;
import de.lmu.ifi.sosy.tbial.model.rolecards.EvilCodeMonkey;
import de.lmu.ifi.sosy.tbial.model.rolecards.HonestDeveloper;
import de.lmu.ifi.sosy.tbial.model.rolecards.Manager;
import de.lmu.ifi.sosy.tbial.model.visitor.PlayCardVisitor;

public class UserStatPanelTest extends PageTestBase {

	private MockGame game;
	private PlayCardVisitor visitor;
	protected Player player, player2, player3, player4, player5, player6, player7;

	ModalWindow modal;

	@Before
	public void setUp() {
		setupApplication();
		database.register("testuser", "1");
	}

	private void signIn() {
		TBIALSession session = getSession();
		session.signIn("testuser", "1");
	}

	private void setUpGame() {
		User user = getSession().getUser();
		User user2 = new User("", "");
		User user3 = new User("", "");
		User user4 = new User("", "");
		User user5 = new User("", "");
		User user6 = new User("", "");
		User user7 = new User("", "");

		this.game = new MockGame("g1", (short) 7, false, user, true);
		TBIALApplication.getInstance().addGame(game);
		TBIALApplication.getInstance().joinGame(getSession().getUser(), game);
		game.join(user2);
		game.join(user3);
		game.join(user4);
		game.join(user5);
		game.join(user6);
		game.join(user7);

		this.player = game.getPlayer(getSession().getUser());
		this.player2 = game.getPlayer(user2);
		this.player3 = game.getPlayer(user3);
		this.player4 = game.getPlayer(user4);
		this.player5 = game.getPlayer(user5);
		this.player6 = game.getPlayer(user6);
		this.player7 = game.getPlayer(user7);

		game.init();
		visitor = new PlayCardVisitor(game);

		modal = new ModalWindow("modal");

	}

	@Test
	public void linkOnGamePage() {
		signIn();

		TabPage page = tester.startPage(TabPage.class);
		Game gamePageGame = new Game("g2", (short) 7, false, getSession().getUser());
		TBIALApplication.getInstance().addGame(gamePageGame);
		TBIALApplication.getInstance().joinGame(getSession().getUser(), gamePageGame);
		page.setGameLobbyTab(gamePageGame.getName());
		GameLobbyPanel gameLobby = page.getCurrentGameLobby();

		gamePageGame.join(new User("", ""));
		gamePageGame.join(new User("", ""));
		gamePageGame.join(new User("", ""));

		gameLobby.startGame();

		tester.startPage(GamePage.create());
		TagTester tag = tester.getTagByWicketId("playerstat");
		Assert.assertNotNull(tag);
	}

	@Test
	public void allStatsShown() {
		signIn();

		modal = new ModalWindow("modal");

		UserStatPanel userStatPanel = tester.startComponentInPage(new UserStatPanel(modal.getId()));
		tester.assertComponent(modal.getId(), userStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("gameswon");
		Assert.assertEquals(tag.getValue(), "0");
		tag = tester.getTagByWicketId("gameslost");
		Assert.assertEquals(tag.getValue(), "0");
		tag = tester.getTagByWicketId("gamesparticipated");
		Assert.assertEquals(tag.getValue(), "0");
		tag = tester.getTagByWicketId("bugsplayed");
		Assert.assertEquals(tag.getValue(), "0");
		tag = tester.getTagByWicketId("systemintegrationparticipated");
		Assert.assertEquals(tag.getValue(), "0");
	}

	@Test
	public void allStatsAreShownWithCorrectValues() {
		signIn();
		User user = getSession().getUser();

		user.incrementGamesWon(database);
		user.incrementGamesLost(database);
		user.incrementGamesParticipated(database);
		user.incrementBugsPlayed(database);
		user.incrementSystemIntegrationParticipated(database);

		modal = new ModalWindow("modal");

		UserStatPanel userStatPanel = tester.startComponentInPage(new UserStatPanel(modal.getId()));
		tester.assertComponent(modal.getId(), userStatPanel.getClass());
		TagTester tag = tester.getTagByWicketId("gameswon");
		Assert.assertEquals(tag.getValue(), "1");
		tag = tester.getTagByWicketId("gameslost");
		Assert.assertEquals(tag.getValue(), "1");
		tag = tester.getTagByWicketId("gamesparticipated");
		Assert.assertEquals(tag.getValue(), "1");
		tag = tester.getTagByWicketId("bugsplayed");
		Assert.assertEquals(tag.getValue(), "1");
		tag = tester.getTagByWicketId("systemintegrationparticipated");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void testUpdateParticipatedGame() {
		signIn();
		setUpGame();

		UserStatPanel userStatPanel = tester.startComponentInPage(new UserStatPanel(modal.getId()));
		tester.assertComponent(modal.getId(), userStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("gamesparticipated");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void testUpdateBugPlayed() {
		signIn();
		setUpGame();

		game.setCurrentPlayer(player);
		game.cleanHandcards(player);
		game.cleanHandcards(player2);
		TurnCard bug = game.drawCardofClass(player, BugReport.class);
		bug.setTargetPlayer(player2);
		bug.play(this.visitor);

		UserStatPanel userStatPanel = tester.startComponentInPage(new UserStatPanel(modal.getId()));
		tester.assertComponent(modal.getId(), userStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("bugsplayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void testSystemIntgrationParitipated() {
		signIn();
		setUpGame();

		// User as initiator
		game.setCurrentPlayer(player);
		game.cleanHandcards(player);
		game.cleanHandcards(player2);
		TurnCard sysInt = game.drawCardofClass(player, SystemIntegration.class);
		sysInt.setTargetPlayer(player2);
		sysInt.play(this.visitor);

		// User as target
		game.setCurrentPlayer(player2);
		game.cleanHandcards(player);
		game.cleanHandcards(player2);
		sysInt = game.drawCardofClass(player2, SystemIntegration.class);
		sysInt.setTargetPlayer(player);
		sysInt.play(this.visitor);

		UserStatPanel userStatPanel = tester.startComponentInPage(new UserStatPanel(modal.getId()));
		tester.assertComponent(modal.getId(), userStatPanel.getClass());
		TagTester tag = tester.getTagByWicketId("systemintegrationparticipated");
		Assert.assertEquals(tag.getValue(), "2");
	}

	@Test
	public void testUpdateGameWon() {
		signIn();
		setUpGame();
		makeUserWin();

		UserStatPanel userStatPanel = tester.startComponentInPage(new UserStatPanel(modal.getId()));
		tester.assertComponent(modal.getId(), userStatPanel.getClass());
		TagTester tag = tester.getTagByWicketId("gameswon");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void testUpdateGameLost() {
		signIn();
		setUpGame();
		makeUserLose();
		UserStatPanel userStatPanel = tester.startComponentInPage(new UserStatPanel(modal.getId()));
		tester.assertComponent(modal.getId(), userStatPanel.getClass());
		TagTester tag = tester.getTagByWicketId("gameslost");
		Assert.assertEquals(tag.getValue(), "1");
	}

	public void makeUserWin() {
		RoleCard playerRole = game.getPlayer(getSession().getUser()).getRole();
		Player manager = game.getManager();
		Player consultant = game.getConsultant();
		List<Player> evilCodeMonkeys = game.getEvilCodeMonkeys();
		List<Player> honestDevelopers = game.getHonestDevelopers();
		Player honestDeveloper = honestDevelopers.get(0);

		assertTrue(playerRole instanceof Manager);
		// one honest developer is fired
		while (honestDeveloper.getHp() > 0) {
			game.decHp(honestDeveloper, consultant);
		}

		assertFalse(honestDeveloper.isAlive());
		assertTrue(honestDeveloper.getState() == PlayerState.DEAD);

		// consultant is fired
		while (consultant.getHp() > 0) {
			game.decHp(consultant, manager);
		}

		assertFalse(consultant.isAlive());
		assertTrue(consultant.getState() == PlayerState.DEAD);

		// all evil code monkeys are fired
		for (Player evilCodeMonkey : evilCodeMonkeys) {
			while (evilCodeMonkey.getHp() > 0) {
				game.decHp(evilCodeMonkey, manager);
			}

			assertFalse(evilCodeMonkey.isAlive());
			assertTrue(evilCodeMonkey.getState() == PlayerState.DEAD);
		}

		assertTrue(game.getWinners().size() == 3);

		assertFalse(game.getWinners().contains(consultant));
		assertFalse(game.getWinners().containsAll(evilCodeMonkeys));

		// manager and all honest developers win
		assertTrue(game.getWinners().contains(manager));
		assertTrue(game.getWinners().containsAll(honestDevelopers));
	}

	public void makeUserLose() {
		RoleCard playerRole = game.getPlayer(getSession().getUser()).getRole();
		if (playerRole instanceof Manager || playerRole instanceof HonestDeveloper
				|| playerRole instanceof Consultant) {
			for (Player p : game.getPlayers()) {
				if (p.getRole() instanceof Manager) {
					game.firePlayer(p);
				}
			}
		} else if (playerRole instanceof EvilCodeMonkey) {
			for (Player p : game.getPlayers()) {
				if (p.getRole() instanceof EvilCodeMonkey || p.getRole() instanceof Consultant) {
					game.firePlayer(p);
				}
			}
		}

	}

}
