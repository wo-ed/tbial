package de.lmu.ifi.sosy.tbial.pages;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.PageTestBase;
import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.cards.MockGame;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;

public class LobbyPageTest extends PageTestBase {

	@Before
	public void init() {
		setupApplication();
	}

	@Test
	public void addGamesTest() {
		Game game1 = new MockGame("game1", (short) 4, true, new User("test", ""), true);
		assertThat(game1.getName(), is("game1"));
		assertThat(game1.getCapacity(), is((short) 4));
		TBIALApplication.getInstance().addGame(game1);
		assertTrue(TBIALApplication.getGames().contains((game1)));
	}

}
