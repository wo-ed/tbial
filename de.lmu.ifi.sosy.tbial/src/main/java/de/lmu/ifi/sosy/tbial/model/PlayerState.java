package de.lmu.ifi.sosy.tbial.model;

public enum PlayerState {

	HEISENBUG, ON_TURN, DEFLECT_BUG, PLAY_BUG, STANDUP_MEETING, STEALING_MODE, DRAW_CARD, WAIT, DEAD, SYSTEM_INTEGRATION, PWND, REFACTORING, DROP_NEW_CARD, BORING_MEETING;

}
