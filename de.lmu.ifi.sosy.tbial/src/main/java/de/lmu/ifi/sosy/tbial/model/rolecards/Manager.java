package de.lmu.ifi.sosy.tbial.model.rolecards;

import de.lmu.ifi.sosy.tbial.model.RoleCard;

public final class Manager extends RoleCard {

	private static final long serialVersionUID = 1L;

	public Manager() {
		setVisible(true);
		setName("Manager");
	}
}
