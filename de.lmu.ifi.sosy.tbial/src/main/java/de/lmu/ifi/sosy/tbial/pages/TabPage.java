package de.lmu.ifi.sosy.tbial.pages;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.AjaxSelfUpdatingTimerBehavior;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.extensions.ajax.markup.html.tabs.AjaxTabbedPanel;
import org.apache.wicket.extensions.markup.html.tabs.AbstractTab;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.util.time.Duration;

import de.lmu.ifi.sosy.tbial.AuthenticationRequired;
import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.GameState;
import de.lmu.ifi.sosy.tbial.util.Message;

@AuthenticationRequired
public class TabPage extends MainPage {

	/**
	 * UID for serialization.
	 */
	private static final long serialVersionUID = 1L;

	private List<ITab> tabs = new ArrayList<>();

	private AbstractTab lobbyTab;
	private AbstractTab playerListTab;
	private AbstractTab gameLobbyTab;
	private AbstractTab userStatTab;

	private static final int lobbyTabIndex = 0;
	private static final int playerListTabIndex = 1;
	private static final int userStatTabIndex = 2;
	private static final int gameLobbyTabIndex = 3;

	private TabPage page;

	private AjaxTabbedPanel<AbstractTab> tabbedPanel;

	private LobbyPanel currentLobby = null;
	private GameLobbyPanel currentGameLobby = null;
	private Panel currentPlayerList = null;
	private UserStatPanel currentUserStat = null;

	private boolean invVisible = false;

	private WebMarkupContainer invitationContainer;

	private DataView<User> inviterView;

	private final AjaxButton showInvButton;

	// Es wird verwendet. Eclise kann das nicht erkennen, da die
	// Uebergabe von Attribute mit String(bei ajax Button Konstruktor) erfolgt.
	private String showInvButtonText;

	private List<User> inviterList;

	public TabPage() {
		super();

		page = (TabPage) this.getPage();

		lobbyTab = new AbstractTab(new Model<>("Games")) {
			private static final long serialVersionUID = 1L;

			@Override
			public Panel getPanel(String panelId) {
				return new LobbyTab(panelId, page);
			}

		};

		tabs.add(lobbyTab);

		playerListTab = new AbstractTab(new Model<>("Players")) {
			private static final long serialVersionUID = 1L;

			@Override
			public Panel getPanel(String panelId) {
				if (getSession().getUser().isAdmin()) {
					return new PlayerListTab(panelId, page, true);
				}
				return new PlayerListTab(panelId, page, false);
			}

		};

		tabs.add(playerListTab);

		userStatTab = new AbstractTab(new Model<>("User Statistics")) {
			private static final long serialVersionUID = 1L;

			@Override
			public Panel getPanel(String panelId) {
				return new UserStatTab(panelId);
			}

		};
		tabs.add(userStatTab);

		tabbedPanel = new AjaxTabbedPanel("tabs", tabs);

		add(tabbedPanel);

		inviterList = new LinkedList<User>();

		Form<?> showInvForm = new Form<>("showInvitation");

		invitationContainer = new WebMarkupContainer("invitationContainer");
		invitationContainer.setOutputMarkupId(true);

		setShowInvBtnText();
		showInvButton = new AjaxButton("showInvButton", new PropertyModel<String>(this, "showInvButtonText")) {
			private static final long serialVersionUID = 1;

			public void onSubmit(AjaxRequestTarget target, Form<?> form) {
				if (invVisible) {
					invVisible = false;
					setShowInvBtnText();
					target.add(this);
				} else {
					invVisible = true;
					setShowInvBtnText();
					inviterList = TBIALApplication.getUsersInvs(getUser());
					target.add(this);
					target.add(invitationContainer);

				}
				inviterView.setVisible(invVisible);
			};
		};

		ListDataProvider<User> listProvider = new ListDataProvider<User>() {
			private static final long serialVersionUID = 1L;

			protected List<User> getData() {
				return inviterList;
			}
		};

		inviterView = new DataView<User>("inviterList", listProvider) {

			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(Item<User> item) {
				User user = (User) item.getModelObject();
				item.add(new Label("name", user.getName()));
				Link<String> accept = new Link<String>("accept") {
					private static final long serialVersionUID = 1L;

					public void onClick() {
						acceptInvitation(user);
					}
				};
				item.add(accept);
				Link<String> decline = new Link<String>("decline") {
					private static final long serialVersionUID = 1L;

					public void onClick() {
						removeInvitation(user);
					}
				};
				item.add(decline);
			}

		};

		invitationContainer.add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(1)));

		invitationContainer.add(inviterView);

		showInvForm.add(showInvButton).add(invitationContainer);
		add(showInvForm);
	}

	protected class LobbyTab extends Panel {
		private static final long serialVersionUID = 1L;

		public LobbyTab(String id, TabPage page) {
			super(id);
			if (currentLobby == null) {
				LobbyPanel lobbyPanel = new LobbyPanel("lobbyTab", page);
				currentLobby = lobbyPanel;
			}
			currentLobby.refresh();

			add(currentLobby);
		}

	};

	protected class PlayerListTab extends Panel {
		private static final long serialVersionUID = 1L;

		public PlayerListTab(String id, TabPage page, boolean admin) {
			super(id);
			Panel playerListPanel;
			if (admin) {
				playerListPanel = new AdminPlayerListPanel("playerListTab", page);
			} else {
				playerListPanel = new PlayerListPanel("playerListTab", page);
			}
			currentPlayerList = playerListPanel;
			add(playerListPanel);
		}

	};

	protected class UserStatTab extends Panel {
		private static final long serialVersionUID = 1L;

		public UserStatTab(String id) {
			super(id);
			UserStatPanel userStatPanel = new UserStatPanel("userStatTab");
			currentUserStat = userStatPanel;
			add(currentUserStat);
		}

	};

	protected class GameLobbyTab extends Panel {
		private static final long serialVersionUID = 1L;

		public GameLobbyTab(String id, TabPage page, String game) {
			super(id);
			GameLobbyPanel gameLobbyPanel = new GameLobbyPanel("gameLobbyTab", page);
			currentGameLobby = gameLobbyPanel;
			add(currentGameLobby);
		}

	};

	public AjaxTabbedPanel<AbstractTab> getTabbedPanel() {
		return tabbedPanel;
	}

	public AbstractTab getLobbyTab() {
		return lobbyTab;
	}

	public AbstractTab getPlayerListTab() {
		return playerListTab;
	}

	public AbstractTab getUserStatTab() {
		return userStatTab;
	}

	public AbstractTab getGameLobbyTab() {
		return gameLobbyTab;
	}

	public void selectLobbyTab() {
		tabbedPanel.setSelectedTab(lobbyTabIndex);
	}

	public void selectPlayerListTab() {
		tabbedPanel.setSelectedTab(playerListTabIndex);
	}

	public void selectUserStatTab() {
		tabbedPanel.setSelectedTab(userStatTabIndex);
	}

	public void selectGameLobbyTab() {
		tabbedPanel.setSelectedTab(gameLobbyTabIndex);
	}

	public void setGameLobbyTab(String game) {

		gameLobbyTab = new AbstractTab(new Model<>("Game " + game)) {
			private static final long serialVersionUID = 1L;

			@Override
			public Panel getPanel(String panelId) {
				return new GameLobbyTab(panelId, page, game);
			}

		};

		if (tabs.size() > 3) {

			tabs.set(gameLobbyTabIndex, gameLobbyTab);
		} else {

			tabs.add(gameLobbyTab);
		}
		selectGameLobbyTab();
	}

	public void removeGameLobbyTab() {
		selectLobbyTab();
		tabs.remove(gameLobbyTabIndex);
		currentGameLobby = null;
		this.setResponsePage(this);
	}

	public void tryToJoinGame(Game game) {

		if (game == null) {
			// do nothing
			return;
		}

		else if (game.getState() == GameState.PAUSED) {
			joinGame(getUser(), game);
			setResponsePage(GamePage.create());
		}

		else if (currentGameLobby == null) {
			joinGame(getUser(), game);
		}

		else if (currentGameLobby.getGame().equals(game)) {

			selectGameLobbyTab();
		}

		else {

			String currentGameTitle = currentGameLobby.getGame().getName();
			String newGameTitle = game.getName();

			String message = String.format("You are about to leave game %s and enter game %s. Do you want to continue?",
					currentGameTitle, newGameTitle);

			currentLobby.replaceWith(new ConfirmationPanel(currentLobby.getId(), message) {
				private static final long serialVersionUID = 1L;

				@Override
				protected void onCancel() {
					this.replaceWith(currentLobby);
				}

				@Override
				protected void onConfirm() {
					currentGameLobby.leaveGame(getUser());
					joinGame(getUser(), game);
				}
			});

		}

	}

	// Schnittstelle zur GameLobby
	public void joinGame(User user, Game game) {
		if (TBIALApplication.getInstance().joinGame(user, game).isPresent()) {
			setGameLobbyTab(game.getName());
			return;
		}
	}

	public GameLobbyPanel getCurrentGameLobby() {
		return currentGameLobby;
	}

	public LobbyPanel getCurrentLobby() {
		return currentLobby;
	}

	public Panel getCurrentPlayerList() {
		return currentPlayerList;
	}

	/**
	 * Returns a list of all messages that should be displayed in the current
	 * chat window. Should be overridden concerning Game Chat / Whispering.
	 */
	@Override
	protected IModel<List<Message>> getMessagesToBeShown() {
		LoadableDetachableModel<List<Message>> model = new LoadableDetachableModel<List<Message>>() {
			private static final long serialVersionUID = 1L;

			@Override
			protected List<Message> load() {
				List<Message> messages;
				if (tabbedPanel.getSelectedTab() == gameLobbyTabIndex) {
					String gameName = currentGameLobby.getGame().getName();
					messages = TBIALApplication.getMessagesForUsername(getUser().getName(), gameName);
				} else {
					messages = TBIALApplication.getMessagesForUsername(getUser().getName());
				}
				// TODO whispering
				// if (getUser() != null) {
				// messages.addAll(TBIALApplication.getMessagesForKey(getUser().getName()));
				// }
				return messages;
			}
		};
		return model;
	}

	public Game getGame(String game) {
		return TBIALApplication.getInstance().getGame(game);
	}

	/**
	 * Sends a chat message to the global chat. Should be overridden concerning
	 * Game Chat / Whispering.
	 * 
	 * @param chatMessage
	 */
	@Override
	protected void sendMsg(String txt) {
		User sender = getUser();
		Message chatMessage;
		if (tabbedPanel.getSelectedTab() == gameLobbyTabIndex) {
			String gameName = currentGameLobby.getGame().getName();
			chatMessage = new Message(txt, sender, gameName);
		} else {
			chatMessage = new Message(txt, sender);
		}

		TBIALApplication.addMessage(chatMessage);
	}

	public void acceptInvitation(User sender) {
		// Join Game
		Game gameForJoin = TBIALApplication.getInstance().findGame(sender);
		tryToJoinGame(gameForJoin);
		removeInvitation(sender);
	}

	public void removeInvitation(User sender) {
		TBIALApplication.getInvitations().get(getUser().getName()).remove(sender);
	}

	public boolean isInvVisible() {
		return invVisible;
	}

	public void setInvVisible(boolean invVisible) {
		this.invVisible = invVisible;
	}

	public WebMarkupContainer getInvitationContainer() {
		return invitationContainer;
	}

	public void setShowInvBtnText() {
		if (invVisible) {
			showInvButtonText = "Show invitations";
		} else {
			showInvButtonText = "Hide invitations";
		}
	}

}
