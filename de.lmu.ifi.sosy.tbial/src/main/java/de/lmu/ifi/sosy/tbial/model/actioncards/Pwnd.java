package de.lmu.ifi.sosy.tbial.model.actioncards;

import de.lmu.ifi.sosy.tbial.model.StealingCard;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.visitor.CardVisitor;

public final class Pwnd extends StealingCard {

	private static final long serialVersionUID = 1L;

	public Pwnd() {
		setName("Pwnd");
	}

	@Override
	public void play(CardVisitor v) {
		v.visit(this);
	}
}
