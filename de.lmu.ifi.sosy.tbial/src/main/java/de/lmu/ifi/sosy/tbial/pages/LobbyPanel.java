
package de.lmu.ifi.sosy.tbial.pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.AjaxSelfUpdatingTimerBehavior;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.time.Duration;

import de.lmu.ifi.sosy.tbial.AuthenticationRequired;
import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.TBIALSession;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.GameState;
import de.lmu.ifi.sosy.tbial.util.TypesForFilters;

@AuthenticationRequired
public class LobbyPanel extends Panel {

	/**
	 * UID for serialization.
	 */
	private static final long serialVersionUID = 1L;
	private Form<?> form = new Form<>("form5");
	private Button join;
	private Button filter;
	private Button newGame;

	private Form<?> passwordForm;
	private PasswordTextField passwordField;
	private Button cancel;
	private Button enter;
	private boolean passwordFormVisible = false;
	private Label passwordCheck;

	private List<Game> gamesUsed = new ArrayList<>();

	private Game gameForJoin = null;

	private TabPage page;

	private ModalWindow askPasswordModal;
	private FilterPanel filterPanel;

	private Map<TypesForFilters, String> filters = new HashMap<>();

	/**
	 * Constructor for the LobbyPage
	 */
	public LobbyPanel(String id, TabPage page) {
		super(id);
		this.page = page;
		gamesUsed.addAll(TBIALApplication.getGames());
		this.filterPanel = new FilterPanel("lobbyTab", page, this.filters);
		filters.put(TypesForFilters.STATE, "empty");
		filters.put(TypesForFilters.PLAYERS, "empty");
		filters.put(TypesForFilters.PRIVACYTYPE, "empty");
		filters.put(TypesForFilters.MAXPLAYERS, "empty");
		addComponents();
	}

	private void addComponents() {

		WebMarkupContainer listContainer = new WebMarkupContainer("theContainer");

		listContainer.setOutputMarkupId(true);

		@SuppressWarnings("unchecked")
		final DataView dataView = new DataView("simple", new ListDataProvider<>(getGamesUsed())) {

			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(Item item) {

				final Game game = (Game) item.getModelObject();
				Label state = new Label("state", game.getState() + "");
				item.add(state);

				Label name1 = new Label("name", game.getName());
				item.add(name1);

				Label players = new Label("players", game.getPlayers().size() + "/" + game.getCapacity());
				item.add(players);

				Label privacy = new Label("privacy", game.isPrivate() + "");
				item.add(privacy);

				item.setOutputMarkupId(true);

				item.add(new AjaxEventBehavior("click") {

					private static final long serialVersionUID = 1L;

					@Override
					protected void onEvent(AjaxRequestTarget target) {

						Game oldGame = gameForJoin;
						gameForJoin = (Game) item.getModelObject();
						item.add(new AttributeAppender("style", "background-color:blue; color:white;"));
						target.add(item);
						if (gameForJoin != oldGame) {
							passwordFormVisible = false;
							passwordForm.setVisible(passwordFormVisible);
							target.add(passwordForm);
						}
					}

				});

				User user = game.getOwner();

				boolean userIsPlayer = ((TBIALSession) getSession()).getUser().getName().equals(user.getName());

				if (userIsPlayer) {
					item.add(new AttributeModifier("style", "font-weight:bolder;"));
				}

				if (item.getModelObject() == gameForJoin) {
					item.add(new AttributeAppender("style", "background-color:blue; color:white;"));
				}

			}

		};

		listContainer.add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(1)) {

			private static final long serialVersionUID = 1L;

			@Override
			protected final void onPostProcessTarget(final AjaxRequestTarget target) {
				// update Games hence DataView ListProvider dont refesh list
				getGamesUsed();
			}

		});

		listContainer.add(dataView);

		join = new Button("join") {

			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				if (gameForJoin == null)
					return;

				if (!gameForJoin.isPrivate()) {
					page.tryToJoinGame(gameForJoin);
				} else {
					passwordFormVisible = true;
					passwordForm.setVisible(passwordFormVisible);
				}
			};

		};

		filter = new Button("filter") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				filterPanel = new FilterPanel("lobbyTab", page, filters);
				LobbyPanel.this.replaceWith(filterPanel);
			}
		};

		newGame = new Button("newGame") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				gamesUsed.clear();
				gamesUsed.addAll(TBIALApplication.getGames());
				LobbyPanel.this.replaceWith(new CreateGamePanel("lobbyTab", page));
			}

		};
		// add(dataView);
		add(listContainer);
		form.add(join);
		form.add(filter);
		form.add(newGame);
		add(form);

		passwordForm = new Form<>("gamePwForm");
		passwordForm.setVisible(passwordFormVisible);
		passwordField = new PasswordTextField("gamePw", new Model<>(""));
		passwordCheck = new Label("gamePwCheck", new Model<>(""));

		cancel = new Button("gamePwCancel") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				passwordFormVisible = false;
				passwordForm.setVisible(passwordFormVisible);
			};

		};

		enter = new Button("gamePwEnter") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				String s = passwordField.getModelObject();
				if (gameForJoin.validatePassword(s)) {
					page.tryToJoinGame(gameForJoin);
					passwordFormVisible = false;
					passwordForm.setVisible(passwordFormVisible);
					passwordCheck.setDefaultModelObject("");
				} else {
					passwordCheck.setDefaultModelObject("Wrong password!");
				}
			};

		};

		passwordForm.setVisible(passwordFormVisible);

		passwordForm.add(passwordField);
		passwordForm.add(passwordCheck);
		passwordForm.add(cancel);
		passwordForm.add(enter);
		add(passwordForm);
	}

	public void filterCancel(String id, TabPage page) {
		this.refresh();

	}

	/**
	 * the method is used to filter the games
	 * 
	 * @param id
	 *            pageId
	 * @param filters
	 *            the Filters from the FilterPanel
	 * @return creates a new LobbyPanel with filtered games
	 */

	public void filterUpdate(String id, Map<TypesForFilters, String> filters) {
		this.filters = filters;
		List<Game> gamesFiltered = new ArrayList<>();
		gamesFiltered.addAll(TBIALApplication.getGames());

		if (filters.isEmpty()) {
			gamesUsed.clear();
			gamesUsed.addAll(gamesFiltered);
			return;
		}

		String playersString = filters.get(TypesForFilters.PLAYERS);
		String maxPlayersString = filters.get(TypesForFilters.MAXPLAYERS);
		String typeString = filters.get(TypesForFilters.PRIVACYTYPE);
		String stateString = filters.get(TypesForFilters.STATE);

		if (!("empty").equals(playersString) || !("empty").equals(maxPlayersString) || !("empty").equals(typeString)
				|| !("empty").equals(stateString)) {

			this.gamesUsed.clear();
			gamesFiltered.stream().filter(g -> {
				short pl = (short) g.getPlayers().size();
				short mP = g.getCapacity();
				boolean privacy = g.isPrivate();
				GameState st = g.getState();

				Boolean isGameFiltered = false;

				if (!("empty").equals(playersString) && !("empty").equals(maxPlayersString)
						&& !("empty").equals(typeString) && !("empty").equals(stateString)) {

					short players = getPlayersFilter(filters.get(TypesForFilters.PLAYERS));
					short maxPlayers = getMaxPlayersFilter(filters.get(TypesForFilters.MAXPLAYERS));
					boolean type = typePrivacy(filters.get(TypesForFilters.PRIVACYTYPE));
					GameState state = getState(filters.get(TypesForFilters.STATE));
					Boolean isPlayers = (players == pl);
					Boolean isMaxPlayers = (maxPlayers == mP);
					Boolean isType = (type == privacy);
					Boolean isState = (state.equals(st));

					isGameFiltered = filterExact(isPlayers, isMaxPlayers, isType, isState);

				} else if (("empty").equals(playersString) && !("empty").equals(maxPlayersString)
						&& !("empty").equals(typeString) && !("empty").equals(stateString)) {

					short maxPlayers = getMaxPlayersFilter(filters.get(TypesForFilters.MAXPLAYERS));
					boolean type = typePrivacy(filters.get(TypesForFilters.PRIVACYTYPE));
					GameState state = getState(filters.get(TypesForFilters.STATE));
					Boolean isMaxPlayers = (maxPlayers == mP);
					Boolean isType = (type == privacy);
					Boolean isState = (state.equals(st));

					isGameFiltered = filterExact(true, isMaxPlayers, isType, isState);

				} else if (!("empty").equals(playersString) && ("empty").equals(maxPlayersString)
						&& !("empty").equals(typeString) && !("empty").equals(stateString)) {

					short players = getPlayersFilter(filters.get(TypesForFilters.PLAYERS));
					boolean type = typePrivacy(filters.get(TypesForFilters.PRIVACYTYPE));
					GameState state = getState(filters.get(TypesForFilters.STATE));
					Boolean isPlayers = (players == pl);
					Boolean isType = (type == privacy);
					Boolean isState = (state.equals(st));

					isGameFiltered = filterExact(isPlayers, true, isType, isState);

				} else if (!("empty").equals(playersString) && !("empty").equals(maxPlayersString)
						&& ("empty").equals(typeString) && !("empty").equals(stateString)) {

					short players = getPlayersFilter(filters.get(TypesForFilters.PLAYERS));
					short maxPlayers = getMaxPlayersFilter(filters.get(TypesForFilters.MAXPLAYERS));
					GameState state = getState(filters.get(TypesForFilters.STATE));
					Boolean isPlayers = (players == pl);
					Boolean isMaxPlayers = (maxPlayers == mP);
					Boolean isState = (state.equals(st));

					isGameFiltered = filterExact(isPlayers, isMaxPlayers, true, isState);

				} else if (!("empty").equals(playersString) && !("empty").equals(maxPlayersString)
						&& !("empty").equals(typeString) && ("empty").equals(stateString)) {

					short players = getPlayersFilter(filters.get(TypesForFilters.PLAYERS));
					short maxPlayers = getMaxPlayersFilter(filters.get(TypesForFilters.MAXPLAYERS));
					boolean type = typePrivacy(filters.get(TypesForFilters.PRIVACYTYPE));
					Boolean isPlayers = (players == pl);
					Boolean isMaxPlayers = (maxPlayers == mP);
					Boolean isType = (type == privacy);

					isGameFiltered = filterExact(isPlayers, isMaxPlayers, isType, true);

				} else if (("empty").equals(playersString) && ("empty").equals(maxPlayersString)
						&& !("empty").equals(typeString) && !("empty").equals(stateString)) {

					boolean type = typePrivacy(filters.get(TypesForFilters.PRIVACYTYPE));
					GameState state = getState(filters.get(TypesForFilters.STATE));
					Boolean isType = (type == privacy);
					Boolean isState = (state.equals(st));

					isGameFiltered = filterExact(true, true, isType, isState);

				} else if (("empty").equals(playersString) && !("empty").equals(maxPlayersString)
						&& ("empty").equals(typeString) && !("empty").equals(stateString)) {

					short maxPlayers = getMaxPlayersFilter(filters.get(TypesForFilters.MAXPLAYERS));
					GameState state = getState(filters.get(TypesForFilters.STATE));
					Boolean isMaxPlayers = (maxPlayers == mP);
					Boolean isState = (state.equals(st));

					isGameFiltered = filterExact(true, isMaxPlayers, true, isState);

				} else if (("empty").equals(playersString) && !("empty").equals(maxPlayersString)
						&& !("empty").equals(typeString) && ("empty").equals(stateString)) {

					short maxPlayers = getMaxPlayersFilter(filters.get(TypesForFilters.MAXPLAYERS));
					boolean type = typePrivacy(filters.get(TypesForFilters.PRIVACYTYPE));
					Boolean isMaxPlayers = (maxPlayers == mP);
					Boolean isType = (type == privacy);

					isGameFiltered = filterExact(true, isMaxPlayers, isType, true);

				} else if (!("empty").equals(playersString) && ("empty").equals(maxPlayersString)
						&& ("empty").equals(typeString) && !("empty").equals(stateString)) {

					short players = getPlayersFilter(filters.get(TypesForFilters.PLAYERS));
					GameState state = getState(filters.get(TypesForFilters.STATE));
					Boolean isPlayers = (players == pl);
					Boolean isState = (state.equals(st));

					isGameFiltered = filterExact(isPlayers, true, true, isState);

				} else if (!("empty").equals(playersString) && ("empty").equals(maxPlayersString)
						&& !("empty").equals(typeString) && ("empty").equals(stateString)) {

					short players = getPlayersFilter(filters.get(TypesForFilters.PLAYERS));
					boolean type = typePrivacy(filters.get(TypesForFilters.PRIVACYTYPE));
					Boolean isPlayers = (players == pl);
					Boolean isType = (type == privacy);

					isGameFiltered = filterExact(isPlayers, true, isType, true);

				} else if (!("empty").equals(playersString) && !("empty").equals(maxPlayersString)
						&& ("empty").equals(typeString) && ("empty").equals(stateString)) {

					short players = getPlayersFilter(filters.get(TypesForFilters.PLAYERS));
					short maxPlayers = getMaxPlayersFilter(filters.get(TypesForFilters.MAXPLAYERS));
					Boolean isPlayers = (players == pl);
					Boolean isMaxPlayers = (maxPlayers == mP);

					isGameFiltered = filterExact(isPlayers, isMaxPlayers, true, true);

				} else if (("empty").equals(playersString) && ("empty").equals(maxPlayersString)
						&& ("empty").equals(typeString) && !("empty").equals(stateString)) {

					GameState state = getState(filters.get(TypesForFilters.STATE));
					Boolean isState = (state.equals(st));

					isGameFiltered = filterExact(true, true, true, isState);

				} else if (("empty").equals(playersString) && !("empty").equals(maxPlayersString)
						&& ("empty").equals(typeString) && ("empty").equals(stateString)) {

					short maxPlayers = getMaxPlayersFilter(filters.get(TypesForFilters.MAXPLAYERS));
					Boolean isMaxPlayers = (maxPlayers == mP);

					isGameFiltered = filterExact(true, isMaxPlayers, true, true);

				} else if (!("empty").equals(playersString) && ("empty").equals(maxPlayersString)
						&& ("empty").equals(typeString) && ("empty").equals(stateString)) {

					short players = getPlayersFilter(filters.get(TypesForFilters.PLAYERS));
					Boolean isPlayers = (players == pl);

					isGameFiltered = filterExact(isPlayers, true, true, true);

				} else if (("empty").equals(playersString) && ("empty").equals(maxPlayersString)
						&& !("empty").equals(typeString) && ("empty").equals(stateString)) {

					boolean type = typePrivacy(filters.get(TypesForFilters.PRIVACYTYPE));

					Boolean isType = (type == privacy);

					isGameFiltered = filterExact(true, true, isType, true);

				}
				return isGameFiltered;

			}).forEach(g -> gamesUsed.add(g));

		} else {
			gamesUsed.clear();
			gamesUsed.addAll(gamesFiltered);
		}

	}

	/**
	 * the method is used in .filter (Java streams) while filtering the games.
	 * If true is returned, the game will be listed, otherwise not
	 * 
	 * @param players
	 *            the param shows whether the amount of the players of the game
	 *            in .filter is the same as given from the site's FilterPanel
	 * @param maxPlayers
	 * @param privacyType
	 * @param state
	 * @return
	 */
	private Boolean filterExact(Boolean players, Boolean maxPlayers, Boolean privacyType, Boolean state) {

		if ((players) && (maxPlayers) && (privacyType) && (state)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * private Boolean filterExactNoPlayers(Boolean maxPlayers, Boolean
	 * privacyType, Boolean state) {
	 * 
	 * if ((maxPlayers) && (privacyType) && (state)) { return true; } else {
	 * return false; } }
	 * 
	 * private Boolean filterExactNoMaxPlayers(Boolean players, Boolean
	 * privacyType, Boolean state) {
	 * 
	 * if ((players) && (privacyType) && (state)) { return true; } else { return
	 * false; } }
	 * 
	 * private Boolean filterExactNoPrivacyType(Boolean players, Boolean
	 * maxPlayers, Boolean state) {
	 * 
	 * if ((players) && (maxPlayers) && (state)) { return true; } else { return
	 * false; } }
	 */

	private static GameState getState(String st) {
		if (st.equals("WAITING")) {
			return GameState.WAITING;
		} else if (st.equals("RUNNING")) {
			return GameState.RUNNING;
		} else {
			return GameState.PAUSED;
		}
	}

	private static short getMaxPlayersFilter(String mP) {
		short maxPlayers = (short) Integer.parseInt(mP);
		return maxPlayers;
	}

	private static short getPlayersFilter(String p) {
		short players = (short) Integer.parseInt(p);
		return players;
	}

	private static boolean typePrivacy(String type) {
		if (type.equals("private")) {
			return true;
		} else if (type.equals("public")) {
			return false;
		}
		// TODO Auto-generated method stub
		return false;
	}

	public List<Game> getGamesUsed() {
		this.filterUpdate("tabPage", filters);
		return this.gamesUsed;
	}

	public FilterPanel getFilterPanel() {
		return filterPanel;
	}

	public void setFilterPanel(FilterPanel filterPanel) {
		this.filterPanel = filterPanel;
	}

	public void refresh() {
		getGamesUsed();
	}

}
