package de.lmu.ifi.sosy.tbial.components;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.PropertyModel;

import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.TBIALSession;
import de.lmu.ifi.sosy.tbial.model.Card;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.pages.GamePage;

/**
 * Zelle zur Anzeige der einzelnen Spieler am Tisch
 * 
 * @author Stephan
 *
 */
public class PlayerPanel extends CourtPanel {

	private Label hpLabel;
	private Label prestigeLabel;
	private Label nameLabel;
	private Label roleLabel;
	private Label characterLabel;
	private int playerId;
	private PlayerFieldPanel playerField;

	private GamePage page;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PlayerPanel(String id, int playerId, GamePage page, double rotation, int row, int column) {
		super(id, rotation, row, column);
		this.playerId = playerId;
		this.page = page;
		initialize();

	}

	/**
	 * Initialize Players Labels for hp, prestige, name and role
	 */
	private void initialize() {
		this.nameLabel = new Label("name", getPanelPlayer().getName());
		WebMarkupContainer attrPanel = new WebMarkupContainer("attrPanel");
		this.hpLabel = new Label("hp", getPanelPlayer().getHp());

		this.prestigeLabel = new Label("prestige", new PropertyModel<>(this, "prestige"));

		this.roleLabel = new Label("role", getPanelPlayer().getRole().getName());
		this.characterLabel = new Label("character", getPanelPlayer().getCharacter().getName());
		attrPanel.add(characterLabel);
		attrPanel.add(nameLabel);
		attrPanel.add(hpLabel);
		attrPanel.add(prestigeLabel);
		attrPanel.add(roleLabel);

		playerField = new PlayerFieldPanel("playerFieldPanel", playerId, page, rotation, row, column);
		add(playerField);
		roleLabel.setVisible(isCardVisible(getPanelPlayer().getRole()));
		add(attrPanel);

		// set attr Panel's position in parent depending on position in grid
		if (row == 3 || column < 2) {
			addStyle(attrPanel, "width:25%;  float:right;");
		} else {
			addStyle(attrPanel, "width:25%;  float:left;");
		}
		/*
		 * if (row == 0) { addStyle(attrPanel, "bottom: -60%;left: 5%;"); if
		 * (column == 1) { addStyle(attrPanel,
		 * "right: -30%; bottom: -60%; text-align: right;"); } } else if (column
		 * == 0) { addStyle(attrPanel, "right: -30%; text-align: right;top 5%");
		 * } else { addStyle(attrPanel, "left: 5%;top 5%"); }
		 */

	}

	/**
	 * Adds a CSS style to a component
	 * 
	 * @param c
	 *            - Component to add
	 * @param style
	 *            - Style to add as String rep
	 */
	private void addStyle(Component c, String style) {
		c.add(new Behavior() {

			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentTag(Component component, ComponentTag tag) {
				tag.put("style", style);
			}
		});
	}

	public void updateRoleLabelVisibility() {
		roleLabel.setVisible(isCardVisible(getPanelPlayer().getRole()));
	}

	/**
	 * Determines visibility of card (whether it's visible or user's card)
	 * 
	 * @param card
	 *            - to determine
	 * @return - visibility for user
	 */
	public boolean isCardVisible(Card card) {
		TBIALSession ts = (TBIALSession) getSession();
		boolean usersPlayer = ts.getUser().getName().equals(getPanelPlayer().getName());
		return usersPlayer || card.isVisible();
	}

	public int getPrestige() {

		int prestige = getPanelPlayer().getCharacter().getPrestige();
		if (!getPanelPlayer().equals(getSessionPlayer())) {
			prestige = getPanelPlayer().getCharacter().getPrestigeSeenByCharacter(getSessionPlayer().getCharacter());
		}

		return (prestige >= 0) ? prestige : 0;
	}

	public Game getGame() {
		return TBIALApplication.getInstance().findGame(page.getUser());
	}

	public Player getPanelPlayer() {
		return getGame().findPlayer(playerId);
	}

	private Player getSessionPlayer() {
		return getGame().getPlayer(((TBIALSession) getSession()).getUser());
	}

	public Label getHpLabel() {
		return hpLabel;
	}

	public Label getPrestigeLabel() {
		return prestigeLabel;
	}

	public Label getNameLabel() {
		return nameLabel;
	}

	public Label getRoleLabel() {
		return roleLabel;
	}

	public Label getCharacterLabel() {
		return characterLabel;
	}

	public PlayerFieldPanel getPlayerField() {
		return playerField;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public HandCardPanel getHandCardPanel() {
		return playerField.getHandCards();
	}

}
