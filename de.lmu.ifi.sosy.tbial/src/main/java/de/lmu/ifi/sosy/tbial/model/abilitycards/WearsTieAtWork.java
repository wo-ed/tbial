package de.lmu.ifi.sosy.tbial.model.abilitycards;

import de.lmu.ifi.sosy.tbial.model.AbilityCard;
import de.lmu.ifi.sosy.tbial.model.visitor.CardVisitor;

public final class WearsTieAtWork extends AbilityCard {

	private static final long serialVersionUID = 1L;

	public WearsTieAtWork() {
		setName("Wears Tie");
	}

	@Override
	public void play(CardVisitor v) {
		v.visit(this);
	}
}
