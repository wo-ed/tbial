package de.lmu.ifi.sosy.tbial.pages;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.wicketstuff.googlecharts.AbstractChartData;
import org.wicketstuff.googlecharts.Chart;
import org.wicketstuff.googlecharts.ChartAxis;
import org.wicketstuff.googlecharts.ChartAxisType;
import org.wicketstuff.googlecharts.ChartDataEncoding;
import org.wicketstuff.googlecharts.ChartProvider;
import org.wicketstuff.googlecharts.ChartType;
import org.wicketstuff.googlecharts.IChartData;
import org.wicketstuff.googlecharts.LineStyle;
import org.wicketstuff.googlecharts.SolidFill;

import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;

public class GameStatPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Game game;

	private Label gameTitle;

	private Label bugsPlayed;
	private Label excusesPlayed;
	private Label solutionsPlayed;
	private Label specialCardsPlayed;
	private Label abilityCardsPlayed;
	private Label stumblingCardsPlayed;

	private Label durationInTime;
	private Label durationInTurns;

	private ListView<ChartProvider> charts;

	public GameStatPanel(String id, Game game) {
		super(id);
		this.game = game;
		// game.setEndTime(new Date());
		addComponents();
	}

	public ListView<ChartProvider> buildCharts() {

		List<ChartProvider> providers = getChartProviders();

		ListView<ChartProvider> charts = new ListView<ChartProvider>("charts", providers) {

			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<ChartProvider> item) {

				Chart chart = new Chart("chart", (ChartProvider) item.getModel().getObject());
				item.add(chart);
			}
		};

		return charts;

	}

	public ListView<ChartProvider> getChartList() {
		return charts;
	}

	private void addComponents() {

		gameTitle = new Label("gameTitle", new Model<>(game.getName()));
		add(gameTitle);

		bugsPlayed = new Label("bugsPlayed", new PropertyModel<>(game, "BugsPlayed"));
		add(bugsPlayed);

		excusesPlayed = new Label("excusesPlayed", new PropertyModel<>(game, "ExcusesPlayed"));
		add(excusesPlayed);

		solutionsPlayed = new Label("solutionsPlayed", new PropertyModel<>(game, "SolutionsPlayed"));
		add(solutionsPlayed);

		specialCardsPlayed = new Label("specialCardsPlayed", new PropertyModel<>(game, "SpecialCardsPlayed"));
		add(specialCardsPlayed);

		abilityCardsPlayed = new Label("abilityCardsPlayed", new PropertyModel<>(game, "AbilityCardsPlayed"));
		add(abilityCardsPlayed);

		stumblingCardsPlayed = new Label("stumblingCardsPlayed", new PropertyModel<>(game, "StumblingCardsPlayed"));
		add(stumblingCardsPlayed);

		durationInTime = new Label("durationInTime", new PropertyModel<>(game, "DurationInTime"));
		add(durationInTime);

		durationInTurns = new Label("durationInTurns", new PropertyModel<>(game, "TurnsCounter"));
		add(durationInTurns);

		charts = buildCharts();

		add(charts);

	}

	public List<ChartProvider> getChartProviders() {

		List<ChartProvider> providers = new ArrayList<ChartProvider>();

		Color[] colors = { Color.GREEN, Color.BLUE, Color.RED, Color.CYAN, Color.YELLOW, Color.BLACK, Color.GRAY };
		String[] legendIndex = new String[game.getPlayers().size()];
		Color[] colorIndex = new Color[game.getPlayers().size()];
		LineStyle[] lineStyleIndex = new LineStyle[game.getPlayers().size()];
		{
			int counter = 0;
			for (Player p : game.getPlayers()) {
				legendIndex[counter] = p.getName();
				colorIndex[counter] = colors[counter];
				lineStyleIndex[counter] = new LineStyle(3, 2, 0);
				counter++;
			}
		}

		ChartAxis axisLeft = new ChartAxis(ChartAxisType.LEFT);
		axisLeft.setPositions(
				new double[] { 0.0, 100.0 / 6.0, 200.0 / 6.0, 300.0 / 6.0, 400.0 / 6.0, 500.0 / 6.0, 100.0 });
		axisLeft.setLabels(new String[] { "", "1", "2", "3", "4", "5", "HP 6" });

		ChartProvider provider;

		int neededProviders = (game.getTurnsCounter() + 1) / 10;
		if (((game.getTurnsCounter() + 1) % 10) > 0) {
			neededProviders++;
		}
		for (int i = 0; i < neededProviders; i++) {
			IChartData data = extractData(i);

			provider = new ChartProvider(new Dimension(500, 160), ChartType.LINE, data);
			provider.setColors(colorIndex);
			provider.setLegend(legendIndex);
			provider.setLineStyles(lineStyleIndex);
			provider.setBackgroundFill(new SolidFill(new Color(0, 0, 0, 0)));
			addXaxis(provider, i);
			provider.addAxis(axisLeft);

			providers.add(provider);
		}

		return providers;
	}

	private IChartData extractData(int i) {
		return new AbstractChartData(ChartDataEncoding.TEXT, 100) {

			private static final long serialVersionUID = 1L;

			public double[][] getData() {
				int countPlayers = game.getPlayers().size();
				int neededTurns = (game.getTurnsCounter()) - (i * 10);
				if (neededTurns > 10) {
					neededTurns = 10;
				}

				double[][] values = new double[countPlayers][neededTurns];
				int counter = 0;
				for (Player p : game.getPlayers()) {

					for (int j = 0; j < neededTurns; j++) {
						values[counter][j] = ((double) (game.getPlayerHealthPoints().get(p).get(j + i * 10)))
								* (100.0 / 6.0);
					}
					counter++;
				}

				return values;
			}
		};
	}

	private void addXaxis(ChartProvider p, int a) {
		ChartAxis axisBottom = new ChartAxis(ChartAxisType.BOTTOM);
		int neededTurns = (game.getTurnsCounter() + 1) - (a * 10);
		if (neededTurns > 10) {
			neededTurns = 10;
		}
		double[] XaxisPositions = new double[neededTurns];
		String[] XaxisIndex = new String[neededTurns];
		for (int i = 0; i < neededTurns; i++) {
			if (neededTurns > 1) {
				XaxisPositions[i] = ((double) i * 100.0) / (neededTurns - 1.0);
			} else {
				XaxisPositions[i] = 0;
			}
			if (i == neededTurns - 1) {
				XaxisIndex[i] = "" + (i + a * 10) + " Turn";
			} else {
				XaxisIndex[i] = "" + (i + a * 10);
			}
		}
		axisBottom.setPositions(XaxisPositions);
		axisBottom.setLabels(XaxisIndex);
		p.addAxis(axisBottom);
	}

}
