package de.lmu.ifi.sosy.tbial.pages;

import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.AjaxSelfUpdatingTimerBehavior;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.util.time.Duration;

import de.lmu.ifi.sosy.tbial.AuthenticationRequired;
import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.util.Message;

/**
 * 
 * Basic lobby page, <b>should</b> show the list of currently available games.
 * Needs to be extended.
 * 
 * 
 * @author Andreas Schroeder, SWEP 2013 Team.
 * 
 */
@AuthenticationRequired
public class MainPage extends BasePage {

	/**
	 * UID for serialization.
	 */
	private static final long serialVersionUID = 1L;

	private MarkupContainer messagesContainer;

	private Label userName;

	public MarkupContainer getChat() {
		return messagesContainer;
	}

	public MainPage() {
		super();

		userName = new Label("user", new PropertyModel<>(this.getUser(), "Name"));

		add(userName);

		add(buildChatWindow(), buildSendForm());
	}

	private Component buildSendForm() {
		final TextField<String> textField = new TextField<>("newMsg", new Model<String>());
		textField.setOutputMarkupId(true);

		Component submit = new AjaxSubmitLink("sendMsgBtn") {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				String text = textField.getModelObject();
				if (text == null || text.isEmpty()) {
					return;
				}
				sendMsg(text);
				textField.setModelObject("");
				target.add(messagesContainer, textField);
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				throw new UnsupportedOperationException("nor errors allowed :)");
			}

		};
		return new Form<String>("sendMsgForm").add(textField, submit);
	}

	/**
	 * Adds the Chat Window to the page.
	 * 
	 * @return
	 */
	private Component buildChatWindow() {
		messagesContainer = new WebMarkupContainer("messageContainer");

		// Get all messages that shall be displayed in the current chat window.
		IModel<List<Message>> messages = getMessagesToBeShown();

		// Append each message to the @MarkupContainer.
		ListView<Message> listView = new ListView<Message>("message", messages) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(final ListItem<Message> item) {
				this.modelChanging();

				Message message = item.getModelObject();

				Label sender = new Label("sender", new PropertyModel<String>(message, "senderName"));
				item.add(sender);

				Label text = new Label("text", new PropertyModel<String>(message, "text"));
				item.add(text);
			}
		};
		messagesContainer.setOutputMarkupId(true);
		messagesContainer.add(listView);

		AjaxSelfUpdatingTimerBehavior ajaxBehavior = new AjaxSelfUpdatingTimerBehavior(Duration.seconds(2));
		messagesContainer.add(ajaxBehavior);

		return messagesContainer;
	}

	/**
	 * Returns a list of all messages that should be displayed in the current
	 * chat window. Should be overridden concerning Game Chat / Whispering.
	 */
	protected IModel<List<Message>> getMessagesToBeShown() {
		LoadableDetachableModel<List<Message>> model = new LoadableDetachableModel<List<Message>>() {
			private static final long serialVersionUID = 1L;

			@Override
			protected List<Message> load() {
				List<Message> messages = TBIALApplication.getMessagesForUsername(getUser().getName());
				// TODO whispering
				// if (getUser() != null) {
				// messages.addAll(TBIALApplication.getMessagesForKey(getUser().getName()));
				// }
				return messages;
			}
		};
		return model;
	}

	/**
	 * Sends a chat message to the global chat. Should be overridden concerning
	 * Game Chat / Whispering.
	 * 
	 * @param chatMessage
	 */
	protected void sendMsg(String txt) {
		User sender = getUser();
		Message chatMessage = new Message(txt, sender);
		TBIALApplication.addMessage(chatMessage);
	}

	public User getUser() {
		return getSession().getUser();
	}
}
