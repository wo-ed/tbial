package de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob;

import de.lmu.ifi.sosy.tbial.model.abilitycards.PreviousJob;
import de.lmu.ifi.sosy.tbial.model.visitor.CardVisitor;

public class Nasa extends PreviousJob {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Nasa() {
		super("NASA\n (prev. Job)");
		this.prestigeModifier = 3;
	}

	@Override
	public void play(CardVisitor v) {
		v.visit(this);
	}

}
