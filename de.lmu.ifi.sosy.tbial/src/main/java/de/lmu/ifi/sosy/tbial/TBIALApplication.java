package de.lmu.ifi.sosy.tbial;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.apache.wicket.Component;
import org.apache.wicket.Page;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.Session;
import org.apache.wicket.authorization.Action;
import org.apache.wicket.authorization.IAuthorizationStrategy;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;
import org.apache.wicket.request.Url;
import org.apache.wicket.request.component.IRequestableComponent;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.IResource;

import de.lmu.ifi.sosy.tbial.db.Database;
import de.lmu.ifi.sosy.tbial.db.SQLDatabase;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.pages.MainPage;
import de.lmu.ifi.sosy.tbial.pages.TabPage;
import de.lmu.ifi.sosy.tbial.util.ActivationService;
import de.lmu.ifi.sosy.tbial.util.Message;
import de.lmu.ifi.sosy.tbial.util.VerificationToken;
import de.lmu.ifi.sosy.tbial.util.VisibleForTesting;

/**
 * Represents the web application.
 * 
 * @author Andreas Schroeder, Christian Kroiß SWEP 2013 Team.
 * 
 */
public class TBIALApplication extends WebApplication {

	public static ThrowToHeapNotification throwToHeapNotification;

	public static final boolean DEBUG_MODE = false;

	private final Database database;

	private ActivationService userService;

	private final Map<String, Game> games = Collections.synchronizedMap(new TreeMap<>());

	private final List<User> users = Collections.synchronizedList(new ArrayList<>());

	private final Map<User, Game> usersInGame = Collections.synchronizedMap(new HashMap<>());

	private static final int MAX_MSG_COUNT = 1000;
	private static final List<Message> messages = new LinkedList<>();

	public static synchronized List<Message> getMessages() {
		return TBIALApplication.messages;
	};

	/**
	 * Returns a copy of all messages for a specific user determined by the
	 * username.
	 * 
	 * @param username,
	 *            ingame - whether the player is ingame/inside a lobby or not.
	 * @return A list of all messages.
	 */
	public static List<Message> getMessagesForUsername(String username) {
		List<Message> res = new LinkedList<>();
		for (Message message : TBIALApplication.getMessages()) {
			String rec = message.getReceiver();
			if (rec.equals("all") || rec.equals(username)) {
				res.add(message);
			}
		}
		return res;
	}

	/**
	 * Returns a copy of all messages for a specific user determined by the
	 * username.
	 * 
	 * @param username,
	 *            gameName - the name of the game, the user is currently in.
	 * @return A list of all messages.
	 */
	public static List<Message> getMessagesForUsername(String username, String gameName) {
		List<Message> res = new LinkedList<>();
		for (Message message : TBIALApplication.getMessages()) {
			String rec = message.getReceiver();
			if (rec.equals(username) || rec.equals(gameName)) {
				res.add(message);
			}
		}
		return res;
	}

	/**
	 * Removes all messages of a game chat. Should be called when a game is
	 * closed. A iterator is used to remove messages while iterating.
	 * 
	 * @param gameName
	 */
	public static synchronized void deleteMessagesForGame(String gameName) {
		for (Iterator<Message> iter = TBIALApplication.messages.iterator(); iter.hasNext();) {
			Message m = iter.next();
			if (m.getReceiver().equals(gameName)) {
				iter.remove();
			}
		}
	}

	/**
	 * Adds a message to the message list. Should be called, whenever a message
	 * is sent.
	 * 
	 * @param m
	 *            - The message.
	 * @return Returns true if the message was added successfully.
	 */
	public static synchronized boolean addMessage(Message m) {
		if (TBIALApplication.messages.size() >= TBIALApplication.MAX_MSG_COUNT) {
			messages.remove(0);
		}
		return messages.add(m);
	}

	public static void addUser(User u) {
		getInstance().users.add(u);
	}

	public static void removeUser(User u) {
		getInstance().users.remove(u);
	}

	public static List<User> getUsers() {
		return getInstance().users;
	}

	public static Database getDatabase() {
		return getInstance().database;
	}

	public TBIALApplication() {
		this(new SQLDatabase());
	}

	@VisibleForTesting
	TBIALApplication(Database database) {
		super();
		this.database = database;
		userService = new ActivationService();
		userService.initTokens(database);
	}

	@Override
	public Class<TabPage> getHomePage() {
		return TabPage.class;
	}

	/**
	 * Find the game(started, not started) where the user is in null, if user
	 * isn't in a game
	 * 
	 * @param user
	 * @return game of user
	 */
	public synchronized Game findGame(User user) {
		if (games == null) {
			return null;
		}
		if (usersInGame.containsKey(user)) {
			return usersInGame.get(user);
		}
		return null;
	}

	public synchronized Optional<Player> joinGame(User user, Game game) {
		this.usersInGame.put(user, game);
		return game.join(user);
	}

	public synchronized void leaveGame(User user, Game game) {
		this.usersInGame.remove(user);
		game.leaveGame(user);
		if (game.getPlayers().isEmpty())
			deleteGame(game);
	}

	/**
	 * Returns a new {@link TBIALSession} instead of a default Wicket
	 * {@link Session}.
	 */
	@Override
	public TBIALSession newSession(Request request, Response response) {
		return new TBIALSession(request);
	}

	@Override
	protected void init() {
		initMarkupSettings();
		initPageMounts();
		initAuthorization();
		// initExceptionHandling();
	}

	public static TBIALApplication getInstance() {
		return (TBIALApplication) TBIALApplication.get();
	}

	private void initMarkupSettings() {
		if (getConfigurationType().equals(RuntimeConfigurationType.DEPLOYMENT)) {
			getMarkupSettings().setStripWicketTags(true);
			getMarkupSettings().setStripComments(true);
			getMarkupSettings().setCompressWhitespace(true);
		}
	}

	private void initPageMounts() {
		mountPage("home", getHomePage());
		mountPage("login/#{placeholder}", Login.class);
		mountPage("register", Register.class);
		mountPage("main", MainPage.class);
	}

	public static List<String> getGameNames() {
		return new ArrayList<>(getInstance().games.keySet());
	}

	public static List<Game> getGames() {
		return new ArrayList<>(getInstance().games.values());
	}

	public void addGame(Game game) {
		games.put(game.getName(), game);
	}

	/**
	 * <<<<<<< HEAD Public static head of register process
	 * 
	 * @param name
	 *            - new user's name
	 * @param password
	 *            - new user's pw
	 * @param email-
	 *            new user's mail
	 * @return new registred user
	 */
	public synchronized static User registerUser(String name, String password, String email) {
		return getInstance().registerAccount(name, password, email);
	}

	/**
	 * Actual method to register a new user
	 * 
	 * @param name
	 *            - new user's name
	 * @param password
	 *            - new user's pw
	 * @param email-
	 *            new user's mail
	 * @return new registred user
	 */
	private User registerAccount(String name, String password, String email) {

		User user = this.database.register(name, password);
		if (user != null) {
			VerificationToken vToken = new VerificationToken(user);
			userService.getTokenStorage().store(vToken);
			this.database.storeToken(user.getId(), vToken.getToken());
			// Don't send mails in debug mode
			if (!DEBUG_MODE)
				this.userService.getMailService().sendEmail(email, name, generatateActivationLink(vToken.getToken()));
			else
				activateUser(user);
			return user;
		} else
			return null;
	}

	/**
	 * Activate user's account via unique token
	 * 
	 * @param token
	 *            - activation token
	 * @return activated user
	 */
	public synchronized static User activateToken(String token) {
		VerificationToken vt = getInstance().userService.getTokenStorage().getFromString(token);
		if (vt == null)
			return null;
		User user = vt.getUser();
		if (activateUser(vt.getUser()))
			getInstance().userService.getTokenStorage().deleteToken(vt);
		return user;
	}

	/**
	 * set database user active
	 * 
	 * @param user
	 *            - to set active
	 * @return successfull
	 */
	private synchronized static boolean activateUser(User user) {
		return getInstance().database.activateUser(user);
	}

	/**
	 * Generates activation link for token
	 * 
	 * @param token
	 *            - activation token
	 * @return URL to activate token
	 */
	public String generatateActivationLink(String token) {
		PageParameters paras = new PageParameters();
		paras.add("placeholder", "activation");
		paras.add("token", token);
		return getAppUrlForPage(Login.class, paras);
	}

	/**
	 * Returns absolute URL for specific class with specific page params
	 * 
	 * @param pageClass
	 *            - Page class to determine path
	 * @param paras
	 *            - PageParameters used
	 * @return absolute URL
	 */
	private <C extends Page> String getAppUrlForPage(final Class<C> pageClass, PageParameters params) {
		Url relPagePath = Url.parse(RequestCycle.get().urlFor(pageClass, params));
		return RequestCycle.get().getUrlRenderer().renderFullUrl(relPagePath);
	}

	public ActivationService getUserService() {
		return userService;
	}

	public void setUserService(ActivationService userService) {
		this.userService = userService;
	}

	/**
	 * ======= >>>>>>> master initializes authorization so that pages having
	 * {@link AuthenticationRequired} require a valid, signed-in user.
	 */
	private void initAuthorization() {
		getSecuritySettings().setAuthorizationStrategy(new IAuthorizationStrategy() {

			@Override
			public <T extends IRequestableComponent> boolean isInstantiationAuthorized(Class<T> componentClass) {
				boolean requiresAuthentication = componentClass.isAnnotationPresent(AuthenticationRequired.class);
				boolean isSignedIn = ((TBIALSession) Session.get()).isSignedIn();

				if (requiresAuthentication && !isSignedIn)
					// redirect the user to the login page.
					throw new RestartResponseAtInterceptPageException(Login.class);

				// continue.
				return true;
			}

			@Override
			public boolean isActionAuthorized(Component component, Action action) {
				// all actions are authorized.
				return true;
			}

			@Override
			public boolean isResourceAuthorized(IResource arg0, PageParameters arg1) {
				// all resources are authorized
				return true;
			}
		});
	}

	public synchronized void deleteGame(Game game) {
		games.remove(game.getName());
		TBIALApplication.deleteMessagesForGame(game.getName());
	}

	public synchronized Game getGame(String game) {
		if (this.games.containsKey(game))
			return this.games.get(game);
		return null;
	}

	// Key: Einladungempfaenger
	private static Map<String, List<User>> invitations = new HashMap<String, List<User>>();;

	public static void sendInvitation(String receiverName, User sender) {
		if (invitations.containsKey(receiverName)) {
			if (!invitations.get(receiverName).contains(sender) && (!receiverName.equals(sender.getName()))) {
				invitations.get(receiverName).add(sender);
			}
		} else {
			invitations.put(receiverName, new LinkedList<User>());
			invitations.get(receiverName).add(sender);
		}
	}

	public static Map<String, List<User>> getInvitations() {
		return invitations;
	}

	// gibt leere List und keine null zurueck(Da sonst
	// ListProvide null wird.
	public static List<User> getUsersInvs(User user) {
		if (user == null) {
			return new LinkedList<User>();
		}
		if (!invitations.containsKey(user.getName())) {
			invitations.put(user.getName(), new LinkedList<User>());
		}
		return invitations.get(user.getName());
	}
	
	public Game createGame(String name, short capacity, boolean isPrivate, String password, User user) {
		Game game = new Game(name, capacity, isPrivate, password, user);
		// game.storeGameStatsInDB(TBIALApplication.getDatabase());
		this.addGame(game);
		this.usersInGame.put(user, game);
		return game;
	}

	public synchronized boolean isAlreadyLoggedIn(String userName) {
		for (User u : users) {
			if (u.getName().equals(userName))
				return true;
		}
		return false;
	}

	public synchronized void leaveGame(User user) {
		Game g = this.findGame(user);
		if (g != null)
			this.leaveGame(user, g);
	}
	
	public static synchronized boolean deleteUser(User user, String inputPassword) {
		if (!user.checkPassword(inputPassword)) {
			return false;
		}
		return deleteUser(user);
	}
	
	
	public static synchronized boolean deleteUser(User user) {
		TBIALApplication.getInstance().leaveGame(user);
		getDatabase().deleteUser(user.getId());
		return true;
		
		
		
		
		
		// Delete
		//getUsers().remove(user);
		//try {
			//getDatabase().deleteUser(user);
		//} catch (SQLException e) {
			//e.printStackTrace();
		//}
	}
}