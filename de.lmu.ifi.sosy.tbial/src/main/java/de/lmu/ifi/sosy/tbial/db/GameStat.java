package de.lmu.ifi.sosy.tbial.db;

public class GameStat {

	private int gameId;
	private String name;
	private int bugsPlayed;
	private int excusesPlayed;
	private int solutionsPlayed;
	private int specialCardsPlayed;
	private int abilityCardsPlayed;
	private int stumblingCardsPlayed;
	private String durationInTime;
	private int durationInTurns;

	public GameStat(int gameId, String name, int bugsPlayed, int excusesPlayed, int solutionsPlayed,
			int specialCardsPlayed, int abilityCardsPlayed, int stumblingCardsPlayed, String durationInTime,
			int durationInTurns) {

		this.gameId = gameId;
		this.name = name;
		this.bugsPlayed = bugsPlayed;
		this.excusesPlayed = excusesPlayed;
		this.solutionsPlayed = solutionsPlayed;
		this.specialCardsPlayed = specialCardsPlayed;
		this.abilityCardsPlayed = abilityCardsPlayed;
		this.stumblingCardsPlayed = stumblingCardsPlayed;
		this.durationInTime = durationInTime;
		this.durationInTurns = durationInTurns;

	}

	public int getGameId() {
		return gameId;
	}

	public String getName() {
		return name;
	}

	public int getBugsPlayed() {
		return bugsPlayed;
	}

	public int getExcusesPlayed() {
		return excusesPlayed;
	}

	public int getSolutionsPlayed() {
		return solutionsPlayed;
	}

	public int getSpecialCardsPlayed() {
		return specialCardsPlayed;
	}

	public int getAbilityCardsPlayed() {
		return abilityCardsPlayed;
	}

	public int getStumblingCardsPlayed() {
		return stumblingCardsPlayed;
	}

	public String getDurationInTime() {
		return durationInTime;
	}

	public int getDurationInTurns() {
		return durationInTurns;
	}
}
