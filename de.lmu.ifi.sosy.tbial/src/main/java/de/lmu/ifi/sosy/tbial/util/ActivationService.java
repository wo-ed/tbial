package de.lmu.ifi.sosy.tbial.util;

import java.util.List;

import de.lmu.ifi.sosy.tbial.db.Database;

/**
 * Service for user account activation
 * 
 * @author Stephan
 *
 */
public class ActivationService {

	/*
	 * Storage for active tokens
	 */
	private TokenStorage tokenStorage = new TokenStorage();

	/*
	 * Email Service
	 */
	private IEmailService mailService = new EmailService();

	public TokenStorage getTokenStorage() {
		return tokenStorage;
	}

	/**
	 * Stores all active Tokens from Database in storage
	 * 
	 * @param database
	 *            - to connect
	 */
	public void initTokens(Database database) {
		List<VerificationToken> tokens = database.getVerificationTokens();
		tokenStorage.storeAll(tokens);
	}

	public IEmailService getMailService() {
		return mailService;
	}

	public void setMailService(IEmailService mailService) {
		this.mailService = mailService;
	}

}
