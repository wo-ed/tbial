package de.lmu.ifi.sosy.tbial.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;

import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.db.Database;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.abilitycards.BugDelegation;
import de.lmu.ifi.sosy.tbial.model.abilitycards.PreviousJob;
import de.lmu.ifi.sosy.tbial.model.abilitycards.WearsSunglassesAtWork;
import de.lmu.ifi.sosy.tbial.model.abilitycards.WearsTieAtWork;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Accenture;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Google;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Microsoft;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Nasa;
import de.lmu.ifi.sosy.tbial.model.actioncards.BoringMeeting;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.actioncards.Heisenbug;
import de.lmu.ifi.sosy.tbial.model.actioncards.LANParty;
import de.lmu.ifi.sosy.tbial.model.actioncards.LameExcuse;
import de.lmu.ifi.sosy.tbial.model.actioncards.PersonalCoffeeMachine;
import de.lmu.ifi.sosy.tbial.model.actioncards.Pwnd;
import de.lmu.ifi.sosy.tbial.model.actioncards.RedBullDispenser;
import de.lmu.ifi.sosy.tbial.model.actioncards.Refactoring;
import de.lmu.ifi.sosy.tbial.model.actioncards.Solution;
import de.lmu.ifi.sosy.tbial.model.actioncards.StandupMeeting;
import de.lmu.ifi.sosy.tbial.model.actioncards.SystemIntegration;
import de.lmu.ifi.sosy.tbial.model.characters.BruceSchneier;
import de.lmu.ifi.sosy.tbial.model.characters.CharacterCard;
import de.lmu.ifi.sosy.tbial.model.characters.HolierThanThou;
import de.lmu.ifi.sosy.tbial.model.characters.JeffTaylor;
import de.lmu.ifi.sosy.tbial.model.characters.KentBeck;
import de.lmu.ifi.sosy.tbial.model.characters.KonradZuse;
import de.lmu.ifi.sosy.tbial.model.characters.LarryEllison;
import de.lmu.ifi.sosy.tbial.model.characters.LarryPage;
import de.lmu.ifi.sosy.tbial.model.characters.LinusTorvalds;
import de.lmu.ifi.sosy.tbial.model.characters.MarkZuckerberg;
import de.lmu.ifi.sosy.tbial.model.characters.SteveBallmer;
import de.lmu.ifi.sosy.tbial.model.characters.SteveJobs;
import de.lmu.ifi.sosy.tbial.model.characters.TerryWeissman;
import de.lmu.ifi.sosy.tbial.model.characters.TomAndersonCharacterCard;
import de.lmu.ifi.sosy.tbial.model.rolecards.Consultant;
import de.lmu.ifi.sosy.tbial.model.rolecards.EvilCodeMonkey;
import de.lmu.ifi.sosy.tbial.model.rolecards.HonestDeveloper;
import de.lmu.ifi.sosy.tbial.model.rolecards.Manager;
import de.lmu.ifi.sosy.tbial.model.stumblingblockcards.FortranMaintenance;
import de.lmu.ifi.sosy.tbial.model.stumblingblockcards.OffTheJobTraining;
import de.lmu.ifi.sosy.tbial.model.visitor.PlayCardVisitor;

public class Game implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id = -1;

	public static final int MIN_PLAYERS = 4;

	public static final int MAX_PLAYERS = 7;

	private String name;

	protected BiMap<Player, User> players = HashBiMap.create();

	protected LinkedList<Player> availablePlayers = new LinkedList<>();

	private List<TurnCard> heap = new ArrayList<>();

	private List<TurnCard> stack;

	private short capacity;

	private GameState state = GameState.WAITING;

	private boolean isPrivate;

	private String password;

	private User owner;

	private TurnCard standupMeetingCard = null;

	private TurnCard eventCard = null;
	private final PlayCardVisitor visitor;

	// Needed for Game Statistics
	private int bugsPlayed = 0;
	private int excusesPlayed = 0;
	private int solutionsPlayed = 0;
	private int specialCardsPlayed = 0;
	private int abilityCardsPlayed = 0;
	private int stumblingCardsPlayed = 0;

	private Date startTime;
	private Date endTime;

	private HashMap<Player, List<Integer>> playerHealthPoints = new HashMap<>();
	// Es wird turnsCounter+1 Elemente geben(Da 0te turn zukommt)
	private int turnsCounter = 0;

	private Player manager;

	private Player consultant;

	private List<Player> honestDevelopers;

	private List<Player> evilCodeMonkeys;

	private List<Player> winners = new ArrayList<>();
	private boolean hasWinners = false;

	/**
	 * The player who has a turn.
	 */
	private Player currentPlayer;

	private Map<Integer, TurnCard> cardStorage = new HashMap<>();

	/**
	 * 
	 */
	private boolean bugAlreadyPlayed;

	private Set<TurnCard> newTurnCards;

	private StealingCard stealingCard;

	// number of player who still need to response global attack cards(ex.
	// Heisenbug, Boring meeting)
	private int waitingGlobalAttackCard = 0;

	private List<TurnCard> standupMeetingCards = new ArrayList<>();

	private boolean standupMeetingPlayed = false;

	public boolean isStandupMeetingPlayed() {
		return standupMeetingPlayed;
	}

	public void setStandupMeetingPlayed(boolean standupMeetingPlayed) {
		this.standupMeetingPlayed = standupMeetingPlayed;
	}

	public void bugWasPlayed() {
		bugAlreadyPlayed = true;
	}

	public Game(String name, short capacity, boolean isPrivate, User owner) {
		this(name, capacity, isPrivate, "", owner);
	}

	public Game(String name, short capacity, boolean isPrivate, String password, User owner) {
		this.id = -1;
		this.name = name;
		this.capacity = capacity;
		this.isPrivate = isPrivate;
		this.password = password;
		this.setOwner(owner);

		IntStream.range(0, capacity).forEach(i -> this.availablePlayers.add(new Player((short) i)));
		this.join(owner);
		visitor = new PlayCardVisitor(this);
		// eventCard is not allowed to be null!
		this.eventCard = new Solution("");
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public synchronized Optional<Player> join(User user) {
		if (this.getUsers().contains(user)) {
			return Optional.of(getPlayer(user));
		} else if (this.availablePlayers.isEmpty()) {
			return Optional.empty();
		} else {
			Player player = this.availablePlayers.removeFirst();
			player.setName(user.getName());
			this.players.put(player, user);
			if (this.availablePlayers.isEmpty() && (this.state == GameState.PAUSED)) {
				this.resumeGame();
			}
			return Optional.of(player);
		}
	}

	public void removePlayer(User user) {
		Player assignedPlayer = this.players.inverse().get(user);
		assignedPlayer.setName("(empty player)");
		this.players.remove(assignedPlayer);
		this.availablePlayers.add(assignedPlayer);
		if (this.state.equals(GameState.RUNNING)) {
			pauseGame();
		}
	}

	public boolean isReadyToStart() {
		return !isStarted() && getPlayers().size() >= Game.MIN_PLAYERS && getPlayers().size() <= capacity;
	}

	public boolean isStarted() {
		return state == GameState.RUNNING || state == GameState.PAUSED;
	}

	public Set<TurnCard> drawCards(Player player, int count) {
		Set<TurnCard> cardsDrawn = new HashSet<>();

		for (int i = 0; i < count; i++) {
			cardsDrawn.add(drawCard(player));
		}

		return cardsDrawn;
	}

	public boolean attemptDrawCard(Player player) {
		if (player.getCharacter().canDrawCard(player) > 0) {
			drawCard(player);
			player.getCharacter().didDrawCard();
			return true;
		}
		return false;
	}

	public TurnCard drawCard(Player player) {
		if (stack.isEmpty()) {
			changeHeapIntoStack();
		}

		TurnCard card = stack.remove(stack.size() - 1);
		card.setPlayer(player);
		player.getHandcards().add(card);

		return card;
	}

	public void changeHeapIntoStack() {
		stack.clear();
		stack.addAll(heap);
		stack.stream().forEach(c -> c.resetOwnerAndTarget());
		Collections.shuffle(stack);
		heap.clear();
	}

	public void initializeStack() {
		stack = createTurnCards();
		stack.stream().forEach(c -> this.cardStorage.put(c.getId(), c));
		Collections.shuffle(stack);
	}

	public List<TurnCard> createTurnCards() {
		if (TBIALApplication.DEBUG_MODE) {

			return Stream.of(cardStream(10, i -> new BugReport("Nullpointer")),
					cardStream(4, i -> new BugReport("Class not Found")),
					cardStream(10, i -> new Solution("I know regular expressions")), cardStream(1, i -> new LANParty()),
					cardStream(10, i -> new StandupMeeting()), cardStream(2, i -> new BoringMeeting()),
					cardStream(10, i -> new Pwnd()), cardStream(10, i -> new Refactoring())).flatMap(c -> c)
					.collect(Collectors.toList());

		} else {
			return Stream.of(
					// Ability Cards
					cardStream(2, i -> new WearsTieAtWork()), cardStream(1, i -> new Nasa()),
					cardStream(3, i -> new Microsoft()), cardStream(2, i -> new Google()),
					cardStream(2, i -> new Accenture()), cardStream(1, i -> new WearsSunglassesAtWork()),
					cardStream(2, i -> new BugDelegation()),

					// Stumbling Block Cards
					cardStream(1, i -> new FortranMaintenance()), cardStream(3, i -> new OffTheJobTraining()),

					// Action Cards
					cardStream(4, i -> new BugReport("Nullpointer")),
					cardStream(4, i -> new BugReport("Class not Found")),
					cardStream(4, i -> new BugReport("Off By One")),
					cardStream(4, i -> new BugReport("Customer Hates")), cardStream(4, i -> new BugReport("Core Dump")),
					cardStream(4, i -> new BugReport("System Hangs")),
					cardStream(4, i -> new LameExcuse("I'm not Responsible")),
					cardStream(4, i -> new LameExcuse("Works For Me!")),
					cardStream(4, i -> new LameExcuse("It's a Feature")), cardStream(2, i -> new Solution("Coffee")),
					cardStream(2, i -> new Solution("I know regular expressions")),
					cardStream(2, i -> new Solution("Code Fix Session")), cardStream(3, i -> new SystemIntegration()),
					cardStream(4, i -> new Pwnd()), cardStream(1, i -> new RedBullDispenser()),
					cardStream(1, i -> new Heisenbug()), cardStream(2, i -> new StandupMeeting()),

					cardStream(2, i -> new PersonalCoffeeMachine()), cardStream(4, i -> new Refactoring()),
					cardStream(1, i -> new LANParty()), cardStream(2, i -> new BoringMeeting())).flatMap(c -> c)
					.collect(Collectors.toList());

		}
	}

	protected Stream<TurnCard> cardStream(int count, IntFunction<? extends TurnCard> mapper) {
		return IntStream.range(0, count).mapToObj(mapper);
	}

	/**
	 * Handles a player turn. Executed by an 'card on click event'.
	 * 
	 * @param card
	 *            - The card, which is played by the user.
	 */
	public void playCard(TurnCard card) {

		if (!card.getTargetPlayer().isAlive()) {
			card.getPlayer().notifyView("You can not attack dead players!");
			return;
		}

		if (GameState.PAUSED == state) {
			return;
		}

		standupMeetingPlayed = false;
		if (card instanceof StandupMeeting) {
			standupMeetingPlayed = true;
		}

		// this is an additional rule
		if ((card instanceof Solution) && (livingPlayers() <= 2)) {
			// card.getPlayer().setState(PlayerState.ON_TURN);
			return;
		}
		card.play(visitor);

	}

	public int livingPlayers() {
		int count = 0;
		for (Player player : getPlayers()) {
			if (player.getState() != PlayerState.DEAD) {
				count++;
			}
		}
		return count;
	}

	/**
	 * Returns true if the current player has already played a bug, false
	 * otherwise.
	 */
	public boolean canPlayBug() {
		if (currentPlayer.getCharacter().canPlayMultipleBugs()) {
			return true;
		} else {
			return !bugAlreadyPlayed;
		}
	}

	/**
	 * At the beginning of each turn, the player draws two cards from the stack.
	 */
	public void drawCardsOnTurn() {
		CharacterCard character = currentPlayer.getCharacter();

		newTurnCards = drawCards(currentPlayer, character.numberOfCardsToDraw());

		if (newTurnCards.size() > character.numberOfNewCardsPlayerCanKeepAfterDraw()) {
			currentPlayer.setState(PlayerState.DROP_NEW_CARD);
			currentPlayer.notifyView("You have to throw "
					+ (newTurnCards.size() - currentPlayer.getCharacter().numberOfNewCardsPlayerCanKeepAfterDraw())
					+ " of your new cards(s) to the heap!");
		}
	}

	/**
	 * Has to be called when a new turn starts. The player may draw two cards
	 * from the stack.
	 */
	public void beginTurn() {
		Player oldCurrent = currentPlayer;
		incTurnsCounter();
		handlePenaltyCards();
		// Only Draw cards if not killed by FM
		if (oldCurrent.equals(currentPlayer))
			drawCardsOnTurn();

	}

	private void handlePenaltyCards() {
		boolean endTurn = false;

		for (TurnCard card : new ArrayList<>(currentPlayer.getPenaltyCards())) {
			if (card instanceof FortranMaintenance) {
				((FortranMaintenance) card).handleCard(this);
			} else if (card instanceof OffTheJobTraining) {
				endTurn = endTurn | ((OffTheJobTraining) card).handleCard(this);
			}
		}

		// End turn without drawing cards
		if (endTurn)
			endTurn(currentPlayer);
	}

	/**
	 * Ends the player's turn turn or sets his state to DROP_CARD if he has too
	 * many hand cards
	 * 
	 * @param player
	 *            Must be the current player
	 * @return true if the player is the current player and he has not too many
	 *         cards
	 */
	public boolean tryEndTurn(Player player) {
		if (!getCurrentPlayer().equals(player)) {
			player.notifyView("It's not your turn!");
			return false;
		}

		if (player.getState() == PlayerState.STANDUP_MEETING) {
			return false;
		}

		return endTurn(player);
	}

	/**
	 * The current player finished her/his turn. The currentPlayer is set to the
	 * next player.
	 * 
	 * @param player
	 * 
	 * @return true if the turn was finished successfully. False if the
	 *         currentPlayer has still to many handcards or must interact with
	 *         the ui.
	 */

	public boolean endTurn(Player player) {

		if (player.equals(currentPlayer)) {

			if (!playerHasTooManyCards(player)) {
				setNextPlayer();

				for (Player p : this.getAllPlayers()) {
					if (p.isAlive()) {
						p.getCharacter().endTurn();
						p.notifyView("It's " + getCurrentPlayer().getName() + "'s turn");
					}
				}

				bugAlreadyPlayed = false;
				updateHealthPoints();
				return true;
			} else {
				int toDrop = currentPlayer.getHandcards().size() - currentPlayer.getHp();
				player.notifyView("You have to drop " + toDrop + " card(s) before you can end your turn!");
				return false;
			}
		}
		return false;
	}

	public boolean playerHasTooManyCards(Player player) {
		return player.getHandcards().size() > player.getHp();
	}

	/**
	 * Sets currentPlayer to the next player.
	 */
	private void setNextPlayer() {

		if (currentPlayer.isAlive())
			currentPlayer.setState(PlayerState.WAIT);
		currentPlayer = this.getNextPlayer(currentPlayer);
		currentPlayer.setState(PlayerState.ON_TURN);

		beginTurn();
	}

	public boolean attemptThrowCardToHeap(TurnCard card) {
		if (card.getPlayer().getState() == PlayerState.ON_TURN) {
			if (KentBeck.class.isInstance(card.getPlayer().getCharacter())) {
				int oldHp = card.getPlayer().getCharacter().getHp();
				int toDrop = ((KentBeck) card.getPlayer().getCharacter()).cardDropped(card.getPlayer(), this);
				if (toDrop == 0 && oldHp < card.getPlayer().getCharacter().getHp()) {
					card.getPlayer().notifyView("1 Hp gained.");
				}
			}
			throwCardToHeap(card);
			return true;
		} else if (card.getPlayer().getState() == PlayerState.DROP_NEW_CARD) {
			if (newTurnCards.remove(card)) {
				throwCardToHeap(card);
				if (newTurnCards.size() == card.getPlayer().getCharacter().numberOfNewCardsPlayerCanKeepAfterDraw()) {
					card.getPlayer().setState(PlayerState.ON_TURN);
					card.getPlayer().notifyView("You can continue your turn.");
				}
				return true;
			} else {
				card.getPlayer()
						.notifyView("You have to throw "
								+ (newTurnCards.size()
										- card.getPlayer().getCharacter().numberOfNewCardsPlayerCanKeepAfterDraw())
								+ " of your new cards(s) to the heap!");
			}
		} else if (KentBeck.class.isInstance(card.getPlayer().getCharacter())) {
			if (card.getPlayer().getMaxHp() > card.getPlayer().getHp()) {
				KentBeck kent = (KentBeck) card.getPlayer().getCharacter();
				if (kent.canDropCard(card.getPlayer())) {
					int toDrop = kent.cardDropped(card.getPlayer(), this);
					if (toDrop == 0) {
						card.getPlayer().notifyView("1 Hp gained.");
					} else {
						card.getPlayer().notifyView("Drop " + toDrop + " more card to gain 1 Hp");
					}
					throwCardToHeap(card);
					return true;
				} else {
					card.getPlayer().notifyView("You have to less handcards to gain Hp");
				}
			} else {
				card.getPlayer().notifyView("You can not pass your max Hps!");
			}
		}

		return false;
	}

	public boolean steal(TurnCard cardToBeStolen) {
		Player attacker = getCurrentPlayer();

		if (attacker.getState() == PlayerState.PWND) {
			return pwnd(cardToBeStolen);
		} else if (attacker.getState() == PlayerState.REFACTORING) {
			return refactoring(cardToBeStolen);
		} else {
			return false;
		}
	}

	private void checkStolenCardModifiesPrestige(TurnCard cardToBeStolen) {
		if (PreviousJob.class.isInstance(cardToBeStolen)) {
			cardToBeStolen.getPlayer().getCharacter().setPrestige(0);
		} else if (WearsSunglassesAtWork.class.isInstance(cardToBeStolen)) {
			cardToBeStolen.getPlayer().getCharacter().removeOthersPrestigeModifier();
		} else if (WearsTieAtWork.class.isInstance(cardToBeStolen)) {
			cardToBeStolen.getPlayer().getCharacter().removeSelfPrestigeModifier();
		}
	}

	public boolean pwnd(TurnCard cardToBeStolen) {
		Player attacker = getCurrentPlayer();
		Player target = (cardToBeStolen.getTargetPlayer() == null) ? cardToBeStolen.getPlayer()
				: cardToBeStolen.getTargetPlayer();

		if (attacker.getState() != PlayerState.PWND) {
			attacker.notifyView("wrong state wrong.");
			return false;
		}

		if (!checkStealingCardConditions(cardToBeStolen)) {
			attacker.notifyView("Something went wrong.");
			return false;
		}

		if (!checkPrestige(attacker, target)) {
			attacker.notifyView("Your prestige is too low to play pwnd on this player.");
			return false;
		}

		target.getHandcards().remove(cardToBeStolen);
		if (target.getBuffCards().remove(cardToBeStolen)) {
			checkStolenCardModifiesPrestige(cardToBeStolen);
		}
		target.getPenaltyCards().remove(cardToBeStolen);

		cardToBeStolen.setPlayer(attacker);
		attacker.getHandcards().add(cardToBeStolen);

		attacker.setState(PlayerState.ON_TURN);
		stealingCard = null;
		return true;
	}

	public boolean refactoring(TurnCard cardToBeStolen) {
		Player attacker = getCurrentPlayer();

		if (attacker.getState() != PlayerState.REFACTORING) {
			return false;
		}

		if (!checkStealingCardConditions(cardToBeStolen)) {
			return false;
		}

		this.throwCardToHeap(cardToBeStolen);

		attacker.setState(PlayerState.ON_TURN);
		stealingCard = null;
		return true;
	}

	private boolean checkStealingCardConditions(TurnCard cardToBeStolen) {
		Player attacker = getCurrentPlayer();
		Player target = (cardToBeStolen.getTargetPlayer() == null) ? cardToBeStolen.getPlayer()
				: cardToBeStolen.getTargetPlayer();

		if (target.equals(attacker) || stealingCard == null) {
			return false;
		}

		if (!stealingCard.getTargetPlayer().equals(target)) {
			attacker.notifyView("You must steal from the attacked player");
			return false;
		}

		return true;
	}

	private boolean checkPrestige(Player attacker, Player target) {
		return attacker.getCharacter().getPrestige() >= target.getCharacter().getPrestige();
	}

	/**
	 * Throws a card selected by the player to the heap.
	 * 
	 * @param card
	 */
	public void throwCardToHeap(TurnCard card) {
		card.getPlayer().getHandcards().remove(card);
		if (card.getPlayer().getBuffCards().remove(card)) {
			checkStolenCardModifiesPrestige(card);
		}
		card.getPlayer().getPenaltyCards().remove(card);

		if (card.getTargetPlayer() != null) {
			card.getTargetPlayer().getHandcards().remove(card);
			card.getTargetPlayer().getBuffCards().remove(card);
			card.getTargetPlayer().getPenaltyCards().remove(card);
		}
		getHeap().add(card);

	}

	/**
	 * Initializes the game. Set game state to running. Adapt game capacity to
	 * amount of currently joined players. Initialize stack. Distribute player
	 * roles. Set currentPlayer to Manager.
	 */
	public boolean init() {

		if (!this.isReadyToStart())
			return false;

		this.state = GameState.RUNNING;
		this.startTime = new Date();

		this.initializeStack();
		this.setCapacity((short) getPlayers().size());
		this.availablePlayers.clear();
		this.initializePlayerAttributes();

		evilCodeMonkeys = new ArrayList<>();
		honestDevelopers = new ArrayList<>();

		// Sets the manager as first player starting the game.
		for (Player p : this.getAllPlayers()) {
			if (TBIALApplication.DEBUG_MODE) {
				if (p == getPlayer(getOwner())) {
					currentPlayer = p;
					currentPlayer.setState(PlayerState.ON_TURN);
					beginTurn();
					break;
				}
			} else {
				RoleCard role = p.getRole();

				if (role instanceof Manager) {
					manager = p;
					currentPlayer = manager;
					currentPlayer.setState(PlayerState.ON_TURN);
					beginTurn();
					// break;
				}
				if (role instanceof Consultant) {
					consultant = p;
				}

				if (role instanceof EvilCodeMonkey) {
					evilCodeMonkeys.add(p);
				}

				if (role instanceof HonestDeveloper) {
					honestDevelopers.add(p);
				}
			}

			// Update statistics
			this.getUser(p).incrementGamesParticipated(TBIALApplication.getDatabase());
		}

		initHealthPoints();

		return true;
	}

	private void initializePlayerAttributes() {
		ArrayList<RoleCard> roles = createRoleCards(getPlayers().size());
		ArrayList<CharacterCard> characters = createCharacterCards();

		getPlayers().forEach(player -> {
			initializePlayerAttributes(player, roles, characters);
		});
	}

	protected void initializePlayerAttributes(Player player, ArrayList<RoleCard> roles,
			ArrayList<CharacterCard> characters) {
		int roleIndex = new Random().nextInt(roles.size());
		RoleCard role = roles.remove(roleIndex);
		player.setRole(role);

		int characterIndex = new Random().nextInt(characters.size());
		CharacterCard character = characters.remove(characterIndex);
		character.initHp(role);
		player.setCharacter(character);

		player.getHandcards().clear();
		this.drawCards(player, player.getHp());
	}

	public ArrayList<RoleCard> createRoleCards(int playersCount) {
		ArrayList<RoleCard> roles = Lists.newArrayList(
				Arrays.asList(new Manager(), new Consultant(), new EvilCodeMonkey(), new EvilCodeMonkey()));

		if (playersCount >= 5) {
			roles.add(new HonestDeveloper());
		}
		if (playersCount >= 6) {
			roles.add(new EvilCodeMonkey());
		}
		if (playersCount == 7) {
			roles.add(new HonestDeveloper());
		}
		return roles;
	}

	public ArrayList<CharacterCard> createCharacterCards() {
		// List needs length between 4 and 7 (else init fails)
		ArrayList<CharacterCard> characters = Lists.newArrayList(Arrays.asList(new BruceSchneier(),
				new HolierThanThou(), new JeffTaylor(), new KentBeck(), new KonradZuse(), new LarryEllison(),
				new LarryPage(), new LinusTorvalds(), new MarkZuckerberg(), new SteveBallmer(), new SteveJobs(),
				new TerryWeissman(), new TomAndersonCharacterCard()));
		return characters;
	}

	public boolean userCanStartGame(User user) {
		return this.isReadyToStart() && this.getOwner() != null && user == this.getOwner();
	}

	public synchronized void leaveGame(User user) {
		// Update statistics
		user.incrementGamesLost(TBIALApplication.getDatabase());

		this.removePlayer(user);

		if (this.getOwner().equals(user) && this.getUsers().iterator().hasNext()) {
			User newGameOwner = this.getUsers().iterator().next();
			this.setOwner(newGameOwner);
		}
	}

	public void endStealMode(Player player) {
		player.setState(PlayerState.WAIT);
		getCurrentPlayer().setState(PlayerState.ON_TURN);
		if (MarkZuckerberg.class.isInstance(player.getCharacter())) {
			MarkZuckerberg mark = (MarkZuckerberg) player.getCharacter();
			mark.resetCardsToSteal();
			mark.setPlayerToSteal(null);
		}
	}

	public boolean attemptStealCard(TurnCard toSteal, Player stealer, Player stolen) {

		if (stealer.getCharacter().canStealFrom(stealer, stolen)) {
			stealCard(toSteal, stealer, stolen);
			return true;
		}
		return false;

	}

	private void stealCard(TurnCard toSteal, Player stealer, Player stolen) {
		stealer.addHandCard(toSteal);
		stolen.removeHandcard(toSteal);
		stolen.removeBuffCard(toSteal);
		stolen.removePenaltyCard(toSteal);
		toSteal.setPlayer(stealer);
		toSteal.setTargetPlayer(null);
		stolen.notifyView(stealer.getName() + " stole '" + toSteal.getName() + "'");

		if (MarkZuckerberg.class.isInstance(stealer.getCharacter())) {
			MarkZuckerberg mark = (MarkZuckerberg) stealer.getCharacter();
			mark.stoleCard();
			if (mark.getCountCardsToSteal() > 0) {
				stealer.notifyView("You can steal " + mark.getCountCardsToSteal() + " from " + stolen.getName()
						+ "'s cards (hand- and tablecards). To steal click on the card you want to steal. If you don't want to steal more cards click 'pass'");
				return;
			}
		}
		this.endStealMode(stealer);
	}

	public void incHp(Player player) {
		player.getCharacter().incHp();
	}

	public void decHp(Player player, Player attacker) {
		if (!player.isAlive())
			return;
		// Player is fired
		if (player.getCharacter().decHp() < 1) {
			player.setState(PlayerState.DEAD);
			player.getRole().setVisible(true);

			firePlayer(player);

			if (this.hasWinners())
				return;

			this.additionalRules(attacker, player);
			// endTurn if curren player fired
			if (player.equals(currentPlayer))
				this.endTurn(player);

		} else if (player.getCharacter() instanceof TomAndersonCharacterCard) {
			TomAndersonCharacterCard tom = ((TomAndersonCharacterCard) player.getCharacter());
			tom.drawCard();
			player.notifyView("You lost " + tom.canDrawCard(player) + " Hp you can draw " + tom.canDrawCard(player)
					+ " cards from stack till end of Turn");

		} else if (player.getCharacter() instanceof MarkZuckerberg) {
			if (!player.equals(attacker)) {
				MarkZuckerberg mark = (MarkZuckerberg) player.getCharacter();
				mark.canStealCard();
				mark.setPlayerToSteal(attacker);
				this.getCurrentPlayer().setState(PlayerState.WAIT);
				player.setState(PlayerState.STEALING_MODE);
				player.notifyView(attacker.getName() + " cost you " + mark.getCountCardsToSteal()
						+ " Hp. You can steal " + mark.getCountCardsToSteal()
						+ " of his/her cards (hand- and tablecards). Click on the card you want to steal, if you don't want to steal more cards click end turn "
						+ mark.getName().toString());
			}
		}
	}

	private void transferAllCardsToHeap(Player from) {
		this.transferAllCardsToHeap(from.getBuffCards());
		this.transferAllCardsToHeap(from.getHandcards());
		this.transferAllCardsToHeap(from.getPenaltyCards());

	}

	public void firePlayer(Player player) {

		player.notifyView("You are fired!");

		Optional<Player> p = isLarryPageInGame();
		if (p.isPresent()) {
			p.get().notifyView(player.getName() + " was fired and you took his/her cards (Larry Page)");

			transferAllCards(p.get(), player);
		} else {
			transferAllCardsToHeap(player);
		}

		if (checkWinningConditions(player)) {
			this.state = GameState.GAMEOVER;
			setEndTime(new Date());
			this.updateHealthPoints();
			this.updateWinsAndLoses();
			hasWinners = true;

			updateGameStatsInDB(TBIALApplication.getDatabase());
		}

	}

	public boolean checkWinningConditions(Player firedPlayer) {
		RoleCard role = firedPlayer.getRole();
		if (role instanceof Manager) {
			// if there are any evil code monkeys left
			if (stillHasEvilCodeMonkeys()) {
				// the evil code monkeys win
				for (Player evilCodeMonkey : getEvilCodeMonkeys()) {
					winners.add(evilCodeMonkey);
				}
				return true;
			}
			// if only the consultant is left
			else {
				// he wins
				winners.add(getConsultant());
				return true;
			}
		} else if (role instanceof HonestDeveloper) {
			// do nothing
			return false;
		} else if (role instanceof EvilCodeMonkey) {
			// if all evil code monkeys and the consultant are fired
			if (!stillHasEvilCodeMonkeys() && !stillHasConsultant()) {
				// the manager and the honest developers win
				winners.add(getManager());
				for (Player honestDeveloper : getHonestDevelopers()) {
					winners.add(honestDeveloper);
				}
				return true;
			}
		} else if (role instanceof Consultant) {
			// if all evil code monkeys are fired
			if (!stillHasEvilCodeMonkeys()) {
				// the manager and the honest developers win
				winners.add(getManager());
				for (Player honestDeveloper : getHonestDevelopers()) {
					winners.add(honestDeveloper);
				}
				return true;
			}
		} else {
			String message = String.format("Unknown role: %s", role.getName());
			try {
				throw new Exception(message);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	private void transferAllCardsToHeap(List<TurnCard> cards) {
		new ArrayList<>(cards).stream().forEach(c -> throwCardToHeap(c));
	}

	private void transferAllCards(Player to, Player from) {
		if (!to.equals(from)) {
			from.getHandcards().stream().forEach(tc -> {
				tc.setPlayer(to);
				to.addHandCard(tc);
			});
			from.getHandcards().clear();

			from.getBuffCards().stream().forEach(tc -> {
				tc.setPlayer(to);
				to.addHandCard(tc);
			});
			from.getBuffCards().clear();

			from.getPenaltyCards().stream().forEach(tc -> {
				to.addPenaltyCard(tc);
				tc.setTargetPlayer(to);
			});
			from.getPenaltyCards().clear();
		}
	}

	private Optional<Player> isLarryPageInGame() {

		for (Player p : this.getAllPlayers()) {
			if (p.getCharacter() instanceof LarryPage && p.isAlive())
				return Optional.of(p);
		}
		return Optional.empty();
	}

	private void additionalRules(Player sourcePlayer, Player targetPlayer) {
		// punishment
		additionalRulePunishment(sourcePlayer, targetPlayer);
		// reward
		additionalRuleReward(sourcePlayer, targetPlayer);
	}

	private void additionalRulePunishment(Player sourcePlayer, Player targetPlayer) {
		if (sourcePlayer.getRole() instanceof Manager) {
			if (!(sourcePlayer == targetPlayer) && (targetPlayer.getRole() instanceof HonestDeveloper)
					&& (targetPlayer.getState() == PlayerState.DEAD)) {
				punishment(sourcePlayer);
			}
		}
	}

	private void additionalRuleReward(Player sourcePlayer, Player targetPlayer) {
		if (!(sourcePlayer == targetPlayer) && (targetPlayer.getRole() instanceof EvilCodeMonkey)
				&& (targetPlayer.getHp() < 1)) {
			reward(sourcePlayer);
		}

	}

	private void reward(Player player) {
		drawCards(player, 3);
	}

	private void punishment(Player player) {
		throwAllHandcardsToHeap(player);
	}

	private void throwAllHandcardsToHeap(Player player) {
		this.transferAllCardsToHeap(player.getHandcards());
	}

	public void setBugsPlayed(int bugsPlayed) {
		this.bugsPlayed = bugsPlayed;
	}

	public int getBugsPlayed() {
		return bugsPlayed;
	}

	public void incBugsPlayed() {
		bugsPlayed++;
	}

	public void setExcusesPlayed(int excusesPlayed) {
		this.excusesPlayed = excusesPlayed;
	}

	public int getExcusesPlayed() {
		return excusesPlayed;
	}

	public void incExcusesPlayed() {
		excusesPlayed++;
	}

	public void setSolutionsPlayed(int solutionsPlayed) {
		this.solutionsPlayed = solutionsPlayed;
	}

	public int getSolutionsPlayed() {
		return solutionsPlayed;
	}

	public void incSolutionsPlayed() {
		solutionsPlayed++;
	}

	public void setSpecialCardsPlayed(int specialCardsPlayed) {
		this.specialCardsPlayed = specialCardsPlayed;
	}

	public int getSpecialCardsPlayed() {
		return specialCardsPlayed;
	}

	public void incSpecialCardsPlayed() {
		specialCardsPlayed++;
	}

	public void setSpecialAbilityPlayed(int abilityCardsPlayed) {
		this.abilityCardsPlayed = abilityCardsPlayed;
	}

	public int getAbilityCardsPlayed() {
		return abilityCardsPlayed;
	}

	public void incAbilityCardsPlayed() {
		abilityCardsPlayed++;
	}

	public void setStumblingCardsPlayed(int stumblingCardsPlayed) {
		this.stumblingCardsPlayed = stumblingCardsPlayed;
	}

	public int getStumblingCardsPlayed() {
		return stumblingCardsPlayed;
	}

	public void incStumblingCardsPlayed() {
		stumblingCardsPlayed++;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public String getDurationInTime() {
		long millis = endTime.getTime() - startTime.getTime();
		String duration = String.format("%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
				TimeUnit.MILLISECONDS.toMinutes(millis)
						- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)));
		return duration;
	}

	public void initHealthPoints() {
		for (Player player : getAllPlayers()) {
			List<Integer> hpList = new ArrayList<>();
			int hp = player.getHp();
			hpList.add(hp);
			playerHealthPoints.put(player, hpList);
		}
	}

	public void updateHealthPoints() {
		for (Player player : getAllPlayers()) {
			int hp = player.getHp();
			List<Integer> hpList = playerHealthPoints.get(player);
			hpList.add(hp);
		}
	}

	public HashMap<Player, List<Integer>> getPlayerHealthPoints() {
		return playerHealthPoints;
	}

	public int getTurnsCounter() {
		return turnsCounter;
	}

	public void setTurnsCounter(int turnsCounter) {
		this.turnsCounter = turnsCounter;
	}

	public void incTurnsCounter() {
		this.turnsCounter++;
	}

	public void deflectBug(TurnCard bug, boolean deflect, TurnCard... deflector) {
		if (deflect) {
			getCurrentPlayer().setState(PlayerState.ON_TURN);
			deflector[0].getPlayer().setState(PlayerState.WAIT);
			getCurrentPlayer().notifyView(deflector[0].getPlayer().getName() + " deflected the bug");
			if (bug != null)
				throwCardToHeap(bug);
			for (TurnCard t : deflector)
				throwCardToHeap(t);
		} else {
			bug.getTargetPlayer().notifyView(bug.getPlayer().getName() + " attacked you. You lost 1 Hp");
			bug.getTargetPlayer().setState(PlayerState.WAIT);
			bug.getPlayer().setState(PlayerState.ON_TURN);
			this.decHp(bug.getTargetPlayer(), bug.getPlayer());
			this.throwCardToHeap(bug);

		}
	}

	public Player getNextPlayer(Player player) {
		List<Player> orderedList = new LinkedList<>();
		orderedList.addAll(getAllPlayers());
		Collections.sort(orderedList);
		// skipping of dead players
		orderedList = orderedList.stream().filter(p -> p.isAlive()).collect(Collectors.toList());
		return orderedList.get((orderedList.indexOf(player) + 1) % orderedList.size());
	}

	public void pauseGame() {
		this.state = GameState.PAUSED;
	}

	public void resumeGame() {
		this.state = GameState.RUNNING;
	}

	public boolean validatePassword(String s) {
		if (this.password.equals(s)) {
			return true;
		} else {
			return false;
		}
	}

	public Set<Player> getAllPlayers() {
		Set<Player> result = new HashSet<>(this.availablePlayers);
		result.addAll(players.keySet());
		return result;
	}

	public boolean tryBugDelegation(TurnCard bug) {

		// Try Bug Delegation Card
		if (bug.getTargetPlayer().hasBugDelegation()) {
			Random r = new Random();
			Float probability = r.nextFloat();
			if (probability < bug.getTargetPlayer().getCharacter().calculateChance(0.25f)) {
				this.delegateBug(bug);
				return true;
			}
		}
		// Try Terry Weissman Card
		if (bug.getTargetPlayer().getCharacter().hasBugDelegation()) {
			Random r = new Random();
			Float probability = r.nextFloat();
			if (probability < bug.getTargetPlayer().getCharacter().calculateChance(0.25f)) {
				this.delegateBug(bug);
				return true;
			}
		}

		bug.getTargetPlayer().notifyView("The bug delegation failed.");
		return false;
	}

	public void delegateBug(TurnCard bug) {
		bug.getPlayer().notifyView(bug.getTargetPlayer().getName() + " delegated your bug to the heap");
		bug.getTargetPlayer().notifyView("You delegated " + bug.getPlayer().getName() + "'s bug successfully");
		this.throwCardToHeap(bug);
	}

	public boolean getIsPrivate() {
		return isPrivate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	public Set<Player> getPlayers() {
		return players.keySet();
	}

	public Set<User> getUsers() {
		return players.values();
	}

	public Player getPlayer(User user) {
		return players.inverse().get(user);
	}

	public User getUser(Player player) {
		return players.get(player);
	}

	public List<Player> getAvailablePlayers() {
		return availablePlayers;
	}

	public List<TurnCard> getHeap() {
		return heap;
	}

	public void setHeap(List<TurnCard> heap) {
		this.heap = heap;
	}

	public List<TurnCard> getStack() {
		return stack;
	}

	public void setStack(List<TurnCard> stack) {
		this.stack = stack;
	}

	public short getCapacity() {
		return capacity;
	}

	public void setCapacity(short capacity) {
		this.capacity = capacity;
	}

	public GameState getState() {
		return state;
	}

	public boolean isPrivate() {
		return isPrivate;
	}

	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public TurnCard getEventCard() {
		return eventCard;
	}

	public void setEventCard(TurnCard eventCard) {
		this.eventCard = eventCard;
	}

	public void setBugAlreadyPlayed(boolean played) {
		this.bugAlreadyPlayed = played;
	}

	public synchronized Player findPlayer(int playerId) {
		for (Player p : this.getAllPlayers()) {
			if (p.getId() == playerId)
				return p;
		}
		return null;
	}

	public void drawCardsForStandupMeeting(int count) {
		standupMeetingCards = new ArrayList<>();
		if (stack.isEmpty()) {
			changeHeapIntoStack();
		}

		for (int i = 0; i < count; i++) {
			TurnCard card = stack.remove(stack.size() - 1);
			standupMeetingCards.add(card);
		}

	}

	public void drawOneStandupMeetingCard(Player player, TurnCard card) {
		card.setPlayer(player);
		player.getHandcards().add(card);
		if (standupMeetingCards.remove(card)) {

			player.setState(PlayerState.WAIT);
			player.notifyView("Please wait until the standup meeting ends!");

			Player nextPlayer = getNextPlayer(player);

			while (nextPlayer.getState() == PlayerState.DEAD) {
				nextPlayer = getNextPlayer(nextPlayer);
			}

			if (standupMeetingCard.getPlayer().equals(nextPlayer)) {
				nextPlayer.setState(PlayerState.ON_TURN);

				for (Player p : getAllPlayers()) {
					p.notifyView("It's " + currentPlayer.getName() + "'s turn!");
				}
				throwCardToHeap(standupMeetingCard);
				standupMeetingCard = null;
			} else {
				nextPlayer.setState(PlayerState.STANDUP_MEETING);
				nextPlayer.notifyView("Draw one standup meeting card!");
			}
		}

	}

	public List<TurnCard> getStandupMeetingCards() {
		return standupMeetingCards;
	}

	public Player getManager() {
		return manager;
	}

	public Player getConsultant() {
		return consultant;
	}

	public List<Player> getHonestDevelopers() {
		return honestDevelopers;
	}

	public List<Player> getEvilCodeMonkeys() {
		return evilCodeMonkeys;
	}

	public boolean stillHasEvilCodeMonkeys() {
		for (Player player : getPlayers()) {
			if ((player.getRole() instanceof EvilCodeMonkey) && (player.getState() != PlayerState.DEAD)) {
				return true;
			}
		}
		return false;
	}

	public boolean stillHasConsultant() {
		for (Player player : getPlayers()) {
			if ((player.getRole() instanceof Consultant) && (player.getState() != PlayerState.DEAD)) {
				return true;
			}
		}
		return false;
	}

	public List<Player> getWinners() {
		return winners;
	}

	public boolean hasWinners() {
		return hasWinners;
	}

	public TurnCard getCard(int id) {
		return this.cardStorage.get(id);
	}

	public StealingCard getStealingCard() {
		return stealingCard;
	}

	public void setStealingCard(StealingCard stealingCard) {
		this.stealingCard = stealingCard;
	}

	public int getWaitingGlobalAttackCard() {
		return waitingGlobalAttackCard;
	}

	public void setWaitingGlobalAttackCard(int waitingGlobalAttackCard) {
		this.waitingGlobalAttackCard = waitingGlobalAttackCard;
	}

	public void decWaitingGlobalAttackCard() {
		this.waitingGlobalAttackCard--;
		if (waitingGlobalAttackCard == 0) {
			if (eventCard.getPlayer().getState() == PlayerState.DEAD) {
				tryEndTurn(eventCard.getPlayer());
			} else if (eventCard.getPlayer().getState() == PlayerState.WAIT) {
				eventCard.getPlayer().setState(PlayerState.ON_TURN);
			}

			sendEventCardToHep();
		}
	}

	public void sendEventCardToHep() {

		eventCard.getPlayer().removePenaltyCard(eventCard);
		heap.add(eventCard);
		eventCard = null;
	}

	public void storeGameStatsInDB(Database db) {
		db.storeGameStats(this);
	}

	public void updateGameStatsInDB(Database db) {
		db.updateGameStats(this);
	}

	public TurnCard getStandupMeetingCard() {
		return standupMeetingCard;
	}

	public void setStandupMeetingCard(TurnCard standupMeetingCard) {
		this.standupMeetingCard = standupMeetingCard;

	}

	public void updateWinsAndLoses() {
		for (Player p : players.keySet()) {
			if (winners.contains(p)) {
				this.getUser(p).incrementGamesWon(TBIALApplication.getDatabase());
			} else {
				this.getUser(p).incrementGamesLost(TBIALApplication.getDatabase());
			}
		}
	}

	public void passEvent(Player player) {

		PlayerState state = player.getState();

		if (state == PlayerState.DEFLECT_BUG) {

			TurnCard card = player.findBugInPenaltyCards();
			deflectBug(card, false, card);

		} else if (state == PlayerState.STEALING_MODE) {
			this.endStealMode(player);
		} else if (state == PlayerState.SYSTEM_INTEGRATION) {
			this.visitor.endSystemIntegration(player);
		} else if (state == PlayerState.HEISENBUG || state == PlayerState.BORING_MEETING) {
			passGolbalEvent(player);
		}
	}

	private void passGolbalEvent(Player player) {
		player.setState(PlayerState.WAIT);
		player.notifyView("You are out " + eventCard.getName() + "of now.");
		decHp(player, eventCard.getPlayer());
		decWaitingGlobalAttackCard();
	}

	public void throwAllBugsFromSystemIntegrationToHeap(Player looser, Player winner) {
		this.throwAllBugsFromPenaltyToHeap(looser);
		this.throwAllBugsFromPenaltyToHeap(winner);
	}

	private void throwAllBugsFromPenaltyToHeap(Player player) {
		new ArrayList<>(player.getPenaltyCards()).stream().forEach(c -> {
			if (player.getCharacter().isBugReport(c))
				this.throwCardToHeap(c);
		});

	}

}
