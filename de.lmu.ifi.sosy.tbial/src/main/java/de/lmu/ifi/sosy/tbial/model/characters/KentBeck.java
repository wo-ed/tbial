package de.lmu.ifi.sosy.tbial.model.characters;

import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.PlayerState;

public class KentBeck extends CharacterCard {

	private static final long serialVersionUID = 1L;

	private int cardsDropped;

	public KentBeck() {
		super("Kent Beck");
	}

	public boolean gainHp(Player player, Game game) {
		if (player.isAlive() && this.getMaxHp() > this.getHp() && cardsDropped == 2) {
			game.incHp(player);

			reset();
			return true;
		}
		return false;
	}

	public int cardDropped(Player p, Game game) {
		if (++cardsDropped == 2) {
			this.gainHp(p, game);
		}
		return cardsDropped;
	}

	public void reset() {
		cardsDropped = 0;
	}

	public boolean canDropCard(Player p) {

		if (p.getState() == PlayerState.ON_TURN)
			return true;

		if (p.getHandcards().size() + cardsDropped > 1)
			return true;

		return false;
	}

}
