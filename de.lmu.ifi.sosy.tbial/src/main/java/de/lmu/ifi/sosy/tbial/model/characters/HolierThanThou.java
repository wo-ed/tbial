package de.lmu.ifi.sosy.tbial.model.characters;

public class HolierThanThou extends CharacterCard {

	private static final long serialVersionUID = 1L;

	public HolierThanThou() {
		super("Holier Than Thou");
		othersPrestigeModifier = -1;
	}

	@Override
	public void addOthersPrestigeModifier() {
		othersPrestigeModifier = -2;
	}

	@Override
	public void removeOthersPrestigeModifier() {
		othersPrestigeModifier = -1;
	}
}
