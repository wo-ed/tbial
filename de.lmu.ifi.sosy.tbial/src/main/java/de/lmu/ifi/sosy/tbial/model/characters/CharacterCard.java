package de.lmu.ifi.sosy.tbial.model.characters;

import de.lmu.ifi.sosy.tbial.model.PersistentCard;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.RoleCard;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.actioncards.LameExcuse;
import de.lmu.ifi.sosy.tbial.model.rolecards.Manager;

public abstract class CharacterCard extends PersistentCard {

	private static final long serialVersionUID = 1L;
	private int hp;
	private int prestige = 0;
	protected int maxHp = 4;
	protected int cardsToDraw = 0;
	protected boolean hasBugDelegation = false;
	private boolean canPlayMultipleBugs = false;

	protected int othersPrestigeModifier = 0;
	protected int selfPrestigeModifier = 0;

	public CharacterCard() {
		super();
	}

	public CharacterCard(String name) {
		super();
		this.name = name;
	}

	public int getHp() {
		return hp;
	}

	public int getPrestige() {
		return prestige;
	}

	public int getPrestigeSeenByCharacter(CharacterCard card) {
		return this.getPrestige() + this.getSelfPrestigeModifier() + card.getOthersPrestigeModifier();
	}

	public boolean isLameExcuce(TurnCard tc) {
		return (LameExcuse.class.isInstance(tc));
	}

	public boolean isBugReport(TurnCard tc) {
		return (BugReport.class.isInstance(tc));
	}

	protected int getSelfPrestigeModifier() {
		return selfPrestigeModifier;
	}

	protected int getOthersPrestigeModifier() {
		return othersPrestigeModifier;
	}

	public void setPrestige(int prestige) {
		this.prestige = prestige;
	}

	public int getMaxHp() {
		return maxHp;
	}

	public int incHp() {
		if (hp > 0 && hp < maxHp)
			this.hp++;

		return hp;
	}

	public int decHp() {
		if (hp > 0)
			this.hp--;
		return hp;
	}

	public void initHp(RoleCard role) {
		if (role instanceof Manager) {
			maxHp++;
		}
		hp = maxHp;
	}

	public boolean canPlayMultipleBugs() {
		return canPlayMultipleBugs;
	}

	public int canDrawCard(Player p) {
		return this.cardsToDraw;
	}

	public void didDrawCard() {
		this.cardsToDraw--;
	}

	public void resetCardsToDraw() {
		this.cardsToDraw = 0;
	}

	public int numberOfLameExcusesNeededToDeflectBug() {
		return 1;
	}

	public Float calculateChance(float probability) {
		return probability;
	}

	public boolean hasBugDelegation() {
		return this.hasBugDelegation;
	}

	public void giveBugDelegation(boolean enable) {
		this.hasBugDelegation = enable;
	}

	public boolean canStealFrom(Player thisPlayer, Player stolen) {
		return (thisPlayer.getState() == PlayerState.STEALING_MODE);
	}

	public void endTurn() {
		resetCardsToDraw();
	}

	public int numberOfCardsToDraw() {
		return 2;
	}

	public int numberOfNewCardsPlayerCanKeepAfterDraw() {
		return 2;
	}

	public void enablePlayMultipleBugs() {
		this.canPlayMultipleBugs = true;
	}

	public void disablePlayMultipleBug() {
		this.canPlayMultipleBugs = false;
	}

	public void addSelfPrestigeModifier() {
		selfPrestigeModifier = 1;
	}

	public void addOthersPrestigeModifier() {
		othersPrestigeModifier = -1;
	}

	public void removeSelfPrestigeModifier() {
		selfPrestigeModifier = 0;
	}

	public void removeOthersPrestigeModifier() {
		othersPrestigeModifier = 0;
	}

}