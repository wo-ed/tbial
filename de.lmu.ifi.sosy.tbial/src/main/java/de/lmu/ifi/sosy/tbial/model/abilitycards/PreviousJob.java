package de.lmu.ifi.sosy.tbial.model.abilitycards;

import de.lmu.ifi.sosy.tbial.model.AbilityCard;

public abstract class PreviousJob extends AbilityCard {

	private static final long serialVersionUID = 1L;

	protected int prestigeModifier;

	public PreviousJob(String name) {
		setName(name);
	}

	public int getPrestigeModifier() {
		return prestigeModifier;
	}

}
