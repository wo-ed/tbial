package de.lmu.ifi.sosy.tbial.pages;

import java.util.Collection;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.model.Game;

public class CreateGamePanel extends Panel {

	private short capacity = 4;

	private final Button buttonFour;
	private final Button buttonFive;
	private final Button buttonSix;
	private final Button buttonSeven;

	private final Button isPrivateButton;
	private final Button isNotPrivateButton;

	private boolean isPrivate = false;

	private static final long serialVersionUID = 1L;

	private final Button createButton;
	private final Button cancelButton;

	private final TextField<String> nameField;

	private final PasswordTextField passwordField;

	private String name;
	private String password;

	private Label nameCheckLabel;
	private Label currentCapacityLabel;
	private Label currentIsPrivateLabel;

	private TabPage page;

	public CreateGamePanel(String id, TabPage page) {
		super(id);
		this.page = page;
		nameCheckLabel = new Label("nameCheck", new Model<>(""));
		nameCheckLabel.setOutputMarkupId(true);
		currentCapacityLabel = new Label("capacityView", new Model<>(capacity));
		currentCapacityLabel.setOutputMarkupId(true);
		currentIsPrivateLabel = new Label("isPrivateView", new Model<>(this.showIsPrivate()));
		currentIsPrivateLabel.setOutputMarkupId(true);

		Form<?> capacityForm = new Form<>("submitCapacity");
		Form<?> isPrivateForm = new Form<>("submitIsPrivate");
		Form<?> createGameForm = new Form<>("CreateGame");
		Form<?> nameFieldForm = new Form<>("nameFieldForm");
		Form<?> passwordForm = new Form<>("passwordForm");

		nameField = new TextField<>("name", new Model<>(""));
		passwordField = new PasswordTextField("password", new Model<>(""));

		buttonFour = new Button("button4") {
			private static final long serialVersionUID = 1;

			@Override
			public void onSubmit() {
				submitCapacityButton((short) 4);
			};
		};
		buttonFive = new Button("button5") {
			private static final long serialVersionUID = 1;

			@Override
			public void onSubmit() {
				submitCapacityButton((short) 5);
			};
		};
		buttonSix = new Button("button6") {
			private static final long serialVersionUID = 1;

			@Override
			public void onSubmit() {
				submitCapacityButton((short) 6);
			};
		};
		buttonSeven = new Button("button7") {
			private static final long serialVersionUID = 1;

			@Override
			public void onSubmit() {
				submitCapacityButton((short) 7);
			};
		};

		isNotPrivateButton = new Button("isNotPrivatebutton") {
			private static final long serialVersionUID = 1;

			public void onSubmit() {
				submitIsPrivateButton(false);
			};
		};
		isPrivateButton = new Button("isPrivatebutton") {
			private static final long serialVersionUID = 1;

			@Override
			public void onSubmit() {
				submitIsPrivateButton(true);
			};
		};

		cancelButton = new Button("cancelbutton") {
			private static final long serialVersionUID = 1;

			// Back to lobby
			@Override
			public void onSubmit() {
				CreateGamePanel.this.replaceWith(new LobbyPanel("lobbyTab", page));
			};
		};
		cancelButton.setDefaultFormProcessing(false);

		createButton = new Button("createbutton") {
			/**
			 * UID for serialization.
			 */
			private static final long serialVersionUID = 1;

			// Read name and password and create game
			@Override
			public void onSubmit() {

				if (page.getCurrentGameLobby() != null) {
					page.getCurrentGameLobby().leaveGame(page.getUser());
				}

				if (validateGameName(name)) {
					Game game = createGame(name, password);
					page.setGameLobbyTab(game.getName());
				}
			};
		};

		// Beobachtet namefield
		OnChangeAjaxBehavior onNameChange = new OnChangeAjaxBehavior() {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				name = nameField.getModelObject();
				setName(name);

				if (!validateGameName(name)) {
					nameCheckLabel.setDefaultModelObject("Game name already exists. Please take other name.");
				} else {
					nameCheckLabel.setDefaultModelObject("");
				}
				target.add(nameCheckLabel);
			}
		};
		// Beobachtet passwordfield
		OnChangeAjaxBehavior onPasswordChange = new OnChangeAjaxBehavior() {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				password = passwordField.getModelObject();
				setPassword(password);
			}
		};

		nameField.add(onNameChange);
		passwordField.add(onPasswordChange);

		capacityForm.add(currentCapacityLabel);
		isPrivateForm.add(currentIsPrivateLabel);

		capacityForm.add(buttonFour).add(buttonFive).add(buttonSix).add(buttonSeven);
		isPrivateForm.add(isNotPrivateButton).add(isPrivateButton);
		createGameForm.add(cancelButton).add(createButton);

		nameFieldForm.add(nameCheckLabel).add(nameField);

		passwordForm.add(passwordField);

		add(nameFieldForm);
		add(passwordForm);
		add(capacityForm);
		add(isPrivateForm);
		add(createGameForm);
	}

	public Game createGame(String name, String password) {
		// Falls kein Game Name angegeben ist, nach Username erstellen
		if (name == null) {
			if (page.getUser() != null) {
				name = page.getUser().getName() + "'s game";
			}
		}

		// kontrollieren, ob generierte Spielname schon existiert.
		boolean isValid = false;
		while (!isValid) {
			int counter = 0;
			if (counter == 0) {
				isValid = validateGameName(name);
			} else {
				isValid = validateGameName(name + " " + counter);
			}

			// Falls aktuelle name akzeptabel
			if (isValid) {
				// bei erste Iteration kein String hinzufuegen
				if (counter == 0) {
					break;
				} else {
					name = name + " " + counter;
				}
			}

			else {
				counter++;
			}
		}

		return TBIALApplication.getInstance().createGame(name, this.capacity, this.isPrivate, password, page.getUser());
	}

	/**
	 * kontrolliert, ob der Spielname bereits existiert
	 * 
	 * @param name
	 *            zu validierende Spielname
	 * @return true: Spielname existiert noch nicht.
	 */
	public boolean validateGameName(String gameName) {
		boolean result = true;
		Collection<String> games = TBIALApplication.getGameNames();
		if (games != null) {
			for (String g : games) {
				if (g != null) {
					if (g.equals(gameName)) {
						result = false;
					}
				}
			}
		}
		return result;
	}

	// gibt isPrivate als String zurueck
	public String showIsPrivate() {
		if (isPrivate) {
			return "private.";
		} else {
			return "public.";
		}
	}

	public void submitCapacityButton(short cap) {
		this.capacity = cap;
		currentCapacityLabel.setDefaultModelObject(capacity);
	}

	public void submitIsPrivateButton(boolean isPriv) {
		this.isPrivate = isPriv;
		if (!isPriv) {
			currentIsPrivateLabel.setDefaultModelObject("public.");
		} else {
			currentIsPrivateLabel.setDefaultModelObject("private.");
		}
	}

	public void setName(String n) {
		this.name = n;
	}

	public void setPassword(String p) {
		this.password = p;
	}
}
