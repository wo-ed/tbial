package de.lmu.ifi.sosy.tbial.pages;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import de.lmu.ifi.sosy.tbial.AuthenticationRequired;
import de.lmu.ifi.sosy.tbial.Login;
import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.TBIALSession;
import de.lmu.ifi.sosy.tbial.db.User;

@AuthenticationRequired
public class DeleteAccountPage extends MainPage {

	private static final long serialVersionUID = 1L;
	private PasswordTextField passwordField;
	private Label labelTitle;

	public DeleteAccountPage() {
		super();
		initialize();
	}

	private void initialize() {
		addComponents();
	}

	@Override
	public TBIALSession getSession() {
		return (TBIALSession) super.getSession();
	}
	
	public User getUser() {
		return getSession().getUser();
	}

	private void addComponents() {
		labelTitle = new Label("title", new Model<>("Delete your account"));
		add(labelTitle);
		
		Form<?> form = new Form<>("deleteForm");
		add(form);
		
		FeedbackPanel feedback = new FeedbackPanel("deleteFeedback");
		feedback.setOutputMarkupId(true);
		form.add(feedback);
		
		passwordField = new PasswordTextField("passwordField", new Model<>(""));
		passwordField.setRequired(true);
		form.add(passwordField);
		
		Button submit = new Button("deleteAccountBtn") {
			private static final long serialVersionUID = 1L;
			
			@Override
			public void onSubmit() {
				super.onSubmit();
				deleteUser();
			}
		};
		
		submit.setOutputMarkupId(true);
		form.add(submit);
	}
	
	public void deleteUser() {
		if(getSession().getUser().isAdmin()) {
			error("You cannot delete your admin account.");
			return;
		}
		
		if(TBIALApplication.deleteUser(getUser(), passwordField.getInput())) {
			if (getSession() instanceof AuthenticatedWebSession) {
				((AuthenticatedWebSession) getSession()).signOut();
			}
			setResponsePage(Login.class, new PageParameters().add("message", "Account deleted"));
			getSession().invalidate();
		} else {
			error("Wrong password");
		}
	}
}
