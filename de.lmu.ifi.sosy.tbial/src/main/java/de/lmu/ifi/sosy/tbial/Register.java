package de.lmu.ifi.sosy.tbial;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;

import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.pages.BasePage;
import de.lmu.ifi.sosy.tbial.util.EmailValidator;

/**
 * 
 * Displays a user registration page. Verifies that the entered user name
 * doesn't match an existing one. This is checked on submit and also during
 * editing of the user name field (using AJAX).
 * 
 * @author Andreas Schroeder, SWEP 2013 Team.
 * 
 */
public class Register extends BasePage {

	/**
	 * UID for serialization.
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager.getLogger(Register.class);

	private TextField<String> nameField;

	private TextField<String> emailField;

	private PasswordTextField passwordField;

	private PasswordTextField passwordConfirmationField;

	private FeedbackPanel messagePanel;

	private Button registerButton;

	private Label messageLabel;

	public Register() {
		messagePanel = new FeedbackPanel("feedback");
		add(messagePanel);

		Form<?> form = new Form<>("register");

		nameField = new TextField<String>("name", new Model<>(""));
		nameField.setRequired(true);
		emailField = new TextField<String>("email", new Model<>(""));
		emailField.setRequired(true);
		messageLabel = new Label("nameFeedback", new Model<>(" "));
		messageLabel.setOutputMarkupId(true);
		passwordField = new PasswordTextField("password", new Model<>(""));
		passwordConfirmationField = new PasswordTextField("passwordConfirm", new Model<>(""));
		registerButton = new Button("register") {

			/**
			 * UID for serialization.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onSubmit() {
				String name = nameField.getModelObject();
				String password = passwordField.getModelObject();
				String confirm = passwordConfirmationField.getModelObject();
				String email = emailField.getModelObject();
				performRegistration(name, password, confirm, email);
			};
		};
		form.add(nameField).add(messageLabel).add(passwordField).add(registerButton).add(passwordConfirmationField)
				.add(emailField);

		OnChangeAjaxBehavior onNameChange = new OnChangeAjaxBehavior() {

			/**
			 * UID for serialization.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				String name = nameField.getModelObject();
				if (getDatabase().nameTaken(name)) {
					messageLabel.setDefaultModelObject("Name already taken.");
				} else {
					messageLabel.setDefaultModelObject(" ");
				}
				target.add(messageLabel);
			}
		};

		nameField.add(onNameChange);

		add(form);
	}

	private void performRegistration(String name, String password, String confirm, String email) {
		if (!password.equals(confirm)) {
			error("Password and confirmation do not match. Please verify and try again.");
			LOGGER.debug("User registration for '" + name + "' failed: confirmation and password do not match");
			return;
		}
		if (!TBIALApplication.DEBUG_MODE)
			if (!EmailValidator.validateEmail(email)) {
				error("Email ist keine gültige Addresse");
				LOGGER.debug("User registration for '" + name + "' failed: email is not valid");
				return;
			}

		User user = TBIALApplication.registerUser(name, password, email);
		if (user != null) {
			if (TBIALApplication.DEBUG_MODE) {
				getSession().setSignedIn(user);
				TBIALApplication.addUser(getSession().getUser());
				setResponsePage(getApplication().getHomePage());
				info("Registration successful! You are now logged in.");
			} else {
				info("Registration successful! We sent you an activation mail, please verify your account");
				setResponsePage(Login.class);
			}

			LOGGER.info("New user '" + name + "' registration successful ");
		} else {
			error("A user with that name already exists. Please choose another name.");
			LOGGER.debug("New user '" + name + "' registration failed: user already exists");
		}
	}

}
