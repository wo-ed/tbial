package de.lmu.ifi.sosy.tbial.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import de.lmu.ifi.sosy.tbial.ConfigurationException;
import de.lmu.ifi.sosy.tbial.DatabaseException;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.util.VerificationToken;

/**
 * A database using JNDI data source to connect to a real database.
 * 
 * @author Andreas Schroeder, SWEP 2014 Team.
 * 
 */
public class SQLDatabase implements Database {

	public static final String JNDI_PATH_DB = "java:/comp/env/jdbc/tbial";

	private final DataSource dataSource;

	public SQLDatabase() {
		try {
			InitialContext ctx = new InitialContext();
			dataSource = (DataSource) ctx.lookup(JNDI_PATH_DB);

		} catch (NamingException e) {
			throw new ConfigurationException("Error while looking up data source in JNDI.", e);
		}
		if (dataSource == null) {
			throw new ConfigurationException("No data source registered in JNDI for " + JNDI_PATH_DB);
		}
	}

	@Override
	public User getUser(String name) {
		Objects.requireNonNull(name, "name is null");

		try (Connection connection = getConnection();
				PreparedStatement query = userByNameQuery(name, connection);
				ResultSet result = query.executeQuery()) {

			return getUserFromResult(result);
		} catch (SQLException e) {
			throw new DatabaseException("Error while querying for user in DB.", e);
		}
	}

	@Override
	public boolean nameTaken(String name) {
		Objects.requireNonNull(name, "name is null");

		try (Connection connection = getConnection();
				PreparedStatement query = userByNameQuery(name, connection);
				ResultSet result = query.executeQuery()) {

			return result.next();
		} catch (SQLException e) {
			throw new DatabaseException("Error while querying for user in DB.", e);
		}
	}

	private User getUserFromResult(ResultSet result) throws SQLException {
		if (result.next()) {
			int id = result.getInt("ID");
			boolean isAdmin = result.getBoolean("ADMIN");
			String name = result.getString("NAME");
			String salt = result.getString("SALT");
			String hash = result.getString("HASH");
			boolean active = result.getBoolean("ACTIVE");
			int gamesWon = result.getInt("GAMESWON");
			int gamesLost = result.getInt("GAMESLOST");
			int gamesParticipated = result.getInt("GAMESPARTICIPATED");
			int bugsPlayed = result.getInt("BUGSPLAYED");
			int systemIntegrationParticipated = result.getInt("SYSTEMINTEGRATIONPARTICIPATED");
			return new User(id, name, isAdmin, salt, hash, gamesWon, gamesLost, gamesParticipated, bugsPlayed,
					systemIntegrationParticipated, active);
		} else {
			return null;
		}
	}

	private Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}

	private Connection getConnection(boolean autocommit) throws SQLException {
		Connection connection = dataSource.getConnection();
		connection.setAutoCommit(autocommit);
		return connection;
	}

	private PreparedStatement userByNameQuery(String name, Connection connection) throws SQLException {
		PreparedStatement statement = connection.prepareStatement("SELECT * FROM USERS WHERE NAME=?");
		statement.setString(1, name);
		return statement;
	}

	private ResultSet executeUpdate(PreparedStatement insertUser) throws SQLException {
		try {
			insertUser.executeUpdate();
			return insertUser.getGeneratedKeys();
		} catch (SQLIntegrityConstraintViolationException ex) {
			return null;
		}
	}

	private PreparedStatement insertUserStatement(String name, String salt, String hash, boolean active,
			Connection connection) throws SQLException {
		PreparedStatement insertUser;
		insertUser = connection.prepareStatement("INSERT INTO USERS (NAME, SALT, HASH, ACTIVE) VALUES (?,?,?,?)",
				Statement.RETURN_GENERATED_KEYS);
		insertUser.setString(1, name);
		insertUser.setString(2, salt);
		insertUser.setString(3, hash);
		insertUser.setBoolean(4, active);
		return insertUser;
	}

	private PreparedStatement insertTokenStatement(String token, int userID, Connection connection)
			throws SQLException {
		PreparedStatement insertToken;
		insertToken = connection.prepareStatement("INSERT INTO TOKENS (TOKEN, UID) VALUES (?,?)",
				Statement.RETURN_GENERATED_KEYS);
		insertToken.setString(1, token);
		insertToken.setInt(2, userID);
		return insertToken;
	}

	private PreparedStatement activateUserStatement(String name, Connection connection) throws SQLException {
		PreparedStatement activateUser;
		activateUser = connection.prepareStatement("UPDATE USERS SET ACTIVE = ? WHERE NAME = ?",
				Statement.RETURN_GENERATED_KEYS);
		activateUser.setBoolean(1, true);
		activateUser.setString(2, name);
		return activateUser;
	}

	@Override
	public User register(String name, String password) {
		Objects.requireNonNull(name, "name is null");
		Objects.requireNonNull(password, "password is null");

		User user = new User(name, password);

		try (Connection connection = getConnection(false);
				PreparedStatement insert = insertUserStatement(name, user.getSalt(), user.getHash(), false, connection);
				ResultSet result = executeUpdate(insert)) {

			if (result != null && result.next()) {
				int id = result.getInt(1);
				user.setId(id);
				connection.commit();
				return user;
			} else {
				connection.rollback();
				return null;
			}

		} catch (SQLException ex) {
			throw new DatabaseException("Error while registering user " + name, ex);
		}
	}

	@Override
	public void storeToken(int id, String token) {
		Objects.requireNonNull(id, "id is null");
		Objects.requireNonNull(token, "token is null");

		try (Connection connection = getConnection(false);
				PreparedStatement insertToken = insertTokenStatement(token, id, connection);
				ResultSet result = executeUpdate(insertToken)) {
			if (result != null) {
				connection.commit();
			} else {
				connection.rollback();
			}

		} catch (SQLException ex) {
			throw new DatabaseException("Error while string token " + token, ex);
		}

	}

	@Override
	public boolean activateUser(User user) {
		Objects.requireNonNull(user, "user is null");

		user.setActive(true);

		try (Connection connection = getConnection(false);
				PreparedStatement activate = activateUserStatement(user.getName(), connection);
				ResultSet result = executeUpdate(activate)) {
			PreparedStatement deleteToken = deleteTokenStatement(user.getId(), connection);
			executeUpdate(deleteToken);

			if (result != null) {
				connection.commit();
				return true;
			} else {
				connection.rollback();
				return false;
			}

		} catch (SQLException ex) {
			throw new DatabaseException("Error while activating user " + user.getName(), ex);
		}
	}

	private PreparedStatement deleteTokenStatement(int userID, Connection connection) throws SQLException {
		PreparedStatement statement;
		statement = connection.prepareStatement("DELETE FROM TOKENS WHERE UID = ?");
		statement.setInt(1, userID);
		return statement;
	}

	@Override
	public List<VerificationToken> getVerificationTokens() {
		try (Connection connection = getConnection(false);
				PreparedStatement query = tokenQuery(connection);
				ResultSet result = query.executeQuery()) {
			List<VerificationToken> tokens = getTokensFromResult(result, connection);
			connection.commit();
			return tokens;
		} catch (SQLException e) {

			throw new DatabaseException("Error while querying for user in DB.", e);
		}
	}

	private List<VerificationToken> getTokensFromResult(ResultSet result, Connection connection) throws SQLException {
		List<VerificationToken> resultList = new ArrayList<>();
		while (result.next()) {
			String token = result.getString(2);
			int uid = result.getInt(3);
			try (PreparedStatement query = userByIdQuery(uid, connection); ResultSet userRes = query.executeQuery()) {
				User user = this.getUserFromResult(userRes);
				resultList.add(new VerificationToken(token, user));
			} catch (SQLException e) {
				throw new DatabaseException("Error while querying for user in DB.", e);
			}

		}
		return resultList;
	}

	private PreparedStatement tokenQuery(Connection connection) throws SQLException {
		PreparedStatement statement = connection.prepareStatement("SELECT * FROM TOKENS");
		return statement;
	}

	private PreparedStatement userByIdQuery(int id, Connection connection) throws SQLException {
		PreparedStatement statement = connection.prepareStatement("SELECT * FROM USERS WHERE ID=?");
		statement.setInt(1, id);
		return statement;
	}

	@Override
	public void updateUser(User user) {
		Objects.requireNonNull(user, "user is null");

		try (Connection connection = getConnection();
				PreparedStatement query = updateUserStatement(user.getName(), user.getGamesWon(), user.getGamesLost(),
						user.getGamesParticipated(), user.getBugsPlayed(), user.getSystemIntegrationParticipated(),
						connection);) {
			int recordsUpdated = query.executeUpdate();
			if (recordsUpdated != 1)
				throw new DatabaseException("Error while updating user in DB.", null);
		} catch (SQLException e) {
			throw new DatabaseException("Error while updating user in DB.", e);
		}
	}

	private PreparedStatement updateUserStatement(String userName, int gamesWon, int gamesLost, int gamesParticipated,
			int bugsPlayed, int systemIntegrationParticipated, Connection connection) throws SQLException {
		PreparedStatement updateUser;
		updateUser = connection.prepareStatement(
				"UPDATE USERS SET GAMESWON = ?, GAMESLOST = ?, GAMESPARTICIPATED = ?, BUGSPLAYED = ?, SYSTEMINTEGRATIONPARTICIPATED = ? WHERE NAME = ?");
		updateUser.setInt(1, gamesWon);
		updateUser.setInt(2, gamesLost);
		updateUser.setInt(3, gamesParticipated);
		updateUser.setInt(4, bugsPlayed);
		updateUser.setInt(5, systemIntegrationParticipated);
		updateUser.setString(6, userName);
		return updateUser;
	}

	@Override
	public void updateAdminRights(User user, boolean grant) throws SQLException {
		Objects.requireNonNull(user, "user is null");

		Connection connection = getConnection();
		PreparedStatement query = connection.prepareStatement("UPDATE USERS SET ADMIN = ? WHERE NAME = ?");
		query.setBoolean(1, grant);
		query.setString(2, user.getName());
		int recordsUpdated = query.executeUpdate();
		if (recordsUpdated != 1)
			throw new DatabaseException("Error while granting admin rights to user in DB.", null);
		user.setAdmin(grant);
	} 
	
	

	private PreparedStatement insertGameStatement(String name, Connection connection) throws SQLException {
		PreparedStatement insertGame;
		insertGame = connection.prepareStatement("INSERT INTO GAMES (NAME) VALUES (?)",
				Statement.RETURN_GENERATED_KEYS);
		insertGame.setString(1, name);

		return insertGame;
	}

	@Override
	public boolean storeGameStats(Game game) {
		Objects.requireNonNull(game, "game is null");

		// game already in db
		if (game.getId() != -1) {

			return false;
		}

		try (Connection connection = getConnection(false);
				PreparedStatement insertGame = insertGameStatement(game.getName(), connection);
				ResultSet result = executeUpdate(insertGame)) {
			if (result != null && result.next()) {
				int id = result.getInt(1);
				game.setId(id);
				connection.commit();
				// return true;
			} else {

				connection.rollback();
				return false;
			}

		} catch (SQLException ex) {
			throw new DatabaseException("Error while storing game " + game.getName(), ex);
		}

		for (User user : game.getUsers()) {
			try (Connection connection2 = getConnection(false);
					PreparedStatement insertHealthPoints = insertHealthPointsStatement(game.getId(), user.getId(),
							connection2);
					ResultSet result2 = executeUpdate(insertHealthPoints)) {
				if (result2 != null && result2.next()) {

					connection2.commit();

				} else {
					connection2.rollback();

					return false;
				}

			}

			catch (SQLException e) {
				throw new DatabaseException("Error while updating healthpoints in DB.", e);
			}
		}

		return true;

	}

	@Override
	public boolean updateGameStats(Game game) {
		Objects.requireNonNull(game, "game is null");

		try (Connection connection = getConnection(false);
				PreparedStatement query = updateGameStatement(game.getId(), game.getBugsPlayed(),
						game.getExcusesPlayed(), game.getSolutionsPlayed(), game.getSpecialCardsPlayed(),
						game.getAbilityCardsPlayed(), game.getStumblingCardsPlayed(), game.getDurationInTime(),
						game.getTurnsCounter(), connection);
				ResultSet result = executeUpdate(query)) {

			if (result != null && result.next()) {
				connection.commit();
			} else {
				connection.rollback();
				return false;
			}
		} catch (SQLException e) {
			throw new DatabaseException("Error while updating game in DB.", e);
		}

		for (User user : game.getUsers()) {
			Player player = game.getPlayer(user);
			String healthPoints = game.getPlayerHealthPoints().get(player).toString();
			try (Connection connection2 = getConnection(false);
					PreparedStatement updateHP = updateHealthPointsStatement(game.getId(), user.getId(), healthPoints,
							connection2);
					ResultSet result2 = executeUpdate(updateHP)) {
				if (result2 != null && result2.next()) {

					connection2.commit();

				} else {
					connection2.rollback();
					return false;
				}

			}

			catch (SQLException e) {
				throw new DatabaseException("Error while updating healthpoints in DB.", e);
			}
		}

		return true;
	}

	private PreparedStatement updateGameStatement(int gameID, int bugsPlayed, int excusesPlayed, int solutionsPlayed,
			int specialCardsPlayed, int abilityCardsPlayed, int stumblingCardsPlayed, String durationInTime,
			int durationInTurns, Connection connection) throws SQLException {
		PreparedStatement updateGame;
		updateGame = connection.prepareStatement(
				"UPDATE GAMES SET BUGSPLAYED = ?, EXCUSESPLAYED = ?, SOLUTIONSPLAYED = ?, SPECIALCARDSPLAYED = ?, ABILITYCARDSPLAYED = ?, STUMBLINGCARDSPLAYED = ?, DURATIONINTIME = ?, DURATIONINTURNS = ? WHERE GID = ?",
				Statement.RETURN_GENERATED_KEYS);
		updateGame.setInt(1, bugsPlayed);
		updateGame.setInt(2, excusesPlayed);
		updateGame.setInt(3, solutionsPlayed);
		updateGame.setInt(4, specialCardsPlayed);
		updateGame.setInt(5, abilityCardsPlayed);
		updateGame.setInt(6, stumblingCardsPlayed);

		updateGame.setString(7, durationInTime);
		updateGame.setInt(8, durationInTurns);

		updateGame.setInt(9, gameID);
		return updateGame;
	}

	private PreparedStatement insertHealthPointsStatement(int gameID, int userID, Connection connection)
			throws SQLException {
		PreparedStatement insertHP;
		insertHP = connection.prepareStatement("INSERT INTO HEALTHPOINTS (GID, UID) VALUES (?,?)",
				Statement.RETURN_GENERATED_KEYS);
		insertHP.setInt(1, gameID);
		insertHP.setInt(2, userID);
		return insertHP;
	}

	private PreparedStatement updateHealthPointsStatement(int gameId, int userId, String hp, Connection connection)
			throws SQLException {
		PreparedStatement updateHP;
		updateHP = connection.prepareStatement("UPDATE HEALTHPOINTS SET HEALTHPOINTS = ? WHERE GID = ? AND UID = ?",
				Statement.RETURN_GENERATED_KEYS);
		updateHP.setString(1, hp);
		updateHP.setInt(2, gameId);
		updateHP.setInt(3, userId);

		return updateHP;
	}

	private PreparedStatement getGameStatStatement(Integer gameId, Connection connection) throws SQLException {
		PreparedStatement statement = connection.prepareStatement("SELECT * FROM GAMES WHERE GID=?");
		statement.setInt(1, gameId);
		return statement;
	}

	private GameStat getGameStatFromResult(ResultSet result) throws SQLException {
		if (result.next()) {
			int id = result.getInt("GID");
			String name = result.getString("NAME");
			int bugsPlayed = result.getInt("BUGSPLAYED");
			int excusesPlayed = result.getInt("EXCUSESPLAYED");
			int solutionsPlayed = result.getInt("SOLUTIONSPLAYED");
			int specialCardsPlayed = result.getInt("SPECIALCARDSPLAYED");
			int abilityCardsPlayed = result.getInt("ABILITYCARDSPLAYED");
			int stumblingCardsPlayed = result.getInt("STUMBLINGCARDSPLAYED");
			String durationInTime = result.getString("DURATIONINTIME");
			int durationInTurns = result.getInt("DURATIONINTURNS");

			return new GameStat(id, name, bugsPlayed, excusesPlayed, solutionsPlayed, specialCardsPlayed,
					abilityCardsPlayed, stumblingCardsPlayed, durationInTime, durationInTurns);
		} else {
			return null;
		}
	}

	@Override
	public GameStat getGameStat(Integer gameId) {
		Objects.requireNonNull(gameId, "game id is null");

		try (Connection connection = getConnection();
				PreparedStatement query = getGameStatStatement(gameId, connection);
				ResultSet result = query.executeQuery()) {

			return getGameStatFromResult(result);
		} catch (SQLException e) {
			throw new DatabaseException("Error while querying for game in DB.", e);
		}
	}

	private PreparedStatement getHealthPointsStatement(Integer gameId, Integer userId, Connection connection)
			throws SQLException {
		PreparedStatement statement = connection.prepareStatement("SELECT * FROM HEALTHPOINTS WHERE GID=? AND UID=?");
		statement.setInt(1, gameId);
		statement.setInt(2, userId);
		return statement;
	}

	private HealthPoints getHealthPointsFromResult(ResultSet result) throws SQLException {
		if (result.next()) {
			int gid = result.getInt("GID");
			int uid = result.getInt("UID");
			String healthPoints = result.getString("HEALTHPOINTS");

			return new HealthPoints(gid, uid, healthPoints);
		} else {
			return null;
		}
	}

	@Override
	public HealthPoints getHealthPoints(Integer gameId, Integer userId) {
		Objects.requireNonNull(gameId, "game id is null");
		Objects.requireNonNull(gameId, "user id is null");

		try (Connection connection = getConnection();
				PreparedStatement query = getHealthPointsStatement(gameId, userId, connection);
				ResultSet result = query.executeQuery()) {

			return getHealthPointsFromResult(result);
		} catch (SQLException e) {
			throw new DatabaseException("Error while querying for healthpoints in DB.", e);
		}
	}

	@Override
	public void deleteUser(int userId) {
		try (Connection connection = getConnection();
			PreparedStatement deleteUser = deleteUserStatement(userId, connection); 
				PreparedStatement deleteToken = deleteTokenStatement(userId, connection);) {
			
			deleteToken.execute();
			int recordsDeleted = deleteUser.executeUpdate();
			if (recordsDeleted!= 1) {
				throw new DatabaseException("Error while deleting user in DB.", null);
			}
		} catch (SQLException e) {
			throw new DatabaseException("Error while deleting user in DB.", e);
		}
	}

	private PreparedStatement deleteUserStatement(int id, Connection connection) throws SQLException {
		PreparedStatement deleteUser;
		deleteUser = connection.prepareStatement("DELETE FROM USERS WHERE ID = ?");
		deleteUser.setInt(1, id);
		return deleteUser;
	}

	@Override
	public void deleteUser(User user) throws SQLException {
		Objects.requireNonNull(user, "user is null");

		Connection connection = getConnection();
		PreparedStatement query = connection.prepareStatement("DELETE FROM USERS WHERE NAME = ?");
		query.setString(1, user.getName());
		int recordsUpdated = query.executeUpdate();
		if (recordsUpdated != 1)
			throw new DatabaseException("Error while deleting user from DB.", null);		
	}
}