package de.lmu.ifi.sosy.tbial.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.lmu.ifi.sosy.tbial.components.CourtPanel;

public class EmailService implements IEmailService {

	private static final String SENDER_KEY = "mail.from";
	private static final String PW_KEY = "password";
	private static final String AUTH_KEY = "mail.smtp.auth";

	private static final Logger LOGGER = LogManager.getLogger(CourtPanel.class);

	public EmailService() {
	}

	@Override
	public boolean sendEmail(String email, String username, String link) {

		EmailModel model;
		try {
			model = this.fetchConfig();

		} catch (Exception i) {
			i.printStackTrace();
			LOGGER.warn("Reading from config failed, use defaultconfig");
			model = defaultConfig();
		}
		try {
			MimeMessage message = new MimeMessage(model.session);
			message.setFrom(new InternetAddress(model.from));

			message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));

			message.setSubject("Activate your tbial account");
			message.setText(
					"Hey " + username + ",\n \n" + "please klick on the link below to verify your tbial account!\n"
							+ link + "\n\n" + "Regards Team Blue");

			Transport.send(message);
			LOGGER.info("Email send: " + email);

			return true;
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
		return false;
	}

	private EmailModel fetchConfig() throws Exception {
		// Path currentRelativePath = Paths.get("");
		// String s = currentRelativePath.toAbsolutePath().toString();
		// Path path = Paths.get("/emailConfig.txt");
		InputStream input = EmailService.class.getResourceAsStream("emailConfig.txt");
		List<String> doc = new BufferedReader(new InputStreamReader(input, StandardCharsets.UTF_8)).lines()
				.collect(Collectors.toList());

		final Properties props = System.getProperties();
		EmailModel res = new EmailModel();

		doc.forEach(l -> {
			if (l.contains("=") && !l.trim().startsWith("#")) {
				String[] splits = l.split("=");
				if (!splits[0].trim().equalsIgnoreCase(PW_KEY)) {
					props.put(splits[0].trim(), splits[1].trim());

					if (splits[0].trim().equals(SENDER_KEY)) {
						res.from = splits[1].trim();
					} else if (splits[0].trim().equals(AUTH_KEY)) {
						res.auth = splits[1].trim().equals("true");
					}

				} else {
					res.pw = splits[1].trim();
				}
			}
		});

		Session session;
		if (res.auth) {
			session = Session.getInstance(props, new javax.mail.Authenticator() {

				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(res.from, res.pw);
				}
			});
		} else {
			session = Session.getDefaultInstance(props);
		}
		res.session = session;
		return res;
	}

	private EmailModel defaultConfig() {
		EmailModel res = new EmailModel();
		res.pw = "sosy.blue";
		res.from = "tbial.blue@gmail.com";
		Properties properties = System.getProperties();
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.port", "587");
		Session session = Session.getInstance(properties, new javax.mail.Authenticator() {

			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(res.from, res.pw);
			}
		});
		res.auth = true;
		res.session = session;
		return res;
	}

	class EmailModel {

		private Session session;
		private boolean auth;
		private String pw;
		private String from;

	}
}
