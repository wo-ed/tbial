package de.lmu.ifi.sosy.tbial.model;

import java.awt.Color;
import java.io.Serializable;
import java.util.Random;

import de.lmu.ifi.sosy.tbial.model.actioncards.BoringMeeting;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.actioncards.Heisenbug;
import de.lmu.ifi.sosy.tbial.model.actioncards.LANParty;
import de.lmu.ifi.sosy.tbial.model.actioncards.LameExcuse;
import de.lmu.ifi.sosy.tbial.model.actioncards.PersonalCoffeeMachine;
import de.lmu.ifi.sosy.tbial.model.actioncards.RedBullDispenser;
import de.lmu.ifi.sosy.tbial.model.actioncards.Solution;
import de.lmu.ifi.sosy.tbial.model.actioncards.StandupMeeting;
import de.lmu.ifi.sosy.tbial.util.Utils;

public abstract class Card implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Random RANDOM = new Random();

	private static int idCounter = 0;

	private final int id;

	public Card() {
		initColor();
		id = Card.idCounter++;
	}

	public Card(String name) {
		this.name = name;
		id = Card.idCounter++;
	}

	protected String name = "";
	private Color bgColor;
	private Color fontColor;

	private boolean visible;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public Color getBgColor() {
		return bgColor;
	}

	public Color getFontColor() {
		return fontColor;
	}

	public int getId() {
		return id;
	}
	
	public void initColor(){
		if(this instanceof BugReport){
			//black
			bgColor = new Color(0, 0, 0);
		} else if(this instanceof Solution){
			//green
			bgColor = new Color(50,205,50);
		} else if(this instanceof LameExcuse){
			//grey
			bgColor = new Color(211,211,211);
		} else if(this instanceof StumblingBlockCard){
			//pink
			bgColor = new Color(255,182,193);
		} else if(this instanceof StealingCard){
			//gold
			bgColor = new Color(212,175,55);
		} else if(this instanceof AbilityCard){
			//blue
			bgColor = new Color(30, 144, 255);
		} else if(this instanceof PersonalCoffeeMachine || this instanceof RedBullDispenser){
			//cyan
			bgColor = new Color(0,255,255);
		} else if(this instanceof Heisenbug || this instanceof BoringMeeting){
			//red
			bgColor = new Color(255,0,0);
		} else if(this instanceof LANParty){
			//orange
			bgColor = new Color(255,165,0);
		} else if(this instanceof StandupMeeting){
			//blueviolet
			bgColor = new Color(138,43,226);
		} else {
			bgColor = new Color(RANDOM.nextInt(255), RANDOM.nextInt(255), RANDOM.nextInt(255));
		}

		fontColor = Utils.getContrastColor(bgColor);
	}
}
