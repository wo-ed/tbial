package de.lmu.ifi.sosy.tbial.model.abilitycards;

import de.lmu.ifi.sosy.tbial.model.AbilityCard;
import de.lmu.ifi.sosy.tbial.model.visitor.CardVisitor;

public final class WearsSunglassesAtWork extends AbilityCard {

	private static final long serialVersionUID = 1L;

	public WearsSunglassesAtWork() {
		setName("Wears Sunglasses");
	}

	@Override
	public void play(CardVisitor v) {
		v.visit(this);
	}

}
