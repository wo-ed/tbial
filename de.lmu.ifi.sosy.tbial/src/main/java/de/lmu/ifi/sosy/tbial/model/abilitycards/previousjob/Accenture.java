package de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob;

import de.lmu.ifi.sosy.tbial.model.abilitycards.PreviousJob;
import de.lmu.ifi.sosy.tbial.model.visitor.CardVisitor;

public class Accenture extends PreviousJob {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Accenture() {
		super("Accenture\n (prev. Job)");
		this.prestigeModifier = 0;
	}

	@Override
	public void play(CardVisitor v) {
		v.visit(this);
	}

}
