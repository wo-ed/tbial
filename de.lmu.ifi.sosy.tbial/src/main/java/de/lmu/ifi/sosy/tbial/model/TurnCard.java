package de.lmu.ifi.sosy.tbial.model;

import de.lmu.ifi.sosy.tbial.model.visitor.CardVisitor;

public abstract class TurnCard extends Card {

	private static final long serialVersionUID = 1L;

	/**
	 * The player who plays this card.
	 */
	private Player player;

	/**
	 * The affected player.
	 */
	private Player targetPlayer;

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Player getTargetPlayer() {
		return targetPlayer;
	}

	public void setTargetPlayer(Player to) {
		this.targetPlayer = to;
	}

	public abstract void play(CardVisitor v);

	public void resetOwnerAndTarget() {
		targetPlayer = null;
		player = null;

	}

}
