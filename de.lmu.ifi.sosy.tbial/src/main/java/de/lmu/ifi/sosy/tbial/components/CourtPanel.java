package de.lmu.ifi.sosy.tbial.components;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.panel.Panel;

import de.lmu.ifi.sosy.tbial.pages.GamePage;

/**
 * Entspricht einem Panel(/Cell) am Spiel tisch
 * 
 * @author Stephan
 *
 */
public class CourtPanel extends Panel {

	// Rotation des Panels (akt: 0deg, 90deg, 180deg or 270deg)
	protected double rotation;

	protected int row, column;

	protected static final Logger LOGGER = LogManager.getLogger(CourtPanel.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CourtPanel(String id, double rotation, int row, int column) {
		super(id);
		this.rotation = rotation;
		this.row = row;
		this.column = column;
	}

	public CourtPanel(String id, double rotation, boolean isSmall, int row, int column) {
		this(id, rotation, row, column);
		if (isSmall)
			add(new AttributeModifier("class", "innerRepeaterSmall"));
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

	/**
	 * FactoryInterface um die einzelnen Zellen zu erzeugen
	 * 
	 * @author Stephan
	 *
	 */
	public interface ICourtPanelFactory extends Serializable {

		/**
		 * Factory Methode zum erzeugen von Panels
		 * 
		 * @param id
		 * @param rotation
		 * @return
		 */
		public Panel createPanel(String id, double rotation, GamePage page, int row, int column);

		/**
		 * FactoryClass für das PlayerPanel
		 * 
		 * @author Stephan
		 *
		 */
		public static class PlayerPanelFactory implements ICourtPanelFactory {

			private int playerId;

			public PlayerPanelFactory(int playerId) {
				this.playerId = playerId;
			}

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Panel createPanel(String id, double rotation, GamePage page, int row, int column) {
				return new PlayerPanel(id, playerId, page, rotation, row, column);
			}
		}

		/**
		 * FactoryClass für leere Panel
		 * 
		 * @author Stephan
		 *
		 */
		public static class EmptyPanelFactory implements ICourtPanelFactory {

			private boolean isSmall;

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public EmptyPanelFactory(boolean isSmall) {
				this.isSmall = isSmall;
			}

			@Override
			public Panel createPanel(String id, double rotation, GamePage page, int row, int column) {

				Panel p = new CourtPanel(id, rotation, isSmall, row, column);
				p.add(new Behavior() {

					private static final long serialVersionUID = 1L;

					@Override
					public void onComponentTag(Component component, ComponentTag tag) {
						tag.put("style", "height:" + 0 + "%;");
					}
				});

				return p;
			}

		}

		/**
		 * FactoryClass für das HeapStackPanel
		 * 
		 * @author Stephan
		 *
		 */
		public static class HeapStackPanelFactory implements ICourtPanelFactory {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public HeapStackPanelFactory() {
			}

			@Override
			public Panel createPanel(String id, double rotation, GamePage page, int row, int column) {
				Panel p = new HeapStackPanel(id, page, rotation, row, column);
				p.add(new Behavior() {

					private static final long serialVersionUID = 1L;

					@Override
					public void onComponentTag(Component component, ComponentTag tag) {
						int height = 50;
						tag.put("style", "height:" + height + "%; top: -50%");
					}
				});

				return p;
			}
		}

		public static class EventPanelFactory implements ICourtPanelFactory {

			private static final long serialVersionUID = 1L;

			@Override
			public Panel createPanel(String id, double rotation, GamePage page, int row, int column) {
				Panel p = new EventPanel(id, page, rotation, row, column);
				p.add(new Behavior() {

					private static final long serialVersionUID = 1L;

					@Override
					public void onComponentTag(Component component, ComponentTag tag) {
						int height = 50;
						tag.put("style", "height:" + height + "%;  top: 50%");
					}
				});

				return p;
			}
		}
	}
}