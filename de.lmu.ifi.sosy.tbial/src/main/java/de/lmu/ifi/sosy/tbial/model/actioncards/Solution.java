package de.lmu.ifi.sosy.tbial.model.actioncards;

import de.lmu.ifi.sosy.tbial.model.ActionCard;
import de.lmu.ifi.sosy.tbial.model.visitor.CardVisitor;

public final class Solution extends ActionCard {
	
	private static final long serialVersionUID = 1L;

	public Solution(String name) {
		setName(name);
	}

	public void play(CardVisitor v) {
		v.visit(this);
	}
}
