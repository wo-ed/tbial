package de.lmu.ifi.sosy.tbial.components;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.TBIALSession;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.pages.GamePage;
import de.lmu.ifi.sosy.tbial.util.Utils;
import wicketdnd.DragSource;
import wicketdnd.Operation;
import wicketdnd.Reject;
import wicketdnd.Transfer;

public class EventPanel extends CourtPanel {

	private static final long serialVersionUID = 1L;

	WebMarkupContainer container;

	private GamePage page;

	public EventPanel(String id, GamePage page, double rotation, int row, int column) {
		super(id, rotation, row, column);
		this.page = page;

		drawCards();

	}

	public void drawStandupMeetingCards() {

		List<Integer> standupMeetingCards = page.getGame().getStandupMeetingCards().stream().map(c -> c.getId())
				.collect(Collectors.toList());
		ListView<Integer> smc = new ListView<Integer>("standupMeetingCards", standupMeetingCards) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Integer> item) {
				// TODO Auto-generated method stub
				Label label = new Label("standupMeetingCard",
						new PropertyModel<>(getCard(item.getModelObject()), "name"));

				label.setEscapeModelStrings(false);
				label.setOutputMarkupId(true);
				item.add(label);
				item.setOutputMarkupId(true);
				item.setMarkupId("" + item.getModelObject());

				item.add(new AttributeModifier("style", new Model<String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {

						int translationX = item.getIndex() * 100 + (item.getIndex() + 1) * 19;

						int translationY;

						if (item.getIndex() > 3) {

							translationX = (item.getIndex() - 4) * 100 + (item.getIndex() - 4 + 1) * 19;

							translationY = 120;
						} else {
							translationY = 10;
						}
						String hexBgColor = "ffa500";
						String fontColor = "ffa500";
						hexBgColor = Utils.colorToHex(getCard(item.getModelObject()).getBgColor());
						fontColor = Utils.colorToHex(getCard(item.getModelObject()).getFontColor());

						return "width: 20%; height: 100%; transform: translateX(" + translationX + "%) translateY("
								+ translationY + "%); color: " + fontColor + "; background: " + hexBgColor + ";";
					}
				}));
			}

		};
		container.add(smc);

		container.add(new Behavior() {
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentTag(Component component, ComponentTag tag) {
				tag.put("style", "height: 40%; right: 40%; position: relative");
			}
		});

		if (isDragEnabled()) {
			container.add(new DragSource(Operation.MOVE) {
				private static final long serialVersionUID = 1L;

				@Override
				public void onAfterDrop(AjaxRequestTarget target, Transfer transfer) {
					if (transfer.getOperation() == Operation.MOVE) {
						// target.add(container);
					}

				}

				@Override
				public void onBeforeDrop(Component drag, Transfer transfer) throws Reject {
					transfer.setData(drag.getDefaultModelObject());
				}
			}.drag("div.card"));
		}

		add(container);
	}

	public boolean isDragEnabled() {
		return getSessionPlayer().getState() == PlayerState.STANDUP_MEETING;
	}

	public void drawCards() {
		container = new WebMarkupContainer("cardsContainer");
		container.setOutputMarkupId(true);

		drawStandupMeetingCards();

		container.add(new Behavior() {
			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentTag(Component component, ComponentTag tag) {
				tag.put("style", "height: 40%; right: 40%; position: relative");
			}
		});

		add(container);
	}

	private Player getSessionPlayer() {
		return getGame().getPlayer(((TBIALSession) getSession()).getUser());
	}

	public Game getGame() {
		return TBIALApplication.getInstance().findGame(page.getUser());
	}

	public TurnCard getCard(int id) {
		return getGame().getCard(id);
	}

	public boolean shouldBeVisible() {
		this.container.setVisible(!getGame().getStandupMeetingCards().isEmpty());
		return true;
		/**
		 * if (!page.getGame().getEventCard().getName().equals("")) { return
		 * true; } if (!getGame().getStandupMeetingCards().isEmpty()) { return
		 * true; } return false;
		 */
	}

}
