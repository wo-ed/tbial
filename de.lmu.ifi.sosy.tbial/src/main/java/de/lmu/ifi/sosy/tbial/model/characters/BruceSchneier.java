package de.lmu.ifi.sosy.tbial.model.characters;

public class BruceSchneier extends CharacterCard {

	private static final long serialVersionUID = 1L;

	public BruceSchneier() {
		super("Bruce Schneier");
	}

	@Override
	public boolean canPlayMultipleBugs() {
		return true;
	}
}
