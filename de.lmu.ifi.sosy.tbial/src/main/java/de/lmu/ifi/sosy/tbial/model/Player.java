package de.lmu.ifi.sosy.tbial.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import de.lmu.ifi.sosy.tbial.model.abilitycards.BugDelegation;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.actioncards.LameExcuse;
import de.lmu.ifi.sosy.tbial.model.actioncards.Solution;
import de.lmu.ifi.sosy.tbial.model.characters.CharacterCard;
import de.lmu.ifi.sosy.tbial.model.characters.SteveBallmer;

public class Player implements Serializable, Comparable<Player> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private RoleCard role;
	private List<TurnCard> handcards = new ArrayList<>();
	private CharacterCard character;
	private List<TurnCard> penaltyCards = new ArrayList<>();
	private List<TurnCard> buffCards = new ArrayList<>();
	private PlayerState state = PlayerState.WAIT;

	private String viewUpdate = "";

	public boolean canDefelectBugFromCharacter(CharacterCard character) {
		int numberOfLameExcuses = 0;
		for (TurnCard c : this.getHandcards()) {
			// checks if using solution kills
			if (c instanceof Solution && this.getHp() > 1) {
				return true;
			} else if (c instanceof LameExcuse) {
				numberOfLameExcuses++;
				if (numberOfLameExcuses >= character.numberOfLameExcusesNeededToDeflectBug())
					return true;
			} else if (c instanceof BugReport) {
				if (SteveBallmer.class.isInstance(this.getCharacter())) {
					numberOfLameExcuses++;
					if (numberOfLameExcuses >= character.numberOfLameExcusesNeededToDeflectBug())
						return true;
				}
			}
		}
		return false;
	}

	public TurnCard findBugInPenaltyCards() {
		TurnCard bug = null;
		for (TurnCard tc : getPenaltyCards()) {
			if (tc instanceof BugReport) {
				bug = tc;
				break;
			} else if (tc instanceof LameExcuse) {
				bug = tc;
				break;
			}
		}
		return bug;
	}

	public void notifyView(String obj) {
		this.viewUpdate = obj;
	}

	public Optional<String> getViewUpdate() {
		if (this.viewUpdate.isEmpty())
			return Optional.empty();
		else {
			String copy = this.viewUpdate;
			this.viewUpdate = "";
			return Optional.of(copy);
		}

	}

	public Player(int id) {
		this.id = id;
	}

	public int getHp() {
		if (character != null)
			return character.getHp();
		return 0;
	}

	public int getMaxHp() {
		return character.getMaxHp();
	}

	public boolean isAlive() {
		return character.getHp() > 0;
	}

	public RoleCard getRole() {
		return role;
	}

	public void setRole(RoleCard role) {
		this.role = role;
	}

	public List<TurnCard> getHandcards() {
		return handcards;
	}

	@Override
	public int compareTo(Player p) {
		return Integer.compare(getId(), p.getId());
	}

	public int getId() {
		return id;
	}

	public void setId(short id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Helper to sort Playerslist depending of users position in List (after
	 * sort() user will be first)
	 * 
	 * @author Stephan
	 *
	 */
	public static class ComperatorById implements Comparator<Player> {

		private int max;
		private int start;

		/**
		 * 
		 * @param start
		 *            - Index of User
		 * @param max
		 *            - Count Players
		 */
		public ComperatorById(int start, int max) {
			this.max = max;
			this.start = start;
		}

		@Override
		public int compare(Player o1, Player o2) {
			int idP1 = o1.getId() - start;
			int idP2 = o2.getId() - start;
			idP1 = (idP1 >= 0) ? idP1 : (idP1 + max);
			idP2 = (idP2 >= 0) ? idP2 : (idP2 + max);
			return Integer.compare(idP1, idP2);
		}
	}

	public List<TurnCard> getPenaltyCards() {
		return penaltyCards;
	}

	public void addPenaltyCard(TurnCard t) {
		penaltyCards.add(t);
	}

	public boolean removePenaltyCard(TurnCard t) {
		return penaltyCards.remove(t);
	}

	public void clearPenaltyCard() {
		penaltyCards.clear();
	}

	public List<TurnCard> getBuffCards() {
		return buffCards;
	}

	public void addBuffCard(TurnCard t) {
		buffCards.add(t);
	}

	public boolean removeBuffCard(TurnCard t) {
		return getBuffCards().remove(t);
	}

	public void clearBuffCard() {
		buffCards.clear();
	}

	public CharacterCard getCharacter() {
		return character;
	}

	public void setCharacter(CharacterCard character) {
		this.character = character;
	}

	public boolean hasBugDelegation() {
		;
		for (TurnCard c : buffCards) {
			if (c instanceof BugDelegation) {
				return true;
			}
		}
		return false;
	}

	public void addHandCard(TurnCard tc) {
		this.handcards.add(tc);
	}

	public PlayerState getState() {
		return state;
	}

	public void setState(PlayerState state) {
		this.state = state;
	}

	public boolean removeHandcard(TurnCard card) {
		return getHandcards().remove(card);
	}

	public boolean hasBugReportCard() {
		for (TurnCard c : getHandcards()) {
			if (c instanceof BugReport) {
				return true;
			}
			if (SteveBallmer.class.isInstance(character)) {
				if (c instanceof LameExcuse) {
					return true;
				}
			}
		}
		return false;
	}

	public Optional<TurnCard> getOldPrestigeCard(Class<? extends AbilityCard> clazz) {
		for (TurnCard tc : this.getBuffCards()) {
			if (clazz.isInstance(tc)) {
				return Optional.of(tc);
			}
		}
		return Optional.empty();
	}

	public boolean canAttack(Player targetPlayer) {
		return (getCharacter().getPrestige() >= targetPlayer.getCharacter().getPrestigeSeenByCharacter(getCharacter()));
	}

	public void excusePlayed(TurnCard card) {
		this.handcards.remove(card);
		this.buffCards.add(card);
	}

	public TurnCard[] getExcusesPlayed() {
		List<TurnCard> res = this.buffCards.stream().filter(tc -> character.isLameExcuce(tc))
				.collect(Collectors.toList());

		return res.toArray(new TurnCard[res.size()]);
	}

}