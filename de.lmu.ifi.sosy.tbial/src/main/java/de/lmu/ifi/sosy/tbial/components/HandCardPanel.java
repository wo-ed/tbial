package de.lmu.ifi.sosy.tbial.components;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.TBIALSession;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.characters.KentBeck;
import de.lmu.ifi.sosy.tbial.pages.GamePage;
import de.lmu.ifi.sosy.tbial.util.Utils;
import wicketdnd.DragSource;
import wicketdnd.DropTarget;
import wicketdnd.Location;
import wicketdnd.Operation;
import wicketdnd.Reject;
import wicketdnd.Transfer;
import wicketdnd.theme.HumanTheme;

public class HandCardPanel extends CourtPanel {

	private static final long serialVersionUID = 1L;

	private int playerId;

	private GamePage page;

	public HandCardPanel(String id, int playerId, GamePage page, double rotation, int row, int column) {
		super(id, rotation, row, column);
		this.playerId = playerId;
		this.page = page;
		drawHandCards();
	}

	private void drawHandCards() {
		WebMarkupContainer container = new WebMarkupContainer("handCardsContainer");
		container.setOutputMarkupId(true);
		container.add(new HumanTheme());
		WebMarkupContainer dropContainer = new WebMarkupContainer("dropContainer");
		dropContainer.setOutputMarkupId(true);
		container.add(dropContainer);
		List<Integer> cards = getPanelPlayer().getHandcards().stream().map(c -> c.getId()).collect(Collectors.toList());

		ListView<Integer> hand = new ListView<Integer>("hand", cards) {

			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Integer> item) {
				item.setOutputMarkupId(true);
				item.setMarkupId("" + item.getModelObject());
				if (isUserPanelOwner()) {
					Label label = new Label("handCard", new PropertyModel<>(getCard(item.getModelObject()), "name"));
					label.setEscapeModelStrings(false);
					item.add(label);

				} else {
					item.add(new Label("handCard", ""));
					item.add(new AttributeAppender("class", "cardBack card"));
				}

				// Assign colors, translation and rotation to a card
				item.add(new AttributeAppender("style", new Model<String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {

						int translationX;
						if (!isUserPanelOwner()) {
							translationX = 60 * item.getIndex() - 160;
						} else {
							translationX = 80 * item.getIndex() - 160;
						}
						int translationY = -7 + 3 * Math.abs(3 - item.getIndex());
						int rotationZ = -15 + item.getIndex() * 5;
						String hexBgColor = "ffa500";
						String fontColor = "ffa500";
						if (isUserPanelOwner()) {
							hexBgColor = Utils.colorToHex(getCard(item.getModelObject()).getBgColor());
							fontColor = Utils.colorToHex(getCard(item.getModelObject()).getFontColor());
						}
						String s = "width: 20%; color: " + fontColor + "; background: " + hexBgColor + ";";
						s += " transform: translateX(" + translationX + "%) translateY(" + translationY + "%) rotateZ("
								+ rotationZ + "deg);";

						return s;
					}
				}));

				if (isDropEnabled()) {
					dropContainer.add(new AttributeAppender("style", new Model<String>() {

						private static final long serialVersionUID = 1L;

						@Override
						public String getObject() {
							return "z-index:1;";
						}
					}));
				}

				if (isUserPanelOwner()) {
					item.add(new AttributeModifier("class", "card"));
				} else {
					item.add(new AttributeModifier("class", "cardBack card"));
				}
			}
		};
		container.add(hand);

		if (isDragEnabled()) {
			container.add(new DragSource(Operation.MOVE) {
				private static final long serialVersionUID = 1L;

				@Override
				public void onAfterDrop(AjaxRequestTarget target, Transfer transfer) {
					if (transfer.getOperation() == Operation.MOVE) {
						// target.add(container);
					}
					LOGGER.debug("onAfterDrop: " + transfer.getData().toString());

				}

				@Override
				public void onBeforeDrop(Component drag, Transfer transfer) throws Reject {
					transfer.setData(drag.getDefaultModelObject());
					LOGGER.debug("onBeforeDrop: " + transfer.getData().toString());
				}
			}.drag("div.card, div.cardBack.card"));
		}

		if (isDropEnabled()) {
			DropTarget dropTarget = new DropTarget(Operation.MOVE) {
				private static final long serialVersionUID = 1L;

				@Override
				public void onDrop(AjaxRequestTarget target, Transfer transfer, Location location) throws Reject {
					TurnCard card = getCard(transfer.getData());

					if (isPwndAllowed()) {
						getGame().steal(card);
						target.add(container);
					} else if (isStandupMeetingAllowed()) {
						getGame().drawOneStandupMeetingCard(getSessionPlayer(), card);

					} else if (isStealingMode()) {
						getGame().attemptStealCard(card, getSessionPlayer(), determineStolen(card));
					} else if (canDrawCard()) {
						getGame().attemptDrawCard(getSessionPlayer());
					}
				}

				@Override
				public void onDrag(AjaxRequestTarget target, Location location) {

				}

			}.dropCenter("div.dropContainer");

			container.add(dropTarget);

		}
		add(container);
	}

	public boolean isStealingMode() {
		return getSessionPlayer().getState() == PlayerState.STEALING_MODE && isUserPanelOwner();
	}

	public boolean isPwndAllowed() {
		return getSessionPlayer().getState() == PlayerState.PWND && isUserPanelOwner();
	}

	public boolean isStandupMeetingAllowed() {
		return getSessionPlayer().getState() == PlayerState.STANDUP_MEETING && isUserPanelOwner();
	}

	public boolean canDrawCard() {
		return getSessionPlayer().getCharacter().canDrawCard(getSessionPlayer()) > 0;
	}

	public boolean isDropEnabled() {
		return isPwndAllowed() || isStandupMeetingAllowed() || canDrawCard() || isStealingMode();
	}

	public boolean isDragEnabled() {
		if (isUserPanelOwner()) {
			return getSessionPlayer().getState() == PlayerState.ON_TURN
					|| getSessionPlayer().getState() == PlayerState.DEFLECT_BUG
					|| getSessionPlayer().getState() == PlayerState.SYSTEM_INTEGRATION
					|| getSessionPlayer().getState() == PlayerState.DROP_NEW_CARD || isKentBeck()
					|| getSessionPlayer().getState() == PlayerState.HEISENBUG
					|| getSessionPlayer().getState() == PlayerState.BORING_MEETING;
		} else {
			return canUserStealCard() || ((getSessionPlayer().getState() == PlayerState.PWND
					|| getSessionPlayer().getState() == PlayerState.REFACTORING)
					&& getGame().getStealingCard().getTargetPlayer().equals(getPanelPlayer()));
		}
	}

	private boolean canUserStealCard() {
		// cannot steal one owns handcards
		if (isUserPanelOwner())
			return false;

		if (getSessionPlayer().getState() == PlayerState.STEALING_MODE) {
			return getSessionPlayer().getCharacter().canStealFrom(getSessionPlayer(), getPanelPlayer());
		}

		return false;
	}

	private boolean isKentBeck() {
		boolean hasEnoughCards = false;
		if (KentBeck.class.isInstance(getSessionPlayer().getCharacter())) {
			KentBeck kent = (KentBeck) getSessionPlayer().getCharacter();
			hasEnoughCards = kent.canDropCard(getSessionPlayer());
		}

		return getSessionPlayer().getHp() < getSessionPlayer().getMaxHp() && hasEnoughCards;
	}

	private Player getSessionPlayer() {
		return getGame().getPlayer(((TBIALSession) getSession()).getUser());
	}

	public Player getPanelPlayer() {
		return getGame().findPlayer(playerId);
	}

	public boolean isUserPanelOwner() {
		return getPanelPlayer().equals(getSessionPlayer());
	}

	private static class DisplayNoneBehavior extends AttributeAppender {

		private static final long serialVersionUID = 1L;

		private DisplayNoneBehavior() {
			super("style", Model.of("display: none"));
		}

		@Override
		public boolean isTemporary(Component component) {
			return true;
		}
	}

	private Player determineStolen(TurnCard card) {
		return (card.getTargetPlayer() != null) ? card.getTargetPlayer() : card.getPlayer();
	}

	public Game getGame() {
		return TBIALApplication.getInstance().findGame(page.getUser());
	}

	public TurnCard getCard(int id) {
		return getGame().getCard(id);
	}
}
