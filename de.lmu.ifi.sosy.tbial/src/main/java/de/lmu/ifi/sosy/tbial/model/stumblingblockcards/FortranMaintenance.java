package de.lmu.ifi.sosy.tbial.model.stumblingblockcards;

import java.util.Random;

import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.StumblingBlockCard;
import de.lmu.ifi.sosy.tbial.model.visitor.CardVisitor;

public final class FortranMaintenance extends StumblingBlockCard {

	private static final long serialVersionUID = 1L;

	public FortranMaintenance() {
		setName("Fortran Maintenance");
	}

	@Override
	public void play(CardVisitor v) {
		v.visit(this);
	}

	public void handleCard(Game game) {
		Player player = this.getPlayer();
		Player targetPlayer = this.getTargetPlayer();
		Float probability = new Random().nextFloat();

		if (probability > targetPlayer.getCharacter().calculateChance(0.85f)) {
			targetPlayer.notifyView("You have been assigned a Fortran Maintenance. Your Hp was decreased by 3 points.");
			game.decHp(targetPlayer, player);
			game.decHp(targetPlayer, player);
			game.decHp(targetPlayer, player);
			game.throwCardToHeap(this);
		} else {
			// pass card on to next player
			targetPlayer.notifyView("Lucky you! The Fortran Maintenance was assigned to the next player.");
			Player nextPlayer = game.getNextPlayer(targetPlayer);
			targetPlayer.removePenaltyCard(this);
			this.setTargetPlayer(nextPlayer);
			nextPlayer.addPenaltyCard(this);
			nextPlayer.notifyView("Fortran Maintenance was passed on to you.");
		}
	}

}
