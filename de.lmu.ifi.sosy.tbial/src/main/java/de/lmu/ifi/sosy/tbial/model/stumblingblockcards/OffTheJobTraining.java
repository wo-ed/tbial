package de.lmu.ifi.sosy.tbial.model.stumblingblockcards;

import java.util.Random;

import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.StumblingBlockCard;
import de.lmu.ifi.sosy.tbial.model.visitor.CardVisitor;

public final class OffTheJobTraining extends StumblingBlockCard {

	private static final long serialVersionUID = 1L;

	public OffTheJobTraining() {
		setName("Off The Job");
	}

	@Override
	public void play(CardVisitor v) {
		v.visit(this);
	}

	/**
	 * returns true if this round has to be skipped
	 */
	public boolean handleCard(Game game) {
		Float probability = new Random().nextFloat();

		Player player = this.getTargetPlayer();
		game.throwCardToHeap(this);

		if (probability < player.getCharacter().calculateChance(0.25f)) {
			player.notifyView("You have to skip this turn because you are on training.");
			return false;
		} else {
			player.notifyView("Lucky you! Your training was cancelled. Enjoy your turn.");
			return true;
		}
	}
}
