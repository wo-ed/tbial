package de.lmu.ifi.sosy.tbial.util;

public enum TypesForFilters {
	STATE, PLAYERS, PRIVACYTYPE, MAXPLAYERS;
}
