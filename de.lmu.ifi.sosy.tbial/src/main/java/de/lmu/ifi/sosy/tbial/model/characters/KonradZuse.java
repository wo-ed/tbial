package de.lmu.ifi.sosy.tbial.model.characters;

public class KonradZuse extends CharacterCard {

	private static final long serialVersionUID = 1L;

	public KonradZuse() {
		super("Konrad Zuse");
		this.maxHp = 3;
		selfPrestigeModifier = 1;
	}

	@Override
	public void addSelfPrestigeModifier() {
		selfPrestigeModifier = 2;
	}

	@Override
	public void removeSelfPrestigeModifier() {
		selfPrestigeModifier = 1;
	}

}
