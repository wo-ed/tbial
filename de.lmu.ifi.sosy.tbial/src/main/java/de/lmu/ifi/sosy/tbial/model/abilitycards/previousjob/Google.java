package de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob;

import de.lmu.ifi.sosy.tbial.model.abilitycards.PreviousJob;
import de.lmu.ifi.sosy.tbial.model.visitor.CardVisitor;

public class Google extends PreviousJob {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Google() {
		super("Google \n (prev. Job)");
		this.prestigeModifier = 2;
	}

	@Override
	public void play(CardVisitor v) {
		v.visit(this);
	}

}
