package de.lmu.ifi.sosy.tbial.db;

public class HealthPoints {

	private int gameId;
	private int userId;
	private String healthpoints;

	public HealthPoints(int gameId, int userId, String healthpoints) {
		this.gameId = gameId;
		this.userId = userId;
		this.healthpoints = healthpoints;
	}

	public int getGameId() {
		return gameId;
	}

	public int getUserId() {
		return userId;
	}

	public String getHealthpoints() {
		return healthpoints;
	}

}
