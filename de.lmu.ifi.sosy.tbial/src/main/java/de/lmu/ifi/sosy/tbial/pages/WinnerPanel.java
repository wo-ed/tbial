package de.lmu.ifi.sosy.tbial.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;

public class WinnerPanel extends Panel {

	private static final long serialVersionUID = 1L;

	private List<Player> winners;

	private IModel winnerListModel;
	private ListView<Player> winnerList;

	public WinnerPanel(String id, Game game) {
		super(id);
		this.winners = game.getWinners();
		addComponents();
	}

	private void addComponents() {

		winnerListModel = new LoadableDetachableModel() {
			private static final long serialVersionUID = 1L;

			@Override
			protected Object load() {
				return new ArrayList<>(winners);
			}
		};

		winnerList = new ListView<Player>("winnerList", winnerListModel) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Player> item) {
				Player winner = item.getModelObject();
				item.add(new Label("winnerName", winner.getName()));
				item.add(new Label("winnerRole", winner.getRole().getName()));
			}
		};

		add(winnerList);

	}

}