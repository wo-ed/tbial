package de.lmu.ifi.sosy.tbial.pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.AjaxSelfUpdatingTimerBehavior;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow.CloseButtonCallback;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.util.time.Duration;

import de.lmu.ifi.sosy.tbial.AuthenticationRequired;
import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.components.PlayerPanel;
import de.lmu.ifi.sosy.tbial.components.RepeatingCourtView;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.Player.ComperatorById;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.util.Message;

/**
 * Extends MainPage, represents Courttable
 * 
 * @author Stephan
 *
 */
@AuthenticationRequired
public class GamePage extends MainPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private RepeatingCourtView repeatingView;

	private ModalWindow modal;

	private ModalWindow winnerModal;

	private Label gameInfo;

	private String info = "See your tasks here";
	private ModalWindow gameModal;

	AjaxSubmitLink pass;

	/**
	 * required empty constructor
	 */
	public GamePage() {
		super();
	}

	/**
	 * creates initial Page for game
	 * 
	 * @param game
	 *            - game to display
	 * @return webpages
	 */
	public static GamePage create() {
		GamePage page = new GamePage();

		page.initCourt();

		// user's player panel visible

		// First round started.

		return page;
	}

	/**
	 * init View elements of table
	 */
	private void initCourt() {

		addPlayerStatLink();
		addGameStatLink();

		addWinnerPage();

		List<Player> players = new ArrayList<>(getGame().getAllPlayers());
		// Custom sort: user's sits in front
		int id = (getGame().getUsers().contains(this.getUser())) ? getGame().getPlayer(this.getUser()).getId() : 0;

		Collections.sort(players, new ComperatorById(id, players.size()));

		this.gameInfo = new Label("systemInfo", new PropertyModel<>(this, "info"));

		WebMarkupContainer c = new WebMarkupContainer("container");

		// big background for more than 5 Players
		WebMarkupContainer background = new WebMarkupContainer("background");
		// if (players.size() > 5)
		background.add(new AttributeModifier("class", "bgimgBig"));

		repeatingView = new RepeatingCourtView("outerRepeater", players, getGame(), this);

		background.add(repeatingView);
		c.add(gameInfo);
		c.add(background);
		this.addUpdateBehavior(c);
		add(c);

		add(createEndTurnBtn());
		add(createLeaveGameBtn());
		// add(createPassBtn());

		Form<String> passForm = new Form<String>("passForm");
		pass = new AjaxSubmitLink("passBtn") {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				getGame().passEvent(getGame().getPlayer(getUser()));
			}

		};

		passForm.add(pass);
		pass.setOutputMarkupId(true);
		pass.setVisible(false);
		passForm.add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(2)) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected final void onPostProcessTarget(final AjaxRequestTarget target) {
				// also update visibility of players role

				PlayerState state = getGame().getPlayer(getUser()).getState();
				// System.out.println(state);
				if (state == PlayerState.DEFLECT_BUG || state == PlayerState.HEISENBUG
						|| state == PlayerState.BORING_MEETING || state == PlayerState.STEALING_MODE
						|| state == PlayerState.SYSTEM_INTEGRATION) {
					pass.setVisible(true);
				} else {
					pass.setVisible(false);
				}

			}

		});
		add(passForm);
	}

	private void addPlayerStatLink() {
		addModelStatPage();

		AjaxLink<Void> link = new AjaxLink<Void>("playerstat") {

			/**
			 * UID for serialization.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				modal.setContent(new UserStatPanel(modal.getContentId()));
				modal.show(target);

			}
		};

		add(link);
	}

	private void addModelStatPage() {
		add(modal = new ModalWindow("modal"));

		modal.setCookieName("modal");
		modal.setTitle("User Statistics");

	}

	private void addGameStatLink() {
		addGameModalStatPage();
		AjaxLink<Void> link = new AjaxLink<Void>("gamestat") {

			/**
			 * UID for serialization.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				getGame().setEndTime(new Date());
				gameModal.setContent(new GameStatPanel(gameModal.getContentId(), getGame()));
				gameModal.show(target);
			}
		};

		add(link);
	}

	private void addGameModalStatPage() {
		add(gameModal = new ModalWindow("gameModal"));
		gameModal.setCookieName("gameModal");
		gameModal.setTitle("Game Statistics");
	}

	private Component createEndTurnBtn() {

		Component submit = new AjaxSubmitLink("nextPlayerBtn") {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				getGame().tryEndTurn(getGame().getPlayer(getUser()));
			}

		};
		return new Form<String>("nextPlayerForm").add(submit);
	}

	public void updateGameInfo(String update) {
		this.info = update;
	}

	private Component createLeaveGameBtn() {

		Component submit = new AjaxSubmitLink("leaveGameBtn") {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				TBIALApplication.getInstance().leaveGame(getUser(), getGame());
				setResponsePage(TabPage.class);
			}

		};
		return new Form<String>("leaveGameForm").add(submit);
	}

	@Override
	protected void sendMsg(String txt) {
		User sender = getUser();
		Message chatMessage = new Message(txt, sender, getGame().getName());
		TBIALApplication.addMessage(chatMessage);
	}

	@Override
	protected IModel<List<Message>> getMessagesToBeShown() {
		LoadableDetachableModel<List<Message>> model = new LoadableDetachableModel<List<Message>>() {
			private static final long serialVersionUID = 1L;

			@Override
			protected List<Message> load() {
				List<Message> messages = TBIALApplication.getMessagesForUsername(getUser().getName(),
						getGame().getName());
				// TODO whispering
				// if (getUser() != null) {
				// messages.addAll(TBIALApplication.getMessagesForKey(getUser().getName()));
				// }
				return messages;
			}
		};
		return model;
	}

	public String getInfo() {
		return this.info;
	}

	private void checkForPlayerUpdates() {

		Optional<String> opt = getGame().getPlayer(getUser()).getViewUpdate();
		if (opt.isPresent()) {
			this.resolveViewUpdate(opt.get());
		}

		for (PlayerPanel p : repeatingView.getPlayerPanels().values()) {
			p.updateRoleLabelVisibility();
		}

	}

	/**
	 * Adds Ajax Timer Update Behavior to component
	 * 
	 * @param c
	 *            - component to add
	 */
	private void addUpdateBehavior(Component c) {
		AjaxSelfUpdatingTimerBehavior updateBehavior = new AjaxSelfUpdatingTimerBehavior(Duration.seconds(1)) {

			private static final long serialVersionUID = 1L;

			@Override
			protected final void onPostProcessTarget(final AjaxRequestTarget target) {
				// also update visibility of players role
				checkForPlayerUpdates();

				if (getGame().hasWinners()) {

					winnerModal.setCloseButtonCallback(new CloseButtonCallback() {

						private static final long serialVersionUID = 1L;

						@Override
						public boolean onCloseButtonClicked(AjaxRequestTarget target) {
							TBIALApplication.getInstance().deleteGame(getGame());
							setResponsePage(getApplication().getHomePage());
							return false;
						}

					});
					// winnerModal.setAutoSize(true);
					winnerModal.show(target);
					repeatingView.getEventPanel().shouldBeVisible();

				}
			}

		};
		c.add(updateBehavior);
	}

	public Game getGame() {
		Game game = TBIALApplication.getInstance().findGame(getUser());
		return game;
	}

	public RepeatingCourtView getGameCourt() {
		return repeatingView;
	}

	private void resolveViewUpdate(Object arg) {
		// TODO
		String update = " ";
		update = arg.toString();
		this.updateGameInfo(update);
	}

	private void addWinnerPage() {
		add(winnerModal = new ModalWindow("winnerModal"));

		winnerModal.setCookieName("winnerModal");

		winnerModal.setPageCreator(new ModalWindow.PageCreator() {
			private static final long serialVersionUID = 1L;

			@Override
			public Page createPage() {

				return new WinnerPage(getGame());
			}

		});
	}

	public ModalWindow getGameModal() {
		return gameModal;
	}

}
