package de.lmu.ifi.sosy.tbial.pages;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

import de.lmu.ifi.sosy.tbial.TBIALSession;
import de.lmu.ifi.sosy.tbial.db.User;

public class UserStatPanel extends Panel {

	private static final long serialVersionUID = 1L;
	private Label labelTitle;
	private Label labelGamesWon;
	private Label labelGamesLost;
	private Label labelGamesParticipated;
	private Label labelbugsPlayed;
	private Label labelSystemIntegrationParticipated;
	private User user;

	public UserStatPanel(String id) {
		super(id);
		this.user = getSession().getUser();
		initialize();
	}

	private void initialize() {
		addComponents();
	}

	@Override
	public TBIALSession getSession() {
		return (TBIALSession) super.getSession();
	}

	private void addComponents() {
		labelTitle = new Label("title", new Model<>(user.getName()));
		add(labelTitle);
		labelGamesWon = new Label("gameswon", new Model<>(user.getGamesWon()));
		add(labelGamesWon);
		labelGamesLost = new Label("gameslost", new Model<>(user.getGamesLost()));
		add(labelGamesLost);
		labelGamesParticipated = new Label("gamesparticipated", new Model<>(user.getGamesParticipated()));
		add(labelGamesParticipated);
		labelbugsPlayed = new Label("bugsplayed", new Model<>(user.getBugsPlayed()));
		add(labelbugsPlayed);
		labelSystemIntegrationParticipated = new Label("systemintegrationparticipated",
				new Model<>(user.getSystemIntegrationParticipated()));
		add(labelSystemIntegrationParticipated);
	}
}
