package de.lmu.ifi.sosy.tbial.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.extensions.ajax.markup.html.tabs.AjaxTabbedPanel;
import org.apache.wicket.extensions.markup.html.tabs.AbstractTab;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

import de.lmu.ifi.sosy.tbial.model.Game;

public class WinnerPage extends WebPage {

	private List<ITab> tabs = new ArrayList<>();

	private AbstractTab winnerTab;
	private AbstractTab gameStatTab;

	private AjaxTabbedPanel<AbstractTab> tabbedPanel;

	private Game game;

	public WinnerPage(Game game) {
		super();

		this.game = game;

		winnerTab = new AbstractTab(new Model<>("Winners")) {
			private static final long serialVersionUID = 1L;

			@Override
			public Panel getPanel(String panelId) {
				return new WinnerTab(panelId, game);
			}

		};

		tabs.add(winnerTab);

		gameStatTab = new AbstractTab(new Model<>("Game Statistics")) {
			private static final long serialVersionUID = 1L;

			@Override
			public Panel getPanel(String panelId) {
				return new GameStatTab(panelId, game);
			}

		};

		tabs.add(gameStatTab);

		tabbedPanel = new AjaxTabbedPanel("tabs", tabs);

		add(tabbedPanel);

	}

	protected class WinnerTab extends Panel {
		private static final long serialVersionUID = 1L;

		public WinnerTab(String id, Game game) {
			super(id);
			WinnerPanel winnerPanel = new WinnerPanel("winnerTab", game);
			add(winnerPanel);
		}

	};

	protected class GameStatTab extends Panel {
		private static final long serialVersionUID = 1L;

		public GameStatTab(String id, Game game) {
			super(id);
			GameStatPanel gameStatPanel = new GameStatPanel("gameStatTab", game);
			add(gameStatPanel);
		}

	}

	public AjaxTabbedPanel<AbstractTab> getTabbedPanel() {
		return tabbedPanel;
	};

}
