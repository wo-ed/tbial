package de.lmu.ifi.sosy.tbial;

import org.apache.wicket.ajax.AjaxRequestTarget;

import de.lmu.ifi.sosy.tbial.model.TurnCard;

public class ThrowToHeapNotification {
    private final TurnCard card;
    private final AjaxRequestTarget target;
    
    public ThrowToHeapNotification(TurnCard card, AjaxRequestTarget target) {
		this.card = card;
		this.target = target;
	}
    
    public TurnCard getCard() {
    	return card;
    }
    
    public AjaxRequestTarget getTarget() {
    	return target;
    }
}
