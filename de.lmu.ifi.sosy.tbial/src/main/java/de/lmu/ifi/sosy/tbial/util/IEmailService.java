package de.lmu.ifi.sosy.tbial.util;

public interface IEmailService {

	public boolean sendEmail(String email, String username, String link);

}
