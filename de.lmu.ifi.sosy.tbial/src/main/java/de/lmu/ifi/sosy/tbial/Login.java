package de.lmu.ifi.sosy.tbial;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.pages.BasePage;


/**
 * 
 * Displays a login form. Redirects to {@link TBIALApplication#getHomePage()} on
 * success, or to the original destination if one existed.
 * 
 * @author Andreas Schroeder, SWEP 2013 Team.
 * 
 */
public class Login extends BasePage {

	/**
	 * UID for serialization.
	 */
	private static final long serialVersionUID = 1L;

	private final Button loginButton;

	private final TextField<String> nameField;

	private final PasswordTextField passwordField;

	/**
	 * Constructor for activation Link
	 * 
	 * @param parameters
	 */
	public Login(PageParameters parameters) {
		this();
		
		String token = parameters.get("token").toString();
		if(token != null) {
			User user = TBIALApplication.activateToken(token);
			// auto signIn
			if (user != null) {
				getSession().setSignedIn(user);
				TBIALApplication.addUser(getSession().getUser());
				continueToOriginalDestination();
				setResponsePage(getApplication().getHomePage());
			} else {
				error("Account already active.");
			}
		}
		
		String message = parameters.get("message").toString();
		if(message != null) {
			info(message);
		}
	}

	public Login() {
		add(new FeedbackPanel("feedback"));

		Form<?> form = new Form<>("login");

		nameField = new TextField<>("name", new Model<>(""));
		nameField.setRequired(true);
		passwordField = new PasswordTextField("password", new Model<>(""));
		loginButton = new Button("loginbutton") {

			/**
			 * UID for serialization.
			 */
			private static final long serialVersionUID = 1;

			public void onSubmit() {
				String name = nameField.getModelObject();
				String password = passwordField.getModelObject();
				performLogin(name, password);
			};
		};
		form.add(nameField).add(passwordField).add(loginButton);

		add(form);
	}

	private void performLogin(String name, String password) {
		if (TBIALApplication.getInstance().isAlreadyLoggedIn(name)) {
			error("Your account is currently in use!");
			return;
		}
		if (getSession().signIn(name, password)) {
			User user = getSession().getUser();
			TBIALApplication.addUser(user);
			continueToOriginalDestination();
			setResponsePage(getApplication().getHomePage());	
		} else {
			error("Wrong login/password or user not jet verified.");
		}
	}
}
