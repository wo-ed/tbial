package de.lmu.ifi.sosy.tbial.model.characters;

import de.lmu.ifi.sosy.tbial.model.Player;

public class JeffTaylor extends CharacterCard {

	private static final long serialVersionUID = 1L;

	public JeffTaylor() {
		super("Jeff Taylor");
	}

	@Override
	public int canDrawCard(Player p) {
		if (!p.getCharacter().equals(this))
			return 0;
		return (p.getHandcards().isEmpty()) ? 1 : 0;
	}
}
