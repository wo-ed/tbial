package de.lmu.ifi.sosy.tbial.model.abilitycards;

import de.lmu.ifi.sosy.tbial.model.AbilityCard;
import de.lmu.ifi.sosy.tbial.model.visitor.CardVisitor;

public final class BugDelegation extends AbilityCard {

	private static final long serialVersionUID = 1L;

	public BugDelegation() {
		setName("Bug Delegation");
	}

	@Override
	public void play(CardVisitor v) {
		v.visit(this);
	}

}
