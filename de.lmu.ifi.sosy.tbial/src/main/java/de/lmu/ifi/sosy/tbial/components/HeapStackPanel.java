package de.lmu.ifi.sosy.tbial.components;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.AjaxSelfUpdatingTimerBehavior;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.util.time.Duration;

import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.TBIALSession;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.Player.ComperatorById;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.characters.KentBeck;
import de.lmu.ifi.sosy.tbial.pages.GamePage;
import de.lmu.ifi.sosy.tbial.util.Utils;
import wicketdnd.DragSource;
import wicketdnd.DropTarget;
import wicketdnd.Location;
import wicketdnd.Operation;
import wicketdnd.Reject;
import wicketdnd.Transfer;
import wicketdnd.theme.HumanTheme;

/**
 * Zelle am Spieltisch in der Heap und Stack liegen
 * 
 * @author Stephan
 *
 */
public class HeapStackPanel extends CourtPanel {

	private static final long serialVersionUID = 1L;

	private GamePage page;

	private WebMarkupContainer container;

	private int heapSize;

	public HeapStackPanel(String id, GamePage page, double rotation, int row, int column) {
		super(id, rotation, row, column);
		this.page = page;
		container = new WebMarkupContainer("heapStackContainer");
		container.setOutputMarkupId(true);
		List<Player> players = new ArrayList<>(getGame().getAllPlayers());
		int idPlayer = (getGame().getUsers().contains(((TBIALSession) getSession()).getUser()))
				? getGame().getPlayer(((TBIALSession) getSession()).getUser()).getId() : 0;
		Collections.sort(players, new ComperatorById(idPlayer, players.size()));

		int playersSize = players.size();
		heapSize = getGame().getHeap().size(); // Keep track of the inital
												// amount of cards on the heap.
												// -> ThrowToHeap-Animation
		container.setOutputMarkupId(true);
		container.setEscapeModelStrings(false);
		container.add(new HumanTheme());
		container.add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(1)) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onPostProcessTarget(AjaxRequestTarget target) {
				super.onPostProcessTarget(target);
				int currentSize = getGame().getHeap().size();
				if (currentSize > heapSize) {
					getGame().getHeap().subList(heapSize, currentSize).stream().forEach(i -> {

						if (playersSize == 4) {
							for (int j = 0; j < players.size(); j++) {
								if (i.getPlayer().equals(players.get(j))) {

									List<TurnCard> hand = i.getPlayer().getHandcards();
									boolean handContains = hand.contains(i);
									int x = 0;
									int y = 0;
									switch (j) {
									case 0:
										x = -500;
										if (handContains) {
											y = -300;
										} else {
											y = 0;
										}

										break;
									case 1:
										x = -500;
										y = -220;
										break;
									case 2:
										x = -300;
										y = -150;
										break;
									case 3:
										x = -390;

										break;
									default:
										x = 500;
										y = 0;
										break;

									}
									animateCard(target, i, x, y);
									heapSize = heapSize + 1;
									return;
								}
							}

						}
					});

				}
			}
		});

		add(container);

		initStack(getGame().getStack().stream().map(c -> c.getId()).collect(Collectors.toList()));
		initHeap(getGame().getHeap().stream().map(c -> c.getId()).collect(Collectors.toList()));
		add(container);
	}

	private void initStack(List<Integer> stack) {
		WebMarkupContainer stackContainer = new WebMarkupContainer("stackContainer");
		stackContainer.setOutputMarkupId(true);

		ListView<Integer> stackList = new ListView<Integer>("stack", stack) {

			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Integer> item) {

				// Assign colors and translation to the card
				item.setOutputMarkupId(true);
				item.setMarkupId("" + item.getModelObject());
				item.add(new AttributeModifier("style", new Model<String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {

						// -50 percent is for centering vertically
						// 4 is just a random factor
						int translationY = -50 - item.getIndex() / 4;
						int translationX = item.getIndex() / 4;

						String bgColor = Utils.colorToHex(getCard(item.getModelObject()).getBgColor());
						String fontColor = Utils.colorToHex(getCard(item.getModelObject()).getFontColor());

						String style = String.format(
								"color: %s; background: %s; transform: translateX(%s%%) translateY(%s%%);", fontColor,
								bgColor, translationX, translationY);

						return style;
					}
				}));
			}
		};

		if (getGame().getPlayer(page.getUser()).getCharacter().canDrawCard(getGame().getPlayer(page.getUser())) > 0)
			stackContainer.add(new DragSource(Operation.MOVE) {
				private static final long serialVersionUID = 1L;

				@Override
				public void onAfterDrop(AjaxRequestTarget target, Transfer transfer) {
					if (transfer.getOperation() == Operation.MOVE) {
						target.add(container);
					}
					LOGGER.debug("onAfterDrop" + transfer.getData().toString());

				}

				@Override
				public void onBeforeDrop(Component drag, Transfer transfer) throws Reject {
					transfer.setData(drag.getDefaultModelObject());
					LOGGER.debug("onAfterDrop" + drag.getDefaultModelObject().toString());

				}
			}.drag("div.card"));

		stackContainer.add(stackList);

		container.add(stackContainer);
	}

	private void initHeap(List<Integer> heap) {
		WebMarkupContainer heapContainer = new WebMarkupContainer("heapContainer");
		heapContainer.setOutputMarkupId(true);
		ListView<Integer> heapList = new ListView<Integer>("heap", heap) {

			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Integer> item) {
				item.add(new Label("heapCardName", new PropertyModel<>(getCard(item.getModelObject()), "name"))
						.setEscapeModelStrings(false));

				// Assign colors, translation and rotation to the card
				item.add(new AttributeModifier("style", new Model<String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {
						int maxDegrees = 12;
						int degrees = (item.getIndex() + item.getIndex() % 5) % maxDegrees;

						int translationY = -50 - item.getIndex() / 4;
						int translationX = 0;

						if (item.getIndex() % 2 == 0) {
							degrees = -degrees;
							translationX = item.getIndex() / 4;
						}

						String bgColor = Utils.colorToHex(getCard(item.getModelObject()).getBgColor());
						String fontColor = Utils.colorToHex(getCard(item.getModelObject()).getFontColor());

						String style = String.format(
								"color: %s; background: %s; transform: rotate(%sdeg) translateX(%s%%) translateY(%s%%);",
								fontColor, bgColor, degrees, translationX, translationY);

						return style;
					}
				}));
			}
		};

		heapContainer.add(heapList);

		if (isDropEnabled()) {
			DropTarget dropTarget = new DropTarget(Operation.MOVE) {
				private static final long serialVersionUID = 1L;

				@Override
				public void onDrop(AjaxRequestTarget target, Transfer transfer, Location location) throws Reject {
					TurnCard card = getCard(transfer.getData());

					if (isRefactoringAllowed()) {
						getGame().steal(card);
						// target.add(heapContainer);
					} else if (isDropCardAllowed()) {
						if (card.getPlayer().equals(getSessionPlayer())) {
							throwCardToHeap(target, card.getId());
							// target.add(heapContainer);
						}
					}
				}

				@Override
				public void onDrag(AjaxRequestTarget target, Location location) {

				}

			}.dropCenter("div");
			heapContainer.add(dropTarget);
		}

		heapContainer.add(heapList);
		container.add(heapContainer);
	}

	public boolean isRefactoringAllowed() {
		return getSessionPlayer().getState() == PlayerState.REFACTORING;
	}

	public boolean isDropCardAllowed() {
		return getSessionPlayer().getState() == PlayerState.ON_TURN
				|| getSessionPlayer().getState() == PlayerState.DROP_NEW_CARD
				|| KentBeck.class.isInstance(getSessionPlayer().getCharacter());
	}

	public boolean isDropEnabled() {
		return isDropCardAllowed() || isRefactoringAllowed();
	}

	private Player getSessionPlayer() {
		return getGame().getPlayer(((TBIALSession) getSession()).getUser());
	}

	private void throwCardToHeap(AjaxRequestTarget target, Integer card) {
		GamePage page = (GamePage) getPage();

		page.getGame().attemptThrowCardToHeap(getCard(card));
		// TBIALApplication.throwToHeapNotification = new
		// ThrowToHeapNotification(card, target);
	}

	public Game getGame() {
		return TBIALApplication.getInstance().findGame(page.getUser());
	}

	public TurnCard getCard(int id) {
		return getGame().getCard(id);
	}

	public void animateCard(AjaxRequestTarget target, TurnCard card, int x, int y) {

		target.prependJavaScript("notify|jQuery('#" + card.getId() + "').animate({" + "'top': '" + x + "px',"
				+ "'left': '" + y + "px'}" + ", 1000, notify);");

	}
}
