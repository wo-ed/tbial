package de.lmu.ifi.sosy.tbial.components;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.TBIALSession;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.pages.GamePage;
import de.lmu.ifi.sosy.tbial.util.Utils;
import wicketdnd.DragSource;
import wicketdnd.DropTarget;
import wicketdnd.Location;
import wicketdnd.Operation;
import wicketdnd.Reject;
import wicketdnd.Transfer;
import wicketdnd.theme.HumanTheme;

public class PlayerFieldPanel extends CourtPanel {

	private static final long serialVersionUID = 1L;

	private int playerId;
	private WebMarkupContainer container;

	private HandCardPanel handCards;
	private GamePage page;

	private boolean userEqualsPlayer;

	public PlayerFieldPanel(String id, int playerId, GamePage page, double rotation, int row, int column) {
		super(id, rotation, row, column);
		this.playerId = playerId;
		this.page = page;
		userEqualsPlayer = getPanelPlayer().equals(getSessionPlayer());

		container = new WebMarkupContainer("fieldCardsContainer");
		container.setOutputMarkupId(true);

		/*
		 * dummies player.setPenaltyCards(player.getHandcards());
		 * player.getBuffCards().add(new LameExcuse("Works For Me!"));
		 * player.getBuffCards().add(new LameExcuse("Works For Me!"));
		 * player.getBuffCards().add(new LameExcuse("Works For Me!"));
		 */

		drawPenaltyCards();
		drawBuffCards();

		handCards = new HandCardPanel("handCardPanel", playerId, page, rotation, row, column);
		container.add(handCards);
		container.add(new HumanTheme());

		add(container);
		this.add(new Behavior() {

			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentTag(Component component, ComponentTag tag) {
				String sty = "";
				if (row == 0) {
					sty += "transform: rotate(180deg);";
				} else if (column == 0) {
					sty += "transform: rotate(90deg);";
				} else if (column == 2) {
					sty += "transform: rotate(-90deg);";
				} else {
				}

				if (row == 3 || column < 2) {
					sty += "float: left";
				} else {
					sty += "float: right";
				}

				tag.put("style", sty);
			}
		});
	}

	public void drawPenaltyCards() {

		List<Integer> cards = getPanelPlayer().getPenaltyCards().stream().map(c -> c.getId())
				.collect(Collectors.toList());

		WebMarkupContainer dropZone = new WebMarkupContainer("dropZone");
		dropZone.setOutputMarkupId(true);

		WebMarkupContainer dropZonePenalty = new WebMarkupContainer("dropZonePenalty");
		dropZonePenalty.setOutputMarkupId(true);
		dropZone.add(dropZonePenalty);
		container.add(dropZone);

		TBIALSession ts = (TBIALSession) getSession();
		final boolean userEqualsPlayer = ts.getUser().getName().equals(getPanelPlayer().getName());

		ListView<Integer> pcs = new ListView<Integer>("penaltyCards", cards) {

			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Integer> item) {
				Label label = new Label("penaltyCard", new PropertyModel<>(getCard(item.getModelObject()), "name"));
				label.setEscapeModelStrings(false);
				label.setOutputMarkupId(true);
				item.add(label);

				item.setMarkupId("" + item.getModelObject());

				// Assign colors, translation and rotation to a card
				item.add(new AttributeModifier("style", new Model<String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {

						int translationX;
						translationX = 100 * item.getIndex() - 200;
						String hexBgColor = "ffa500";
						String fontColor = "ffa500";
						hexBgColor = Utils.colorToHex(getCard(item.getModelObject()).getBgColor());
						fontColor = Utils.colorToHex(getCard(item.getModelObject()).getFontColor());
						return "width: 20%; color: " + fontColor + "; background: " + hexBgColor
								+ "; transform: translateX(" + translationX + "%);";
					}
				}));

				if (getSessionPlayer().getState() == PlayerState.DEFLECT_BUG) {
					item.add(new AttributeAppender("style", new Model<String>() {

						private static final long serialVersionUID = 1L;

						@Override
						public String getObject() {
							return "z-index:2;";
						}
					}));
				}
			}
		};

		if (isPenaltyDropEnabled()) {

			dropZonePenalty.add(new AttributeAppender("style", new Model<String>() {

				private static final long serialVersionUID = 1L;

				@Override
				public String getObject() {
					return "z-index:1;";
				}
			}));
		}
		if (isPenaltyDropEnabled()) {
			DropTarget dropTarget = new DropTarget(Operation.MOVE) {
				private static final long serialVersionUID = 1L;

				@Override
				public void onDrop(AjaxRequestTarget target, Transfer transfer, Location location) throws Reject {

					// Set target player.
					TurnCard card = getCard(transfer.getData());
					LOGGER.debug("onDrop: " + card.toString());

					if (card.getPlayer() == null) {
						if (userEqualsPlayer)
							getGame().attemptDrawCard(getSessionPlayer());
					} else if (getSessionPlayer().getState() == PlayerState.STEALING_MODE) {

						LOGGER.debug("onDrop: try to steal card");
						getGame().attemptStealCard(card, getSessionPlayer(), determineStolen(card));
					} else {
						card.setTargetPlayer(getPanelPlayer());

						// Play card.
						getGame().playCard(card);
					}
					target.add(dropZone);

					LOGGER.debug("onDrop: " + card.toString());
				}

				@Override
				public void onDrag(AjaxRequestTarget target, Location location) {

				}

			}.dropCenter("div.dropZonePenalty");

			dropZone.add(dropTarget);
		}

		if (isPenaltyDragEnabled()) {
			dropZone.add(new DragSource(Operation.MOVE) {
				private static final long serialVersionUID = 1L;

				@Override
				public void onAfterDrop(AjaxRequestTarget target, Transfer transfer) {
					if (transfer.getOperation() == Operation.MOVE) {
						// target.add(container);
					}

					// TurnCard card = transfer.getData();
				}

				@Override
				public void onBeforeDrop(Component drag, Transfer transfer) throws Reject {
					transfer.setData(drag.getDefaultModelObject());
				}
			}.drag("div.card"));
		}

		dropZone.add(pcs);
		dropZone.add(new HumanTheme());

		container.add(dropZone);

		container.add(new HumanTheme());
		if (

		canUserStealCard())
			container.add(new DragSource(Operation.MOVE) {
				private static final long serialVersionUID = 1L;

				@Override
				public void onAfterDrop(AjaxRequestTarget target, Transfer transfer) {
					if (transfer.getOperation() == Operation.MOVE) {
						target.add(container);
					}
					LOGGER.debug("onAfterDrop: " + transfer.getData().toString());

				}

				@Override
				public void onBeforeDrop(Component drag, Transfer transfer) throws Reject {
					transfer.setData(drag.getDefaultModelObject());
					LOGGER.debug("onBeforeDrop: " + drag.getDefaultModelObject().toString());
				}
			}.drag("div.card"));
	}

	public void drawBuffCards() {
		List<Integer> cards = getPanelPlayer().getBuffCards().stream().map(c -> c.getId()).collect(Collectors.toList());

		WebMarkupContainer buffContainer = new WebMarkupContainer("buffPanel");
		buffContainer.setOutputMarkupId(true);

		ListView<Integer> bcs = new ListView<Integer>("buffCards", cards) {

			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Integer> item) {
				Label label = new Label("buffCard", new PropertyModel<>(getCard(item.getModelObject()), "name"));
				label.setEscapeModelStrings(false);
				label.setOutputMarkupId(true);
				item.add(label);
				item.setMarkupId("" + item.getModelObject());

				// Assign colors, translation and rotation to a card
				item.add(new AttributeModifier("style", new Model<String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {

						int translationX;
						translationX = 100 * item.getIndex() - 200;
						String hexBgColor = "ffa500";
						String fontColor = "ffa500";
						hexBgColor = Utils.colorToHex(getCard(item.getModelObject()).getBgColor());
						fontColor = Utils.colorToHex(getCard(item.getModelObject()).getFontColor());
						return "width: 20%; color: " + fontColor + "; background: " + hexBgColor
								+ "; transform: translateX(" + translationX + "%);";
					}
				}));
			}
		};

		if (isBuffDragEnabled()) {
			buffContainer.add(new DragSource(Operation.MOVE) {
				private static final long serialVersionUID = 1L;

				@Override
				public void onAfterDrop(AjaxRequestTarget target, Transfer transfer) {
					if (transfer.getOperation() == Operation.MOVE) {
						// target.add(container);
					}
				}

				@Override
				public void onBeforeDrop(Component drag, Transfer transfer) throws Reject {
					transfer.setData(drag.getDefaultModelObject());
				}
			}.drag("div.card"));
		}

		buffContainer.add(bcs);
		container.add(buffContainer);
	}

	public boolean isBuffDragEnabled() {
		if (isUserPanelOwner()) {
			return false;
		} else {
			return (canUserStealCard() || (getSessionPlayer().getState() == PlayerState.PWND
					|| getSessionPlayer().getState() == PlayerState.REFACTORING)
					&& getGame().getStealingCard().getTargetPlayer().equals(getPanelPlayer()));
		}
	}

	private boolean canUserStealCard() {
		// cannot steal one owns handcards
		if (isUserEqualsPlayer())
			return false;

		if (getSessionPlayer().getState() == PlayerState.STEALING_MODE) {
			return getSessionPlayer().getCharacter().canStealFrom(getSessionPlayer(), getPanelPlayer());
		}

		return false;
	}

	public boolean isPenaltyDropEnabled() {
		return getSessionPlayer().getState() == PlayerState.ON_TURN ||
				getSessionPlayer().getState() == PlayerState.DEFLECT_BUG ||
				getSessionPlayer().getState() == PlayerState.SYSTEM_INTEGRATION ||
				getSessionPlayer().getState() == PlayerState.HEISENBUG ||
				getSessionPlayer().getState() == PlayerState.BORING_MEETING;
	}

	public boolean isPenaltyDragEnabled() {
		if (isUserPanelOwner()) {
			return false;
		} else {
			return (canUserStealCard() || (getSessionPlayer().getState() == PlayerState.PWND
					|| getSessionPlayer().getState() == PlayerState.REFACTORING)
					&& getGame().getStealingCard().getTargetPlayer().equals(getPanelPlayer()));
		}
	}

	private Player getSessionPlayer() {
		return getGame().getPlayer(((TBIALSession) getSession()).getUser());
	}

	public boolean isUserPanelOwner() {
		return getPanelPlayer().equals(getSessionPlayer());
	}

	public Player getPanelPlayer() {
		return getGame().findPlayer(playerId);
	}

	public Game getGame() {
		return TBIALApplication.getInstance().findGame(page.getUser());
	}

	public HandCardPanel getHandCards() {
		return handCards;
	}

	public boolean isUserEqualsPlayer() {
		return userEqualsPlayer;
	}

	public TurnCard getCard(int id) {
		return getGame().getCard(id);
	}

	private Player determineStolen(TurnCard card) {
		return (card.getTargetPlayer() != null) ? card.getTargetPlayer() : card.getPlayer();
	}

}
