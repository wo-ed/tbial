package de.lmu.ifi.sosy.tbial.model.characters;

public class SteveJobs extends CharacterCard {

	private static final long serialVersionUID = 1L;

	public SteveJobs() {
		super("Steve Jobs");
	}

	@Override
	public Float calculateChance(float probability) {
		// Equals Probability + (1 - Probability) * Probability
		return (2 - probability) * probability;
	}
}
