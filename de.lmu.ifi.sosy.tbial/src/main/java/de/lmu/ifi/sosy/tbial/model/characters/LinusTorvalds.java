package de.lmu.ifi.sosy.tbial.model.characters;

public class LinusTorvalds extends CharacterCard {

	private static final long serialVersionUID = 1L;

	public LinusTorvalds() {
		super("Linus Torvalds");
	}

	@Override
	public int numberOfLameExcusesNeededToDeflectBug() {
		return 2;
	}
}
