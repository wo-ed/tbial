package de.lmu.ifi.sosy.tbial.model.characters;

public class LarryEllison extends CharacterCard {

	private static final long serialVersionUID = 1L;

	public LarryEllison() {
		super("Larry Ellison");
	}

	@Override
	public int numberOfCardsToDraw() {
		return 3;
	}
}
