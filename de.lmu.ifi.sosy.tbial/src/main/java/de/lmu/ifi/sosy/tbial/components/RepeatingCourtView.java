package de.lmu.ifi.sosy.tbial.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;

import de.lmu.ifi.sosy.tbial.components.CourtPanel.ICourtPanelFactory;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.pages.GamePage;

/**
 * Nested ListView um ein dynamisches Grid an Spieltisch Panels zu erzeugen
 * (Wegen gerader und ungerader Spieleranzahl)
 * 
 * @extends ListView<List<ICourtModel>>
 * @author Stephan
 *
 */
public class RepeatingCourtView extends ListView<List<ICourtPanelFactory>> {

	private int rows;

	private Map<Integer, PlayerPanel> playerPanels = new HashMap<>();
	private HeapStackPanel heapStack;
	private EventPanel eventPanel;

	private GamePage page;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RepeatingCourtView(String id, List<Player> players, Game game, GamePage page) {
		super(id, createNestedFactoryList(players, game));
		this.page = page;
		rows = players.size() / 2 + 1;
		if (players.size() == 4 || players.size() == 5) {
			rows = 4;
		}
	}

	@Override
	protected void populateItem(ListItem<List<ICourtPanelFactory>> item) {
		List<ICourtPanelFactory> list = item.getModelObject();
		// set height der panels dynamisch - abhg der Anzahl an notwendigen
		// Zeilen
		item.add(new Behavior() {

			private static final long serialVersionUID = 1L;

			@Override
			public void onComponentTag(Component component, ComponentTag tag) {
				tag.put("style", "height:" + (100 / rows) + "%");
			}
		});

		int currRow = item.getIndex();
		// erstellt nested List (Zeilen im Grid)
		// RepeatingView because each cellitem has unique id
		// -> different styles possible
		RepeatingView rv = new RepeatingView("innerRepeater");
		// Simple Rotation der Panels
		int deg = 90;
		// erste Zeile soll dem Spieler gegenüber sitzen
		int degAct = (currRow != 0) ? deg : deg * 2;

		int currColumn = 0;

		for (ICourtPanelFactory cm : list) {
			Panel p = cm.createPanel(rv.newChildId(), degAct, page, currRow, currColumn);

			if (p instanceof HeapStackPanel)
				heapStack = (HeapStackPanel) p;
			else if (p instanceof EventPanel) {
				eventPanel = (EventPanel) p;

			} else if (p instanceof PlayerPanel) {
				playerPanels.put(((PlayerPanel) p).getPanelPlayer().getId(), (PlayerPanel) p);

				PlayerState currentState = ((PlayerPanel) p).getPanelPlayer().getState();

				if (currentState == PlayerState.ON_TURN || currentState == PlayerState.DEFLECT_BUG
						|| currentState == PlayerState.PLAY_BUG || currentState == PlayerState.STANDUP_MEETING
						|| currentState == PlayerState.SYSTEM_INTEGRATION || currentState == PlayerState.STEALING_MODE
						|| currentState == PlayerState.HEISENBUG || currentState == PlayerState.BORING_MEETING
						|| currentState == PlayerState.DROP_NEW_CARD || currentState == PlayerState.PWND
						|| currentState == PlayerState.REFACTORING) {
					p.add(new AttributeAppender("style", "background: rgba(255,255,255, 0.5);"));
					p.get("attrPanel").add(new AttributeAppender("style", "color: black;"));

				}

			}
			rv.add(p);

			if (currRow != 0)
				degAct = degAct - deg;

			currColumn++;
		}
		item.add(rv);

	}

	/**
	 * Helper Methode um aus der einfachen Liste an Spielern eine verschachtelte
	 * an Spielfeldpanels erzeugt
	 * 
	 * @param players
	 * @return - verschachtelte Liste an Spielfeldpanels
	 */
	public static List<List<ICourtPanelFactory>> createNestedFactoryList(List<Player> players, Game game) {
		// LinkedList to addFirst (build list reverse users sits downwards)
		LinkedList<List<ICourtPanelFactory>> result = new LinkedList<>();
		// LinkedList to get first and last each step
		LinkedList<Player> helper = new LinkedList<>();
		helper.addAll(players);

		if (helper.size() < 4) {
			throw new IllegalArgumentException("There must be more than 3 players in game");
		}

		// First Row:
		List<ICourtPanelFactory> firstRow = new ArrayList<>();
		firstRow.add(new ICourtPanelFactory.EmptyPanelFactory(false));
		firstRow.add(new ICourtPanelFactory.PlayerPanelFactory(helper.removeFirst().getId()));
		firstRow.add(new ICourtPanelFactory.EmptyPanelFactory(false));
		result.addFirst(firstRow);

		// falls es mehr als 5 spieler da sind
		if (helper.size() > 4) {
			// Remaining Rows:
			while (helper.size() > 2) {

				List<ICourtPanelFactory> row = new ArrayList<>();
				row.add(new ICourtPanelFactory.PlayerPanelFactory(helper.removeFirst().getId()));
				// nur ein Panel für Stack & Heap erzeugen
				if (helper.size() > 3) {
					row.add(new ICourtPanelFactory.EventPanelFactory());
				} else {
					row.add(new ICourtPanelFactory.HeapStackPanelFactory());
				}
				row.add(new ICourtPanelFactory.PlayerPanelFactory(helper.removeLast().getId()));
				result.addFirst(row);
			}
		} else {
			List<ICourtPanelFactory> row = new ArrayList<>();
			// nur ein Panel für Stack & Heap erzeugen
			// isBig = true;
			row.add(new ICourtPanelFactory.PlayerPanelFactory(helper.removeFirst().getId()));
			row.add(new ICourtPanelFactory.EventPanelFactory());
			row.add(new ICourtPanelFactory.EmptyPanelFactory(false));
			result.addFirst(row);

			List<ICourtPanelFactory> rowNext = new ArrayList<>();
			rowNext.add(new ICourtPanelFactory.EmptyPanelFactory(false));
			rowNext.add(new ICourtPanelFactory.HeapStackPanelFactory());
			rowNext.add(new ICourtPanelFactory.PlayerPanelFactory(helper.removeLast().getId()));
			result.addFirst(rowNext);
		}

		// Last Row:
		List<ICourtPanelFactory> lastRow = new ArrayList<>();
		if (helper.size() < 2) {
			lastRow.add(new ICourtPanelFactory.EmptyPanelFactory(false));
			lastRow.add(new ICourtPanelFactory.PlayerPanelFactory(helper.removeFirst().getId()));
			lastRow.add(new ICourtPanelFactory.EmptyPanelFactory(false));
		} else {
			lastRow.add(new ICourtPanelFactory.EmptyPanelFactory(true));
			lastRow.add(new ICourtPanelFactory.PlayerPanelFactory(helper.removeFirst().getId()));
			lastRow.add(new ICourtPanelFactory.PlayerPanelFactory(helper.removeFirst().getId()));
			lastRow.add(new ICourtPanelFactory.EmptyPanelFactory(true));
		}
		result.addFirst(lastRow);
		return result;
	}

	public Map<Integer, PlayerPanel> getPlayerPanels() {
		return playerPanels;
	}

	public HeapStackPanel getHeapStack() {
		return heapStack;
	}

	public void setHeapStack(HeapStackPanel heapStack) {
		this.heapStack = heapStack;
	}

	public EventPanel getEventPanel() {
		return eventPanel;
	}

	public void setEventPanel(EventPanel eventPanel) {
		this.eventPanel = eventPanel;
	}

}
