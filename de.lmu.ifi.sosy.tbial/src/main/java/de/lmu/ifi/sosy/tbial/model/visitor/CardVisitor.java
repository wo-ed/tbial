package de.lmu.ifi.sosy.tbial.model.visitor;

import java.io.Serializable;

import de.lmu.ifi.sosy.tbial.model.abilitycards.BugDelegation;
import de.lmu.ifi.sosy.tbial.model.actioncards.BoringMeeting;
import de.lmu.ifi.sosy.tbial.model.abilitycards.WearsSunglassesAtWork;
import de.lmu.ifi.sosy.tbial.model.abilitycards.WearsTieAtWork;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Accenture;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Google;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Microsoft;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Nasa;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.actioncards.Heisenbug;
import de.lmu.ifi.sosy.tbial.model.actioncards.LANParty;
import de.lmu.ifi.sosy.tbial.model.actioncards.LameExcuse;
import de.lmu.ifi.sosy.tbial.model.actioncards.PersonalCoffeeMachine;
import de.lmu.ifi.sosy.tbial.model.actioncards.Pwnd;
import de.lmu.ifi.sosy.tbial.model.actioncards.RedBullDispenser;
import de.lmu.ifi.sosy.tbial.model.actioncards.Refactoring;
import de.lmu.ifi.sosy.tbial.model.actioncards.Solution;
import de.lmu.ifi.sosy.tbial.model.actioncards.StandupMeeting;
import de.lmu.ifi.sosy.tbial.model.actioncards.SystemIntegration;
import de.lmu.ifi.sosy.tbial.model.stumblingblockcards.FortranMaintenance;
import de.lmu.ifi.sosy.tbial.model.stumblingblockcards.OffTheJobTraining;

public interface CardVisitor extends Serializable {

	public void visit(BoringMeeting card);
	
	public void visit(Heisenbug card);
	public void visit(BugReport card);

	public void visit(LameExcuse card);

	public void visit(Solution card);

	public void visit(LANParty lanParty);

	public void visit(RedBullDispenser redBullDispenser);

	public void visit(PersonalCoffeeMachine personalCoffeeMachine);

	public void visit(Pwnd pwnd);

	public void visit(Refactoring refactoring);

	public void visit(BugDelegation card);

	public void visit(FortranMaintenance card);

	public void visit(OffTheJobTraining card);

	public void visit(SystemIntegration systemIntegration);

	public void visit(StandupMeeting standupMeeting);

	public void visit(WearsTieAtWork card);

	public void visit(WearsSunglassesAtWork card);

	public void visit(Nasa card);

	public void visit(Google card);

	public void visit(Microsoft card);

	public void visit(Accenture card);
}