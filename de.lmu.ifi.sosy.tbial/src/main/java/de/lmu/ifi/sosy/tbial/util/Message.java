package de.lmu.ifi.sosy.tbial.util;
import java.io.Serializable;
import de.lmu.ifi.sosy.tbial.db.User;
public class Message implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * The text of the message.
	 */
	private final String text;
	
	/**
	 * The sender of the message.
	 */
	private final User sender;
	
	/**
	 * Receiver of the message. Can be a Username (whisper), game name or "all".
	 */
	private final String receiver; 
	
	public Message(String text, User sender, String receiver) {
		this.text = text;
		this.sender = sender;
		this.receiver = receiver;
	}
	
	public Message(String text, User sender) {
		this(text, sender, "all");
	}
	public User getSender() {
		return sender;
	}
	public String getReceiver() {
		return receiver;
	}
	public String getText() {
		return text;
	}
	
	public String senderName() {
		return sender.getName();
	}
	@Override
	public String toString() {
		return "Message [text=" + text + ", sender=" + sender + ", receiver=" + receiver + "]";
	}
	
	
	
}