package de.lmu.ifi.sosy.tbial.model.actioncards;

import de.lmu.ifi.sosy.tbial.model.StealingCard;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.visitor.CardVisitor;

public final class Refactoring extends StealingCard {

	private static final long serialVersionUID = 1L;

	public Refactoring() {
		setName("Refactoring");
	}

	@Override
	public void play(CardVisitor v) {
		v.visit(this);
	}

}
