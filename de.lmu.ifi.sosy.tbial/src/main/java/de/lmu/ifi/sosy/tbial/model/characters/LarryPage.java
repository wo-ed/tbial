package de.lmu.ifi.sosy.tbial.model.characters;

public class LarryPage extends CharacterCard {

	private static final long serialVersionUID = 1L;

	public LarryPage() {
		super("Larry Page");
	}

}
