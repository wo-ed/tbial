package de.lmu.ifi.sosy.tbial.model.characters;

public class TomAndersonCharacterCard extends CharacterCard {
	private static final long serialVersionUID = 1L;

	public TomAndersonCharacterCard() {
		super("Tom Anderson");
	}

	public void drawCard() {
		this.cardsToDraw++;
	}

}