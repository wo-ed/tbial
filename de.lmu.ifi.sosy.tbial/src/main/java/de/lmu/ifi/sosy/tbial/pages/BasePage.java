package de.lmu.ifi.sosy.tbial.pages;

import org.apache.wicket.Session;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.Link;

import de.lmu.ifi.sosy.tbial.Login;
import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.TBIALSession;
import de.lmu.ifi.sosy.tbial.db.Database;

/**
 * 
 * Basic page with style template as well as access to {@link TBIALSession} and
 * {@link Database}.
 * 
 * @author Andreas Schroeder, SWEP 2013 Team.
 * 
 */
public abstract class BasePage extends WebPage {

	/**
	 * UID for serialization.
	 */
	private static final long serialVersionUID = 1L;

	private Link<Void> signOutLink;
	private Link<Void> deleteAccountLink;

	protected Database getDatabase() {
		return TBIALApplication.getDatabase();
	}

	@Override
	public TBIALSession getSession() {
		return (TBIALSession) super.getSession();
	}

	public BasePage() {
		signOutLink = new Link<Void>("signout") {

			/**
			 * UID for serialization.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				Session session = getSession();
				if (session instanceof AuthenticatedWebSession) {
					((AuthenticatedWebSession) session).signOut();
				}
				BasePage.this.setResponsePage(Login.class);
				session.invalidate();
			}
		};

		add(signOutLink);

		if (!getSession().isSignedIn()) {
			signOutLink.setVisible(false);
			signOutLink.setEnabled(false);
		}

		deleteAccountLink = new Link<Void>("deleteAccount") {

			/**
			 * UID for serialization.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				setResponsePage(DeleteAccountPage.class);
			}
		};

		add(deleteAccountLink);

		if (!getSession().isSignedIn()) {
			deleteAccountLink.setVisible(false);
			deleteAccountLink.setEnabled(false);
		}
	}
}
