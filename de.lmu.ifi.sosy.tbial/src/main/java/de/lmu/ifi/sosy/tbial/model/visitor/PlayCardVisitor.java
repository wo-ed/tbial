package de.lmu.ifi.sosy.tbial.model.visitor;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.Player.ComperatorById;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.abilitycards.BugDelegation;
import de.lmu.ifi.sosy.tbial.model.abilitycards.PreviousJob;
import de.lmu.ifi.sosy.tbial.model.abilitycards.WearsSunglassesAtWork;
import de.lmu.ifi.sosy.tbial.model.abilitycards.WearsTieAtWork;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Accenture;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Google;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Microsoft;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Nasa;
import de.lmu.ifi.sosy.tbial.model.actioncards.BoringMeeting;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.actioncards.Heisenbug;
import de.lmu.ifi.sosy.tbial.model.actioncards.LANParty;
import de.lmu.ifi.sosy.tbial.model.actioncards.LameExcuse;
import de.lmu.ifi.sosy.tbial.model.actioncards.PersonalCoffeeMachine;
import de.lmu.ifi.sosy.tbial.model.actioncards.Pwnd;
import de.lmu.ifi.sosy.tbial.model.actioncards.RedBullDispenser;
import de.lmu.ifi.sosy.tbial.model.actioncards.Refactoring;
import de.lmu.ifi.sosy.tbial.model.actioncards.Solution;
import de.lmu.ifi.sosy.tbial.model.actioncards.StandupMeeting;
import de.lmu.ifi.sosy.tbial.model.actioncards.SystemIntegration;
import de.lmu.ifi.sosy.tbial.model.characters.SteveBallmer;
import de.lmu.ifi.sosy.tbial.model.stumblingblockcards.FortranMaintenance;
import de.lmu.ifi.sosy.tbial.model.stumblingblockcards.OffTheJobTraining;

public class PlayCardVisitor implements CardVisitor, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final Game game;

	private TurnCard systemInegretation;

	public PlayCardVisitor(Game game) {
		this.game = game;
	}

	@Override
	public void visit(BugReport card) {
		if (card.getPlayer().getState() == PlayerState.ON_TURN
				|| card.getPlayer().getState() == PlayerState.SYSTEM_INTEGRATION) {
			playBug(card);
		} else if (card.getPlayer().getState() == PlayerState.DEFLECT_BUG) {
			if (SteveBallmer.class.isInstance(card.getPlayer().getCharacter())) {
				deflectWithExcuse(card);
			}
		} else if (card.getPlayer().getState() == PlayerState.BORING_MEETING) {
			defendGlobalAttack(card);
			game.incBugsPlayed();
		}
	}

	private void deflectWithExcuse(TurnCard card) {
		game.incExcusesPlayed();
		TurnCard bug = card.getPlayer().findBugInPenaltyCards();
		if (bug.getPlayer().getCharacter().numberOfLameExcusesNeededToDeflectBug() == 1)
			game.deflectBug(bug, true, card);
		else {
			card.getTargetPlayer().excusePlayed(card);
			TurnCard[] excusesPlayed = card.getPlayer().getExcusesPlayed();
			if (bug.getPlayer().getCharacter().numberOfLameExcusesNeededToDeflectBug() == excusesPlayed.length) {
				game.deflectBug(bug, true, excusesPlayed);
			}
		}
	}

	@Override
	public void visit(LameExcuse card) {
		// use LameExcuse for bug deflection
		if (card.getPlayer().getState() == PlayerState.DEFLECT_BUG) {
			deflectWithExcuse(card);
		} else if (card.getPlayer().getState() == PlayerState.ON_TURN
				|| card.getPlayer().getState() == PlayerState.SYSTEM_INTEGRATION) {
			if (SteveBallmer.class.isInstance(card.getPlayer().getCharacter())) {
				playBug(card);
			}
		} else if (card.getPlayer().getState() == PlayerState.HEISENBUG) {
			defendGlobalAttack(card);
			game.incExcusesPlayed();
		}
	}

	/**
	 * Abstraction of play bug: reuse with lame excuse for Steve Ballmer
	 * 
	 * @param card
	 */
	private void playBug(TurnCard card) {
		if (card.getPlayer().getState() == PlayerState.ON_TURN) {

			if (!game.canPlayBug()) {
				card.getPlayer().notifyView("You can not play multiple bugs in one turn");
				return;
			}

			if (card.getPlayer().equals(card.getTargetPlayer())) {
				card.getPlayer().notifyView("You can not play bugs on yourself!");
				return;
			}

			if (!card.getPlayer().canAttack(card.getTargetPlayer())) {
				card.getPlayer().notifyView("You can not play bugs on players with more prestige!");
				return;
			}

			// Update statistics
			game.bugWasPlayed();
			game.incBugsPlayed();
			game.getUser(card.getPlayer()).incrementBugsPlayed(TBIALApplication.getDatabase());

			if (game.tryBugDelegation(card))
				return;

			Player targetPlayer = card.getTargetPlayer();
			targetPlayer.addPenaltyCard(card);
			card.getPlayer().getHandcards().remove(card);
			card.getPlayer().setState(PlayerState.WAIT);
			targetPlayer.setState(PlayerState.DEFLECT_BUG);

			if (!targetPlayer.canDefelectBugFromCharacter(card.getPlayer().getCharacter())) {
				game.deflectBug(card, false, card);
			} else {
				targetPlayer.notifyView(card.getPlayer().getName()
						+ " Attacked you. Select a card to deflect it or click on attackers card if not.");
			}

		} else if (card.getPlayer().getState() == PlayerState.SYSTEM_INTEGRATION) {
			Player actor = card.getPlayer();
			Player reactor = systemInegretation.getPlayer() == card.getPlayer() ? systemInegretation.getTargetPlayer()
					: systemInegretation.getPlayer();

			if (!card.getTargetPlayer().equals(reactor)) {
				actor.notifyView("Your opponent in this duell is " + reactor.getName());
				return;
			}

			if (reactor.hasBugReportCard()) {
				reactor.setState(PlayerState.SYSTEM_INTEGRATION);
				actor.setState(PlayerState.WAIT);
				actor.removeHandcard(card);
				reactor.addPenaltyCard(card);
			} else {
				game.throwCardToHeap(card);
				endSystemIntegration(reactor, actor);
			}
		}
	}

	@Override
	public void visit(Solution card) {
		Player p = card.getPlayer();
		if (p.getState() == PlayerState.ON_TURN) {
			if (p.getHp() < p.getMaxHp()) {
				game.incHp(p);
				game.throwCardToHeap(card);
				game.incSolutionsPlayed();
			} else {
				p.notifyView("You have reached your max Hp");
			}
		} else if (p.getState() == PlayerState.DEFLECT_BUG) {
			game.deflectBug(card.getPlayer().findBugInPenaltyCards(), true, card);
			game.incSolutionsPlayed();
		} else if (p.getState() == PlayerState.HEISENBUG) {
			defendGlobalAttack(card);
			game.incSolutionsPlayed();
		}
	}

	private void defendGlobalAttack(TurnCard c) {
		String s;
		switch (c.getPlayer().getState()) {
		case HEISENBUG:
			s = "heisenbug";
			break;
		case BORING_MEETING:
			s = "boring meeting";
			break;
		default:
			s = "";
		}
		c.getPlayer().setState(PlayerState.WAIT);
		c.getPlayer().notifyView("You are out of " + s + " now.");
		game.throwCardToHeap(c);
		game.decWaitingGlobalAttackCard();
	}

	private Player getTargetPlayerTest(TurnCard card) {
		for (Player p : game.getAllPlayers()) {
			if (p != card.getPlayer()) {
				return p;
			}
		}
		return null;
	}

	@Override
	public void visit(LANParty lanParty) {
		if (lanParty.getPlayer().equals(lanParty.getTargetPlayer()))
			if (lanParty.getPlayer().getState() == PlayerState.ON_TURN) {
				this.game.getAllPlayers().stream().forEach(p -> {
					game.incHp(p);
					p.notifyView(p.getName() + " played Lan-Pary card");
				});
				game.throwCardToHeap(lanParty);
				game.incSpecialCardsPlayed();
			}
	}

	@Override
	public void visit(RedBullDispenser redBullDispenser) {

		if (redBullDispenser.getPlayer().equals(redBullDispenser.getTargetPlayer()))
			if (redBullDispenser.getPlayer().getState() == PlayerState.ON_TURN) {
				game.drawCards(redBullDispenser.getPlayer(), 3);
				game.throwCardToHeap(redBullDispenser);
				game.incSpecialCardsPlayed();
			}
	}

	@Override
	public void visit(PersonalCoffeeMachine personalCoffeeMachine) {
		if (personalCoffeeMachine.getPlayer().equals(personalCoffeeMachine.getTargetPlayer()))
			if (personalCoffeeMachine.getPlayer().getState() == PlayerState.ON_TURN) {
				game.drawCards(personalCoffeeMachine.getPlayer(), 2);
				game.throwCardToHeap(personalCoffeeMachine);
				game.incSpecialCardsPlayed();
			}

	}

	@Override
	public void visit(Pwnd pwnd) {
		if (pwnd.getPlayer().equals(pwnd.getTargetPlayer())) {
			pwnd.getPlayer().notifyView("You can not play stealing cards on yourself!");
			return;
		}

		if (!pwnd.getPlayer().canAttack(pwnd.getTargetPlayer())) {
			pwnd.getPlayer().notifyView("You can not steal cards from players with more prestige!");
			return;
		}

		if (!pwnd.getPlayer().canAttack(pwnd.getTargetPlayer())) {
			pwnd.getPlayer().notifyView("You can not steal from a players with more prestige!");
			return;
		}

		pwnd.getPlayer().setState(PlayerState.PWND);
		game.setStealingCard(pwnd);
		game.throwCardToHeap(pwnd);
		game.incSpecialCardsPlayed();
	}

	@Override
	public void visit(Refactoring refactoring) {
		if (refactoring.getPlayer().equals(refactoring.getTargetPlayer())) {
			refactoring.getPlayer().notifyView("You can not play stealing cards on yourself!");
			return;
		}

		refactoring.getPlayer().setState(PlayerState.REFACTORING);
		game.setStealingCard(refactoring);
		game.throwCardToHeap(refactoring);
		game.incSpecialCardsPlayed();
	}

	public void visit(BugDelegation card) {
		Player p = card.getPlayer();

		if (!(p.getState() == PlayerState.ON_TURN)) {
			return;
		}

		if (card.getPlayer().equals(card.getTargetPlayer()))
			if (!p.hasBugDelegation()) {
				p.removeHandcard(card);
				p.addBuffCard(card);
				game.incAbilityCardsPlayed();
			}
	}

	@Override
	public void visit(FortranMaintenance card) {
		if (card.getPlayer().getState() == PlayerState.ON_TURN) {
			if (card.getPlayer().equals(card.getTargetPlayer())) {
				card.getPlayer().removeHandcard(card);
				card.getTargetPlayer().addPenaltyCard(card);
				game.incStumblingCardsPlayed();
			} else {
				card.getPlayer().notifyView("You must play Fortran Maintenance on yourself.");
			}
		}
	}

	@Override
	public void visit(OffTheJobTraining card) {

		if (!card.getPlayer().equals(card.getTargetPlayer()))
			if (card.getPlayer().getState() == PlayerState.ON_TURN) {
				card.getPlayer().removeHandcard(card);
				card.getTargetPlayer().addPenaltyCard(card);
				game.incStumblingCardsPlayed();
			}
	}

	@Override
	public void visit(SystemIntegration card) {

		if (!card.getPlayer().equals(card.getTargetPlayer()))
			if (card.getPlayer().getState() == PlayerState.ON_TURN) {
				// Update statistics
				updateSystemIntegrationParticipated(card.getTargetPlayer(), card.getPlayer());

				if (card.getTargetPlayer() == null)
					card.setTargetPlayer(getTargetPlayerTest(card));

				Player targetPlayer = card.getTargetPlayer();

				game.incSpecialCardsPlayed();

				if (!targetPlayer.hasBugReportCard()) {
					// Victim has no bug to play.
					game.decHp(targetPlayer, card.getPlayer());
					game.throwCardToHeap(card);
					return;
				}

				targetPlayer.addPenaltyCard(card);
				systemInegretation = card;
				card.getPlayer().getHandcards().remove(card);
				targetPlayer.setState(PlayerState.SYSTEM_INTEGRATION);
				card.getPlayer().setState(PlayerState.WAIT);

			}
	}

	/**
	 * sets all player state in Heisenbug who has Bug card decHp otherwise
	 */
	@Override
	public void visit(Heisenbug card) {
		// Check whether player is allowed to play
		if (!(card.getPlayer().getState() == PlayerState.ON_TURN)) {
			return;
		}
		game.incSpecialCardsPlayed();
		// For the case, nobody can react
		List<Player> players = game.getAllPlayers().stream().filter(p -> p.isAlive()).collect(Collectors.toList());
		int counter = players.size();
		Collections.sort(players, new ComperatorById(card.getPlayer().getId(), players.size()));
		for (Player p : players) {
			boolean hasBug = false;
			for (TurnCard t : p.getHandcards()) {
				if (t instanceof Solution || t instanceof LameExcuse) {
					hasBug = true;
					p.setState(PlayerState.HEISENBUG);
					p.notifyView(card.getPlayer().getName()
							+ " played Heisenbug. Play a lame excuse or solution card or lose a mental health point!");
					break;
				}
			}
			if (!hasBug) {
				counter--;
				p.notifyView(card.getPlayer().getName()
						+ " played Heisenbug. You lost a mental health point, because you don't have any of lame excuse or solution.");
				game.decHp(p, card.getPlayer());
				if (p.getState() != PlayerState.DEAD) {
					p.setState(PlayerState.WAIT);
				}
			}
		}
		// if card effect is automatically over, because no one can react.
		if (counter == 0) {
			game.throwCardToHeap(card);
			if (card.getPlayer().getState() == PlayerState.WAIT) {
				card.getPlayer().setState(PlayerState.ON_TURN);
			} else {
				// case the player is dead
				game.tryEndTurn(card.getPlayer());
			}
		} else {
			card.getPlayer().removeHandcard(card);
			game.setEventCard(card);
			game.setWaitingGlobalAttackCard(counter);
			card.getPlayer().addPenaltyCard(card);
		}

	}

	@Override
	public void visit(BoringMeeting card) {
		// Check whether player is allowed to play
		if (!(card.getPlayer().getState() == PlayerState.ON_TURN)) {
			return;
		}

		game.incSpecialCardsPlayed();

		// For the case, nobody can react
		List<Player> players = game.getAllPlayers().stream().filter(p -> p.isAlive()).collect(Collectors.toList());
		int counter = players.size();
		Collections.sort(players, new ComperatorById(card.getPlayer().getId(), players.size()));

		for (Player p : players) {
			boolean canReact = false;
			for (TurnCard t : p.getHandcards()) {
				if (t instanceof BugReport) {
					canReact = true;
					p.setState(PlayerState.BORING_MEETING);
					p.notifyView(card.getPlayer().getName()
							+ " started boring meeting. Play a bug card or lose a mental health point!");
					break;
				}
			}
			if (!canReact) {
				counter--;
				p.notifyView(card.getPlayer().getName()
						+ " started boring meeting. You lost a mental health point, because you don't have any bug card.");
				game.decHp(p, card.getPlayer());
				if (p.getState() != PlayerState.DEAD) {
					p.setState(PlayerState.WAIT);
				}
			}
		}
		// if card effect is automatically over, because no one can react.
		if (counter == 0) {
			game.throwCardToHeap(card);
			if (card.getPlayer().getState() == PlayerState.WAIT) {
				card.getPlayer().setState(PlayerState.ON_TURN);
			} else {
				// case the player is dead
				game.tryEndTurn(card.getPlayer());
			}
		} else {
			card.getPlayer().removeHandcard(card);
			card.getPlayer().addPenaltyCard(card);
			game.setEventCard(card);
			game.setWaitingGlobalAttackCard(counter);
		}

	}

	@Override
	public void visit(StandupMeeting standupMeeting) {

		if (standupMeeting.getPlayer().getState() == PlayerState.ON_TURN) {
			if (standupMeeting.getPlayer().equals(standupMeeting.getTargetPlayer())) {
				standupMeeting.getPlayer().removeHandcard(standupMeeting);
				standupMeeting.getTargetPlayer().addPenaltyCard(standupMeeting);

				standupMeeting.getPlayer().notifyView("You started a standup meeting. Draw one card!");

				int countPlayers = 0;

				for (Player p : game.getAllPlayers()) {
					if (p.getState() != PlayerState.DEAD) {
						countPlayers++;
					}
				}

				Player currentPlayer = standupMeeting.getPlayer();

				game.setStandupMeetingCard(standupMeeting);
				game.drawCardsForStandupMeeting(countPlayers);
				game.incSpecialCardsPlayed();
				currentPlayer.setState(PlayerState.STANDUP_MEETING);

			} else {
				standupMeeting.getPlayer().notifyView("You must play Standup Meeting on yourself.");
			}
		}
	}

	@Override
	public void visit(WearsTieAtWork card) {
		if (this.canPlayCardOnYourself(card)) {
			Player player = card.getPlayer();
			if (player.getOldPrestigeCard(WearsTieAtWork.class).isPresent()) {
				TurnCard oldTie = player.getOldPrestigeCard(WearsTieAtWork.class).get();
				player.notifyView("You can only wear one tie at work. You threw away your old tie.");
				game.throwCardToHeap(oldTie);
			} else {

				player.notifyView("You're now wearing a tie at work.");
			}
			player.getCharacter().addSelfPrestigeModifier();
			game.incAbilityCardsPlayed();
			player.removeHandcard(card);
			player.addBuffCard(card);

		}
	}

	@Override
	public void visit(WearsSunglassesAtWork card) {
		if (this.canPlayCardOnYourself(card)) {
			Player player = card.getPlayer();
			if (player.getOldPrestigeCard(WearsSunglassesAtWork.class).isPresent()) {
				TurnCard oldSunglas = player.getOldPrestigeCard(WearsSunglassesAtWork.class).get();
				player.notifyView("You can only wear one sunglass at work. You threw away your old sunglass.");
				game.throwCardToHeap(oldSunglas);
			} else {
				player.notifyView("You're now wearing a sunglass at work.");
			}
			player.getCharacter().addOthersPrestigeModifier();
			game.incAbilityCardsPlayed();
			player.removeHandcard(card);
			player.addBuffCard(card);
		}

	}

	@Override
	public void visit(Nasa card) {
		this.playPreviousJobCard(card);

	}

	@Override
	public void visit(Google card) {
		this.playPreviousJobCard(card);

	}

	@Override
	public void visit(Microsoft card) {
		this.playPreviousJobCard(card);

	}

	@Override
	public void visit(Accenture card) {
		if (this.playPreviousJobCard(card)) {
			card.getPlayer().getCharacter().enablePlayMultipleBugs();
		}
	}

	private boolean canPlayCardOnYourself(TurnCard card) {
		if (canPlayCard(card)) {
			if (card.getTargetPlayer().equals(card.getPlayer())) {

				return true;
			}
			card.getPlayer().notifyView("You must play " + card.getName() + " on yourself!");
		}
		return false;
	}

	private boolean canPlayCard(TurnCard card) {
		if (card.getPlayer().getState() == PlayerState.ON_TURN) {
			return true;
		}
		card.getPlayer().notifyView("You can not play " + card.getName() + ", because it's not your turn!");
		return false;
	}

	private boolean playPreviousJobCard(PreviousJob card) {
		Player player = card.getPlayer();
		if (canPlayCardOnYourself(card)) {

			if (player.getOldPrestigeCard(PreviousJob.class).isPresent()) {
				TurnCard oldPrevJob = player.getOldPrestigeCard(PreviousJob.class).get();
				player.notifyView("You replaced your previous job at " + oldPrevJob.getName() + " with a new job at"
						+ card.getName() + ".");
				game.throwCardToHeap(oldPrevJob);
			} else {
				player.notifyView("Your previous job was at " + card.getName() + ".");
			}

			player.getCharacter().setPrestige(card.getPrestigeModifier());
			game.incAbilityCardsPlayed();
			player.removeHandcard(card);
			player.addBuffCard(card);
			return true;

		}
		return false;
	}

	private void endSystemIntegration(Player looser, Player winner) {

		systemInegretation.getTargetPlayer().setState(PlayerState.WAIT);
		systemInegretation.getPlayer().setState(PlayerState.ON_TURN);
		game.throwCardToHeap(systemInegretation);
		game.throwAllBugsFromSystemIntegrationToHeap(looser, winner);
		systemInegretation = null;
		game.decHp(looser, winner);

	}

	public void updateSystemIntegrationParticipated(Player target, Player initiator) {
		game.getUser(target).incrementSystemIntegrationParticipated(TBIALApplication.getDatabase());
		game.getUser(initiator).incrementSystemIntegrationParticipated(TBIALApplication.getDatabase());
	}

	public void endSystemIntegration(Player loser) {
		Player winner = systemInegretation.getPlayer().equals(loser) ? systemInegretation.getTargetPlayer()
				: systemInegretation.getPlayer();
		endSystemIntegration(loser, winner);

	}

}