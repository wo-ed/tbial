package de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob;

import de.lmu.ifi.sosy.tbial.model.abilitycards.PreviousJob;
import de.lmu.ifi.sosy.tbial.model.visitor.CardVisitor;

public class Microsoft extends PreviousJob {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Microsoft() {
		super("Microsoft\n (prev. Job)");
		this.prestigeModifier = 1;
	}

	@Override
	public void play(CardVisitor v) {
		v.visit(this);
	}

}
