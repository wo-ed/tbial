package de.lmu.ifi.sosy.tbial.db;

import static java.util.Objects.requireNonNull;

import java.io.Serializable;

import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.util.Utils;

/**
 * 
 * A user with a user name and a plain-text password.
 * 
 * @author Andreas Schroeder, SWEP 2013 Team.
 * 
 */
public class User implements Serializable {

	/**
	 * UID for serialization.
	 */
	private static final long serialVersionUID = 1L;

	private int id = -1;

	private String name;

	private String salt;

	private String hash;

	private boolean isActive = false;

	private int gamesWon = 0;

	private int gamesLost = 0;

	private int gamesParticipated = 0;

	private int bugsPlayed = 0;

	private int systemIntegrationParticipated = 0;
	
	private boolean isAdmin;

	public User(String name, String password) {
		this(-1, name, password, false);
	}

	public User(int id, String name, String password, boolean isAdmin) {
		this(id, name, isAdmin, Utils.generateSalt(), "", 0, 0, 0, 0, 0, false);
		this.hash = Utils.hashPasswordWithSalt(requireNonNull(password), salt);
		this.isAdmin = isAdmin;
	}

	public User(int id, String name, boolean isAdmin, String salt, String hash, int gamesWon, int gamesLost, int gamesParticipated,
			int bugsPlayed, int systemIntegrationParticipated, boolean isActive) {
		this.id = id;
		this.name = requireNonNull(name);
		this.salt = requireNonNull(salt);
		this.hash = requireNonNull(hash);
		this.isActive = isActive;
		this.gamesWon = gamesWon;
		this.gamesLost = gamesLost;
		this.gamesParticipated = gamesParticipated;
		this.bugsPlayed = bugsPlayed;
		this.systemIntegrationParticipated = systemIntegrationParticipated;
		this.isAdmin = isAdmin;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = requireNonNull(name);
	}

	/**
	 * Returns the database id of the user. This id is only used for persistence
	 * and is only set when a user object is written or read form the database.
	 * The id is not included in {@link #equals(Object)} and {@link #hashCode()}
	 * .
	 * 
	 * @return the database id of the user.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the user's persistence id. This is package private so only database
	 * implementations can use it.
	 * 
	 * @param id
	 *            the user's persistence id.
	 */
	void setId(int id) {
		this.id = id;
	}

	public String getSalt() {
		return salt;
	}

	public String getHash() {
		return hash;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public int getGamesWon() {
		return gamesWon;
	}

	public int getGamesLost() {
		return gamesLost;
	}

	public int getGamesParticipated() {
		return gamesParticipated;
	}

	public int getBugsPlayed() {
		return bugsPlayed;
	}

	public int getSystemIntegrationParticipated() {
		return systemIntegrationParticipated;
	}

	public void incrementGamesWon(Database db) {
		if (TBIALApplication.DEBUG_MODE) {
			return;
		}
		gamesWon++;
		updateUserInDB(db);
	}

	public void incrementGamesLost(Database db) {
		if (TBIALApplication.DEBUG_MODE) {
			return;
		}
		gamesLost++;
		updateUserInDB(db);
	}

	public void incrementGamesParticipated(Database db) {
		if (TBIALApplication.DEBUG_MODE) {
			return;
		}
		gamesParticipated++;
		updateUserInDB(db);
	}

	public void incrementBugsPlayed(Database db) {
		if (TBIALApplication.DEBUG_MODE) {
			return;
		}
		bugsPlayed++;
		updateUserInDB(db);
	}

	public void incrementSystemIntegrationParticipated(Database db) {
		if (TBIALApplication.DEBUG_MODE) {
			return;
		}
		systemIntegrationParticipated++;
		updateUserInDB(db);
	}

	private void updateUserInDB(Database db) {
		db.updateUser(this);
	}
	
	public boolean isAdmin() {
		return isAdmin;
	}
	
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	
	public boolean checkPassword(String inputPassword) {
		String inputHash = Utils.hashPasswordWithSalt(inputPassword, getSalt());
		return inputHash.equals(getHash());
	}
}
