package de.lmu.ifi.sosy.tbial.model.actioncards;

import de.lmu.ifi.sosy.tbial.model.ActionCard;
import de.lmu.ifi.sosy.tbial.model.visitor.CardVisitor;

public final class PersonalCoffeeMachine extends ActionCard {

	private static final long serialVersionUID = 1L;

	public PersonalCoffeeMachine() {
		setName("Personal Coffee Machine");
	}

	@Override
	public void play(CardVisitor v) {
		v.visit(this);

	}

}
