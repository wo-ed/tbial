package de.lmu.ifi.sosy.tbial.pages;

import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.AjaxSelfUpdatingTimerBehavior;
import org.apache.wicket.ajax.attributes.AjaxCallListener;
import org.apache.wicket.ajax.attributes.AjaxRequestAttributes;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.time.Duration;

import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.TBIALSession;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;

public class AdminPlayerListPanel extends Panel {

	private static final long serialVersionUID = 1L;

	private WebMarkupContainer userContainer;
	private DataView<User> userView;

	private List<User> searchCriteriaUsers;

	private final TextField<String> searchField;

	public AdminPlayerListPanel(String id, TabPage page) {
		super(id);
		Form<?> form = new Form<>("searchForm");
		searchField = new TextField<>("searchUser", new Model<>(""));

		OnChangeAjaxBehavior onNameChange = new OnChangeAjaxBehavior() {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				List<User> users = TBIALApplication.getUsers();
				searchCriteriaUsers = Collections.synchronizedList(new LinkedList<User>());
				String name = searchField.getModelObject();
				if (name == null || name.equals("")) {
					searchCriteriaUsers.addAll(users);
				} else {
					for (User user : users) {
						// Ignore caps.
						if (user.getName().toLowerCase().startsWith(name.toLowerCase())) {
							searchCriteriaUsers.add(user);
						}
					}

				}
			}
		};

		searchField.add(onNameChange);
		form.add(searchField);
		add(form);
		addComponents();
	}

	public void addComponents() {
		userContainer = new WebMarkupContainer("userContainer");
		userContainer.setOutputMarkupId(true);

		userView = new DataView<User>("userList", new ListDataProvider<>(TBIALApplication.getUsers())) {

			private static final long serialVersionUID = 1L;

			@Override
			public void populateItem(Item<User> item) {
				User user = item.getModelObject();
				Label name = new Label("name", user.getName());
				Label state = new Label("state", getUserGameState(user));
				item.setOutputMarkupId(true);

				User u = ((TBIALSession) getSession()).getUser();
				String linkStr = user.isAdmin() ? "revoke" : "grant";

				MarkupContainer link = new Link<String>("admin") {
					private static final long serialVersionUID = 1L;

					@Override
					public void onClick() {
						try {
							TBIALApplication.getDatabase().updateAdminRights(user, !user.isAdmin());
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}.add(new Label("label", linkStr));
				
				AjaxLink<String> delete = new ConfirmationLink<String>("delete", "Deleting player " + user.getName() + ". Are you sure?") {
					private static final long serialVersionUID = 1L;
					@Override
					public void onClick(AjaxRequestTarget target) {
						TBIALApplication.deleteUser(user);						
					}
				};
				
				item.add(delete);
				delete.setVisible(u != user);

				// Check search criteria.
				boolean hideUser = searchCriteriaUsers != null && !searchCriteriaUsers.contains(user);
				name.setVisible(!hideUser);
				state.setVisible(!hideUser);
				link.setVisible(!hideUser && u != user);

				item.add(name);
				item.add(state);
				item.add(link);

			}

		};

		userContainer.add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(1)));
		userContainer.add(userView);

		add(userContainer);

	}

	/**
	 * findet heraus, ob user in game ist oder noch im Lobby ist.
	 * 
	 * @param u
	 *            User
	 * @return "Free", wenn User im Lobby ist. Sonst nach GameState von
	 *         zugehoerigen Spiel
	 */
	public String getUserGameState(User u) {
		Game g = TBIALApplication.getInstance().findGame(u);
		if (g == null) {
			return "in lobby";
		} else {
			switch (g.getState()) {
			case RUNNING:
				return "in activ game";
			case WAITING:
				return "waiting in game lobby";
			case PAUSED:
				return "in paused game";
			case GAMEOVER:
				return "in lobby";
			default:
				return null;
			}
		}
	}

	public void sendInvitation(User u) {
		TBIALSession s = (TBIALSession) getSession();
		TBIALApplication.sendInvitation(u.getName(), s.getUser());
	}

	public WebMarkupContainer getUserContainer() {
		return userContainer;
	}
	
	public abstract class ConfirmationLink<T> extends AjaxLink<T>
	{
	    private static final long serialVersionUID = 1L;
	    private final String text;
	 
	    public ConfirmationLink( String id, String text)
	    {
	        super( id );
	        this.text = text;
	    }
	 
	    @Override
	    protected void updateAjaxAttributes( AjaxRequestAttributes attributes )
	    {
	        super.updateAjaxAttributes( attributes );
	 
	        AjaxCallListener ajaxCallListener = new AjaxCallListener();
	        ajaxCallListener.onPrecondition( "return confirm('" + text + "');" );
	        attributes.getAjaxCallListeners().add( ajaxCallListener );
	    }
	}
}
