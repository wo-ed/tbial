package de.lmu.ifi.sosy.tbial.util;

import java.util.Collection;
import java.util.HashMap;

import de.lmu.ifi.sosy.tbial.db.User;

/**
 * Stores all VerificationTokens
 * 
 * @author Stephan
 *
 */
public class TokenStorage extends HashMap<String, VerificationToken> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Stores a VerificationToken
	 * 
	 * @param vt
	 *            to store
	 */
	public void store(VerificationToken vt) {
		this.put(vt.getToken(), vt);
	}

	/**
	 * Gets a stored Token by its tokenString
	 * 
	 * @param token
	 *            in String representation
	 * @return VerificationToken
	 */
	public VerificationToken getFromString(String token) {
		return super.get(token);
	}

	/**
	 * Stores a Collection of VerificationToken
	 * 
	 * @param tokens
	 *            Collection to Store
	 * 
	 */
	public void storeAll(Collection<VerificationToken> tokens) {
		tokens.stream().forEach(t -> this.put(t.getToken(), t));
	}

	/**
	 * removes token
	 * 
	 * @param vt
	 *            - to remove
	 */
	public void deleteToken(VerificationToken vt) {
		this.remove(vt.getToken());
	}

	/**
	 * returns token corresponding to user
	 * 
	 * @param user
	 *            - corresponding user
	 * @return token
	 */
	public VerificationToken getFromUser(User user) {
		for (VerificationToken vt : values()) {
			if (vt.getUser().getName().equals(user.getName()))
				return vt;
		}
		return null;

	}

}
