package de.lmu.ifi.sosy.tbial.model.actioncards;

import de.lmu.ifi.sosy.tbial.model.ActionCard;
import de.lmu.ifi.sosy.tbial.model.visitor.CardVisitor;

public final class BugReport extends ActionCard {

	private static final long serialVersionUID = 1L;


	public BugReport(String name) {
		setName(name);
	}

	@Override
	public void play(CardVisitor v) {
		v.visit(this);
	}
}
