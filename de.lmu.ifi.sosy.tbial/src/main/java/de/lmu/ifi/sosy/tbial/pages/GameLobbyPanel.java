package de.lmu.ifi.sosy.tbial.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.AjaxSelfUpdatingTimerBehavior;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.util.time.Duration;

import de.lmu.ifi.sosy.tbial.AuthenticationRequired;
import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.GameState;
import de.lmu.ifi.sosy.tbial.util.Message;

@AuthenticationRequired
public class GameLobbyPanel extends Panel {

	/**
	 * UID for serialization.
	 */
	private static final long serialVersionUID = 1L;

	private PropertyModel<String> gameNameModel;
	private Label gameNameLabel;

	private Label privacyLabel;

	private IModel playerListModel;
	private IModel freePlayerModel;

	private WebMarkupContainer playerContainer;

	private ListView<String> playerList;
	private ListView<Integer> freePlayers;

	private AjaxSubmitLink leaveButton;

	private AjaxSubmitLink startButton;

	private TabPage page;

	public GameLobbyPanel(String id, TabPage page) {
		super(id);
		this.page = page;
		this.initialize();
	}

	private void initialize() {
		addComponents();
	}

	public void addComponents() {

		// Name
		gameNameModel = new PropertyModel<>(getGame(), "Name");
		gameNameLabel = new Label("gameName", gameNameModel);
		add(gameNameLabel);

		// Privacy
		if (getGame().isPrivate()) {
			privacyLabel = new Label("gamePrivacy", "private");
		} else {
			privacyLabel = new Label("gamePrivacy", "public");
		}
		add(privacyLabel);

		// Player List
		playerListModel = new LoadableDetachableModel() {
			private static final long serialVersionUID = 1L;

			@Override
			protected Object load() {
				List<String> result = new ArrayList<>();
				getGame().getPlayers().forEach(p -> result.add(p.getName()));
				return result;
			}
		};

		playerList = new ListView<String>("playerList", playerListModel) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<String> item) {
				String player = item.getModelObject();
				item.add(new Label("playerName", player));
			}
		};

		// Free Player List
		freePlayerModel = new LoadableDetachableModel() {
			private static final long serialVersionUID = 1L;

			@Override
			protected Object load() {
				List<Integer> freePlayerList = new ArrayList<>();
				for (int i = getGame().getPlayers().size() + 1; i <= getGame().getCapacity(); i++) {
					freePlayerList.add(i);
				}
				return freePlayerList;
			}
		};

		freePlayers = new ListView<Integer>("freePlayers", freePlayerModel) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Integer> item) {
				item.add(new Label("placeholder", "?"));
			}
		};

		// Player Container
		playerContainer = new WebMarkupContainer("playerContainer");
		playerContainer.setOutputMarkupId(true);
		playerContainer.add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(1)) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onPostProcessTarget(AjaxRequestTarget target) {
				super.onPostProcessTarget(target);
				if (getGame().isStarted()) {
					showGamePage();
				}
			}
		});

		playerContainer.add(playerList, freePlayers);
		add(playerContainer);

		// Form (Leave + Start)
		Form<?> form = new Form<>("lobbyForm");
		leaveButton = new AjaxSubmitLink("leaveButton") {
			/**
			 * UID for serialization.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				if (!getGame().isStarted()) {
					leaveGame(page.getUser());
				}
			};
		};

		startButton = new AjaxSubmitLink("startButton") {
			/**
			 * UID for serialization.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				startGame();
			}

		};

		form.add(leaveButton).add(startButton);

		add(form);

		form.add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(1)) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected final void onPostProcessTarget(final AjaxRequestTarget target) {
				// also update visibility of players role
				startButton.setVisible(userCanStartGame());
			}

		});
		startButton.setVisible(userCanStartGame());
	}

	public Game getGame() {
		return TBIALApplication.getInstance().findGame(page.getUser());
	}

	public boolean startGame() {
		if (!userCanStartGame()) {
			return false;
		}

		getGame().init();

		getGame().storeGameStatsInDB(TBIALApplication.getDatabase());

		showGamePage();
		return true;
	}

	public boolean userCanStartGame() {
		return getGame() != null && getGame().userCanStartGame(page.getUser())
				&& getGame().getState() == GameState.WAITING;
	}

	private void showGamePage() {
		setResponsePage(GamePage.create());
	}

	public void leaveGame(User user) {
		TBIALApplication.getInstance().leaveGame(user, getGame());
		page.removeGameLobbyTab();
	}

	protected void sendMsg(String txt) {
		User sender = page.getUser();
		Message chatMessage = new Message(txt, sender, getGame().getName());
		TBIALApplication.addMessage(chatMessage);
	}

	protected IModel<List<Message>> getMessagesToBeShown() {
		LoadableDetachableModel<List<Message>> model = new LoadableDetachableModel<List<Message>>() {
			private static final long serialVersionUID = 1L;

			@Override
			protected List<Message> load() {
				List<Message> messages = TBIALApplication.getMessagesForUsername(page.getUser().getName(),
						getGame().getName());
				// TODO whispering
				// if (getUser() != null) {
				// messages.addAll(TBIALApplication.getMessagesForKey(getUser().getName()));
				// }
				return messages;
			}
		};
		return model;
	}
}