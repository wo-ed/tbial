package de.lmu.ifi.sosy.tbial.model;

public enum GameState {

	RUNNING, WAITING, PAUSED, GAMEOVER;
}
