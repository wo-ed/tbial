package de.lmu.ifi.sosy.tbial.model.characters;

public class TerryWeissman extends CharacterCard {

	private static final long serialVersionUID = 1L;

	public TerryWeissman() {
		super("Terry Weissman");
		this.hasBugDelegation = true;
	}

	@Override
	public boolean hasBugDelegation() {
		return true;
	}

	@Override
	public void giveBugDelegation(boolean enable) {
		this.hasBugDelegation = true;
	}
}
