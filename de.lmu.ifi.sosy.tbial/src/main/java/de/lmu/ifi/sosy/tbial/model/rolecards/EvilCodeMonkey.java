package de.lmu.ifi.sosy.tbial.model.rolecards;

import de.lmu.ifi.sosy.tbial.model.RoleCard;

public final class EvilCodeMonkey extends RoleCard {

	private static final long serialVersionUID = 1L;

	public EvilCodeMonkey() {
		setName("Evil Code Monkey");
	}
}
