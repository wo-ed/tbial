package de.lmu.ifi.sosy.tbial.model.actioncards;

import de.lmu.ifi.sosy.tbial.model.ActionCard;
import de.lmu.ifi.sosy.tbial.model.visitor.CardVisitor;

public final class LANParty extends ActionCard {

	private static final long serialVersionUID = 1L;

	public LANParty() {
		setName("LAN Party");
	}

	@Override
	public void play(CardVisitor v) {
		v.visit(this);

	}

}
