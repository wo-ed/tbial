package de.lmu.ifi.sosy.tbial.model.rolecards;

import de.lmu.ifi.sosy.tbial.model.RoleCard;

public final class HonestDeveloper extends RoleCard {

	private static final long serialVersionUID = 1L;

	public HonestDeveloper() {
		setName("Honest Developer");
	}
}
