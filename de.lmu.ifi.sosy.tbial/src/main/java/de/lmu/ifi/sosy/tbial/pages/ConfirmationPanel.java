package de.lmu.ifi.sosy.tbial.pages;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;

public abstract class ConfirmationPanel extends Panel {
	private static final long serialVersionUID = 1L;

	public ConfirmationPanel(String id, String message) {
		super(id);
		add(new Label("message", message));
		add(new Link<Void>("cancel") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				onCancel();
			}
		});
		add(new Link<Void>("confirm") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				onConfirm();
			}
		});
	}

	protected abstract void onCancel();

	protected abstract void onConfirm();

}
