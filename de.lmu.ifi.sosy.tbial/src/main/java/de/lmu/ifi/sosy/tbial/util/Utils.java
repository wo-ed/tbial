package de.lmu.ifi.sosy.tbial.util;

import java.awt.Color;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Random;

import javax.xml.bind.DatatypeConverter;

public class Utils {

	/**
	 * Generates SHA-256 hash with salt using the java.security package
	 * 
	 * @param password
	 *            The password to be hashed
	 * @param salt
	 *            The salt to season the password
	 * @return The hash representing the salted password
	 */
	public static String hashPasswordWithSalt(String password, String salt) {

		return hashString(password + salt);
	}

	/**
	 * Generates SHA-256 hash for String
	 * 
	 * @param toHash
	 *            String to be hashed
	 * @return hashed string
	 */
	public static String hashString(String toHash) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(toHash.getBytes());
			byte[] digest = md.digest();
			String hash = DatatypeConverter.printHexBinary(digest).toUpperCase();

			return hash;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Generates a random 32bit String safe for usage as salt for passwords
	 * 
	 * @return new Random String
	 */
	public static String generateSalt() {
		final Random r = new SecureRandom();
		byte[] salt = new byte[32];
		r.nextBytes(salt);

		return Base64.getEncoder().encodeToString(salt);
	}
	

	/**
	 * Converts a non-transparent color to a hex string.
	 * 
	 * @param color
	 * 				The color to be converted
	 * @return The hex color string
	 */
	public static String colorToHex(Color color) {
		return String.format("#%02x%02x%02x", 
				color.getRed(), color.getGreen(), color.getBlue());
	}
	
	/**
	 * Creates either the color black or white, depending on which one is better visible
	 * on the input color.
	 * 
	 * Can be used to make font visible on a random background color.
	 * 
	 * @param color
	 * 				The color to be converted
	 * @return The contrast color
	 */
	public static Color getContrastColor(Color color) {
		  double y = (299 * color.getRed() + 587 * color.getGreen() + 114 * color.getBlue()) / 1000;
		  return y >= 128 ? Color.black : Color.white;
	}
}
