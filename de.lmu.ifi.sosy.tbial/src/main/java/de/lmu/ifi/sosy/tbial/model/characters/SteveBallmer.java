package de.lmu.ifi.sosy.tbial.model.characters;

import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.actioncards.LameExcuse;

public class SteveBallmer extends CharacterCard {

	private static final long serialVersionUID = 1L;

	public SteveBallmer() {
		super("Steve Ballmer");
	}

	@Override
	public boolean isLameExcuce(TurnCard tc) {
		return (LameExcuse.class.isInstance(tc)) || (BugReport.class.isInstance(tc));
	}

	@Override
	public boolean isBugReport(TurnCard tc) {
		return (LameExcuse.class.isInstance(tc)) || (BugReport.class.isInstance(tc));
	}

}
