package de.lmu.ifi.sosy.tbial.db;

import java.sql.SQLException;
import java.util.List;

import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.util.VerificationToken;

/**
 * 
 * The interface offered by the database (or, more precisely, the data access
 * layer).
 * 
 * @author Andreas Schroeder, SWEP 2013 Team.
 * 
 */
public interface Database {

	/**
	 * Retrieves the user with the given name.
	 * 
	 * @param name
	 *            should not be null.
	 * @return the user with the given name.
	 */
	User getUser(String name);

	/**
	 * Returns whether a user with the given name exits.
	 * 
	 * @param name
	 *            should not be null.
	 * @return {@code true} if the name is already taken, {@code false}
	 *         otherwise.
	 */
	boolean nameTaken(String name);

	/**
	 * Registers a new user with the given name and password.
	 * 
	 * @param name
	 *            should not be null
	 * @param password
	 *            should not be null
	 * @param token
	 *            token to activate account
	 * @return a new user object or {@code null}, if a user with the given name
	 *         already exists in the database.
	 */
	User register(String name, String password);

	/**
	 * Write VerificationToken to Database
	 * 
	 * @param id
	 * @param token
	 */
	void storeToken(int id, String token);

	/**
	 * Activate Useraccount
	 * 
	 * @param user
	 * @return
	 */
	boolean activateUser(User user);

	/**
	 * Get all not yet used Tokens
	 * 
	 * @return
	 */
	List<VerificationToken> getVerificationTokens();

	/**
	 * Updates the database entry of the user with the current values
	 * 
	 * @param user
	 *            The user in the model representation that should be updated in
	 *            the db
	 */
	void updateUser(User user);

	
	/**
	 * Grants admin rights to the given user.
	 * @param user
	 * @throws SQLException 
	 */
	void updateAdminRights(User user, boolean grant) throws SQLException;

	/**
	 * Deletes the database entry of the user with the given id.
	 * 
	 * @param id
	 *            The user id
	 */
	void deleteUser(int id);
	
	/**
	 * Stores game statistics in database.
	 * 
	 * @param game
	 */
	boolean storeGameStats(Game game);

	/**
	 * Updates game statistics in database.
	 * 
	 * @param game
	 */
	boolean updateGameStats(Game game);

	/**
	 * Returns the game stats for the given game id.
	 * 
	 * @param gameId
	 * @return
	 */
	GameStat getGameStat(Integer gameId);

	/**
	 * Returns the health points for the given game and the given user id.
	 * 
	 * @param gameId
	 * @param userId
	 * @return
	 */
	HealthPoints getHealthPoints(Integer gameId, Integer userId);

	/**Deletes the given user from the db..
	 * @param user
	 * @throws SQLException 
	 */
	void deleteUser(User user) throws SQLException;
}
