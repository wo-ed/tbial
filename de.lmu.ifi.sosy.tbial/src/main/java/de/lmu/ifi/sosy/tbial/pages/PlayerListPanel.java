package de.lmu.ifi.sosy.tbial.pages;

import java.sql.SQLException;

import org.apache.wicket.ajax.AjaxSelfUpdatingTimerBehavior;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.util.time.Duration;

import de.lmu.ifi.sosy.tbial.TBIALApplication;
import de.lmu.ifi.sosy.tbial.TBIALSession;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;

public class PlayerListPanel extends Panel {

	private static final long serialVersionUID = 1L;

	private WebMarkupContainer userContainer;
	private DataView<User> userView;

	public PlayerListPanel(String id, TabPage page) {
		super(id);
		addComponents();
	}

	public void addComponents() {
		userContainer = new WebMarkupContainer("userContainer");
		userContainer.setOutputMarkupId(true);

		userView = new DataView<User>("userList", new ListDataProvider<>(TBIALApplication.getUsers())) {

			private static final long serialVersionUID = 1L;

			@Override
			public void populateItem(Item<User> item) {
				User user = item.getModelObject();
				item.add(new Label("name", user.getName()));
				item.add(new Label("state", getUserGameState(user)));
				
				item.setOutputMarkupId(true);
				item.add(new Link<String>("sendInvite") {
					private static final long serialVersionUID = 1L;
					
					@Override
					public void onClick() {
						sendInvitation(user);
					}
				});
			}

		};

		userContainer.add(new AjaxSelfUpdatingTimerBehavior(Duration.seconds(1)));
		userContainer.add(userView);

		add(userContainer);

	}

	/**
	 * findet heraus, ob user in game ist oder noch im Lobby ist.
	 * 
	 * @param u
	 *            User
	 * @return "Free", wenn User im Lobby ist. Sonst nach GameState von
	 *         zugehoerigen Spiel
	 */
	public String getUserGameState(User u) {
		Game g = TBIALApplication.getInstance().findGame(u);
		if (g == null) {
			return "in lobby";
		} else {
			switch (g.getState()) {
			case RUNNING:
				return "in activ game";
			case WAITING:
				return "waiting in game lobby";
			case PAUSED:
				return "in paused game";
			case GAMEOVER:
				return "in lobby";
			default:
				return null;
			}
		}
	}

	public void sendInvitation(User u){
		TBIALSession s = (TBIALSession)getSession();
		TBIALApplication.sendInvitation(u.getName(), s.getUser());
	}

	public WebMarkupContainer getUserContainer() {
		return userContainer;
	}
	
}
