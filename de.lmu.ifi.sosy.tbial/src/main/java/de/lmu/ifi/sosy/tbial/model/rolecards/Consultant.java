package de.lmu.ifi.sosy.tbial.model.rolecards;

import de.lmu.ifi.sosy.tbial.model.RoleCard;

public final class Consultant extends RoleCard {

	private static final long serialVersionUID = 1L;

	public Consultant() {
		setName("Consultant");
	}
}
