package de.lmu.ifi.sosy.tbial.pages;

import java.util.HashMap;
import java.util.Map;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

import de.lmu.ifi.sosy.tbial.AuthenticationRequired;
import de.lmu.ifi.sosy.tbial.util.TypesForFilters;

@AuthenticationRequired
public class FilterPanel extends Panel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Form<?> playersForm = new Form<>("playersForm");
	private Button player0;
	private Button player1;
	private Button player2;
	private Button player3;
	private Button player4;
	private Button player5;
	private Button player6;
	private Button player7;

	private boolean player0Chosen;
	private boolean player1Chosen;
	private boolean player2Chosen;
	private boolean player3Chosen;
	private boolean player4Chosen;
	private boolean player5Chosen;
	private boolean player6Chosen;
	private boolean player7Chosen;

	private Form<?> stateForm = new Form<>("stateForm");
	private Button stateWaiting;
	private Button stateRunning;
	private Button statePaused;

	private boolean stateWaitingChosen;
	private boolean stateRunningChosen;
	private boolean statePausedChosen;

	private Form<?> maxPlayersForm = new Form<>("maxPlayersForm");
	private Button maxPlayers4;
	private Button maxPlayers5;
	private Button maxPlayers6;
	private Button maxPlayers7;

	private boolean maxPlayers4Chosen;
	private boolean maxPlayers5Chosen;
	private boolean maxPlayers6Chosen;
	private boolean maxPlayers7Chosen;

	private Form<?> typeForm = new Form<>("typeForm");
	private Button typePrivate;
	private Button typePublic;

	private boolean typePrivateChosen = false;
	private boolean typePublicChosen = false;

	private Form<?> controlsForm = new Form<>("controlsForm");
	private Button controlCancel;
	private Button controlApply;

	private Map<TypesForFilters, String> filters = new HashMap<TypesForFilters, String>();
	private String players = "empty";
	private String state = "empty";
	private String maxPlayers = "empty";
	private String type = "empty";

	private TabPage page;

	public FilterPanel(String id, TabPage page, Map<TypesForFilters, String> filter) {
		super(id);
		this.filters.clear();
		this.filters.putAll(filter);

		if (filters.isEmpty()) {
			filters.put(TypesForFilters.PLAYERS, players);
			filters.put(TypesForFilters.STATE, state);
			filters.put(TypesForFilters.MAXPLAYERS, maxPlayers);
			filters.put(TypesForFilters.PRIVACYTYPE, type);
		} else {
			players = filters.get(TypesForFilters.PLAYERS);
			state = filters.get(TypesForFilters.STATE);
			maxPlayers = filters.get(TypesForFilters.MAXPLAYERS);
			type = filters.get(TypesForFilters.PRIVACYTYPE);

		}

		initializePlayersButtons();
		initializeStateButtons();
		initializeMaxPlayersButtons();
		initializeTypeButtons();
		initializeControlsButtons();
		this.page = page;

	}

	private void initializeControlsButtons() {
		controlCancel = new Button("controlCancel") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				// page.getCurrentLobby();
				page.getCurrentLobby().filterCancel("lobbyTab", page);
				FilterPanel.this.replaceWith(page.getCurrentLobby());
			}

		};

		controlApply = new Button("controlApply") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				page.getCurrentLobby().filterUpdate("lobbyTab", FilterPanel.this.filters);
				FilterPanel.this.replaceWith(page.getCurrentLobby());
			}

		};

		controlsForm.add(controlCancel);
		controlsForm.add(controlApply);
		add(controlsForm);

	}

	private void initializeTypeButtons() {

		typePrivate = new Button("typePrivate") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				typePrivateChosen = !typePrivateChosen;
				typePublicChosen = false;
				addNoStyles(typePublic);
				if (typePrivateChosen) {
					type = "private";
					addStyleWhenChosen(typePrivate);
				} else {
					type = "empty";
					addNoStyles(typePrivate);
				}
				filters.put(TypesForFilters.PRIVACYTYPE, type);
			}
		};
		typePublic = new Button("typePublic") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				typePublicChosen = !typePublicChosen;
				typePrivateChosen = false;
				addNoStyles(typePrivate);

				if (typePublicChosen) {
					type = "public";
					addStyleWhenChosen(typePublic);
				} else {
					type = "empty";
					addNoStyles(typePublic);
				}
				filters.put(TypesForFilters.PRIVACYTYPE, type);
			}
		};
		if (type.equals("private")) {
			addStyleWhenChosen(typePrivate);
			typePrivateChosen = true;
		} else if (type.equals("public")) {
			addStyleWhenChosen(typePublic);
			typePublicChosen = true;
		}
		typeForm.add(typePrivate);
		typeForm.add(typePublic);
		add(typeForm);

	}

	private void initializeMaxPlayersButtons() {
		maxPlayers4 = new Button("maxPlayers4") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				maxPlayers4Chosen = !maxPlayers4Chosen;
				maxPlayers5Chosen = false;
				maxPlayers6Chosen = false;
				maxPlayers7Chosen = false;
				addNoStyles(maxPlayers5);
				addNoStyles(maxPlayers6);
				addNoStyles(maxPlayers7);
				if (maxPlayers4Chosen) {
					maxPlayers = "4";
					addStyleWhenChosen(maxPlayers4);
				} else {
					maxPlayers = "empty";
					addNoStyles(maxPlayers4);
				}
				filters.put(TypesForFilters.MAXPLAYERS, maxPlayers);
			}

		};

		maxPlayers5 = new Button("maxPlayers5") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				maxPlayers5Chosen = !maxPlayers5Chosen;
				maxPlayers4Chosen = false;
				maxPlayers6Chosen = false;
				maxPlayers7Chosen = false;
				addNoStyles(maxPlayers4);
				addNoStyles(maxPlayers6);
				addNoStyles(maxPlayers7);
				if (maxPlayers5Chosen) {
					maxPlayers = "5";
					addStyleWhenChosen(maxPlayers5);
				} else {
					maxPlayers = "empty";
					addNoStyles(maxPlayers5);
				}
				filters.put(TypesForFilters.MAXPLAYERS, maxPlayers);

			}

		};

		maxPlayers6 = new Button("maxPlayers6") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {

				maxPlayers6Chosen = !maxPlayers6Chosen;
				maxPlayers4Chosen = false;
				maxPlayers5Chosen = false;
				maxPlayers7Chosen = false;
				addNoStyles(maxPlayers4);
				addNoStyles(maxPlayers5);
				addNoStyles(maxPlayers7);
				if (maxPlayers6Chosen) {
					maxPlayers = "6";
					addStyleWhenChosen(maxPlayers6);
				} else {
					maxPlayers = "empty";
					addNoStyles(maxPlayers6);
				}
				filters.put(TypesForFilters.MAXPLAYERS, maxPlayers);
			}

		};

		maxPlayers7 = new Button("maxPlayers7") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				maxPlayers7Chosen = !maxPlayers7Chosen;
				maxPlayers4Chosen = false;
				maxPlayers5Chosen = false;
				maxPlayers6Chosen = false;
				addNoStyles(maxPlayers4);
				addNoStyles(maxPlayers5);
				addNoStyles(maxPlayers6);
				if (maxPlayers7Chosen) {
					maxPlayers = "7";
					addStyleWhenChosen(maxPlayers7);
				} else {
					maxPlayers = "empty";
					addNoStyles(maxPlayers7);
				}
				filters.put(TypesForFilters.MAXPLAYERS, maxPlayers);
			}

		};

		if (maxPlayers.equals("4")) {
			addStyleWhenChosen(maxPlayers4);
			maxPlayers4Chosen = true;
		} else if (maxPlayers.equals("5")) {
			addStyleWhenChosen(maxPlayers5);
			maxPlayers5Chosen = true;
		} else if (maxPlayers.equals("6")) {
			addStyleWhenChosen(maxPlayers6);
			maxPlayers6Chosen = true;
		} else if (maxPlayers.equals("7")) {
			addStyleWhenChosen(maxPlayers7);
			maxPlayers7Chosen = true;
		}
		maxPlayersForm.add(maxPlayers4);
		maxPlayersForm.add(maxPlayers5);
		maxPlayersForm.add(maxPlayers6);
		maxPlayersForm.add(maxPlayers7);
		add(maxPlayersForm);

	}

	private void initializeStateButtons() {
		stateWaiting = new Button("stateWaiting") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				stateWaitingChosen = !stateWaitingChosen;
				statePausedChosen = false;
				stateRunningChosen = false;
				addNoStyles(statePaused);
				addNoStyles(stateRunning);
				if (stateWaitingChosen) {
					state = "WAITING";
					addStyleWhenChosen(stateWaiting);
				} else {
					state = "empty";
					addNoStyles(stateWaiting);
				}
				filters.put(TypesForFilters.STATE, state);

			}

		};

		stateRunning = new Button("stateRunning") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				stateRunningChosen = !stateRunningChosen;
				statePausedChosen = false;
				stateWaitingChosen = false;
				addNoStyles(statePaused);
				addNoStyles(stateWaiting);
				if (stateRunningChosen) {
					state = "RUNNING";
					addStyleWhenChosen(stateRunning);
				} else {
					state = "empty";
					addNoStyles(stateRunning);
				}
				filters.put(TypesForFilters.STATE, state);
			}

		};

		statePaused = new Button("statePaused") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				statePausedChosen = !statePausedChosen;
				stateWaitingChosen = false;
				stateRunningChosen = false;
				addNoStyles(stateWaiting);
				addNoStyles(stateRunning);
				if (statePausedChosen) {
					state = "PAUSED";
					addStyleWhenChosen(statePaused);
				} else {
					state = "empty";
					addNoStyles(statePaused);
				}
				filters.put(TypesForFilters.STATE, state);
			}

		};

		if (state.equals("WAITING")) {
			addStyleWhenChosen(stateWaiting);
			stateWaitingChosen = true;
		} else if (state.equals("RUNNING")) {
			addStyleWhenChosen(stateRunning);
			stateRunningChosen = true;
		} else if (state.equals("PAUSED")) {
			addStyleWhenChosen(statePaused);
			statePausedChosen = true;
		}
		stateForm.add(stateWaiting);
		stateForm.add(stateRunning);
		stateForm.add(statePaused);
		add(stateForm);

	}

	private void initializePlayersButtons() {
		player0 = new Button("player0") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				player0Chosen = !player0Chosen;
				player1Chosen = false;
				player2Chosen = false;
				player3Chosen = false;
				player4Chosen = false;
				player5Chosen = false;
				player6Chosen = false;
				player7Chosen = false;
				addNoStyles(player1);
				addNoStyles(player2);
				addNoStyles(player3);
				addNoStyles(player4);
				addNoStyles(player5);
				addNoStyles(player6);
				addNoStyles(player7);
				if (player0Chosen) {
					players = "0";
					addStyleWhenChosen(player0);
				} else {
					players = "empty";
					addNoStyles(player0);
				}
				filters.put(TypesForFilters.PLAYERS, players);
			}

		};

		player1 = new Button("player1") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				player1Chosen = !player1Chosen;
				player0Chosen = false;
				player2Chosen = false;
				player3Chosen = false;
				player4Chosen = false;
				player5Chosen = false;
				player6Chosen = false;
				player7Chosen = false;
				addNoStyles(player0);
				addNoStyles(player2);
				addNoStyles(player3);
				addNoStyles(player4);
				addNoStyles(player5);
				addNoStyles(player6);
				addNoStyles(player7);
				if (player1Chosen) {
					players = "1";
					addStyleWhenChosen(player1);
				} else {
					players = "empty";
					addNoStyles(player1);
				}
				filters.put(TypesForFilters.PLAYERS, players);
			}

		};

		player2 = new Button("player2") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				player2Chosen = !player2Chosen;
				player0Chosen = false;
				player1Chosen = false;
				player3Chosen = false;
				player4Chosen = false;
				player5Chosen = false;
				player6Chosen = false;
				player7Chosen = false;
				addNoStyles(player0);
				addNoStyles(player1);
				addNoStyles(player3);
				addNoStyles(player4);
				addNoStyles(player5);
				addNoStyles(player6);
				addNoStyles(player7);
				if (player2Chosen) {
					players = "2";
					addStyleWhenChosen(player2);
				} else {
					players = "empty";
					addNoStyles(player2);
				}
				filters.put(TypesForFilters.PLAYERS, players);
			}

		};

		player3 = new Button("player3") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				player3Chosen = !player3Chosen;
				player0Chosen = false;
				player1Chosen = false;
				player2Chosen = false;
				player4Chosen = false;
				player5Chosen = false;
				player6Chosen = false;
				player7Chosen = false;
				addNoStyles(player0);
				addNoStyles(player1);
				addNoStyles(player2);
				addNoStyles(player4);
				addNoStyles(player5);
				addNoStyles(player6);
				addNoStyles(player7);
				if (player3Chosen) {
					players = "3";
					addStyleWhenChosen(player3);
				} else {
					players = "empty";
					addNoStyles(player3);
				}
				filters.put(TypesForFilters.PLAYERS, players);
			}

		};

		player4 = new Button("player4") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				player4Chosen = !player4Chosen;
				player0Chosen = false;
				player1Chosen = false;
				player2Chosen = false;
				player3Chosen = false;
				player5Chosen = false;
				player6Chosen = false;
				player7Chosen = false;
				addNoStyles(player0);
				addNoStyles(player1);
				addNoStyles(player2);
				addNoStyles(player3);
				addNoStyles(player5);
				addNoStyles(player6);
				addNoStyles(player7);
				if (player4Chosen) {
					players = "4";
					addStyleWhenChosen(player4);
				} else {
					players = "empty";
					addNoStyles(player4);
				}
				filters.put(TypesForFilters.PLAYERS, players);
			}

		};

		player5 = new Button("player5") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				player5Chosen = !player5Chosen;
				player0Chosen = false;
				player1Chosen = false;
				player2Chosen = false;
				player3Chosen = false;
				player4Chosen = false;
				player6Chosen = false;
				player7Chosen = false;
				addNoStyles(player0);
				addNoStyles(player1);
				addNoStyles(player2);
				addNoStyles(player3);
				addNoStyles(player4);
				addNoStyles(player6);
				addNoStyles(player7);
				if (player5Chosen) {
					players = "5";
					addStyleWhenChosen(player5);
				} else {
					players = "empty";
					addNoStyles(player5);
				}
				filters.put(TypesForFilters.PLAYERS, players);
			}

		};

		player6 = new Button("player6") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				player6Chosen = !player6Chosen;
				player0Chosen = false;
				player1Chosen = false;
				player2Chosen = false;
				player3Chosen = false;
				player4Chosen = false;
				player5Chosen = false;
				player7Chosen = false;
				addNoStyles(player0);
				addNoStyles(player1);
				addNoStyles(player2);
				addNoStyles(player3);
				addNoStyles(player4);
				addNoStyles(player5);
				addNoStyles(player7);
				if (player6Chosen) {
					players = "6";
					addStyleWhenChosen(player6);
				} else {
					players = "empty";
					addNoStyles(player6);
				}
				filters.put(TypesForFilters.PLAYERS, players);
			}

		};

		player7 = new Button("player7") {
			private static final long serialVersionUID = 6061947160574231701L;

			@Override
			public void onSubmit() {
				player7Chosen = !player7Chosen;
				player0Chosen = false;
				player1Chosen = false;
				player2Chosen = false;
				player3Chosen = false;
				player4Chosen = false;
				player5Chosen = false;
				player6Chosen = false;
				addNoStyles(player0);
				addNoStyles(player1);
				addNoStyles(player2);
				addNoStyles(player3);
				addNoStyles(player4);
				addNoStyles(player5);
				addNoStyles(player6);

				if (player7Chosen) {
					players = "7";
					addStyleWhenChosen(player7);
				} else {
					players = "empty";
					addNoStyles(player7);
				}
				filters.put(TypesForFilters.PLAYERS, players);
			}

		};

		if (players.equals("0")) {
			addStyleWhenChosen(player0);
			player0Chosen = true;
		} else if (players.equals("1")) {
			addStyleWhenChosen(player1);
			player1Chosen = true;
		} else if (players.equals("2")) {
			addStyleWhenChosen(player2);
			player2Chosen = true;
		} else if (players.equals("3")) {
			addStyleWhenChosen(player3);
			player3Chosen = true;
		} else if (players.equals("4")) {
			addStyleWhenChosen(player4);
			player4Chosen = true;
		} else if (players.equals("5")) {
			addStyleWhenChosen(player5);
			player5Chosen = true;
		} else if (players.equals("6")) {
			addStyleWhenChosen(player6);
			player6Chosen = true;
		} else if (players.equals("7")) {
			addStyleWhenChosen(player7);
			player7Chosen = true;
		}

		playersForm.add(player0);
		playersForm.add(player1);
		playersForm.add(player2);
		playersForm.add(player3);
		playersForm.add(player4);
		playersForm.add(player5);
		playersForm.add(player6);
		playersForm.add(player7);
		add(playersForm);

	}

	public Map<TypesForFilters, String> getFilters() {
		return filters;
	}

	public void setFilters(Map<TypesForFilters, String> filters) {
		this.filters = filters;
	}

	private void addStyleWhenChosen(Button button) {
		button.add(new AttributeModifier("style", new Model<String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String getObject() {

				String s = "border:2px solid #008CBA;";
				return s;
			}
		}));
	}

	private void addNoStyles(Button button) {
		button.add(new AttributeModifier("style", new Model<String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String getObject() {

				String s = "";
				return s;
			}
		}));

	}

}
