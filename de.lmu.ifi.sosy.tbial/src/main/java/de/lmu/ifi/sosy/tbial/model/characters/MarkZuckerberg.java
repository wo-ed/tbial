package de.lmu.ifi.sosy.tbial.model.characters;

import java.util.Optional;

import de.lmu.ifi.sosy.tbial.model.Player;

public class MarkZuckerberg extends CharacterCard {

	// TODO PASS

	private static final long serialVersionUID = 1L;

	private Player playerToStealCard;

	private int cardsToSteal = 0;

	public MarkZuckerberg() {
		super("Mark Zuckerberg");
		this.maxHp = 3;
	}

	public Optional<Player> getPlayerToSteal() {
		if (playerToStealCard == null)
			return Optional.empty();
		return Optional.of(playerToStealCard);
	}

	public void setPlayerToSteal(Player playerToSteal) {
		this.playerToStealCard = playerToSteal;
	}

	public int getCountCardsToSteal() {
		return cardsToSteal;
	}

	public void resetCardsToSteal() {
		this.cardsToSteal = 0;
	}

	public void canStealCard() {
		cardsToSteal++;
	}

	public void stoleCard() {
		cardsToSteal--;
	}

	@Override
	public boolean canStealFrom(Player thisPlayer, Player stolen) {
		if (!getPlayerToSteal().isPresent())
			return super.canStealFrom(thisPlayer, stolen);

		if (this.cardsToSteal > 0)
			if (getPlayerToSteal().get().equals(stolen)) {
				return true;
			}
		return false;

	}

	@Override
	public int decHp() {
		super.decHp();
		if (getHp() == 0)
			this.resetCardsToSteal();
		return getHp();
	}

	@Override
	public void endTurn() {
		this.resetCardsToSteal();
	}
}
