package de.lmu.ifi.sosy.tbial.util;

import java.io.Serializable;
import java.util.UUID;

import de.lmu.ifi.sosy.tbial.db.User;

/**
 * VerifikationToken to activate users account
 */
public class VerificationToken implements Serializable, Comparable<VerificationToken> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String token;

	private User user;

	public VerificationToken(String token, User user) {
		super();
		this.token = token;
		this.setUser(user);
	}

	public VerificationToken(User user) {
		this(generateUniqueHashedToken(), user);
	}

	/**
	 * Generates a new unique token and hash
	 * 
	 * @return new unique hashed
	 */
	private static String generateUniqueHashedToken() {
		return hashToken(generateUniqueToken());
	}

	/**
	 * generate unique String with UUID
	 * 
	 * @return unique id
	 */
	private static String generateUniqueToken() {
		return UUID.randomUUID().toString();
	}

	/**
	 * hash Token
	 * 
	 * @param token
	 *            - tohash
	 * @return hashed token
	 */
	private static String hashToken(String token) {
		return Utils.hashString(token);
	}

	public String getToken() {
		return token;
	}

	@Override
	public int compareTo(VerificationToken o) {
		return this.getToken().compareTo(o.getToken());
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
