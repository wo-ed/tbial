package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.wicket.util.tester.FormTester;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.util.EmailValidator;

public class RegisterTest extends PageTestBase {

	@Before
	public void setUp() {
		setupApplication();
		database.register("testuser", "testpassword");
	}

	private void assertAllEmpty() {
		tester.assertModelValue("register:name", "");
		tester.assertModelValue("register:nameFeedback", " ");
		tester.assertModelValue("register:password", null);
		tester.assertModelValue("register:passwordConfirm", null);
		tester.assertModelValue("register:email", "");
	}

	@Test
	public void registerOk() {
		tester.startPage(Register.class);
		tester.assertRenderedPage(Register.class);
		assertAllEmpty();

		// Assert User not Logged in after register
		if (!TBIALApplication.DEBUG_MODE) {
			attemptRegister("user2", "password2", "password2", "valid@mail.de");
			TBIALSession session = getSession();
			assertNotNull(session);
			assertFalse(session.isSignedIn());

			// Assert User created
			User user = database.getUser("user2");
			assertNotNull(user);

			user.setActive(false);
			// Assert user can not login if not active
			assertFalse(session.signIn("user2", "password2"));
			tester.assertRenderedPage(Login.class);

			// test activated user
			database.activateUser(user);
			assertTrue(session.signIn("user2", "password2"));

			tester.startPage(TBIALApplication.get().getHomePage());
			tester.assertRenderedPage(TBIALApplication.get().getHomePage());
			tester.assertModelValue("user", "user2");
		}
	}

	private void attemptRegister(String user, String password, String passwordConfirm, String email) {
		FormTester form = tester.newFormTester("register");
		form.setValue("name", user);
		form.setValue("password", password);
		form.setValue("passwordConfirm", passwordConfirm);
		form.setValue("email", email);
		form.submit("register");
	}

	@Test
	public void registerErrorWhenDuplicateUser() {
		tester.startPage(Register.class);
		tester.assertRenderedPage(Register.class);
		assertAllEmpty();

		attemptRegister("testuser", "testpassword", "testpassword", "valid@mail.de");

		assertNotSignedIn();

		tester.assertRenderedPage(Register.class);

		String errorMsg = "A user with that name already exists. Please choose another name.";
		tester.assertErrorMessages(errorMsg);
		tester.assertFeedback("feedback", errorMsg);
	}

	private void assertNotSignedIn() {
		TBIALSession session = getSession();
		assertNotNull(session);
		assertFalse(session.isSignedIn());
		assertNull(session.getUser());
	}

	@Test
	public void registerErrorWhenWrongPasswordConfirmation() {
		tester.startPage(Register.class);
		tester.assertRenderedPage(Register.class);
		assertAllEmpty();

		attemptRegister("testuser", "testpassword", "bla", "valid@mail.de");

		assertNotSignedIn();

		tester.assertRenderedPage(Register.class);

		String errorMsg = "Password and confirmation do not match. Please verify and try again.";
		tester.assertErrorMessages(errorMsg);
		tester.assertFeedback("feedback", errorMsg);
	}

	@Test
	public void registerFeedbackWhileTyping() {
		tester.startPage(Register.class);
		tester.assertRenderedPage(Register.class);
		assertAllEmpty();
		FormTester form = tester.newFormTester("register");
		form.setValue("name", "test");

		tester.executeAjaxEvent("register:name", "change");
		tester.assertComponentOnAjaxResponse("register:nameFeedback");

		tester.assertModelValue("register:nameFeedback", " ");

		form.setValue("name", "testuser");
		tester.executeAjaxEvent("register:name", "change");
		tester.assertModelValue("register:nameFeedback", "Name already taken.");
	}

	@Test
	public void emailValidatorTest() {
		List<String> notValidMails = new ArrayList<>();
		Collections.addAll(notValidMails, "iamnotvalid", "iam.not.valid");
		List<String> validMails = new ArrayList<>();
		Collections.addAll(validMails, "iam@valid.mail", "iam.a@valid.mail", "i.am.a@valid.mail", "i.am.a@valid.ma.il",
				"iam@valid");
		notValidMails.stream().forEach(s -> assertFalse(EmailValidator.validateEmail(s)));
		validMails.stream().forEach(s -> assertTrue(EmailValidator.validateEmail(s)));
	}

	@Test
	public void activateOnLinkClickTest() {
		tester.startPage(Register.class);
		tester.assertRenderedPage(Register.class);
		assertAllEmpty();

		TBIALApplication app = ((TBIALApplication) TBIALApplication.get());
		User user = TBIALApplication.registerUser("name", "password", "email@valid.com");
		assertNotNull(user);
		user.setActive(false);
		String token = app.getUserService().getTokenStorage().getFromUser(user).getToken();
		String url = app.generatateActivationLink(token);
		tester.executeUrl(url);

		assertTrue(user.isActive());
	}

}
