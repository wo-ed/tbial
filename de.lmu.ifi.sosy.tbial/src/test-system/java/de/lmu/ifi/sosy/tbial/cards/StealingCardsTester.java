package de.lmu.ifi.sosy.tbial.cards;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.StealingCard;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.LANParty;
import de.lmu.ifi.sosy.tbial.model.actioncards.Pwnd;
import de.lmu.ifi.sosy.tbial.model.actioncards.Refactoring;

public class StealingCardsTester extends CardTester {
	
	@Test
	public void testPwndConditions() {
		TurnCard cardToBeStolen = player2.getHandcards().get(0);
		StealingCard stealingCard = new Pwnd();
		PlayerState playerState = PlayerState.PWND;
		
		setOkayConditions(stealingCard, cardToBeStolen, playerState);
		player.getCharacter().setPrestige(2);
		player2.getCharacter().setPrestige(3);
		assertFalse(game.steal(cardToBeStolen));
		
		setOkayConditions(stealingCard, cardToBeStolen, playerState);
		player.getCharacter().setPrestige(3);
		player2.getCharacter().setPrestige(3);
		assertTrue(game.steal(cardToBeStolen));

		setOkayConditions(stealingCard, cardToBeStolen, playerState);
		player.getCharacter().setPrestige(4);
		player2.getCharacter().setPrestige(3);
		assertTrue(game.steal(cardToBeStolen));
		
		testStealingConditions(stealingCard);
	}
	
	@Test
	public void testRefactoringConditions() {
		testStealingConditions(new Refactoring());
	}

	private void testStealingConditions(StealingCard stealingCard) {
		TurnCard cardToBeStolen = player2.getHandcards().get(0);
		PlayerState playerState = null;
		
		if(stealingCard instanceof Pwnd) {
			playerState = PlayerState.PWND;
		} else if(stealingCard instanceof Refactoring) {
			playerState = PlayerState.REFACTORING;
		}
		
		// Stealing card must not be null
		setOkayConditions(stealingCard, cardToBeStolen, playerState);
		game.setStealingCard(null);
		assertFalse(game.steal(cardToBeStolen));
		
		// The attacker must not be the target player
		setOkayConditions(stealingCard, cardToBeStolen, playerState);
		stealingCard.setPlayer(player);
		stealingCard.setTargetPlayer(player);
		cardToBeStolen.setPlayer(player);
		assertFalse(game.steal(cardToBeStolen));
		
		// StealingCard target player must be equal to owner of stolen card
		setOkayConditions(stealingCard, cardToBeStolen, playerState);
		stealingCard.setTargetPlayer(player2);
		cardToBeStolen.setPlayer(player3);
		assertFalse(game.steal(cardToBeStolen));
		
		// Player state must be PWND / REFACTORING
		setOkayConditions(stealingCard, cardToBeStolen, playerState);
		player.setState(PlayerState.ON_TURN);
		assertFalse(game.steal(cardToBeStolen));
		
		// All conditions are okay.
		setOkayConditions(stealingCard, cardToBeStolen, playerState);
		assertTrue(game.steal(cardToBeStolen));
	}

	private void setOkayConditions(StealingCard stealingCard, TurnCard cardToBeStolen, PlayerState playerState) {
		game.setStealingCard(stealingCard);
		stealingCard.setPlayer(player);
		stealingCard.setTargetPlayer(player2);
		cardToBeStolen.setPlayer(player2);
		player.setState(playerState);
	}
	
	@Test
	public void testPwnd() {
		TurnCard stolenHandCard = testStealHandCard(Pwnd.class);
		assertTrue(player.getHandcards().contains(stolenHandCard));
		
		TurnCard stolenBuffCard = testStealBuffCard(Pwnd.class);
		assertTrue(player.getHandcards().contains(stolenBuffCard));
		
		TurnCard stolenPenaltyCard = testStealPenaltyCard(Pwnd.class);
		assertTrue(player.getHandcards().contains(stolenPenaltyCard));
	}
	
	@Test
	public void testRefactoring() {
		TurnCard stolenHandCard = testStealHandCard(Refactoring.class);
		assertTrue(game.getHeap().contains(stolenHandCard));
		
		TurnCard stolenBuffCard = testStealBuffCard(Refactoring.class);
		assertTrue(game.getHeap().contains(stolenBuffCard));

		TurnCard stolenPenaltyCard = testStealPenaltyCard(Refactoring.class);
		assertTrue(game.getHeap().contains(stolenPenaltyCard));
	}
	
	private <T extends StealingCard> TurnCard testStealHandCard(Class<T> clazz) {
		TurnCard cardToBeStolen = player2.getHandcards().get(0);
		testStealCard(clazz, cardToBeStolen);
		
		// Card was stolen from hand cards.
		assertFalse(player2.getHandcards().contains(cardToBeStolen));
		
		return cardToBeStolen;
	}
	
	private <T extends StealingCard> TurnCard testStealBuffCard(Class<T> clazz) {
		// Initialize new buff card
		TurnCard cardToBeStolen = new LANParty();
		cardToBeStolen.setPlayer(player2);
		player2.addBuffCard(cardToBeStolen);
		
		testStealCard(clazz, cardToBeStolen);
		
		// Card was stolen from buff cards.
		assertFalse(player2.getBuffCards().contains(cardToBeStolen));
		
		return cardToBeStolen;
	}
	
	private <T extends StealingCard> TurnCard testStealPenaltyCard(Class<T> clazz) {
		// Initialize new penalty card
		TurnCard cardToBeStolen = new LANParty();
		cardToBeStolen.setPlayer(player2);
		player2.addPenaltyCard(cardToBeStolen);
		
		testStealCard(clazz, cardToBeStolen);
		
		// Card was stolen from penalty cards.
		assertFalse(player2.getPenaltyCards().contains(cardToBeStolen));
		
		return cardToBeStolen;
	}
	
	private <T extends StealingCard> void testStealCard(Class<T> clazz, TurnCard cardToBeStolen) {
		if(clazz == Pwnd.class) {
			testPwndStealing(cardToBeStolen);
		} else if(clazz == Refactoring.class) {
			testRefactoringStealing(cardToBeStolen);
		}
	}
	
	private void testPwndStealing(TurnCard cardToBeStolen) {
		Pwnd pwnd = new Pwnd();
		pwnd.setPlayer(player);
		pwnd.setTargetPlayer(player2);
		player.getHandcards().add(pwnd);
		
		game.playCard(pwnd);
		assertTrue(game.getStealingCard().equals(pwnd));
		assertEquals(PlayerState.PWND, player.getState());
		assertFalse(player.getHandcards().contains(pwnd));
		
		// No card is stolen yet. Playing another card fails.
		TurnCard otherCard = addOtherCardToHand();
		game.playCard(otherCard);
		assertTrue(player.getHandcards().contains(otherCard));
		
		player2.getCharacter().setPrestige(4);
		player.getCharacter().setPrestige(4);

		// Stealing a card
		assertTrue(game.steal(cardToBeStolen));
		assertEquals(PlayerState.ON_TURN, game.getCurrentPlayer().getState());
		assertNull(game.getStealingCard());
		assertEquals(cardToBeStolen.getPlayer(), player);
		
		// A card was stolen. Playing another card works now.
		game.playCard(otherCard);
		assertFalse(player.getHandcards().contains(otherCard));
	}
	
	private void testRefactoringStealing(TurnCard cardToBeStolen) {
		Refactoring refactoring = new Refactoring();
		refactoring.setPlayer(player);
		refactoring.setTargetPlayer(player2);
		player.getHandcards().add(refactoring);
		
		game.playCard(refactoring);
		assertTrue(game.getStealingCard().equals(refactoring));
		assertEquals(PlayerState.REFACTORING, player.getState());
		assertFalse(player.getHandcards().contains(refactoring));
		
		// No card is stolen yet. Playing another card fails.
		TurnCard otherCard = addOtherCardToHand();
		game.playCard(otherCard);
		assertTrue(player.getHandcards().contains(otherCard));

		// Stealing a card
		assertTrue(game.steal(cardToBeStolen));
		assertEquals(PlayerState.ON_TURN, game.getCurrentPlayer().getState());
		assertNull(game.getStealingCard());
		
		// A card was stolen. Playing another card works now.
		game.playCard(otherCard);
		assertFalse(player.getHandcards().contains(otherCard));
	}
	
	private TurnCard addOtherCardToHand() {
		TurnCard otherCard = new LANParty();
		otherCard.setPlayer(player);
		otherCard.setTargetPlayer(player);
		player.getHandcards().add(otherCard);
		return otherCard;
	}
	
}
