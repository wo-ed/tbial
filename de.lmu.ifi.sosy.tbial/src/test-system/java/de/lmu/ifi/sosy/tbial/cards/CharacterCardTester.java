package de.lmu.ifi.sosy.tbial.cards;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Player;

public class CharacterCardTester extends CardTester {

	protected Player player8, player9, player10, player11, player12, player13;
	protected MockGame game2;

	@Override
	@Before
	public void setUp() {
		super.setUp();
		User user8 = new User("", "");
		User user9 = new User("", "");
		User user10 = new User("", "");
		User user11 = new User("", "");
		User user12 = new User("", "");
		User user13 = new User("", "");

		this.game2 = new MockGame("g1", (short) 7, false, user13, false);
		game2.join(user12);
		game2.join(user11);
		game2.join(user10);
		game2.join(user9);
		game2.join(user8);

		this.player8 = game2.getPlayer(user8);
		this.player9 = game2.getPlayer(user9);
		this.player10 = game2.getPlayer(user10);
		this.player11 = game2.getPlayer(user11);
		this.player12 = game2.getPlayer(user12);
		this.player13 = game2.getPlayer(user13);

		game2.init();
	}

	@Test
	public void testHp() {
		// Bruce Schneier + Manager
		Assert.assertEquals(player.getCharacter().getHp(), 5);
		// Holier than Thou
		Assert.assertEquals(player2.getCharacter().getHp(), 4);
		// Jeff Taylor
		Assert.assertEquals(player3.getCharacter().getHp(), 4);
		// Kent Beck
		Assert.assertEquals(player4.getCharacter().getHp(), 4);
		// Konrad Zuse
		Assert.assertEquals(player5.getCharacter().getHp(), 3);
		// Larry Ellison
		Assert.assertEquals(player6.getCharacter().getHp(), 4);
		// Larry Page
		Assert.assertEquals(player7.getCharacter().getHp(), 4);
		// Linus Torvalds + Manager
		Assert.assertEquals(player8.getCharacter().getHp(), 5);
		// Mark Zuckerberg
		Assert.assertEquals(player9.getCharacter().getHp(), 3);
		// Steve Ballmer
		Assert.assertEquals(player10.getCharacter().getHp(), 4);
		// Steve Jobs
		Assert.assertEquals(player11.getCharacter().getHp(), 4);
		// Terry Weissman
		Assert.assertEquals(player12.getCharacter().getHp(), 4);
		// Tom Anderson
		Assert.assertEquals(player13.getCharacter().getHp(), 4);
	}

}
