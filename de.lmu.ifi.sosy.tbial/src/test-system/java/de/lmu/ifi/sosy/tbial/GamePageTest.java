package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.util.visit.IVisit;
import org.apache.wicket.util.visit.IVisitor;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.MockGame;
import de.lmu.ifi.sosy.tbial.components.CourtPanel.ICourtPanelFactory;
import de.lmu.ifi.sosy.tbial.components.HandCardPanel;
import de.lmu.ifi.sosy.tbial.components.PlayerPanel;
import de.lmu.ifi.sosy.tbial.components.RepeatingCourtView;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.abilitycards.WearsSunglassesAtWork;
import de.lmu.ifi.sosy.tbial.model.abilitycards.WearsTieAtWork;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Accenture;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Google;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Microsoft;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Nasa;
import de.lmu.ifi.sosy.tbial.model.actioncards.Refactoring;
import de.lmu.ifi.sosy.tbial.model.rolecards.Manager;
import de.lmu.ifi.sosy.tbial.pages.GamePage;

public class GamePageTest extends PageTestBase {

	@Before
	public void setUp() {
		setupApplication();
		database.register("testuser", "1");
		TBIALSession session = getSession();
		session.signIn("testuser", "1");
	}

	@Test
	public void createNestedCourtPanelFactoryTest() {
		// Test1
		Game game = new Game("", (short) 7, false, getSession().getUser());
		List<Player> playersTest1 = new ArrayList<>();
		for (int i = 0; i < 4; i++) {
			playersTest1.add(new Player(i));
		}

		game.initializeStack();
		List<List<ICourtPanelFactory>> factoryTest1 = RepeatingCourtView.createNestedFactoryList(playersTest1, game);
		assertTrue(factoryTest1.size() == 4);
		for (List<ICourtPanelFactory> l : factoryTest1) {
			assertTrue(l.size() == 3);
		}

		int countPlayerPanelFactorys1 = 0;
		int countHeapStackPanelFactorys1 = 0;

		// Count instances of Factories (2 smalls if player.size not even, 0 if
		// even, 1 Heap Stack , Playerpannels == players size)
		for (List<ICourtPanelFactory> l : factoryTest1) {
			for (ICourtPanelFactory f : l) {
				if (f instanceof ICourtPanelFactory.PlayerPanelFactory) {
					countPlayerPanelFactorys1++;
				} else if (f instanceof ICourtPanelFactory.HeapStackPanelFactory) {
					countHeapStackPanelFactorys1++;
				}
			}
		}

		assertTrue(countPlayerPanelFactorys1 == playersTest1.size());
		assertTrue(countHeapStackPanelFactorys1 == 1);

		// Test 2
		List<Player> playersTest2 = new ArrayList<>();
		for (int i = 0; i < 7; i++) {
			Player p = new Player(i);
			playersTest2.add(p);
		}

		List<List<ICourtPanelFactory>> factoryTest2 = RepeatingCourtView.createNestedFactoryList(playersTest2, game);
		assertTrue(factoryTest2.size() == 4);
		int count = 0;
		for (List<ICourtPanelFactory> l : factoryTest2) {
			if (count != 0)
				assertTrue(l.size() == 3);
			else
				assertTrue(l.size() == 4);
			count++;
		}

		int countPlayerPanelFactorys2 = 0;
		int countHeapStackPanelFactorys2 = 0;

		for (List<ICourtPanelFactory> l : factoryTest2) {
			for (ICourtPanelFactory f : l) {
				if (f instanceof ICourtPanelFactory.PlayerPanelFactory) {
					countPlayerPanelFactorys2++;
				} else if (f instanceof ICourtPanelFactory.HeapStackPanelFactory) {
					countHeapStackPanelFactorys2++;
				}
			}
		}

		assertTrue(countPlayerPanelFactorys2 == playersTest2.size());
		assertTrue(countHeapStackPanelFactorys2 == 1);

	}

	@Test
	public void checkUpdateBeahviorDeadPlayers() {
		TBIALSession session = getSession();
		assertTrue(session.isSignedIn());

		Game game = new Game("1", (short) 7, false, session.getUser());
		TBIALApplication.getInstance().addGame(game);
		TBIALApplication.getInstance().joinGame(session.getUser(), game);
		User u2 = new User("u2", "");
		User u3 = new User("u3", "");
		User u4 = new User("u4", "");

		game.join(u2);
		game.join(u3);
		game.join(u4);

		Player p2 = game.getPlayer(u2);

		game.init();

		GamePage gPage = tester.startPage(GamePage.create());

		tester.assertRenderedPage(GamePage.class);

		PlayerPanel panel2 = gPage.getGameCourt().getPlayerPanels().get(p2.getId());
		if (!(p2.getRole() instanceof Manager))
			assertFalse(panel2.getRoleLabel().isVisible());
		else
			assertTrue(panel2.getRoleLabel().isVisible());

		while (p2.isAlive()) {
			game.decHp(p2, p2);
		}
		tester.executeAllTimerBehaviors(gPage);
		assertTrue(panel2.getRoleLabel().isVisible());

	}

	@Test
	public void createGamePageTest() {
		TBIALSession session = getSession();
		assertTrue(session.isSignedIn());

		Game game = new Game("1", (short) 7, false, session.getUser());
		TBIALApplication.getInstance().addGame(game);
		TBIALApplication.getInstance().joinGame(session.getUser(), game);
		User u2 = new User("u2", "");
		User u3 = new User("u3", "");
		User u4 = new User("u4", "");

		game.join(u2);
		game.join(u3);
		game.join(u4);

		Player p2 = game.getPlayer(u2);
		Player p3 = game.getPlayer(u3);
		Player p4 = game.getPlayer(u4);
		Player p1 = game.getPlayer(session.getUser());

		game.init();

		while (p2.isAlive()) {
			game.decHp(p2, p2);
		}

		GamePage gPage = tester.startPage(GamePage.create());
		tester.assertRenderedPage(GamePage.class);

		// Test Players 1-4

		PlayerPanel panel1 = gPage.getGameCourt().getPlayerPanels().get(p1.getId());
		PlayerPanel panel2 = gPage.getGameCourt().getPlayerPanels().get(p2.getId());
		PlayerPanel panel3 = gPage.getGameCourt().getPlayerPanels().get(p3.getId());
		PlayerPanel panel4 = gPage.getGameCourt().getPlayerPanels().get(p4.getId());

		tester.executeAllTimerBehaviors(gPage);

		// Player 1: Role should be visible -> session user
		assertTrue(panel1.getHpLabel().getDefaultModel().getObject().equals(p1.getHp()));
		assertEquals(panel1.getNameLabel().getDefaultModel().getObject(), (p1.getName()));
		assertTrue(panel1.getRoleLabel().getDefaultModel().getObject().equals(p1.getRole().getName()));
		assertTrue(panel1.getRoleLabel().isVisible());

		// Player 2: Role should be visible -> dead
		assertTrue(panel2.getHpLabel().getDefaultModel().getObject().equals(p2.getHp()));
		assertTrue(panel2.getNameLabel().getDefaultModel().getObject().equals(p2.getName()));
		assertTrue(panel2.getRoleLabel().getDefaultModel().getObject().equals(p2.getRole().getName()));
		assertTrue(panel2.getRoleLabel().isVisible());

		// Player 3: Role should be invisible if not Manager
		assertTrue(panel3.getHpLabel().getDefaultModel().getObject().equals(p3.getHp()));
		assertTrue(panel3.getNameLabel().getDefaultModel().getObject().equals(p3.getName()));
		assertTrue(panel3.getRoleLabel().getDefaultModel().getObject().equals(p3.getRole().getName()));
		if (p3.getRole() instanceof Manager)
			assertTrue(panel3.getRoleLabel().isVisible());
		else
			assertFalse(panel3.getRoleLabel().isVisible());

		// Player 4: Role should be invisible if not Manager
		assertTrue(panel4.getHpLabel().getDefaultModel().getObject().equals(p4.getHp()));
		assertTrue(panel4.getNameLabel().getDefaultModel().getObject().equals(p4.getName()));
		assertTrue(panel4.getRoleLabel().getDefaultModel().getObject().equals(p4.getRole().getName()));
		if (p4.getRole() instanceof Manager)
			assertTrue(panel4.getRoleLabel().isVisible());
		else
			assertFalse(panel4.getRoleLabel().isVisible());

		// Test stack content
		assertFalse(game.getStack().isEmpty());
		MarkupContainer stack = (MarkupContainer) gPage.getGameCourt().getHeapStack()
				.get("heapStackContainer:stackContainer:stack");
		tester.assertListView(stack.getPageRelativePath(),
				game.getStack().stream().map(c -> c.getId()).collect(Collectors.toList()));

		game.getHeap().clear();
		List<Integer> heapIds = new ArrayList<>();
		// The heap needs to be filled in order to test it
		IntStream.range(0, 10).forEach(i -> {
			TurnCard tc = game.getStack().remove(0);
			game.getHeap().add(tc);
			heapIds.add(tc.getId());
		});

		tester.executeAllTimerBehaviors(gPage);
		stack = (MarkupContainer) gPage.getGameCourt().getHeapStack().get("heapStackContainer:stackContainer:stack");
		// Test Stack refresh
		tester.assertListView(stack.getPageRelativePath(),
				game.getStack().stream().map(c -> c.getId()).collect(Collectors.toList()));

		assertFalse(game.getHeap().isEmpty());

		// Test heap content
		MarkupContainer heap = (MarkupContainer) gPage.getGameCourt().getHeapStack()
				.get("heapStackContainer:heapContainer:heap");
		tester.assertListView(heap.getPageRelativePath(), heapIds);

		// Test heap views
		heap.visitChildren(ListItem.class, new IVisitor<ListItem<Integer>, Component>() {

			@Override
			public void component(ListItem<Integer> item, IVisit<Component> visit) {
				Component nameLabel = item.get("heapCardName");
				tester.assertLabel(nameLabel.getPageRelativePath(),
						game.getCard(item.getModel().getObject()).getName());
			}
		});

		// Test heap refresh
		game.getHeap().clear();
		tester.executeAllTimerBehaviors(gPage);
		heap = (MarkupContainer) gPage.getGameCourt().getHeapStack().get("heapStackContainer:heapContainer:heap");
		tester.assertListView(heap.getPageRelativePath(), new ArrayList<>());

		// Test hand cards
		Map<Integer, PlayerPanel> playerPanels = gPage.getGameCourt().getPlayerPanels();
		Set<Player> players = gPage.getGame().getAllPlayers();
		for (Player p : players) {
			// Test hand cards view
			MarkupContainer handCardPanelContainer = (MarkupContainer) playerPanels.get(p.getId()).getHandCardPanel()
					.get("handCardsContainer:hand");
			tester.assertListView(handCardPanelContainer.getPageRelativePath(),
					p.getHandcards().stream().map(c -> c.getId()).collect(Collectors.toList()));
			// Test hand cards content
			HandCardPanel handCardPanel = playerPanels.get(p.getId()).getHandCardPanel();
			handCardPanelContainer.visitChildren(ListItem.class, new IVisitor<ListItem<Integer>, Component>() {

				@Override
				public void component(ListItem<Integer> item, IVisit<Component> visit) {
					Component nameLabel = item.get("handCard");
					if (handCardPanel.isUserPanelOwner()) {
						// User == Player: user can see his own cards
						tester.assertLabel(nameLabel.getPageRelativePath(),
								game.getCard(item.getModelObject()).getName());
					} else {
						// User can't see other players cards
						tester.assertLabel(nameLabel.getPageRelativePath(), "");
					}
				}
			});
		}

		// Test penaltyCards, buffCards
		for (Player p : players) {
			game.drawCard(p);
			p.getBuffCards().add(p.getHandcards().remove(0));
			game.drawCard(p);
			p.getBuffCards().add(p.getHandcards().remove(0));
			game.drawCard(p);
			p.getPenaltyCards().add(p.getHandcards().remove(0));
			game.drawCard(p);
			p.getPenaltyCards().add(p.getHandcards().remove(0));
			tester.executeAllTimerBehaviors(gPage);
			MarkupContainer fieldCardsContainer = (MarkupContainer) playerPanels.get(p.getId()).getPlayerField()
					.get("fieldCardsContainer:buffPanel:buffCards");

			tester.assertListView(fieldCardsContainer.getPageRelativePath(),
					p.getBuffCards().stream().map(c -> c.getId()).collect(Collectors.toList()));

			fieldCardsContainer.visitChildren(ListItem.class, new IVisitor<ListItem<Integer>, Component>() {

				@Override
				public void component(ListItem<Integer> item, IVisit<Component> visit) {
					Component nameLabel = item.get("buffCard");
					tester.assertLabel(nameLabel.getPageRelativePath(), game.getCard(item.getModelObject()).getName());

				}
			});

			fieldCardsContainer = (MarkupContainer) playerPanels.get(p.getId()).getPlayerField()
					.get("fieldCardsContainer:dropZone:penaltyCards");
			tester.assertListView(fieldCardsContainer.getPageRelativePath(),
					p.getPenaltyCards().stream().map(c -> c.getId()).collect(Collectors.toList()));

			fieldCardsContainer.visitChildren(ListItem.class, new IVisitor<ListItem<Integer>, Component>() {

				@Override
				public void component(ListItem<Integer> item, IVisit<Component> visit) {

					Component nameLabel = item.get("penaltyCard");
					tester.assertLabel(nameLabel.getPageRelativePath(), game.getCard(item.getModelObject()).getName());
				}
			});
		}

		/**
		 * // Test event cards game.setEventCard(new Heisenbug()); TurnCard[]
		 * tcs = { new BugDelegation() };
		 * game.setEventParticipatingCards(Arrays.asList(tcs));
		 * tester.executeAllTimerBehaviors(gPage); MarkupContainer eventCards =
		 * (MarkupContainer) gPage.getGameCourt().getEventPanel()
		 * .get("cardsContainer:EventParticipatingCards");
		 * tester.assertListView(eventCards.getPageRelativePath(),
		 * game.getEventParticipatingCards());
		 * 
		 * // Test event views heap.visitChildren(ListItem.class, new
		 * IVisitor<ListItem<TurnCard>, Component>() {
		 * 
		 * @Override public void component(ListItem<TurnCard> item,
		 *           IVisit<Component> visit) { Component nameLabel =
		 *           item.get("card");
		 *           tester.assertLabel(nameLabel.getPageRelativePath(),
		 *           item.getModel().getObject().getName()); } });
		 */
	}

	@Test
	public void checkPrestigeUpdate() {

		// setup
		MockGame game = new MockGame("Prestige Test", (short) 4, false, getSession().getUser(), false);
		TBIALApplication.getInstance().addGame(game);
		TBIALApplication.getInstance().joinGame(getSession().getUser(), game);
		User u2 = new User("u2", "");
		User u3 = new User("u3", "");
		User u4 = new User("u4", "");

		game.join(u2);
		game.join(u3);
		game.join(u4);

		Player p2 = game.getPlayer(u2);
		Player p3 = game.getPlayer(u3);
		Player p4 = game.getPlayer(u4);
		Player p1 = game.getPlayer(getSession().getUser());

		game.init();

		game.cleanAllHandcards();

		TurnCard nasa = game.drawCardofClass(p4, Nasa.class);
		TurnCard google = game.drawCardofClass(p3, Google.class);
		TurnCard microsoft = game.drawCardofClass(p2, Microsoft.class);
		TurnCard accenture = game.drawCardofClass(p1, Accenture.class);
		TurnCard wSunglasses = game.drawCardofClass(p1, WearsSunglassesAtWork.class);
		TurnCard wTieP1 = game.drawCardofClass(p1, WearsTieAtWork.class);
		TurnCard wTieP2 = game.drawCardofClass(p2, WearsTieAtWork.class);

		GamePage gPage = tester.startPage(GamePage.create());
		tester.assertRenderedPage(GamePage.class);

		PlayerPanel panel1 = gPage.getGameCourt().getPlayerPanels().get(p1.getId());
		PlayerPanel panel2 = gPage.getGameCourt().getPlayerPanels().get(p2.getId());
		PlayerPanel panel3 = gPage.getGameCourt().getPlayerPanels().get(p3.getId());
		PlayerPanel panel4 = gPage.getGameCourt().getPlayerPanels().get(p4.getId());

		assertEquals(panel1.getPrestigeLabel().getDefaultModelObject(), 0);
		assertEquals(panel2.getPrestigeLabel().getDefaultModelObject(), 0);
		assertEquals(panel3.getPrestigeLabel().getDefaultModelObject(), 0);
		assertEquals(panel4.getPrestigeLabel().getDefaultModelObject(), 0);

		// check PreviosJob prestige modifier
		game.setCurrentPlayer(p4);
		game.playCard(nasa);
		tester.executeAllTimerBehaviors(gPage);
		assertEquals(panel4.getPrestigeLabel().getDefaultModelObject(), 3);

		game.setCurrentPlayer(p3);
		game.playCard(google);
		tester.executeAllTimerBehaviors(gPage);
		assertEquals(panel3.getPrestigeLabel().getDefaultModelObject(), 2);

		game.setCurrentPlayer(p2);
		game.playCard(microsoft);
		tester.executeAllTimerBehaviors(gPage);
		assertEquals(panel2.getPrestigeLabel().getDefaultModelObject(), 1);

		game.setCurrentPlayer(p1);
		game.playCard(accenture);
		tester.executeAllTimerBehaviors(gPage);
		assertEquals(panel1.getPrestigeLabel().getDefaultModelObject(), 0);

		// check Wear Sunglasses
		game.playCard(wSunglasses);
		tester.executeAllTimerBehaviors(gPage);
		assertEquals(panel1.getPrestigeLabel().getDefaultModelObject(), 0);
		assertEquals(panel2.getPrestigeLabel().getDefaultModelObject(), 0);
		assertEquals(panel3.getPrestigeLabel().getDefaultModelObject(), 1);
		assertEquals(panel4.getPrestigeLabel().getDefaultModelObject(), 2);

		// check Wear Tie
		game.playCard(wTieP1);
		tester.executeAllTimerBehaviors(gPage);
		assertEquals(panel4.getPrestigeLabel().getDefaultModelObject(), 2);
		assertEquals(panel1.getPrestigeLabel().getDefaultModelObject(), 0);

		game.setCurrentPlayer(p2);
		game.playCard(wTieP2);
		tester.executeAllTimerBehaviors(gPage);
		assertEquals(panel2.getPrestigeLabel().getDefaultModelObject(), 1);

		TurnCard ref = game.drawCardofClass(p2, Refactoring.class);
		ref.setTargetPlayer(p4);
		game.playCard(ref);

		// remove nasa
		game.steal(nasa);
		tester.executeAllTimerBehaviors(gPage);
		assertEquals(panel4.getPrestigeLabel().getDefaultModelObject(), 0);
	}

}
