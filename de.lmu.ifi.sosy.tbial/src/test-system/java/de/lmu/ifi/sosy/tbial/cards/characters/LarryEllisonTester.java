package de.lmu.ifi.sosy.tbial.cards.characters;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.CharacterCardTester;
import de.lmu.ifi.sosy.tbial.model.characters.LarryEllison;

public class LarryEllisonTester extends CharacterCardTester {

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void larryEllisonDrawsThreeCardsAndKeepsTwo() {
		// Larry Ellison
		Assert.assertEquals(true, player6.getCharacter() instanceof LarryEllison);
		Assert.assertEquals(4, player6.getCharacter().getHp());
		game.setCurrentPlayer(player6);
	}
}
