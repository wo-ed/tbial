package de.lmu.ifi.sosy.tbial.cards;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.Solution;
import de.lmu.ifi.sosy.tbial.model.actioncards.StandupMeeting;

public class StandupMeetingTester extends CardTester {

	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void drawCardsForStandupMeetingTest() {
		assertTrue(game.getStandupMeetingCards().isEmpty());
		game.drawCardsForStandupMeeting(game.getPlayers().size());
		assertFalse(game.getStandupMeetingCards().isEmpty());
		assertTrue(game.getStandupMeetingCards().size() == 7);
	}

	@Test
	public void drawOneStandupMeetingCardTest() {
		game.drawCardsForStandupMeeting(game.getPlayers().size());
		for (Player p : game.getPlayers()) {
			int countHandCards = p.getHandcards().size();
			TurnCard card = new Solution("Solution");
			game.drawOneStandupMeetingCard(p, card);
			assertTrue(p.getHandcards().size() == countHandCards + 1);
			assertTrue(p.getHandcards().contains(card));
		}
	}

	@Test
	public void visitStandupMeetingCardTest() {

		// normal conditions
		assertTrue(game.getStandupMeetingCards().isEmpty());
		TurnCard standupMeeting = new StandupMeeting();
		player.addHandCard(standupMeeting);
		standupMeeting.setPlayer(player);
		standupMeeting.setTargetPlayer(player);
		player.setState(PlayerState.ON_TURN);
		assertTrue(player.getHandcards().contains(standupMeeting));
		assertFalse(player.getPenaltyCards().contains(standupMeeting));
		game.playCard(standupMeeting);
		assertFalse(player.getHandcards().contains(standupMeeting));
		assertTrue(player.getPenaltyCards().contains(standupMeeting));
		assertEquals(game.getStandupMeetingCard(), standupMeeting);
		assertTrue(game.getStandupMeetingCards().size() == 7);
		assertTrue(player.getState() == PlayerState.STANDUP_MEETING);

		// if one player is dead, there will be one card less on the event panel
		TurnCard standupMeeting2 = new StandupMeeting();
		player.addHandCard(standupMeeting2);
		standupMeeting2.setPlayer(player);
		standupMeeting2.setTargetPlayer(player);
		player.setState(PlayerState.ON_TURN);
		player2.setState(PlayerState.DEAD);
		assertTrue(player.getHandcards().contains(standupMeeting2));
		assertFalse(player.getPenaltyCards().contains(standupMeeting2));
		game.playCard(standupMeeting2);
		assertFalse(player.getHandcards().contains(standupMeeting2));
		assertTrue(player.getPenaltyCards().contains(standupMeeting2));
		assertEquals(game.getStandupMeetingCard(), standupMeeting2);
		assertTrue(game.getStandupMeetingCards().size() == 6);
		assertTrue(player.getState() == PlayerState.STANDUP_MEETING);

	}

}
