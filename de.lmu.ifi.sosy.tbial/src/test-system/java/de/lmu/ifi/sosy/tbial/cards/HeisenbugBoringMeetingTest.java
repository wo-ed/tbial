package de.lmu.ifi.sosy.tbial.cards;

import static org.junit.Assert.*;

import org.junit.Test;

import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.BoringMeeting;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.actioncards.Heisenbug;
import de.lmu.ifi.sosy.tbial.model.actioncards.LameExcuse;
import de.lmu.ifi.sosy.tbial.model.actioncards.Solution;

public class HeisenbugBoringMeetingTest extends CardTester {
	int hpPlayer;
	int hpPlayer2;
	int hpPlayer3;
	int hpPlayer4;
	int hpPlayer5;
	int hpPlayer6;
	int hpPlayer7;
	

	@Test
	public void testHeisenbug(){
		game.setCurrentPlayer(player);
		game.resetCountBugDelegations();
		getMentalHealthPoints();
		clearAllHands();
		
		//player plays heisenbug
		//player has solution card and player2 has lameExecuse
		//all other player suppose to lose a mental health point
		TurnCard hb = game.drawCardofClass(player, Heisenbug.class);		
		TurnCard solution = game.drawCardofClass(player, Solution.class);
		TurnCard lameExe = game.drawCardofClass(player2, LameExcuse.class);
		
		hb.play(this.visitor);
		
		//check player states
		assertTrue(player.getState() == PlayerState.HEISENBUG);
		assertTrue(player2.getState() == PlayerState.HEISENBUG);
		assertTrue(player3.getState() == PlayerState.WAIT);
		assertTrue(player4.getState() == PlayerState.WAIT);
		assertTrue(player5.getState() == PlayerState.WAIT);
		assertTrue(player6.getState() == PlayerState.WAIT);
		assertTrue(player7.getState() == PlayerState.WAIT);
		
		//check player3-7 lost hp
		assertTrue(player3.getHp() == (hpPlayer3-1));
		assertTrue(player4.getHp() == (hpPlayer4-1));
		assertTrue(player5.getHp() == (hpPlayer5-1));
		assertTrue(player6.getHp() == (hpPlayer6-1));
		assertTrue(player7.getHp() == (hpPlayer7-1));
		
		//player defends himself with his solution card
		solution.play(this.visitor);
		assertTrue(player.getHp() == hpPlayer);
		assertTrue(player.getState() == PlayerState.WAIT);
		
		//player2 defends himself with his lame execuse card
		lameExe.play(this.visitor);
		assertTrue(player2.getHp() == hpPlayer2);
		assertTrue(player2.getState() == PlayerState.WAIT);
		
		//Every one took possible action now
		//check player is on_turn
		assertTrue(player.getState() == PlayerState.ON_TURN);
		
	}
	
	@Test
	public void testBoringMeeting(){
		game.setCurrentPlayer(player);
		game.resetCountBugDelegations();
		getMentalHealthPoints();
		clearAllHands();
		
		//player plays BoringMeeting
		//player and player2 have a bug card
		//all other player suppose to lose a mental health point
		TurnCard bm = game.drawCardofClass(player, BoringMeeting.class);		
		TurnCard bug1 = game.drawCardofClass(player, BugReport.class);
		TurnCard bug2 = game.drawCardofClass(player2, BugReport.class);
		
		bm.play(this.visitor);
		
		//check player states
		assertTrue(player.getState() == PlayerState.BORING_MEETING);
		assertTrue(player2.getState() == PlayerState.BORING_MEETING);
		assertTrue(player3.getState() == PlayerState.WAIT);
		assertTrue(player4.getState() == PlayerState.WAIT);
		assertTrue(player5.getState() == PlayerState.WAIT);
		assertTrue(player6.getState() == PlayerState.WAIT);
		assertTrue(player7.getState() == PlayerState.WAIT);
		
		//check player3-7 lost hp
		assertTrue(player3.getHp() == (hpPlayer3-1));
		assertTrue(player4.getHp() == (hpPlayer4-1));
		assertTrue(player5.getHp() == (hpPlayer5-1));
		assertTrue(player6.getHp() == (hpPlayer6-1));
		assertTrue(player7.getHp() == (hpPlayer7-1));
		
		//player defends himself with his solution card
		bug1.play(this.visitor);
		assertTrue(player.getHp() == hpPlayer);
		assertTrue(player.getState() == PlayerState.WAIT);
		
		//player2 defends himself with his lame execuse card
		bug2.play(this.visitor);
		assertTrue(player2.getHp() == hpPlayer2);
		assertTrue(player2.getState() == PlayerState.WAIT);
		
		//Every one took possible action now
		//check player is on_turn
		assertTrue(player.getState() == PlayerState.ON_TURN);
		
	}
	
	public void clearAllHands(){
		for(Player p : game.getPlayers()){
			game.cleanHandcards(p);
		}
	}
	
	public void getMentalHealthPoints(){
		hpPlayer = player.getHp();
		hpPlayer2 = player2.getHp();
		hpPlayer3 = player3.getHp();
		hpPlayer4 = player4.getHp();
		hpPlayer5 = player5.getHp();
		hpPlayer6 = player6.getHp();
		hpPlayer7 = player7.getHp();
	}
	
}
