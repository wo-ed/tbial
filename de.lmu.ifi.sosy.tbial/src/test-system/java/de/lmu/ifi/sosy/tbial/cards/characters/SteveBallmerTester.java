package de.lmu.ifi.sosy.tbial.cards.characters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.CharacterCardTester;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.actioncards.LameExcuse;

public class SteveBallmerTester extends CharacterCardTester {

	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void steveBallmerCanUseExcuses() {

		int oldHp = player10.getHp();
		// Give Ballmer an Excuse
		game2.cleanHandcards(player10);
		// Give Attacker a Bug
		game2.cleanHandcards(player11);

		BugReport b = (BugReport) game2.drawCardofClass(player11, BugReport.class);
		LameExcuse l = (LameExcuse) game2.drawCardofClass(player10, LameExcuse.class);

		b.setTargetPlayer(player10);
		// Attack Ballmer
		game2.setCurrentPlayer(player11);
		game2.playCard(b);

		// Deflect with Excuse
		game2.playCard(l);
		assertEquals(player10.getHp(), oldHp);
		game2.endTurn();
	}

	@Test
	public void steveBallmerCanUseBugsAsExcuses() {
		int oldHp = player10.getHp();
		// Give Ballmer a Bug
		game2.cleanHandcards(player10);

		game2.drawCardofClass(player10, BugReport.class);

		// Give Attacker a Bug
		game2.cleanHandcards(player11);
		BugReport b = (BugReport) game2.drawCardofClass(player11, BugReport.class);
		BugReport b2 = (BugReport) game2.drawCardofClass(player10, BugReport.class);
		b.setTargetPlayer(player10);
		// Attack Ballmer
		game2.setCurrentPlayer(player11);
		game2.playCard(b);

		// Deflect with BugReport
		game2.playCard(b2);
		assertEquals(player10.getHp(), oldHp);
		game2.endTurn();
	}

	@Test
	public void steveBallmerCanUseBugs() {
		// player10 == SteveBallmer
		int oldHpTarget = player11.getHp();
		assertTrue(oldHpTarget > 0);

		game2.setCurrentPlayer(player10);
		game2.cleanHandcards(player10);
		game2.cleanHandcards(player11);
		// Give Ballmer a Bug
		// Give Target an Excuse
		BugReport b = (BugReport) game2.drawCardofClass(player10, BugReport.class);
		TurnCard l = game2.drawCardofClass(player11, LameExcuse.class);
		b.setTargetPlayer(player11);

		// Attack Target and deflect

		game2.playCard(b);

		// Deflect with Excuse
		game2.playCard(l);
		assertEquals(player11.getHp(), oldHpTarget);
		assertTrue(player11.getHandcards().isEmpty());

		game2.endTurn();
		game2.setCurrentPlayer(player10);
		game2.cleanHandcards(player10);
		game2.cleanHandcards(player11);
		// Give Ballmer a Bug
		BugReport b2 = (BugReport) game2.drawCardofClass(player10, BugReport.class);
		b2.setTargetPlayer(player11);

		// Attack Target no deflection
		game2.playCard(b2);
		assertEquals(player11.getHp(), oldHpTarget - 1);
		game2.endTurn();
	}

	@Test
	public void steveBallmerCanUseExcusesAsBugs() {
		// player10 == SteveBallmer
		int oldHpPlayer11 = player11.getHp();
		assertTrue(oldHpPlayer11 > 0);

		// Give Ballmer an Excuse
		game2.setCurrentPlayer(player10);
		game2.cleanHandcards(player11);
		game2.cleanHandcards(player10);

		TurnCard lameExcuseAsBug = game2.drawCardofClass(player10, LameExcuse.class);

		// Give Target an Excuse
		LameExcuse le = (LameExcuse) game2.drawCardofClass(player11, LameExcuse.class);
		lameExcuseAsBug.setTargetPlayer(player11);

		// Attack Target and deflect
		game2.playCard(lameExcuseAsBug);

		// Deflect with Excuse
		game2.playCard(le);

		assertEquals(player11.getHp(), oldHpPlayer11);
		assertTrue(player11.getHandcards().isEmpty());

		game2.endTurn();
		game2.setCurrentPlayer(player10);
		game2.cleanHandcards(player10);
		game2.cleanHandcards(player11);

		// Give Ballmer an Excuse
		LameExcuse le2 = (LameExcuse) game2.drawCardofClass(player10, LameExcuse.class);
		le2.setTargetPlayer(player11);
		// Attack Target no deflection
		game2.playCard(le2);
		assertEquals(player11.getHp(), oldHpPlayer11 - 1);

		oldHpPlayer11 = player11.getHp();
		// Only can Play one excuse/bug per round
		LameExcuse le3 = (LameExcuse) game2.drawCardofClass(player10, LameExcuse.class);
		le3.setTargetPlayer(player11);
		// Attack Target no deflection
		game2.playCard(le3);
		assertEquals(player11.getHp(), oldHpPlayer11);

		BugReport bug = (BugReport) game2.drawCardofClass(player10, BugReport.class);
		bug.setTargetPlayer(player11);
		game2.playCard(bug);
		assertEquals(player11.getHp(), oldHpPlayer11);

		game2.endTurn();

		// Only can Play excuse as bug if on Turn
		LameExcuse le4 = (LameExcuse) game2.drawCardofClass(player10, LameExcuse.class);
		le4.setTargetPlayer(player11);
		// Attack Target no deflection
		game2.playCard(le4);
		assertEquals(player11.getHp(), oldHpPlayer11);
		assertTrue(player10.getHandcards().contains(le4));

		game2.endTurn();

	}

	@Test
	public void othersCanNotUseBugsAsExcuses() {

		// TODO TEST All
	}

	@Test
	public void othersCanNotUseExcusesAsBugs() {

		// TODO TEST All

	}

}
