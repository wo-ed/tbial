package de.lmu.ifi.sosy.tbial.cards;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.actioncards.SystemIntegration;

public class SystemIntegrationTester extends CardTester {
	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void onlyCurrentPlayerCanPlaySystemIntegration() {
		game.setCurrentPlayer(player);
		game.cleanHandcards(player2);

		TurnCard systemInt = game.drawCardofClass(player2, SystemIntegration.class);
		systemInt.setTargetPlayer(player);

		game.playCard(systemInt);

		assertTrue(player2.getHandcards().contains(systemInt));
		assertFalse(player.getPenaltyCards().contains(systemInt));
		assertNotEquals(player.getState(), PlayerState.SYSTEM_INTEGRATION);

		// current player can play bug - no deflection possible
		assertEquals(player, game.getCurrentPlayer());
		game.cleanHandcards(player);
		TurnCard systemInt2 = game.drawCardofClass(player, SystemIntegration.class);
		systemInt2.setTargetPlayer(player2);
		int oldHP = player2.getHp();
		game.playCard(systemInt2);

		assertFalse(player.getHandcards().contains(systemInt2));
		assertTrue(player2.getHp() == oldHP - 1);

		assertTrue(game.getHeap().contains(systemInt2));

	}

	@Test
	public void victimCantDefend() {
		game.setCurrentPlayer(player);
		game.cleanHandcards(player);
		game.cleanHandcards(player2);

		TurnCard c1 = game.drawCardofClass(player, SystemIntegration.class);
		c1.setTargetPlayer(player2);
		int oldHp = player2.getHp();
		assertTrue(player.getState() == PlayerState.ON_TURN);
		game.playCard(c1);

		assertTrue(!player.getHandcards().contains(c1));
		assertTrue(game.getHeap().contains(c1));
		assertTrue(player2.getHp() == oldHp - 1);
		assertTrue(player.getState() == PlayerState.ON_TURN);
	}

	@Test
	public void attackerWins() {
		game.setCurrentPlayer(player);
		game.cleanHandcards(player);
		game.cleanHandcards(player2);

		TurnCard c1 = game.drawCardofClass(player, SystemIntegration.class);
		TurnCard c2 = game.drawCardofClass(player, BugReport.class);
		c2.setTargetPlayer(player2);
		TurnCard c3 = game.drawCardofClass(player2, BugReport.class);
		c3.setTargetPlayer(player);

		c1.setTargetPlayer(player2);
		int oldHpP2 = player2.getHp();
		int oldHpP1 = player.getHp();
		assertTrue(player.getState() == PlayerState.ON_TURN);

		game.playCard(c1);

		assertTrue(player2.getState() == PlayerState.SYSTEM_INTEGRATION);
		assertTrue(player.getState() == PlayerState.WAIT);
		assertTrue(player2.getPenaltyCards().contains(c1));
		assertTrue(player2.getHp() == oldHpP2);
		assertTrue(player.getHp() == oldHpP1);

		game.playCard(c3);

		assertTrue(player.getState() == PlayerState.SYSTEM_INTEGRATION);
		assertTrue(player2.getState() == PlayerState.WAIT);
		assertTrue(player2.getPenaltyCards().contains(c1));
		assertTrue(player.getPenaltyCards().contains(c3));
		assertTrue(player2.getHp() == oldHpP2);
		assertTrue(player.getHp() == oldHpP1);

		game.playCard(c2);

		assertTrue(player2.getState() == PlayerState.WAIT);
		assertTrue(player.getState() == PlayerState.ON_TURN);
		assertTrue(!player2.getPenaltyCards().contains(c1));
		assertTrue(game.getHeap().contains(c2));
		assertTrue(game.getHeap().contains(c1));
		assertTrue(player2.getHp() == oldHpP2 - 1);
		assertTrue(player.getHp() == oldHpP1);
	}

}
