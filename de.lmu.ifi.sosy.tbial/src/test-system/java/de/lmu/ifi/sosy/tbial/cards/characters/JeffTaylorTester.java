package de.lmu.ifi.sosy.tbial.cards.characters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.CharacterCardTester;

public class JeffTaylorTester extends CharacterCardTester {

	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void jeffTaylorCanDrawCardOnNoHandcards() {
		// clean JeffTaylor's Handcards

		// on turn
		game.setCurrentPlayer(player3);
		int oldCountStack = game.getStack().size();
		game.cleanHandcards(player3);
		assertTrue(player3.getHandcards().isEmpty());
		// JeffTaylor can draw on empty Handcards
		assertTrue(game.attemptDrawCard(player3));
		assertEquals(player3.getHandcards().size(), 1);

		// JeffTaylor can not draw on not empty Handcards
		assertFalse(game.attemptDrawCard(player3));
		assertEquals(player3.getHandcards().size(), 1);
		// card drawn from heap
		assertEquals(oldCountStack - 1, game.getStack().size());

		// clean JeffTaylor's Handcards
		game.cleanHandcards(player3);
		assertTrue(player3.getHandcards().isEmpty());

		// not on turn
		game.setCurrentPlayer(player);
		int oldCountStack2 = game.getStack().size();

		// JeffTaylor can draw on empty Handcards
		assertTrue(game.attemptDrawCard(player3));
		assertEquals(player3.getHandcards().size(), 1);

		// JeffTaylor can not draw on not empty Handcards
		assertFalse(game.attemptDrawCard(player3));
		assertEquals(player3.getHandcards().size(), 1);
		// card drawn from heap
		assertEquals(oldCountStack2 - 1, game.getStack().size());
	}

	@Test
	public void othersCanNotDrawCardOnNoHandcards() {
		// clean Handcards player 1
		game.cleanHandcards(player);
		assertTrue(player.getHandcards().isEmpty());

		// on turn
		game.setCurrentPlayer(player);
		int oldCountStack = game.getStack().size();

		// cannot draw on empty Handcards
		assertFalse(game.attemptDrawCard(player));
		assertEquals(player.getHandcards().size(), 0);

		// can not draw on not empty Handcards
		game.drawCard(player);
		assertEquals(player.getHandcards().size(), 1);

		assertFalse(game.attemptDrawCard(player));
		assertEquals(player.getHandcards().size(), 1);

		// card not drawn from stack
		assertEquals(--oldCountStack, game.getStack().size());

		// clean Handcards
		game.cleanHandcards(player);
		assertTrue(player.getHandcards().isEmpty());

		// not on turn
		game.setCurrentPlayer(player);
		int oldCountStack2 = game.getStack().size();

		// cannot draw on empty Handcards
		assertFalse(game.attemptDrawCard(player));
		assertEquals(player.getHandcards().size(), 0);

		// can not draw on not empty Handcards
		game.drawCard(player);
		assertEquals(player.getHandcards().size(), 1);

		assertFalse(game.attemptDrawCard(player));
		assertEquals(player.getHandcards().size(), 1);

		// card not drawn from stack
		assertEquals(--oldCountStack2, game.getStack().size());

		// clean Handcards player2
		// on turn
		game.setCurrentPlayer(player2);
		game.cleanHandcards(player2);
		assertTrue(player2.getHandcards().isEmpty());

		int oldCountStack3 = game.getStack().size();

		// cannot draw on empty Handcards
		assertFalse(game.attemptDrawCard(player2));
		assertEquals(player2.getHandcards().size(), 0);

		// can not draw on not empty Handcards
		game.drawCard(player2);
		assertEquals(player2.getHandcards().size(), 1);

		assertFalse(game.attemptDrawCard(player2));
		assertEquals(player2.getHandcards().size(), 1);

		// card not drawn from stack
		assertEquals(--oldCountStack3, game.getStack().size());

		// clean Handcards
		game.cleanHandcards(player2);
		assertTrue(player2.getHandcards().isEmpty());

		// not on turn
		game.setCurrentPlayer(player2);
		int oldCountStack4 = game.getStack().size();

		// cannot draw on empty Handcards
		assertFalse(game.attemptDrawCard(player2));
		assertEquals(player2.getHandcards().size(), 0);

		// can not draw on not empty Handcards
		game.cleanHandcards(player2);
		game.drawCard(player2);
		assertFalse(game.attemptDrawCard(player2));
		assertEquals(player2.getHandcards().size(), 1);

		// card not drawn from stack
		assertEquals(--oldCountStack4, game.getStack().size());

		// TODO Copy Paste for all others

	}

}
