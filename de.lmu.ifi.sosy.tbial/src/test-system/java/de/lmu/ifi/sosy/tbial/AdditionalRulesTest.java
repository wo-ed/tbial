package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.MockGame;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.Solution;
import de.lmu.ifi.sosy.tbial.model.rolecards.EvilCodeMonkey;
import de.lmu.ifi.sosy.tbial.model.rolecards.HonestDeveloper;
import de.lmu.ifi.sosy.tbial.model.rolecards.Manager;

public class AdditionalRulesTest extends PageTestBase {
	TBIALSession session;
	MockGame game;
	User user;
	User u2;
	User u3;
	User u4;
	User u5;
	Player p1;
	Player p2;
	Player p3;
	Player p4;
	Player p5;

	@Before
	public void setUp() {
		setupApplication();
		user = new User("u1", "");
		game = new MockGame("G1", (short) 5, false, user, true);
		u2 = new User("u2", "");
		u3 = new User("u3", "");
		u4 = new User("u4", "");
		u5 = new User("u5", "");
		game.join(u2);
		game.join(u3);
		game.join(u4);
		game.join(u5);
		game.init();
		p1 = game.getPlayer(user);
		p2 = game.getPlayer(u2);
		p3 = game.getPlayer(u3);
		p4 = game.getPlayer(u4);
		p5 = game.getPlayer(u5);
	}

	@Test
	public void testPunishment() {
		assertTrue(p1.getRole() instanceof Manager);
		assertTrue(p5.getRole() instanceof HonestDeveloper);
		while (p5.getHp() > 0) {
			game.decHp(p5, p1);
		}
		assertFalse(p5.isAlive());
		assertTrue(p1.getHandcards().size() == 0);
	}

	@Test
	public void testReward() {
		assertTrue(p1.getRole() instanceof Manager);
		assertTrue(p3.getRole() instanceof EvilCodeMonkey);
		int handcardsBefore = p1.getHandcards().size();
		while (p3.getHp() > 0) {
			game.decHp(p3, p1);
		}
		assertFalse(p3.isAlive());
		assertTrue(p1.getHandcards().size() == handcardsBefore + 3);
	}

	@Test
	public void testShowdown() {

		// normal case

		assertTrue(game.livingPlayers() == 5);
		game.setCurrentPlayer(p1);

		TurnCard solution1 = game.drawCardofClass(p1, Solution.class);
		solution1.setPlayer(p1);
		assertTrue(p1.getState() == PlayerState.ON_TURN);
		solution1.setTargetPlayer(p2);
		assertTrue(p1.getHandcards().contains(solution1));
		if (p1.getMaxHp() == p1.getHp())
			game.decHp(p1, p1);
		game.playCard(solution1);
		assertTrue(game.getHeap().contains(solution1));
		assertFalse(p1.getHandcards().contains(solution1));

		// special case: p1 cannot play solution

		p3.setState(PlayerState.DEAD);
		p4.setState(PlayerState.DEAD);
		p5.setState(PlayerState.DEAD);

		assertTrue(game.livingPlayers() == 2);

		assertFalse(p1.getState() == PlayerState.DEAD);
		assertFalse(p2.getState() == PlayerState.DEAD);

		game.setCurrentPlayer(p1);
		TurnCard solution2 = game.drawCardofClass(p1, Solution.class);
		solution2.setPlayer(p1);
		assertTrue(p1.getState() == PlayerState.ON_TURN);
		solution2.setTargetPlayer(p2);
		assertTrue(p1.getHandcards().contains(solution2));
		game.playCard(solution1);
		assertTrue(p1.getHandcards().contains(solution2));

	}
}