package de.lmu.ifi.sosy.tbial.cards;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.actioncards.LameExcuse;
import de.lmu.ifi.sosy.tbial.model.actioncards.Solution;

public class BugExcusesSolutionTester extends CardTester {

	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void checkPlayBug() {
		// not current player can not play bug
		game.setCurrentPlayer(player);
		assertNotEquals(player2, game.getCurrentPlayer());
		game.cleanHandcards(player2);
		int oldHpP1 = player.getHp();
		TurnCard bug = game.drawCardofClass(player2, BugReport.class);
		bug.setTargetPlayer(player);
		game.playCard(bug);

		assertTrue(player2.getHandcards().contains(bug));
		assertFalse(player.getPenaltyCards().contains(bug));
		assertEquals(player.getHp(), oldHpP1);
		assertNotEquals(player.getState(), PlayerState.DEFLECT_BUG);

		// current player can play bug - no deflection possible
		assertEquals(player, game.getCurrentPlayer());
		game.cleanHandcards(player);
		int oldHpP2 = player2.getHp();
		TurnCard bug2 = game.drawCardofClass(player, BugReport.class);
		bug2.setTargetPlayer(player2);
		game.playCard(bug2);
		assertFalse(player.getHandcards().contains(bug));
		assertEquals(player2.getHp(), oldHpP2 - 1);
		game.endTurn();
	}

	@Test
	public void checkBugCanBeDeflectedByExcuse() {
		game.setCurrentPlayer(player2);
		game.cleanHandcards(player);
		game.cleanHandcards(player2);
		int oldHpP1 = player.getHp();

		// Deflect Bug with excuse
		TurnCard bug = game.drawCardofClass(player2, BugReport.class);
		TurnCard excuse = game.drawCardofClass(player, LameExcuse.class);
		bug.setTargetPlayer(player);

		game.playCard(bug);

		// Assert player and Game States after bug played
		assertEquals(player2.getState(), PlayerState.WAIT);
		assertEquals(player.getState(), PlayerState.DEFLECT_BUG);
		assertFalse(player2.getHandcards().contains(bug));
		assertTrue(player.getPenaltyCards().contains(bug));

		game.playCard(excuse);

		// Assert both Cards on Stack after deflection
		assertTrue(player.getPenaltyCards().isEmpty());
		assertTrue(player.getHandcards().isEmpty());
		assertTrue(player2.getHandcards().isEmpty());
		assertTrue(game.getHeap().contains(excuse));
		assertTrue(game.getHeap().contains(bug));

		// Assert player and Game States after deflection
		assertEquals(player.getHp(), oldHpP1);
		assertEquals(player.getState(), PlayerState.WAIT);
		assertEquals(player2.getState(), PlayerState.ON_TURN);
		game.endTurn();

	}

	@Test
	public void checkBugCanBeDeflectedBySolution() {
		game.setCurrentPlayer(player2);
		game.cleanHandcards(player);
		game.cleanHandcards(player2);
		int oldHpTarget = player.getHp();

		// Deflect Bug with Solution
		TurnCard bug = game.drawCardofClass(player2, BugReport.class);
		TurnCard solution = game.drawCardofClass(player, Solution.class);
		bug.setTargetPlayer(player);

		game.playCard(bug);

		// Assert player and Game States after bug played
		assertEquals(player2.getState(), PlayerState.WAIT);
		assertEquals(player.getState(), PlayerState.DEFLECT_BUG);
		assertFalse(player2.getHandcards().contains(bug));
		assertTrue(player.getPenaltyCards().contains(bug));

		game.playCard(solution);

		// Assert both Cards on Stack after deflection
		assertTrue(player.getPenaltyCards().isEmpty());
		assertTrue(player.getHandcards().isEmpty());
		assertTrue(player2.getHandcards().isEmpty());
		assertTrue(game.getHeap().contains(solution));
		assertTrue(game.getHeap().contains(bug));

		// Assert player and Game States after deflection
		assertEquals(player.getHp(), oldHpTarget);
		assertEquals(player.getState(), PlayerState.WAIT);
		assertEquals(player2.getState(), PlayerState.ON_TURN);
		game.endTurn();
	}

	@Test
	public void checkSolutionCanBeUsedToIncHp() {
		// Solution can not be used to Gain hp if player is not on turn
		game.setCurrentPlayer(player);
		if (player2.getMaxHp() == player2.getHp())
			game.decHp(player2, player2);
		int oldHp = player2.getHp();
		TurnCard solution = game.drawCardofClass(player2, Solution.class);
		game.playCard(solution);
		assertTrue(player2.getHandcards().contains(solution));
		assertEquals(player2.getHp(), oldHp);
		game.cleanHandcards(player2);

		// Solution can be used to Gain hp if player is on turn
		if (player.getMaxHp() == player.getHp())
			game.decHp(player, player);
		int oldHp2 = player.getHp();

		TurnCard solution2 = game.drawCardofClass(player, Solution.class);
		game.playCard(solution2);
		assertFalse(player.getHandcards().contains(solution2));
		assertEquals(player.getHp(), oldHp2 + 1);

		// Multiple Solutions can be used to Gain hp
		if (player.getMaxHp() == player.getHp())
			game.decHp(player, player);
		int oldHp3 = player.getHp();
		TurnCard solution3 = game.drawCardofClass(player, Solution.class);
		game.playCard(solution3);
		assertFalse(player.getHandcards().contains(solution3));
		assertEquals(player.getHp(), oldHp3 + 1);

		// max Hp can not be passed with Solutions
		while (player.getMaxHp() > player.getHp())
			game.incHp(player);
		int oldHp4 = player.getHp();
		TurnCard solution4 = game.drawCardofClass(player, Solution.class);
		game.playCard(solution4);

		assertTrue(player.getHandcards().contains(solution4));
		assertEquals(player.getHp(), oldHp4);
		game.cleanHandcards(player);

		// Dead players can not revive with Solutions
		game.setCurrentPlayer(player2);
		while (player2.isAlive())
			game.decHp(player2, player2);
		int oldHp5 = player2.getHp();
		TurnCard solution5 = game.drawCardofClass(player2, Solution.class);

		game.playCard(solution5);
		assertTrue(player2.getHandcards().contains(solution5));
		assertEquals(player2.getHp(), oldHp5);

		game.endTurn();
	}

	@Test
	public void checkBugCanNotBeDeflectedBySolutionIfHpIs1() {
		game.setCurrentPlayer(player2);
		game.cleanHandcards(player);
		game.cleanHandcards(player2);

		assertTrue(player.isAlive());

		while (player.getHp() > 1) {
			game.decHp(player, player);
		}

		// Can not Deflect Bug with Solution if target's hp == 1
		TurnCard bug = game.drawCardofClass(player2, BugReport.class);
		TurnCard solution = game.drawCardofClass(player, Solution.class);
		bug.setTargetPlayer(player);

		game.playCard(bug);

		// Assert player and Game States after bug played
		assertEquals(player2.getState(), PlayerState.ON_TURN);
		assertEquals(player.getState(), PlayerState.DEAD);
		assertFalse(player2.getHandcards().contains(bug));
		assertFalse(player.getPenaltyCards().contains(bug));

		assertFalse(player.isAlive());

		game.playCard(solution);

		// Assert BUg on Stack
		assertTrue(player.getPenaltyCards().isEmpty());
		assertTrue(game.getHeap().contains(bug));

		// Assert player and Game States
		assertEquals(player.getState(), PlayerState.DEAD);
		assertEquals(player2.getState(), PlayerState.ON_TURN);
		assertFalse(player.isAlive());
		game.endTurn();
	}

}
