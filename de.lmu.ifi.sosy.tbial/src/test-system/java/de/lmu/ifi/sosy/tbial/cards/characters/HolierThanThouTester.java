package de.lmu.ifi.sosy.tbial.cards.characters;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.CharacterCardTester;
import de.lmu.ifi.sosy.tbial.model.characters.HolierThanThou;

public class HolierThanThouTester extends CharacterCardTester {

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void holierThanThouSeesPlayerWithDecrementedPrestige() {
		// Holier than Thou
		Assert.assertEquals(true, player2.getCharacter() instanceof HolierThanThou);
		Assert.assertEquals(4, player2.getCharacter().getHp());

		Assert.assertEquals(0, player.getCharacter().getPrestige());
		Assert.assertEquals(-1, player.getCharacter().getPrestigeSeenByCharacter(player2.getCharacter()));
	}

}
