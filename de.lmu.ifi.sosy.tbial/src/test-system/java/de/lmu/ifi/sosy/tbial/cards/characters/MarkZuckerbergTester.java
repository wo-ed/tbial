package de.lmu.ifi.sosy.tbial.cards.characters;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.CharacterCardTester;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.abilitycards.BugDelegation;
import de.lmu.ifi.sosy.tbial.model.rolecards.Manager;
import de.lmu.ifi.sosy.tbial.model.stumblingblockcards.FortranMaintenance;

//player9 == Mark Zuckerberg
public class MarkZuckerbergTester extends CharacterCardTester {

	@Before
	public void setUp() {
		super.setUp();
	}

	// TODO only Steal one Card!

	@Test
	public void checkMarkZuckerbergCanTakeHandCardOfCauser() {

		if (player11.getHandcards().isEmpty())
			game2.drawCard(player11);

		game2.decHp(player9, player11);

		TurnCard toSteal = player11.getHandcards().get(0);

		assertTrue(game2.attemptStealCard(toSteal, player9, player11));

		// Can only take 1 card for 1 lost hp
		assertFalse(game2.attemptStealCard(toSteal, player9, player11));

		assertFalse(player11.getHandcards().contains(toSteal));
		assertTrue(player9.getHandcards().contains(toSteal));
		game2.incHp(player9);
	}

	@Test
	public void checkMarkZuckerbergCanTakeBuffCardOfCauser() {

		if (player11.getBuffCards().isEmpty()) {
			player11.addBuffCard(new BugDelegation());
		}

		game2.decHp(player9, player11);

		TurnCard toSteal = player11.getBuffCards().get(0);

		assertTrue(game2.attemptStealCard(toSteal, player9, player11));

		assertFalse(player11.getBuffCards().contains(toSteal));
		assertTrue(player9.getHandcards().contains(toSteal));
		game2.incHp(player9);
	}

	@Test
	public void checkMarkZuckerbergCanTakePenaltyCardOfCauser() {
		if (player11.getPenaltyCards().isEmpty()) {
			player11.addPenaltyCard(new FortranMaintenance());
		}

		game2.decHp(player9, player11);

		TurnCard toSteal = player11.getPenaltyCards().get(0);

		assertTrue(game2.attemptStealCard(toSteal, player9, player11));

		assertFalse(player11.getPenaltyCards().contains(toSteal));
		assertTrue(player9.getHandcards().contains(toSteal));
		game2.incHp(player9);
	}

	@Test
	public void checkMarkZuckerbergIgnoresPrestige() {
		player9.getCharacter().setPrestige(0);
		player10.getCharacter().setPrestige(3);

		if (player10.getHandcards().isEmpty())
			game2.drawCard(player10);

		game2.decHp(player9, player10);

		TurnCard toSteal = player10.getHandcards().get(0);

		assertTrue(game2.attemptStealCard(toSteal, player9, player10));

		assertFalse(player10.getHandcards().contains(toSteal));
		assertTrue(player9.getHandcards().contains(toSteal));
		game2.incHp(player9);
	}

	@Test
	public void checkMarkZuckerbergDoNotHaveToStealCard() {

		if (player10.getHandcards().isEmpty())
			game2.drawCard(player10);
		if (player10.getPenaltyCards().isEmpty()) {
			player10.addPenaltyCard(new FortranMaintenance());
		}
		if (player10.getBuffCards().isEmpty()) {
			player10.addBuffCard(new BugDelegation());
		}

		int marksHandCardsSize = player9.getHandcards().size();
		int attackersHandCardsSize = player10.getHandcards().size();
		int attackersBuffCardsSize = player10.getBuffCards().size();
		int attackersPenaltyCardsSize = player10.getPenaltyCards().size();

		game2.decHp(player9, player10);

		game2.endTurn(player9);

		assertTrue(player9.getHandcards().size() == marksHandCardsSize);
		assertTrue(player10.getHandcards().size() == attackersHandCardsSize);
		assertTrue(player10.getBuffCards().size() == attackersBuffCardsSize);
		assertTrue(player10.getPenaltyCards().size() == attackersPenaltyCardsSize);

		game2.incHp(player9);
	}

	@Test
	public void checkMarkZuckerbergCanNotStealCardIfFired() {
		if (player10.getHandcards().isEmpty())
			game2.drawCard(player10);
		if (player10.getPenaltyCards().isEmpty()) {
			player10.addPenaltyCard(new FortranMaintenance());
		}
		if (player10.getBuffCards().isEmpty()) {
			player10.addBuffCard(new BugDelegation());
		}

		game2.cleanHandcards(player9);

		int attackersHandCardsSize = player10.getHandcards().size();
		int attackersBuffCardsSize = player10.getBuffCards().size();
		int attackersPenaltyCardsSize = player10.getPenaltyCards().size();

		while (player9.getHp() > 1) {
			game2.decHp(player9, player9);
		}

		game2.decHp(player9, player10);

		TurnCard toSteal = player10.getHandcards().get(0);
		assertFalse(game2.attemptStealCard(toSteal, player9, player10));

		assertTrue(player9.getHandcards().size() == 0);
		assertTrue(player10.getHandcards().size() == attackersHandCardsSize);
		assertTrue(player10.getBuffCards().size() == attackersBuffCardsSize);
		assertTrue(player10.getPenaltyCards().size() == attackersPenaltyCardsSize);
	}

	@Test
	public void checkMarkZuckerbergCanSteal3CardsIfSurvivedFM() {
		if (player10.getHandcards().isEmpty())
			game2.drawCard(player10);
		if (player10.getPenaltyCards().isEmpty()) {
			player10.addPenaltyCard(new FortranMaintenance());
		}
		if (player10.getBuffCards().isEmpty()) {
			player10.addBuffCard(new BugDelegation());
		}

		int marksHandCardsSize = player9.getHandcards().size();
		int attackersHandCardsSize = player10.getHandcards().size();
		int attackersBuffCardsSize = player10.getBuffCards().size();
		int attackersPenaltyCardsSize = player10.getPenaltyCards().size();

		player9.getCharacter().initHp(new Manager());

		FortranMaintenance fm = new FortranMaintenance();
		fm.setPlayer(player10);
		fm.setTargetPlayer(player9);

		// TODO
		game2.decHp(player9, player10);
		game2.decHp(player9, player10);
		game2.decHp(player9, player10);

		assertTrue(game2.attemptStealCard(player10.getHandcards().get(0), player9, player10));
		assertTrue(game2.attemptStealCard(player10.getPenaltyCards().get(0), player9, player10));
		assertTrue(game2.attemptStealCard(player10.getBuffCards().get(0), player9, player10));

		assertTrue(player9.getHandcards().size() == marksHandCardsSize + 3);
		assertTrue(player10.getHandcards().size() == attackersHandCardsSize - 1);
		assertTrue(player10.getBuffCards().size() == attackersBuffCardsSize - 1);
		assertTrue(player10.getPenaltyCards().size() == attackersPenaltyCardsSize - 1);
	}

}
