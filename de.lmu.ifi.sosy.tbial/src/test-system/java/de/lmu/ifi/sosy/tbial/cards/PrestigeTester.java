package de.lmu.ifi.sosy.tbial.cards;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.abilitycards.WearsSunglassesAtWork;
import de.lmu.ifi.sosy.tbial.model.abilitycards.WearsTieAtWork;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Accenture;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Google;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Microsoft;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Nasa;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.actioncards.Pwnd;
import de.lmu.ifi.sosy.tbial.model.actioncards.Refactoring;
import de.lmu.ifi.sosy.tbial.model.actioncards.Solution;
import de.lmu.ifi.sosy.tbial.model.characters.BruceSchneier;
import de.lmu.ifi.sosy.tbial.model.characters.JeffTaylor;

public class PrestigeTester extends CardTester {

	@Override
	@Before
	public void setUp() {
		super.setUp();

	}

	@Test
	public void checkOnlyOnePreviousJobCanBePlayed() {

		game.cleanAllHandcards();
		TurnCard nasa = game.drawCardofClass(player, Nasa.class);
		TurnCard google = game.drawCardofClass(player, Google.class);
		TurnCard microsoft = game.drawCardofClass(player, Microsoft.class);
		TurnCard accenture = game.drawCardofClass(player, Accenture.class);

		game.setCurrentPlayer(player);
		microsoft.setTargetPlayer(player);
		game.playCard(microsoft);

		assertEquals(1, player.getCharacter().getPrestige());
		assertTrue(player.getBuffCards().contains(microsoft));

		google.setTargetPlayer(player);
		game.playCard(google);

		assertEquals(2, player.getCharacter().getPrestige());
		assertTrue(player.getBuffCards().contains(google));
		assertFalse(player.getBuffCards().contains(microsoft));

		nasa.setTargetPlayer(player);
		game.playCard(nasa);

		assertEquals(3, player.getCharacter().getPrestige());

		assertTrue(player.getBuffCards().contains(nasa));
		assertFalse(player.getBuffCards().contains(google));
		assertFalse(player.getBuffCards().contains(microsoft));
		accenture.setTargetPlayer(player);
		game.playCard(accenture);

		assertEquals(0, player.getCharacter().getPrestige());

		assertTrue(player.getBuffCards().contains(accenture));
		assertFalse(player.getBuffCards().contains(nasa));
		assertFalse(player.getBuffCards().contains(google));
		assertFalse(player.getBuffCards().contains(microsoft));

		game.throwCardToHeap(accenture);
		game.throwCardToHeap(nasa);
		game.throwCardToHeap(google);
		game.throwCardToHeap(microsoft);
		resetPrestiges();
	}

	@Test
	public void checkOnlyOneWearTieCanBePlayed() {

		game.cleanAllHandcards();
		TurnCard wTie1 = game.drawCardofClass(player, WearsTieAtWork.class);
		TurnCard wTie2 = game.drawCardofClass(player, WearsTieAtWork.class);

		assertNotEquals(wTie1.getId(), wTie2.getId());

		game.setCurrentPlayer(player);
		game.playCard(wTie1);

		assertEquals(1, player.getCharacter().getPrestigeSeenByCharacter(player3.getCharacter()));
		assertTrue(player.getBuffCards().contains(wTie1));

		game.playCard(wTie2);

		assertEquals(1, player.getCharacter().getPrestigeSeenByCharacter(player3.getCharacter()));
		assertTrue(player.getBuffCards().contains(wTie2));
		assertFalse(player.getBuffCards().contains(wTie1));

		game.throwCardToHeap(wTie1);
		game.throwCardToHeap(wTie2);
		resetPrestiges();
	}

	@Test
	public void checkOnlyOneWearSunglassesCanBePlayed() {

		game.cleanAllHandcards();
		TurnCard wSun1 = game.drawCardofClass(player, WearsSunglassesAtWork.class);
		TurnCard wSun2 = game.drawCardofClass(player, WearsSunglassesAtWork.class);

		assertEquals(player.getHandcards().size(), 2);

		assertNotEquals(wSun1.getId(), wSun2.getId());

		game.setCurrentPlayer(player);
		game.playCard(wSun1);

		assertTrue(player.getBuffCards().contains(wSun1));

		game.playCard(wSun2);

		assertNotEquals(wSun1.getId(), wSun2.getId());

		assertTrue(player.getBuffCards().contains(wSun2));
		assertFalse(player.getBuffCards().contains(wSun1));

		game.throwCardToHeap(wSun1);
		game.throwCardToHeap(wSun2);
		resetPrestiges();
	}

	@Test
	public void checkOneCanWearSunglassesAndWearTieAtTheSameTime() {
		game.cleanAllHandcards();
		TurnCard wTie = game.drawCardofClass(player, WearsTieAtWork.class);
		TurnCard wSun = game.drawCardofClass(player, WearsSunglassesAtWork.class);
		TurnCard google = game.drawCardofClass(player3, Google.class);

		game.setCurrentPlayer(player3);
		game.playCard(google);

		game.setCurrentPlayer(player);
		game.playCard(wTie);
		game.playCard(wSun);

		assertTrue(BruceSchneier.class.isInstance(player.getCharacter()));
		assertTrue(JeffTaylor.class.isInstance(player3.getCharacter()));

		assertEquals(1, player.getCharacter().getPrestigeSeenByCharacter(player3.getCharacter()));
		assertEquals(1, player3.getCharacter().getPrestigeSeenByCharacter(player.getCharacter()));
		assertTrue(player.getBuffCards().contains(wSun));
		assertTrue(player.getBuffCards().contains(wTie));

		game.throwCardToHeap(wSun);
		game.throwCardToHeap(wTie);
		game.throwCardToHeap(google);
		resetPrestiges();
	}

	@Test
	public void checkCanNotAttackPlayersWithMorePrestige() {
		game.cleanAllHandcards();
		TurnCard bug = game.drawCardofClass(player, BugReport.class);
		TurnCard google = game.drawCardofClass(player3, Google.class);

		game.setCurrentPlayer(player3);
		google.setTargetPlayer(player3);
		game.playCard(google);

		int oldHpPlayer3 = player3.getHp();

		game.setCurrentPlayer(player);
		game.cleanHandcards(player3);

		bug.setTargetPlayer(player3);
		game.playCard(bug);

		assertEquals(oldHpPlayer3, player3.getHp());
		assertTrue(player.getHandcards().contains(bug));

		game.throwCardToHeap(bug);

		game.cleanAllHandcards();
		TurnCard pwnd = game.drawCardofClass(player, Pwnd.class);
		TurnCard toSteal = game.drawCardofClass(player3, Solution.class);
		game.setCurrentPlayer(player);
		pwnd.setTargetPlayer(player3);
		game.playCard(pwnd);

		game.pwnd(toSteal);

		assertTrue(player3.getHandcards().contains(toSteal));
		assertFalse(player.getHandcards().contains(toSteal));

		game.throwCardToHeap(pwnd);
		game.throwCardToHeap(google);
		game.throwCardToHeap(toSteal);
		resetPrestiges();
	}

	@Test
	public void checkCanAttackPlayersWithLessOrEqualPrestige() {
		game.cleanAllHandcards();
		TurnCard bug = game.drawCardofClass(player, BugReport.class);
		TurnCard google1 = game.drawCardofClass(player3, Google.class);
		TurnCard google2 = game.drawCardofClass(player, Google.class);

		game.setCurrentPlayer(player3);

		game.playCard(google1);

		int oldHpPlayer3 = player3.getHp();

		game.setCurrentPlayer(player);
		game.cleanHandcards(player3);

		game.playCard(google2);

		bug.setTargetPlayer(player3);
		game.playCard(bug);

		assertEquals(oldHpPlayer3 - 1, player3.getHp());

		game.throwCardToHeap(google1);
		game.throwCardToHeap(google2);
		resetPrestiges();
	}

	@Test
	public void checkIgnorePrestige() {
		game.cleanAllHandcards();
		TurnCard refactoring = game.drawCardofClass(player, Refactoring.class);
		TurnCard google = game.drawCardofClass(player3, Google.class);

		game.setCurrentPlayer(player3);
		google.setTargetPlayer(player3);
		game.playCard(google);

		TurnCard toSteal = game.drawCardofClass(player3, Solution.class);
		game.setCurrentPlayer(player);
		refactoring.setTargetPlayer(player3);
		game.playCard(refactoring);

		game.refactoring(toSteal);

		assertTrue(game.getHeap().contains(toSteal));
		assertFalse(player3.getHandcards().contains(toSteal));

		game.throwCardToHeap(refactoring);
		game.throwCardToHeap(google);
		resetPrestiges();

	}

	@Test
	public void checkAccentureCanPlayMultipleBugs() {
		game.cleanAllHandcards();
		TurnCard bug1 = game.drawCardofClass(player5, BugReport.class);
		TurnCard bug2 = game.drawCardofClass(player5, BugReport.class);
		TurnCard accenture = game.drawCardofClass(player5, Accenture.class);

		game.setCurrentPlayer(player5);
		game.cleanHandcards(player3);
		game.cleanHandcards(player4);

		game.playCard(accenture);

		int oldHpP3 = player3.getHp();
		int oldHpP4 = player4.getHp();

		bug1.setTargetPlayer(player3);
		bug2.setTargetPlayer(player4);

		game.playCard(bug1);
		game.playCard(bug2);

		assertEquals(oldHpP3 - 1, player3.getHp());
		assertEquals(oldHpP4 - 1, player4.getHp());

		game.throwCardToHeap(accenture);
		resetPrestiges();
	}

	private void resetPrestiges() {
		game.getAllPlayers().stream().forEach(p -> {
			p.getCharacter().setPrestige(0);
			p.getCharacter().removeSelfPrestigeModifier();
			p.getCharacter().removeOthersPrestigeModifier();
		});
	}

}
