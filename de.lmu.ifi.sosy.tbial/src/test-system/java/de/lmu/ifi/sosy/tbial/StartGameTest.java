package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.wicket.util.tester.FormTester;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.MockGame;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.AbilityCard;
import de.lmu.ifi.sosy.tbial.model.ActionCard;
import de.lmu.ifi.sosy.tbial.model.Card;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.GameState;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.StumblingBlockCard;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.actioncards.LameExcuse;
import de.lmu.ifi.sosy.tbial.model.actioncards.Solution;
import de.lmu.ifi.sosy.tbial.model.rolecards.Consultant;
import de.lmu.ifi.sosy.tbial.model.rolecards.EvilCodeMonkey;
import de.lmu.ifi.sosy.tbial.model.rolecards.HonestDeveloper;
import de.lmu.ifi.sosy.tbial.model.rolecards.Manager;
import de.lmu.ifi.sosy.tbial.pages.GameLobbyPanel;
import de.lmu.ifi.sosy.tbial.pages.GamePage;
import de.lmu.ifi.sosy.tbial.pages.TabPage;

public class StartGameTest extends PageTestBase {

	TBIALSession session;

	@Before
	public void setUp() {
		setupApplication();
		database.register("testuser", "1");
		session = getSession();
		session.signIn("testuser", "1");
	}

	@Test
	public void startGame() {

		User user = session.getUser();

		Game game1 = new Game("g1", (short) 7, false, user);
		TBIALApplication.getInstance().addGame(game1);
		TBIALApplication.getInstance().joinGame(user, game1);
		Player p1 = game1.getPlayer(user);
		assertNotNull(p1);
		assertTrue(game1.getPlayers().size() == 1);

		TabPage tabPage = tester.startPage(TabPage.class);
		tester.assertRenderedPage(TabPage.class);
		tabPage.setGameLobbyTab(game1.getName());
		GameLobbyPanel gameLobby = tabPage.getCurrentGameLobby();

		TBIALSession session = tabPage.getSession();
		session.setUser(user);

		assertTrue(game1.getState() == GameState.WAITING);

		// The game cannot be started without other players
		assertNotReady(gameLobby);

		// Three players is still not enough
		game1.join(new User("u2", ""));
		game1.join(new User("u3", ""));
		assertNotReady(gameLobby);

		// Four players are enough;
		game1.join(new User("u4", ""));
		assertReady(gameLobby);

		// After the game is started, the capacity must adapt to the players
		// count
		assertTrue(game1.getCapacity() == game1.getPlayers().size());
		assertTrue(game1.getAvailablePlayers().isEmpty());

		// The players count must not be higher than the capacity
		assertFalse(game1.join(new User("u5", "")).isPresent());

		// More than four players works
		Game game2 = new Game("g2", (short) 5, false, user);
		TBIALApplication.getInstance().addGame(game2);
		TBIALApplication.getInstance().joinGame(user, game2);
		game2.join(new User("u2", ""));
		game2.join(new User("u3", ""));
		game2.join(new User("u4", ""));
		game2.join(new User("u5", ""));
		assertReady(gameLobby);

		// Not everyone can start the game
		Game game3 = new Game("g3", (short) 4, false, new User("u2", ""));
		TBIALApplication.getInstance().addGame(game3);
		TBIALApplication.getInstance().joinGame(user, game3);
		game3.join(new User("u3", ""));
		game3.join(new User("u4", ""));

		assertNotReady(gameLobby);
		game3.setOwner(null);
		assertNotReady(gameLobby);

		// Only the owner can start it
		game3.setOwner(session.getUser());
		assertReady(gameLobby);

		assertTrue(game3.getState() == GameState.RUNNING);

		// Check the heap and stack size
		int handCardsCount = game3.getAllPlayers().stream().mapToInt(p -> p.getHandcards().size()).sum();
		assertTrue(game3.getStack().size() == 79 - handCardsCount);
		assertTrue(game3.getHeap().isEmpty());

		// The form needs to be tested in order to test the page redirection
		Game game4 = new MockGame("g4", (short) 4, false, user, true);
		TBIALApplication.getInstance().joinGame(user, game4);
		TBIALApplication.getInstance().addGame(game4);
		game4.join(new User("u2", ""));
		game4.join(new User("u3", ""));
		game4.join(new User("u4", ""));

		FormTester formTester = tester.newFormTester("tabs:panel:gameLobbyTab:lobbyForm", false);
		formTester.submit("startButton");

		// The player attributes must be initialized
		game4.getPlayers().forEach(player -> {
			assertNotNull(player.getRole());
			assertTrue(player.getCharacter().getPrestige() == 0);
			if (game4.getCurrentPlayer().equals(player))
				assertEquals(player.getHandcards().size(), player.getHp() + 2);
			else
				assertEquals(player.getHandcards().size(), player.getHp());
		});

		// Manager role cards must be visible, others role cards not
		List<Player> managers = game4.getPlayers().stream().filter(p -> p.getRole() instanceof Manager)
				.collect(Collectors.toList());
		assertTrue(managers.size() == 1);

		Player manager = managers.get(0);
		assertTrue(manager.getRole().isVisible() == true);

		Stream<Player> notManagers = game4.getPlayers().stream().filter(p -> p.getRole() instanceof Manager == false);

		notManagers.forEach(nm -> {
			assertTrue(nm.getRole().isVisible() == false);
		});

		// Assert redirection after button click
		tester.assertRenderedPage(GamePage.class);
	}

	private void assertNotReady(GameLobbyPanel gameLobby) {
		tester.executeAllTimerBehaviors(gameLobby);
		tester.assertInvisible("tabs:panel:gameLobbyTab:lobbyForm:startButton");
		assertFalse(gameLobby.startGame());
	}

	private void assertReady(GameLobbyPanel gameLobby) {
		tester.executeAllTimerBehaviors(gameLobby);
		tester.assertVisible("tabs:panel:gameLobbyTab:lobbyForm:startButton");
		assertTrue(gameLobby.startGame());
	}

	/**
	 * Asserts that the player is redirected to the GamePage when another Player
	 * started the game.
	 */
	@Test
	public void testAutomaticPageRedirection() {

		User user = session.getUser();

		Game game = new Game("g5", (short) 7, false, user);
		TBIALApplication.getInstance().addGame(game);
		TBIALApplication.getInstance().joinGame(user, game);
		TabPage tabPage = tester.startPage(TabPage.class);
		tester.assertRenderedPage(TabPage.class);
		tabPage.setGameLobbyTab(game.getName());

		GameLobbyPanel gameLobby = tabPage.getCurrentGameLobby();
		tester.startComponentInPage(gameLobby);

		// The User is not the owner, so the start button should not be visible
		tester.assertInvisible("gameLobbyTab:lobbyForm:startButton");

		tester.executeAllTimerBehaviors(gameLobby);
		tester.assertComponent("gameLobbyTab", GameLobbyPanel.class);

		IntStream.rangeClosed(0, 5).forEach(i -> game.join(new User(i + "", "")));

		game.init();

		tester.executeAllTimerBehaviors(gameLobby);
		tester.assertRenderedPage(GamePage.class);
	}

	@Test
	public void countTurnCards() {
		Game game = new MockGame("g7", (short) 7, true, new User("", ""), true);
		game.initializeStack();
		List<TurnCard> stack = game.getStack();
		assertTrue(stack.size() == 79);
		assertTrue(stack.stream().filter(c -> c instanceof BugReport).count() == 24);
		assertTrue(stack.stream().filter(c -> c instanceof LameExcuse).count() == 12);
		assertTrue(stack.stream().filter(c -> c instanceof Solution).count() == 6);
		assertTrue(stack.stream().filter(c -> c instanceof AbilityCard).count() == 13);
		assertTrue(stack.stream().filter(c -> c instanceof StumblingBlockCard).count() == 4);
		assertTrue(stack.stream().filter(c -> c instanceof ActionCard && c instanceof BugReport == false
				&& c instanceof LameExcuse == false && c instanceof Solution == false).count() == 20);
	}

	@Test
	public void countRoleCards() {

		for (int playersCount = 4; playersCount <= Game.MAX_PLAYERS; playersCount++) {
			Game game = new MockGame("", (short) playersCount, true, getSession().getUser(), true);

			IntStream.range(1, playersCount).forEach(i -> game.join(new User("", "")));
			assertTrue(game.getPlayers().size() == playersCount);

			if (playersCount >= Game.MIN_PLAYERS) {
				game.init();
				List<Card> roleCards = game.getPlayers().stream().map(p -> p.getRole()).collect(Collectors.toList());

				int monkeyCount = playersCount < 6 ? 2 : 3;
				int honestCount = 0;
				switch (playersCount) {
				case 4:
					honestCount = 0;
					break;
				case 5:
				case 6:
					honestCount = 1;
					break;
				case 7:
					honestCount = 2;
					break;
				}

				assertTrue(roleCards.stream().filter(c -> c instanceof Manager).count() == 1);
				assertTrue(roleCards.stream().filter(c -> c instanceof Consultant).count() == 1);
				assertTrue(roleCards.stream().filter(c -> c instanceof EvilCodeMonkey).count() == monkeyCount);
				assertTrue(roleCards.stream().filter(c -> c instanceof HonestDeveloper).count() == honestCount);
			}
		}
	}
}