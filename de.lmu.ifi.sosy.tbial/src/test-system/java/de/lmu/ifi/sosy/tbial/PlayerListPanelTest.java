package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.wicket.util.tester.TagTester;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.pages.PlayerListPanel;
import de.lmu.ifi.sosy.tbial.pages.TabPage;

public class PlayerListPanelTest extends PageTestBase {

	private TabPage tabPage;

	@Before
	public void setUp() {
		setupApplication();
		database.register("testuser", "1");
		TBIALSession session = getSession();
		session.signIn("testuser", "1");
		TBIALApplication.getUsers().clear();
		TBIALApplication.addUser(new User("testuser", "1"));

	}

	@Test
	public void checkTable() {
		tabPage = tester.startPage(TabPage.class);
		tabPage.selectPlayerListTab();
		tester.assertComponent("tabs:panel:playerListTab", PlayerListPanel.class);

		PlayerListPanel playerListPanel = (PlayerListPanel) tabPage.getCurrentPlayerList();
		tester.startComponentInPage(playerListPanel);

		List<TagTester> tagTesterName = tester.getTagsByWicketId("name");
		assertTrue(tagTesterName.size() == 1);

		List<TagTester> tagTesterState = tester.getTagsByWicketId("state");
		assertTrue(tagTesterState.size() == 1);

		List<TagTester> tagTesterSendInvite = tester.getTagsByWicketId("sendInvite");
		assertTrue(tagTesterSendInvite.size() == 1);

		// add new user
		User newUser = new User("testuser2", "2");
		TBIALApplication.addUser(newUser);
		tester.executeAllTimerBehaviors(playerListPanel);

		// Check updated list
		tagTesterName = tester.getTagsByWicketId("name");
		assertTrue(tagTesterName.size() == 2);
		assertEquals(tagTesterName.get(0).getValue(), "testuser");
		assertEquals(tagTesterName.get(1).getValue(), "testuser2");

		tagTesterState = tester.getTagsByWicketId("state");
		assertTrue(tagTesterState.size() == 2);
		for (int i = 0; i < tagTesterState.size(); i++) {
			assertEquals(tagTesterState.get(i).getValue(), "in lobby");
		}

		tagTesterSendInvite = tester.getTagsByWicketId("sendInvite");
		assertTrue(tagTesterSendInvite.size() == 2);
		for (int i = 0; i < tagTesterSendInvite.size(); i++) {
			assertEquals(tagTesterSendInvite.get(i).getValue(), "Invite");
		}

		// change state of new user(testuser2)
		Game game = new Game("newGame", (short) 4, false, newUser);
		TBIALApplication.getInstance().addGame(game);
		TBIALApplication.getInstance().joinGame(newUser, game);
		tester.executeAllTimerBehaviors(playerListPanel);

		// check user
		tagTesterState = tester.getTagsByWicketId("state");
		assertEquals(tagTesterState.get(1).getValue(), "waiting in game lobby");
	}

}
