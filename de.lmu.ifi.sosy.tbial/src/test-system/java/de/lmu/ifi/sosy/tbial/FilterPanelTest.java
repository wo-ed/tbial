package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.wicket.util.tester.FormTester;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.MockGame;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.GameState;
import de.lmu.ifi.sosy.tbial.pages.FilterPanel;
import de.lmu.ifi.sosy.tbial.pages.LobbyPanel;
import de.lmu.ifi.sosy.tbial.pages.TabPage;
import de.lmu.ifi.sosy.tbial.util.TypesForFilters;

public class FilterPanelTest extends PageTestBase {
	TBIALSession session;
	private List<Game> games;
	TabPage tabPage;

	@Before
	public void setUp() {
		setupApplication();
		games = TBIALApplication.getGames();
		User user = new User("testuser", "");
		database.register("testuser", "testpassword");
		TBIALSession session = getSession();
		session.signIn("testuser", "testpassword");
		Game game = new MockGame("testGameName", (short) 4, false, user, true);
		TBIALApplication.getInstance().addGame(game);
		game.setName("testGameName");
		game.setCapacity((short) 4);
		game.setOwner(user);
		game.setPrivate(true);

		games.add(game);

		tabPage = tester.startPage(TabPage.class);
		tester.assertRenderedPage(TabPage.class);
	}

	@Test
	public void startFilterPageTest() {
		FormTester formFilter = tester.newFormTester("tabs:panel:lobbyTab:form5", false);
		formFilter.submit("filter");
		tester.assertComponent("tabs:panel:lobbyTab", FilterPanel.class);
	}

	@Test
	public void fillFiltersTest() {
		FormTester formFilter = tester.newFormTester("tabs:panel:lobbyTab:form5", false);
		formFilter.submit("filter");
		FilterPanel filterPanel = tabPage.getCurrentLobby().getFilterPanel();
		Map<TypesForFilters, String> filters = filterPanel.getFilters();
		Map<TypesForFilters, String> filtersToTest = new HashMap<>();

		FormTester formTester = tester.newFormTester("tabs:panel:lobbyTab:stateForm", false);
		formTester.submit("stateWaiting");

		filtersToTest.put(TypesForFilters.PLAYERS, "empty");
		filtersToTest.put(TypesForFilters.STATE, "WAITING");
		filtersToTest.put(TypesForFilters.MAXPLAYERS, "empty");
		filtersToTest.put(TypesForFilters.PRIVACYTYPE, "empty");
		assertEquals(filtersToTest, filters);

		FormTester formPlayers = tester.newFormTester("tabs:panel:lobbyTab:playersForm", false);
		formPlayers.submit("player4");

		filtersToTest.put(TypesForFilters.PLAYERS, "4");
		filtersToTest.put(TypesForFilters.STATE, "WAITING");
		filtersToTest.put(TypesForFilters.MAXPLAYERS, "empty");
		filtersToTest.put(TypesForFilters.PRIVACYTYPE, "empty");
		assertEquals(filtersToTest, filters);

		FormTester formMaxPlayers = tester.newFormTester("tabs:panel:lobbyTab:maxPlayersForm", false);
		formMaxPlayers.submit("maxPlayers4");

		filtersToTest.put(TypesForFilters.PLAYERS, "4");
		filtersToTest.put(TypesForFilters.STATE, "WAITING");
		filtersToTest.put(TypesForFilters.MAXPLAYERS, "4");
		filtersToTest.put(TypesForFilters.PRIVACYTYPE, "empty");
		assertEquals(filtersToTest, filters);

		FormTester formPrivacyType = tester.newFormTester("tabs:panel:lobbyTab:typeForm", false);
		formPrivacyType.submit("typePrivate");

		filtersToTest.put(TypesForFilters.PLAYERS, "4");

		filtersToTest.put(TypesForFilters.STATE, "WAITING");
		filtersToTest.put(TypesForFilters.MAXPLAYERS, "4");
		filtersToTest.put(TypesForFilters.PRIVACYTYPE, "private");
		assertEquals(filtersToTest, filters);

	}

	@Test
	public void submitFiltersTest() {
		FormTester formFilter = tester.newFormTester("tabs:panel:lobbyTab:form5", false);
		formFilter.submit("filter");
		FilterPanel filterPanel = tabPage.getCurrentLobby().getFilterPanel();
		Map<TypesForFilters, String> filtersToTest = new HashMap<>();

		filtersToTest.put(TypesForFilters.PLAYERS, "4");
		filtersToTest.put(TypesForFilters.STATE, "WAITING");
		filtersToTest.put(TypesForFilters.MAXPLAYERS, "empty");
		filtersToTest.put(TypesForFilters.PRIVACYTYPE, "private");

		filterPanel.setFilters(filtersToTest);

		FormTester formApplyFilters = tester.newFormTester("tabs:panel:lobbyTab:controlsForm", false);
		formApplyFilters.submit("controlApply");
		tester.assertRenderedPage(TabPage.class);
		tester.assertComponent("tabs:panel:lobbyTab", LobbyPanel.class);
		LobbyPanel lobbyPanel = tabPage.getCurrentLobby();
		lobbyPanel.filterUpdate("lobbyTab", filterPanel.getFilters());
		List<Game> gamesFiltered = lobbyPanel.getGamesUsed();
		for (Game game : gamesFiltered) {
			assertEquals(game.getCapacity(), (short) 4);
			assertEquals(game.getState(), GameState.WAITING);
			assertEquals(game.isPrivate(), true);
		}
	}
}
