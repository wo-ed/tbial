package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.util.tester.TagTester;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.wicketstuff.googlecharts.ChartProvider;

import de.lmu.ifi.sosy.tbial.cards.MockGame;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.abilitycards.BugDelegation;
import de.lmu.ifi.sosy.tbial.model.abilitycards.WearsSunglassesAtWork;
import de.lmu.ifi.sosy.tbial.model.abilitycards.WearsTieAtWork;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Accenture;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Google;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Microsoft;
import de.lmu.ifi.sosy.tbial.model.abilitycards.previousjob.Nasa;
import de.lmu.ifi.sosy.tbial.model.actioncards.BoringMeeting;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.actioncards.Heisenbug;
import de.lmu.ifi.sosy.tbial.model.actioncards.LANParty;
import de.lmu.ifi.sosy.tbial.model.actioncards.LameExcuse;
import de.lmu.ifi.sosy.tbial.model.actioncards.PersonalCoffeeMachine;
import de.lmu.ifi.sosy.tbial.model.actioncards.Pwnd;
import de.lmu.ifi.sosy.tbial.model.actioncards.RedBullDispenser;
import de.lmu.ifi.sosy.tbial.model.actioncards.Refactoring;
import de.lmu.ifi.sosy.tbial.model.actioncards.Solution;
import de.lmu.ifi.sosy.tbial.model.actioncards.StandupMeeting;
import de.lmu.ifi.sosy.tbial.model.actioncards.SystemIntegration;
import de.lmu.ifi.sosy.tbial.model.stumblingblockcards.FortranMaintenance;
import de.lmu.ifi.sosy.tbial.model.stumblingblockcards.OffTheJobTraining;
import de.lmu.ifi.sosy.tbial.model.visitor.PlayCardVisitor;
import de.lmu.ifi.sosy.tbial.pages.GameLobbyPanel;
import de.lmu.ifi.sosy.tbial.pages.GamePage;
import de.lmu.ifi.sosy.tbial.pages.GameStatPanel;
import de.lmu.ifi.sosy.tbial.pages.TabPage;

public class GameStatPanelTest extends PageTestBase {

	TBIALSession session;
	User user;

	MockGame game;

	PlayCardVisitor visitor;

	User u2;
	User u3;
	User u4;

	Player p1;
	Player p2;
	Player p3;
	Player p4;

	GamePage gamePage;

	ModalWindow modal;

	@Before
	public void setUp() {
		setupApplication();
		database.register("testuser", "1");
		session = getSession();
		session.signIn("testuser", "1");
		user = session.getUser();

		game = new MockGame("G1", (short) 4, false, user, true);
		TBIALApplication.getInstance().addGame(game);
		TBIALApplication.getInstance().joinGame(user, game);
		u2 = new User("u2", "");
		u3 = new User("u3", "");
		u4 = new User("u4", "");

		game.join(u2);
		game.join(u3);
		game.join(u4);

		game.init();

		p1 = game.getPlayer(user);
		p2 = game.getPlayer(u2);
		p3 = game.getPlayer(u3);
		p4 = game.getPlayer(u4);

		visitor = new PlayCardVisitor(game);

		game.setEndTime(new Date());

		game.setCurrentPlayer(p1);

		modal = new ModalWindow("modal");

	}

	@Test
	public void linkIsVisible() {

		TabPage page = tester.startPage(TabPage.class);
		Game gamePageGame = new Game("g2", (short) 7, false, getSession().getUser());
		TBIALApplication.getInstance().addGame(gamePageGame);
		TBIALApplication.getInstance().joinGame(getSession().getUser(), gamePageGame);
		page.setGameLobbyTab(gamePageGame.getName());
		GameLobbyPanel gameLobby = page.getCurrentGameLobby();

		gamePageGame.join(new User("", ""));
		gamePageGame.join(new User("", ""));
		gamePageGame.join(new User("", ""));

		gameLobby.startGame();

		GamePage gamePage = tester.startPage(GamePage.create());
		tester.assertRenderedPage(gamePage.getClass());

		TagTester tag = tester.getTagByWicketId("gamestat");
		Assert.assertNotNull(tag);

	}

	@Test
	public void allStatsVisibleAndCorrect() {

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		tester.assertLabel(modal.getId() + ":bugsPlayed", String.valueOf(game.getBugsPlayed()));

		tester.assertLabel(modal.getId() + ":excusesPlayed", String.valueOf(game.getExcusesPlayed()));

		tester.assertLabel(modal.getId() + ":solutionsPlayed", String.valueOf(game.getSolutionsPlayed()));

		tester.assertLabel(modal.getId() + ":specialCardsPlayed", String.valueOf(game.getSpecialCardsPlayed()));

		tester.assertLabel(modal.getId() + ":abilityCardsPlayed", String.valueOf(game.getAbilityCardsPlayed()));

		tester.assertLabel(modal.getId() + ":stumblingCardsPlayed", String.valueOf(game.getStumblingCardsPlayed()));

		tester.assertLabel(modal.getId() + ":durationInTime", game.getDurationInTime());

		tester.assertLabel(modal.getId() + ":durationInTurns", String.valueOf(game.getTurnsCounter()));

		tester.assertListView(modal.getId() + ":charts", gameStatPanel.getChartList().getList());

	}

	@Test
	public void chartTest() {
		// compare with 0-value-chart (recommended by PO)

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		ListView<ChartProvider> chartList = gameStatPanel.getChartList();

		HashMap<Player, List<Integer>> hp = game.getPlayerHealthPoints();

		for (Player p : game.getPlayers()) {
			List<Integer> li = new ArrayList<Integer>();

			int i = 0;
			while (i <= game.getTurnsCounter()) {
				li.add(0);
				i++;
			}

			hp.put(p, new ArrayList<Integer>(li));
		}

		gameStatPanel.buildCharts();

		ListView<ChartProvider> anotherChartList = gameStatPanel.getChartList();

		assertTrue(chartList.size() == anotherChartList.size());

		for (int i = 0; i < chartList.size(); i++) {
			ChartProvider chart = chartList.getList().get(i);
			ChartProvider anotherChart = anotherChartList.getList().get(i);

			double[][] data1 = chart.getData().getData();
			double[][] data2 = anotherChart.getData().getData();

			assertFalse(data1.equals(data2));

		}

	}

	@Test
	public void checkUpdateBugsPlayed() {

		TurnCard bug = new BugReport("bug");

		bug.setPlayer(p1);
		bug.setTargetPlayer(p2);

		bug.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("bugsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateSolutionsPlayedWhenPlayerOnTurn() {
		int hp = p1.getHp();
		p1.getCharacter().decHp();
		assertTrue(p1.getHp() == hp - 1);

		TurnCard solution = new Solution("solution");

		solution.setPlayer(p1);

		solution.play(this.visitor);

		assertTrue(p1.getHp() == hp);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("solutionsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateSolutionsPlayedWhenPlayerToDeflectBug() {
		TurnCard solution = new Solution("solution");

		p1.setState(PlayerState.DEFLECT_BUG);
		solution.setPlayer(p1);

		solution.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("solutionsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateExcusesPlayed() {
		game.setCurrentPlayer(p2);
		TurnCard excuse = game.drawCardofClass(p1, LameExcuse.class);
		TurnCard bug = game.drawCardofClass(p2, BugReport.class);
		bug.setTargetPlayer(p1);
		game.setBugAlreadyPlayedFalse();

		game.playCard(bug);

		game.playCard(excuse);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("excusesPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateSpecialCardsPlayedForSystemIntegration() {
		TurnCard systemIntegration = new SystemIntegration();

		systemIntegration.setPlayer(p1);
		systemIntegration.setTargetPlayer(p2);

		systemIntegration.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("specialCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateSpecialCardsPlayedForPwnd() {
		TurnCard pwnd = new Pwnd();

		pwnd.setPlayer(p1);
		pwnd.setTargetPlayer(p2);

		pwnd.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("specialCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateSpecialCardsPlayedForRedBullDispenser() {
		TurnCard redBullDispenser = new RedBullDispenser();

		redBullDispenser.setPlayer(p1);
		redBullDispenser.setTargetPlayer(p1);

		redBullDispenser.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("specialCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateSpecialCardsPlayedForHeisenbug() {
		TurnCard heisenbug = new Heisenbug();

		heisenbug.setPlayer(p1);

		heisenbug.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("specialCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");

	}

	@Test
	public void checkUpdateSpecialCardsPlayedForStandupMeeting() {
		TurnCard standupMeeting = new StandupMeeting();

		standupMeeting.setPlayer(p1);
		standupMeeting.setTargetPlayer(p1);

		standupMeeting.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("specialCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateSpecialCardsPlayedForPersonalCoffeeMachine() {
		TurnCard personalCoffeeMachine = new PersonalCoffeeMachine();

		personalCoffeeMachine.setPlayer(p1);
		personalCoffeeMachine.setTargetPlayer(p1);

		personalCoffeeMachine.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("specialCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateSpecialCardsPlayedForRefactoring() {
		TurnCard refactoring = new Refactoring();

		refactoring.setPlayer(p1);
		refactoring.setTargetPlayer(p2);

		refactoring.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("specialCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateSpecialCardsPlayedForLANParty() {
		TurnCard lanParty = new LANParty();

		lanParty.setPlayer(p1);
		lanParty.setTargetPlayer(p1);

		lanParty.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("specialCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateSpecialCardsPlayedForBoringMeeting() {
		TurnCard boringMeeting = new BoringMeeting();

		boringMeeting.setPlayer(p1);

		boringMeeting.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("specialCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateStumblingCardsPlayedForFortranMaintenance() {
		TurnCard fortranMaintenance = new FortranMaintenance();

		fortranMaintenance.setPlayer(p1);
		fortranMaintenance.setTargetPlayer(p1);

		fortranMaintenance.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("stumblingCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateStumblingCardsPlayedForOffTheJob() {
		TurnCard offTheJob = new OffTheJobTraining();

		offTheJob.setPlayer(p1);
		offTheJob.setTargetPlayer(p2);

		offTheJob.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("stumblingCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateAbilityCardsPlayedForWearsTie() {
		TurnCard wearsTie = new WearsTieAtWork();

		wearsTie.setPlayer(p1);
		wearsTie.setTargetPlayer(p1);

		wearsTie.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("abilityCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateAbilityCardsPlayedForPreviousJobNasa() {
		TurnCard previousJobNasa = new Nasa();

		previousJobNasa.setPlayer(p1);
		previousJobNasa.setTargetPlayer(p1);

		previousJobNasa.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("abilityCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateAbilityCardsPlayedForPreviousJobMicrosoft() {
		TurnCard previousJobMicrosoft = new Microsoft();

		previousJobMicrosoft.setPlayer(p1);
		previousJobMicrosoft.setTargetPlayer(p1);

		previousJobMicrosoft.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("abilityCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateAbilityCardsPlayedForPreviousJobGoogle() {
		TurnCard previousJobGoogle = new Google();

		previousJobGoogle.setPlayer(p1);
		previousJobGoogle.setTargetPlayer(p1);

		previousJobGoogle.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("abilityCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateAbilityCardsPlayedForPreviousJobAccenture() {
		TurnCard previousJobAccenture = new Accenture();

		previousJobAccenture.setPlayer(p1);
		previousJobAccenture.setTargetPlayer(p1);

		previousJobAccenture.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("abilityCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");
	}

	@Test
	public void checkUpdateAbilityCardsPlayedWearsSunglasses() {
		TurnCard wearsSunglasses = new WearsSunglassesAtWork();

		wearsSunglasses.setPlayer(p1);
		wearsSunglasses.setTargetPlayer(p1);

		wearsSunglasses.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("abilityCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");

	}

	@Test
	public void checkUpdateAbilityCardsPlayedForBugDelegation() {
		TurnCard bugDelegation = new BugDelegation();

		bugDelegation.setPlayer(p1);
		bugDelegation.setTargetPlayer(p1);

		bugDelegation.play(this.visitor);

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("abilityCardsPlayed");
		Assert.assertEquals(tag.getValue(), "1");

	}

	@Test
	public void checkUpdateDurationInTime() {
		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("durationInTime");
		Assert.assertEquals(tag.getValue(), "00:00");

		long miliSec = game.getEndTime().getTime();

		// set end time 5 min later
		game.setEndTime(new Date(miliSec + 300000));

		gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		tag = tester.getTagByWicketId("durationInTime");
		Assert.assertEquals(tag.getValue(), "00:05");

	}

	@Test
	public void checkUpdateDurationInTurns() {

		GameStatPanel gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		TagTester tag = tester.getTagByWicketId("durationInTurns");
		Assert.assertEquals(tag.getValue(), "1");

		game.cleanHandcards(game.getCurrentPlayer());
		game.endTurn();

		gameStatPanel = tester.startComponentInPage(new GameStatPanel(modal.getId(), game));
		tester.assertComponent(modal.getId(), gameStatPanel.getClass());

		tag = tester.getTagByWicketId("durationInTurns");
		Assert.assertEquals(tag.getValue(), "2");

	}

}
