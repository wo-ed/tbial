package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.MockGame;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.PlayerState;

public class WinningConditionsTest extends PageTestBase {
	TBIALSession session;

	User user;
	MockGame game;

	@Before
	public void setUp() {
		setupApplication();
		database.register("testuser", "1");
		session = getSession();
		session.signIn("testuser", "1");
		user = session.getUser();
	}

	@Test
	public void playerIsFired() {

		game = new MockGame("G1", (short) 4, false, user, true);

		User u2 = new User("u2", "");
		User u3 = new User("u3", "");
		User u4 = new User("u4", "");

		game.join(u2);
		game.join(u3);
		game.join(u4);

		game.init();

		Player p1 = game.getPlayer(user);
		Player p2 = game.getPlayer(u2);
		Player p3 = game.getPlayer(u3);
		Player p4 = game.getPlayer(u4);

		assertTrue(p1.isAlive());
		assertTrue(p2.isAlive());
		assertTrue(p3.isAlive());
		assertTrue(p4.isAlive());

		assertTrue(game.getPlayers().contains(p1));
		assertTrue(game.getPlayers().contains(p2));
		assertTrue(game.getPlayers().contains(p3));
		assertTrue(game.getPlayers().contains(p4));

		while (p4.getHp() > 0) {
			game.decHp(p4, p1);
		}

		assertFalse(p4.isAlive());
		assertTrue(p4.getState() == PlayerState.DEAD);
		assertTrue(p4.getHandcards().size() == 0);
	}

	@Test
	public void managerIsFired() {
		// and evil code monkeys win

		game = new MockGame("G1", (short) 4, false, user, true);

		User u2 = new User("u2", "");
		User u3 = new User("u3", "");
		User u4 = new User("u4", "");

		game.join(u2);
		game.join(u3);
		game.join(u4);

		game.init();

		Player manager = game.getManager();
		Player consultant = game.getConsultant();
		List<Player> evilCodeMonkeys = game.getEvilCodeMonkeys();

		Player evilCodeMonkey = evilCodeMonkeys.get(0);
		// one evil code monkey is fired
		while (evilCodeMonkey.getHp() > 0) {
			game.decHp(evilCodeMonkey, consultant);
		}

		assertFalse(evilCodeMonkey.isAlive());
		assertTrue(evilCodeMonkey.getState() == PlayerState.DEAD);

		// manager is fired
		while (manager.getHp() > 0) {
			game.decHp(manager, consultant);
		}

		assertFalse(manager.isAlive());
		assertTrue(manager.getState() == PlayerState.DEAD);

		assertTrue(game.getWinners().size() == 2);

		assertFalse(game.getWinners().contains(manager));
		assertFalse(game.getWinners().contains(consultant));

		// all evil code monkeys win
		assertTrue(game.getWinners().containsAll(evilCodeMonkeys));

	}

	@Test
	public void managerIsFiredAgain() {
		// but consultant wins

		game = new MockGame("G1", (short) 4, false, user, true);

		User u2 = new User("u2", "");
		User u3 = new User("u3", "");
		User u4 = new User("u4", "");

		game.join(u2);
		game.join(u3);
		game.join(u4);

		game.init();

		Player manager = game.getManager();
		Player consultant = game.getConsultant();
		List<Player> evilCodeMonkeys = game.getEvilCodeMonkeys();

		// all evil code monkeys are fired
		for (Player evilCodeMonkey : evilCodeMonkeys) {
			while (evilCodeMonkey.getHp() > 0) {
				game.decHp(evilCodeMonkey, consultant);
			}

			assertFalse(evilCodeMonkey.isAlive());
			assertTrue(evilCodeMonkey.getState() == PlayerState.DEAD);
		}

		// manager is fired
		while (manager.getHp() > 0) {
			game.decHp(manager, consultant);
		}

		assertFalse(manager.isAlive());
		assertTrue(manager.getState() == PlayerState.DEAD);

		assertTrue(game.getWinners().size() == 1);

		assertFalse(game.getWinners().contains(manager));
		assertFalse(game.getWinners().containsAll(evilCodeMonkeys));

		// consultant wins
		assertTrue(game.getWinners().contains(consultant));
	}

	@Test
	public void consultantAndEvilCodeMonkeysAreFired() {
		// manager and all honest developers win

		game = new MockGame("G1", (short) 7, false, user, true);

		User u2 = new User("u2", "");
		User u3 = new User("u3", "");
		User u4 = new User("u4", "");
		User u5 = new User("u5", "");
		User u6 = new User("u6", "");
		User u7 = new User("u7", "");

		game.join(u2);
		game.join(u3);
		game.join(u4);
		game.join(u5);
		game.join(u6);
		game.join(u7);

		game.init();

		Player manager = game.getManager();
		Player consultant = game.getConsultant();
		List<Player> evilCodeMonkeys = game.getEvilCodeMonkeys();
		List<Player> honestDevelopers = game.getHonestDevelopers();

		Player honestDeveloper = honestDevelopers.get(0);
		// one honest developer is fired
		while (honestDeveloper.getHp() > 0) {
			game.decHp(honestDeveloper, consultant);
		}

		assertFalse(honestDeveloper.isAlive());
		assertTrue(honestDeveloper.getState() == PlayerState.DEAD);

		// consultant is fired
		while (consultant.getHp() > 0) {
			game.decHp(consultant, manager);
		}

		assertFalse(consultant.isAlive());
		assertTrue(consultant.getState() == PlayerState.DEAD);

		// all evil code monkeys are fired
		for (Player evilCodeMonkey : evilCodeMonkeys) {
			while (evilCodeMonkey.getHp() > 0) {
				game.decHp(evilCodeMonkey, manager);
			}

			assertFalse(evilCodeMonkey.isAlive());
			assertTrue(evilCodeMonkey.getState() == PlayerState.DEAD);
		}

		assertTrue(game.getWinners().size() == 3);

		assertFalse(game.getWinners().contains(consultant));
		assertFalse(game.getWinners().containsAll(evilCodeMonkeys));

		// manager and all honest developers win
		assertTrue(game.getWinners().contains(manager));
		assertTrue(game.getWinners().containsAll(honestDevelopers));

	}

}