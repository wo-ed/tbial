package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.wicket.util.tester.FormTester;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.components.PlayerPanel;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.GameState;
import de.lmu.ifi.sosy.tbial.pages.GamePage;
import de.lmu.ifi.sosy.tbial.pages.TabPage;

public class RejoinLeaveTest extends PageTestBase {

	TBIALSession session;

	@Before
	public void setUp() {
		setupApplication();
		database.register("testuser", "1");
		session = getSession();
		session.signIn("testuser", "1");
	}

	@Test
	public void LeaveAndRejoinTest() {
		TBIALSession session = getSession();
		assertTrue(session.isSignedIn());

		Game game = new Game("1", (short) 7, false, session.getUser());
		TBIALApplication.getInstance().addGame(game);
		TBIALApplication.getInstance().joinGame(session.getUser(), game);
		User u2 = new User("u2", "");
		User u3 = new User("u3", "");
		User u4 = new User("u4", "");

		game.join(u2);
		game.join(u3);
		game.join(u4);

		game.init();

		GamePage gPage = tester.startPage(GamePage.create());
		tester.assertRenderedPage(GamePage.class);

		PlayerPanel panel1 = gPage.getGameCourt().getPlayerPanels().get(game.getPlayer(session.getUser()).getId());
		// Leave
		pressLeaveGameBtn();
		tester.assertRenderedPage(TabPage.class);
		assertTrue(game.getAvailablePlayers().size() == 1);
		assertTrue(game.getState() == GameState.PAUSED);

		// Join
		TBIALApplication.getInstance().joinGame(session.getUser(), game);
		tester.startPage(GamePage.create());
		tester.assertRenderedPage(GamePage.class);
		assertTrue(game.getAvailablePlayers().isEmpty());
		assertTrue(game.getState() == GameState.RUNNING);
		gPage = (GamePage) tester.getLastRenderedPage();
		PlayerPanel panel2 = gPage.getGameCourt().getPlayerPanels().get(game.getPlayer(session.getUser()).getId());
		assertEquals(panel1.getNameLabel().getDefaultModelObject(), panel2.getNameLabel().getDefaultModelObject());

	}

	public void pressLeaveGameBtn() {
		tester.assertRenderedPage(GamePage.class);
		FormTester form = tester.newFormTester("leaveGameForm");
		form.submit("leaveGameBtn");
	}

}
