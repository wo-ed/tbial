package de.lmu.ifi.sosy.tbial.cards.characters;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.CharacterCardTester;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.characters.BruceSchneier;

public class BruceSchneierTester extends CharacterCardTester {

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void bruceSchneierCanPlayMultipleBugs() {
		// Bruce Schneier + Manager
		Assert.assertEquals(true, player.getCharacter() instanceof BruceSchneier);
		Assert.assertEquals(5, player.getCharacter().getHp());
		game.setCurrentPlayer(player);
		player.getHandcards().clear();
		game.drawCardofClass(player, BugReport.class);

		Assert.assertEquals(true, game.canPlayBug());
		TurnCard card = player.getHandcards().get(0);
		card.setTargetPlayer(player2);
		game.cleanHandcards(player2);
		Assert.assertEquals(true, card instanceof BugReport);
		game.playCard(card);
		Assert.assertEquals(true, game.canPlayBug());
	}
}
