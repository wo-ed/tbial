
package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.apache.wicket.util.tester.TagTester;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.MockGame;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.pages.TabPage;

public class PageTestLobby extends LoginTest {

	private List<Game> games;

	@Override
	@Before
	public void setUp() {
		super.setUp();
		TBIALApplication.getInstance().addGame(new MockGame("g1", (short) 4, false, new User("1", ""), true));
		TBIALApplication.getInstance().addGame(new MockGame("g1", (short) 5, true, "pw1", new User("2", ""), true));
		TBIALApplication.getInstance().addGame(new MockGame("g1", (short) 6, false, new User("3", ""), true));
		TBIALApplication.getInstance().addGame(new MockGame("g1", (short) 7, true, "pw2", new User("4", ""), true));
		this.games = TBIALApplication.getGames();
	}

	@Test
	public void lobbyPageisStarted() {
		tester.startPage(TabPage.class);
		// tester.assertRenderedPage(LobbyPage.class);
		// tester.assertContains("simple");
		// tester.assertContains("state");
		// tester.assertComponent("simple", DataView.class);
	}

	@Test
	public void tableNotEmpty() {
		tester.startPage(TabPage.class);
		List<TagTester> tagTester = tester.getTagsByWicketId("id");
		assertNotNull(tagTester);

		List<TagTester> tagTesterGameNames = tester.getTagsByWicketId("name");
		assertNotNull(tagTesterGameNames);

		List<TagTester> tagTesterPlayers = tester.getTagsByWicketId("player");
		assertNotNull(tagTesterPlayers);

		List<TagTester> tagTesterPrivacy = tester.getTagsByWicketId("privacy");
		assertNotNull(tagTesterPrivacy);
	}

	@Test
	public void tableCorrectlyInitialized() {

		tester.startPage(TabPage.class);
		List<TagTester> tagTester = tester.getTagsByWicketId("state");
		for (int i = 0; i < tagTester.size(); i++) {
			assertEquals(tagTester.get(i).getValue(), games.get(i).getState() + "");
		}

		List<TagTester> tagTesterGameNames = tester.getTagsByWicketId("name");
		for (int i = 0; i < tagTesterGameNames.size(); i++) {
			// assertEquals(tagTesterGameNames.get(i).getValue(),
			// games.get(i).getName());
		}

		List<TagTester> tagTesterPlayers = tester.getTagsByWicketId("player");
		for (int i = 0; i < tagTesterPlayers.size(); i++) {
			assertEquals(tagTesterPlayers.get(i).getValue(),
					games.get(i).getPlayers().size() + "/" + games.get(i).getCapacity());
		}

		List<TagTester> tagTesterPrivacy = tester.getTagsByWicketId("privacy");
		for (int i = 0; i < tagTesterPrivacy.size(); i++) {
			assertEquals(tagTesterPrivacy.get(i).getValue(), String.valueOf(games.get(i).isPrivate()));
		}
	}
}