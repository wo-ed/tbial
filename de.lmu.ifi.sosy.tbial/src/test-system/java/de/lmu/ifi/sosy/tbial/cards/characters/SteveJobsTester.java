package de.lmu.ifi.sosy.tbial.cards.characters;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.CharacterCardTester;
import de.lmu.ifi.sosy.tbial.model.characters.SteveJobs;

public class SteveJobsTester extends CharacterCardTester {

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void steveJobsGetsASecondChance() {
		// Steve Jobs
		Assert.assertEquals(true, player11.getCharacter() instanceof SteveJobs);
		Assert.assertEquals(4, player11.getCharacter().getHp());

		Float expected = (float) (0.15 + (1 - 0.15) * 0.15);
		Float actual = player11.getCharacter().calculateChance(0.15f);

		Assert.assertEquals("Steve Jobs has a double chance", expected, actual);
	}
}
