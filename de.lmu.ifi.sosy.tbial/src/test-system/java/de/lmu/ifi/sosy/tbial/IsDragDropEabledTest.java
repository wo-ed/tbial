package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.MockGame;
import de.lmu.ifi.sosy.tbial.components.HandCardPanel;
import de.lmu.ifi.sosy.tbial.components.HeapStackPanel;
import de.lmu.ifi.sosy.tbial.components.PlayerFieldPanel;
import de.lmu.ifi.sosy.tbial.components.PlayerPanel;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.Pwnd;
import de.lmu.ifi.sosy.tbial.model.actioncards.Refactoring;
import de.lmu.ifi.sosy.tbial.model.actioncards.Solution;
import de.lmu.ifi.sosy.tbial.model.actioncards.StandupMeeting;
import de.lmu.ifi.sosy.tbial.pages.GamePage;

public class IsDragDropEabledTest extends PageTestBase {
	private Player p2;
	private Player p3;
	private Player p4;
	private Player p1;
	private Game game;
	private TBIALSession session;
	private GamePage gPage;
	private HandCardPanel handCardPanel;
	private HandCardPanel handCardPanel2;
	private Map<Integer, PlayerPanel> playerPanels;
	private HeapStackPanel heap;
	private PlayerFieldPanel playerfieldPanel1;
	private PlayerFieldPanel playerfieldPanel2;

	@Before
	public void setUp() {
		setupApplication();
		database.register("p1", "1");
		session = getSession();
		session.signIn("p1", "1");
		game = new MockGame("1", (short) 4, false, session.getUser(), true);
		TBIALApplication.getInstance().addGame(game);
		TBIALApplication.getInstance().joinGame(session.getUser(), game);
		User u2 = new User("u2", "");
		User u3 = new User("u3", "");
		User u4 = new User("u4", "");

		game.join(u2);
		game.join(u3);
		game.join(u4);

		p2 = game.getPlayer(u2);
		p3 = game.getPlayer(u3);
		p4 = game.getPlayer(u4);
		p1 = game.getPlayer(session.getUser());

		game.init();

		gPage = tester.startPage(GamePage.create());
		playerPanels = gPage.getGameCourt().getPlayerPanels();
		handCardPanel = playerPanels.get(p1.getId()).getHandCardPanel();
		handCardPanel2 = playerPanels.get(p2.getId()).getHandCardPanel();
		heap = gPage.getGameCourt().getHeapStack();

		playerfieldPanel1 = playerPanels.get(p1.getId()).getPlayerField();
		playerfieldPanel2 = playerPanels.get(p2.getId()).getPlayerField();

	}

	@Test
	public void testHandCardPanelIsDragEnabled() {
		p1.setState(PlayerState.ON_TURN);

		assertTrue(handCardPanel.isDragEnabled());

		p1.setState(PlayerState.DEFLECT_BUG);
		assertTrue(handCardPanel.isDragEnabled());

		p1.setState(PlayerState.SYSTEM_INTEGRATION);
		assertTrue(handCardPanel.isDragEnabled());

		// assert that not a session User cannot drag

		p2.setState(PlayerState.ON_TURN);

		assertFalse(handCardPanel2.isDragEnabled());

		p2.setState(PlayerState.DEFLECT_BUG);
		assertFalse(handCardPanel2.isDragEnabled());

		p2.setState(PlayerState.SYSTEM_INTEGRATION);
		assertFalse(handCardPanel2.isDragEnabled());

		// p1 can steal from p2 if he played PWND
		TurnCard pwnd = (TurnCard) new Pwnd();
		pwnd.setPlayer(p1);
		pwnd.setTargetPlayer(p2);
		p1.setState(PlayerState.ON_TURN);
		game.playCard(pwnd);
		assertTrue(handCardPanel2.isDragEnabled());

		// p1 can steal from p2 if he played PWND
		TurnCard refactoring = (TurnCard) new Refactoring();
		refactoring.setPlayer(p1);
		refactoring.setTargetPlayer(p2);
		p1.setState(PlayerState.ON_TURN);
		game.playCard(refactoring);
		assertTrue(handCardPanel2.isDragEnabled());

	}

	@Test
	public void testHandCardPanelIsDropEnabled() {
		// can drop a card to your hand if Pwnd is your card played and you are
		// a session user
		TurnCard pwnd = (TurnCard) new Pwnd();
		pwnd.setPlayer(p1);
		pwnd.setTargetPlayer(p2);
		p1.setState(PlayerState.ON_TURN);
		game.playCard(pwnd);
		assertTrue(handCardPanel.isDropEnabled());

		// can drop a card to your hand if StandupMeeting is your card played
		// and you are a session user
		TurnCard standupMeeting = (TurnCard) new StandupMeeting();
		standupMeeting.setPlayer(p1);
		standupMeeting.setTargetPlayer(p1);
		p1.setState(PlayerState.ON_TURN);
		p1.addHandCard(standupMeeting);
		game.playCard(standupMeeting);
		assertTrue(handCardPanel.isDropEnabled());

	}

	@Test
	public void isDropEnabledOnHeapTest() {
		// p1 can throw another's player card to heap if he is in refactoring
		// mode
		TurnCard refactoring = (TurnCard) new Refactoring();
		refactoring.setPlayer(p1);
		refactoring.setTargetPlayer(p2);
		p1.setState(PlayerState.ON_TURN);
		game.playCard(refactoring);
		assertTrue(heap.isDropEnabled());

	}

	@Test
	public void isBuffDragEnabledTest() {
		// cannot drag own buff cards
		p1.getBuffCards().add(new Solution("1"));
		assertFalse(playerfieldPanel1.isBuffDragEnabled());
		// cannot drag other player's cards
		p2.getBuffCards().add(new Solution("1"));
		assertFalse(playerfieldPanel2.isBuffDragEnabled());
		// can drag if not own and refactoring or pwnd

		TurnCard pwnd = (TurnCard) new Pwnd();
		pwnd.setPlayer(p1);
		pwnd.setTargetPlayer(p2);
		p1.setState(PlayerState.ON_TURN);
		game.playCard(pwnd);
		assertTrue(playerfieldPanel2.isBuffDragEnabled());

		// p1 can steal from p2 if he played PWND
		TurnCard refactoring = (TurnCard) new Refactoring();
		refactoring.setPlayer(p1);
		refactoring.setTargetPlayer(p2);
		p1.setState(PlayerState.ON_TURN);
		game.playCard(refactoring);
		assertTrue(playerfieldPanel2.isBuffDragEnabled());

	}

	@Test
	public void isPenaltyDragEnabledTest() {
		// cannot drag own buff cards
		p1.getPenaltyCards().add(new Solution("3"));

		// can drag if not own and refactoring or pwnd

		TurnCard pwnd = (TurnCard) new Pwnd();
		pwnd.setPlayer(p1);
		pwnd.setTargetPlayer(p2);
		p1.setState(PlayerState.ON_TURN);
		game.playCard(pwnd);
		assertTrue(playerfieldPanel2.isPenaltyDragEnabled());

		// p1 can steal from p2 if he played PWND
		TurnCard refactoring = (TurnCard) new Refactoring();
		refactoring.setPlayer(p1);
		refactoring.setTargetPlayer(p2);
		p1.setState(PlayerState.ON_TURN);
		game.playCard(refactoring);
		assertTrue(playerfieldPanel2.isPenaltyDragEnabled());

	}

	@Test
	public void isPenaltyDropEnabledTest() {
		p1.setState(PlayerState.ON_TURN);
		assertTrue(playerfieldPanel2.isPenaltyDropEnabled());
		assertTrue(playerfieldPanel1.isPenaltyDropEnabled());

		p1.setState(PlayerState.DEFLECT_BUG);
		assertTrue(playerfieldPanel2.isPenaltyDropEnabled());
		assertTrue(playerfieldPanel1.isPenaltyDropEnabled());

		p1.setState(PlayerState.SYSTEM_INTEGRATION);
		assertTrue(playerfieldPanel2.isPenaltyDropEnabled());
		assertTrue(playerfieldPanel1.isPenaltyDropEnabled());

		p2.setState(PlayerState.ON_TURN);
		assertTrue(playerfieldPanel2.isPenaltyDropEnabled());
		assertTrue(playerfieldPanel1.isPenaltyDropEnabled());

		p1.setState(PlayerState.WAIT);
		assertFalse(playerfieldPanel2.isPenaltyDropEnabled());
		assertFalse(playerfieldPanel1.isPenaltyDropEnabled());

		p1.setState(PlayerState.DRAW_CARD);
		assertFalse(playerfieldPanel2.isPenaltyDropEnabled());
		assertFalse(playerfieldPanel1.isPenaltyDropEnabled());

		p1.setState(PlayerState.PWND);
		assertFalse(playerfieldPanel2.isPenaltyDropEnabled());
		assertFalse(playerfieldPanel1.isPenaltyDropEnabled());

		p1.setState(PlayerState.REFACTORING);
		assertFalse(playerfieldPanel2.isPenaltyDropEnabled());
		assertFalse(playerfieldPanel1.isPenaltyDropEnabled());

		p1.setState(PlayerState.STANDUP_MEETING);
		assertFalse(playerfieldPanel2.isPenaltyDropEnabled());
		assertFalse(playerfieldPanel1.isPenaltyDropEnabled());

		p1.setState(PlayerState.STEALING_MODE);
		assertFalse(playerfieldPanel2.isPenaltyDropEnabled());
		assertFalse(playerfieldPanel1.isPenaltyDropEnabled());

	}

}
