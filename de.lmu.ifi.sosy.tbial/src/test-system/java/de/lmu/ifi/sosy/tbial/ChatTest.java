package de.lmu.ifi.sosy.tbial;
import static org.junit.Assert.assertTrue;
import java.util.List;
import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.util.tester.FormTester;
import org.apache.wicket.util.visit.IVisit;
import org.apache.wicket.util.visit.IVisitor;
import org.junit.Before;
import org.junit.Test;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.pages.TabPage;
import de.lmu.ifi.sosy.tbial.util.Message;
public class ChatTest extends PageTestBase {
	
	private TBIALSession session;
	private User foo;
	private User bar;
	private TabPage page;
	
	@Before
	public void setUp() {
		setupApplication();
		foo = new User("foo", "");
		bar = new User("bar", "");
		database.register("foo", "1");
		session = (TBIALSession) getSession();
		session.signIn("foo", "1");
		session.setUser(foo);
		
		page = tester.startPage(TabPage.class);
		
	}
	
	@Test
	public void chatWindowVisible() {
		tester.assertRenderedPage(TabPage.class);
		tester.assertVisible("messageContainer");
		tester.assertVisible("sendMsgForm");
		assertTrue(page != null);
	}
	
	@Test
	public void noEmptyMsgSent() {
		List<Message> globalMessages = TBIALApplication.getMessages();
		globalMessages.clear();
		
		// Send msg.
		String msg = "";
		sendMsg(msg);		
		assertTrue(globalMessages.size() == 0);
	}
	
	@Test
	public void firstMsgSent() {
		List<Message> globalMessages = TBIALApplication.getMessages();
		assertTrue(globalMessages != null);
		
		// Ensure that the list is empty.
		globalMessages.clear();
		
		// Send msg.
		String msg = "Dies ist eine Textnachricht.";
		sendMsg(msg);
		
		assertTrue(globalMessages.size() > 0);
		Message m = globalMessages.get(0);
		
		// Message text is correct.
		String text = m.getText();
		assertTrue(msg.equals(text));
		
		// Messsage sender is correct.
		User sender = m.getSender();
		assertTrue(sender == foo);
	}
	
	@Test
	public void msgIsVisibleInChat() {
		List<Message> globalMessages = TBIALApplication.getMessages();
		globalMessages.clear();
		
		// Send msg.
		String msg = "Dies ist eine Textnachricht.";
		sendMsg(msg);
		// Msg was added.
		
		assertTrue(globalMessages.size() > 0);
		
		// Update page.
		tester.executeAllTimerBehaviors(page);
		
		// Check if message was added to chat.
		MarkupContainer chat = page.getChat();
		chat.visitChildren(new IVisitor<Component, List<Component>>() {
			@Override
			public void component(Component c, IVisit<List<Component>> visit) {
				tester.assertVisible(c.getPageRelativePath());
				if (c instanceof Label) {
					Object o = c.getDefaultModelObject();
					assertTrue(msg.equals(o) || foo.getName().equals(o));
				}
			}
		});
	}
	
	@Test
	public void msgVisibleForOtherUser() {
		// Send msg.
		String msgFoo = "Dies ist eine Textnachricht von Foo.";
		sendMsg(msgFoo);
		
		// Change User
		session.setUser(bar);
		String msgBar = "Dies ist eine Textnachricht von Bar.";
		sendMsg(msgBar);
		
		// Update page.
		tester.executeAllTimerBehaviors(page);
		
		// Check if message was added to chat.
		MarkupContainer chat = page.getChat();
		chat.visitChildren(new IVisitor<Component, List<Component>>() {
			@Override
			public void component(Component c, IVisit<List<Component>> visit) {
				tester.assertVisible(c.getPageRelativePath());
				if (c instanceof Label) {
					Object o = c.getDefaultModelObject();
					assertTrue(msgFoo.equals(o) || foo.getName().equals(o)
							|| msgBar.equals(o) || bar.getName().equals(o));
				}
			}
		});
	}
	
	private void sendMsg(String msg) {
		FormTester form = tester.newFormTester("sendMsgForm");
		form.setValue("newMsg", msg);
		form.submit("sendMsgBtn");
	}
}