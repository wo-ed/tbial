package de.lmu.ifi.sosy.tbial.cards.characters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.CharacterCardTester;

//player13 == Tom Anderson
public class TomAndersonTester extends CharacterCardTester {

	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void checkTomAndersonAndFortanMaintenance() {
		// TODO check Draw 3 cards
		// TODO check dont draw on die
	}

	@Test
	public void checkTomAndersonCanDrawCardFromStackOnLosingHp() {
		game2.setCurrentPlayer(player10);
		game2.cleanHandcards(player13);

		// TomAnderson draws card on losing hp
		assertTrue(player13.getHp() > 1);
		int oldHandcardSize = player13.getHandcards().size();
		int oldHp = player13.getHp();
		int countCartsStack = game2.getStack().size();

		this.game2.decHp(player13, player10);
		assertTrue(oldHandcardSize == player13.getHandcards().size());
		assertTrue(countCartsStack == game2.getStack().size());

		assertTrue(game2.attemptDrawCard(player13));

		int newHp = player13.getHp();
		int newHandcardSize = player13.getHandcards().size();
		assertTrue(oldHandcardSize + 1 == newHandcardSize);
		assertTrue(oldHp - 1 == newHp);
		// assert Card was drawn from stack
		assertTrue(game2.getStack().size() == countCartsStack - 1);

		// TomAnderson dont draw card on losing hp
		assertTrue(player13.getHp() > 1);
		int oldHandcardSize2 = player13.getHandcards().size();
		int oldHp2 = player13.getHp();
		this.game2.decHp(player13, player11);

		int countCartsStack2 = game2.getStack().size();
		assertTrue(countCartsStack2 == game2.getStack().size());

		assertTrue(oldHandcardSize2 == player13.getHandcards().size());

		this.game2.cleanHandcards(game2.getCurrentPlayer());
		game2.endTurn();
		int countCartsStack3 = game2.getStack().size();

		assertFalse(game2.attemptDrawCard(player13));

		int newHp2 = player13.getHp();
		int newHandcardSize2 = player13.getHandcards().size();
		assertEquals(oldHandcardSize2, newHandcardSize2);
		assertEquals(oldHp2 - 1, newHp2);
		assertEquals(countCartsStack3, game2.getStack().size());
	}

	@Test
	public void checkOthersCanNotDrawCardFromStackOnLosingHp() {

		int oldSize1 = player.getHandcards().size();
		this.game.decHp(player, player2);
		assertTrue(oldSize1 == player.getHandcards().size());
		assertFalse(game.attemptDrawCard(player));
		assertTrue(oldSize1 == player.getHandcards().size());

		int oldSize2 = player2.getHandcards().size();
		this.game.decHp(player2, player);
		assertTrue(oldSize2 == player2.getHandcards().size());
		assertFalse(game.attemptDrawCard(player2));
		assertTrue(oldSize2 == player2.getHandcards().size());

		int oldSize3 = player3.getHandcards().size();
		this.game.decHp(player3, player4);
		assertTrue(oldSize3 == player3.getHandcards().size());
		assertFalse(game.attemptDrawCard(player3));
		assertTrue(oldSize3 == player3.getHandcards().size());

		int oldSize4 = player4.getHandcards().size();
		this.game.decHp(player4, player3);
		assertTrue(oldSize4 == player4.getHandcards().size());
		assertFalse(game.attemptDrawCard(player4));
		assertTrue(oldSize4 == player4.getHandcards().size());

		int oldSize5 = player5.getHandcards().size();
		this.game.decHp(player5, player6);
		assertTrue(oldSize5 == player5.getHandcards().size());
		assertFalse(game.attemptDrawCard(player5));
		assertTrue(oldSize5 == player5.getHandcards().size());

		int oldSize6 = player6.getHandcards().size();
		this.game.decHp(player6, player5);
		assertTrue(oldSize6 == player6.getHandcards().size());
		assertFalse(game.attemptDrawCard(player6));
		assertTrue(oldSize6 == player6.getHandcards().size());

		int oldSize7 = player7.getHandcards().size();
		this.game.decHp(player7, player7);
		assertTrue(oldSize7 == player7.getHandcards().size());
		assertFalse(game.attemptDrawCard(player7));
		assertTrue(oldSize7 == player7.getHandcards().size());

		int oldSize8 = player8.getHandcards().size();
		this.game2.decHp(player8, player13);
		assertTrue(oldSize8 == player8.getHandcards().size());
		assertFalse(game2.attemptDrawCard(player8));

		int oldSize9 = player9.getHandcards().size();
		this.game2.decHp(player9, player10);
		assertTrue(oldSize9 == player9.getHandcards().size());
		assertFalse(game2.attemptDrawCard(player9));
		assertTrue(oldSize9 == player9.getHandcards().size());

		int oldSize10 = player10.getHandcards().size();
		this.game2.decHp(player10, player9);
		assertTrue(oldSize10 == player10.getHandcards().size());
		assertFalse(game2.attemptDrawCard(player10));
		assertTrue(oldSize10 == player10.getHandcards().size());

		int oldSize11 = player11.getHandcards().size();
		this.game2.decHp(player11, player12);
		assertTrue(oldSize11 == player11.getHandcards().size());
		assertFalse(game2.attemptDrawCard(player11));
		assertTrue(oldSize11 == player11.getHandcards().size());

		int oldSize12 = player12.getHandcards().size();
		this.game2.decHp(player12, player11);
		assertTrue(oldSize12 == player12.getHandcards().size());
		assertFalse(game2.attemptDrawCard(player12));
		assertTrue(oldSize12 == player12.getHandcards().size());
	}

}
