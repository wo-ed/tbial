package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.pages.GameLobbyPanel;
import de.lmu.ifi.sosy.tbial.pages.LobbyPanel;
import de.lmu.ifi.sosy.tbial.pages.TabPage;

public class GameLobbyTest extends PageTestBase {

	TBIALSession session;

	@Before
	public void setUp() {
		setupApplication();
		database.register("testuser", "1");
		session = getSession();
		session.signIn("testuser", "1");
	}

	@Test
	public void gameLobbyPageTest() {
		User user1 = session.getUser();
		Game game = new Game("G1", (short) 4, false, user1);
		TBIALApplication.getInstance().addGame(game);
		TBIALApplication.getInstance().joinGame(user1, game);
		TabPage tabPage = tester.startPage(TabPage.class);
		tester.assertRenderedPage(TabPage.class);

		tabPage.setGameLobbyTab(game.getName());
		tester.assertComponent("tabs:panel:gameLobbyTab", GameLobbyPanel.class);

		GameLobbyPanel gameLobby = tabPage.getCurrentGameLobby();
		tester.startComponentInPage(gameLobby);

		tester.assertLabel("gameLobbyTab:gameName", "G1");
		tester.assertLabel("gameLobbyTab:gamePrivacy", "public");

		ArrayList<String> playerList = new ArrayList<>();
		game.getPlayers().forEach(p -> playerList.add(p.getName()));
		assertTrue(playerList.size() == 1);
		tester.assertListView("gameLobbyTab:playerContainer:playerList", playerList);

		ArrayList<Integer> freePlayerList = new ArrayList<>();
		for (int i = game.getPlayers().size() + 1; i <= game.getCapacity(); i++) {
			freePlayerList.add(i);
		}
		assertTrue(freePlayerList.size() == 3);
		tester.assertListView("gameLobbyTab:playerContainer:freePlayers", freePlayerList);

		User user2 = new User("user2", "password2");
		TBIALApplication.getInstance().joinGame(user2, game);

		tester.executeAllTimerBehaviors(gameLobby);

		playerList.clear();
		game.getPlayers().forEach(p -> playerList.add(p.getName()));
		assertTrue(playerList.size() == 2);
		tester.assertListView("gameLobbyTab:playerContainer:playerList", playerList);

		freePlayerList = new ArrayList<>();
		for (int i = game.getPlayers().size() + 1; i <= game.getCapacity(); i++) {
			freePlayerList.add(i);
		}
		assertTrue(freePlayerList.size() == 2);
		tester.assertListView("gameLobbyTab:playerContainer:freePlayers", freePlayerList);

		tester.assertVisible("gameLobbyTab:lobbyForm:leaveButton");
		tester.assertInvisible("gameLobbyTab:lobbyForm:startButton");
	}

	@Test
	public void joinGameLobbyTest() throws InterruptedException {

		// first session
		User user1 = session.getUser();

		Game game = new Game("G3", (short) 4, false, user1);
		TBIALApplication.getInstance().addGame(game);
		TBIALApplication.getInstance().joinGame(user1, game);

		TabPage tabPage1 = tester.startPage(TabPage.class);
		tester.assertRenderedPage(TabPage.class);

		tabPage1.setGameLobbyTab(game.getName());
		tester.assertComponent("tabs:panel:gameLobbyTab", GameLobbyPanel.class);

		GameLobbyPanel gameLobby1 = tabPage1.getCurrentGameLobby();
		tester.startComponentInPage(gameLobby1);

		ArrayList<String> playerList = new ArrayList<>();
		game.getPlayers().forEach(p -> playerList.add(p.getName()));
		assertTrue(playerList.size() == 1);
		tester.assertListView("gameLobbyTab:playerContainer:playerList", playerList);

		ArrayList<Integer> freePlayerList = new ArrayList<>();
		for (int i = game.getPlayers().size() + 1; i <= game.getCapacity(); i++) {
			freePlayerList.add(i);
		}
		assertTrue(freePlayerList.size() == 3);
		tester.assertListView("gameLobbyTab:playerContainer:freePlayers", freePlayerList);

		// second session
		User user2 = new User("user2", "password2");

		TabPage tabPage2 = tester.startPage(TabPage.class);
		tester.assertRenderedPage(TabPage.class);
		tester.assertComponent("tabs:panel:lobbyTab", LobbyPanel.class);

		tabPage2.joinGame(user2, game);
		tester.assertComponent("tabs:panel:gameLobbyTab", GameLobbyPanel.class);

		GameLobbyPanel gameLobby2 = tabPage2.getCurrentGameLobby();
		assertNotNull(gameLobby2);
		tester.startComponentInPage(gameLobby2);

		playerList.clear();
		game.getPlayers().forEach(p -> playerList.add(p.getName()));
		assertEquals(playerList.size(), 2);
		tester.assertListView("gameLobbyTab:playerContainer:playerList", playerList);

		freePlayerList = new ArrayList<>();
		for (int i = game.getPlayers().size() + 1; i <= game.getCapacity(); i++) {
			freePlayerList.add(i);
		}
		assertEquals(freePlayerList.size(), 2);
		tester.assertListView("gameLobbyTab:playerContainer:freePlayers", freePlayerList);

		tester.assertVisible("gameLobbyTab:lobbyForm:leaveButton");
		tester.assertInvisible("gameLobbyTab:lobbyForm:startButton");
	}

	@Test
	public void leaveGameLobbyTest() {

		TabPage tabPage1 = tester.startPage(TabPage.class);
		tester.assertRenderedPage(TabPage.class);

		User user1 = session.getUser();
		User user2 = new User("user2", "password2");

		Game game = new Game("G3", (short) 4, false, user1);
		TBIALApplication.getInstance().addGame(game);
		TBIALApplication.getInstance().joinGame(user1, game);

		Player player1 = game.getPlayer(user1);
		TBIALApplication.getInstance().joinGame(user2, game);
		Player player2 = game.getPlayer(user2);

		tabPage1.setGameLobbyTab(game.getName());
		tester.assertComponent("tabs:panel:gameLobbyTab", GameLobbyPanel.class);

		GameLobbyPanel gameLobby1 = tabPage1.getCurrentGameLobby();
		tester.startComponentInPage(gameLobby1);

		assertTrue(gameLobby1.getGame().getOwner().equals(user1));

		assertTrue(gameLobby1.getGame().getPlayers().contains(player1));
		assertTrue(gameLobby1.getGame().getUsers().contains(user1));

		assertTrue(gameLobby1.getGame().getPlayers().contains(player2));
		assertTrue(gameLobby1.getGame().getUsers().contains(user2));

		assertTrue(gameLobby1.getGame().getPlayers().size() == 2);

		ArrayList<String> playerList = new ArrayList<>();
		game.getPlayers().forEach(p -> playerList.add(p.getName()));
		assertTrue(playerList.size() == 2);
		tester.assertListView("gameLobbyTab:playerContainer:playerList", playerList);

		ArrayList<Integer> freePlayerList = new ArrayList<>();
		for (int i = game.getPlayers().size() + 1; i <= game.getCapacity(); i++) {
			freePlayerList.add(i);
		}
		assertTrue(freePlayerList.size() == 2);
		tester.assertListView("gameLobbyTab:playerContainer:freePlayers", freePlayerList);

		gameLobby1.leaveGame(user1);
		assertTrue(game.getPlayers().size() == 1);

		TabPage tabPage2 = tester.startPage(TabPage.class);
		tester.assertRenderedPage(TabPage.class);

		TBIALApplication.getInstance().joinGame(user1, game);
		tabPage2.setGameLobbyTab(game.getName());
		tester.assertComponent("tabs:panel:gameLobbyTab", GameLobbyPanel.class);

		GameLobbyPanel gameLobby2 = tabPage2.getCurrentGameLobby();

		tester.startComponentInPage(gameLobby2);

		assertTrue(gameLobby2.getGame().getOwner().equals(user2));

		assertTrue(gameLobby2.getGame().getPlayers().size() == 2);

		playerList.clear();
		game.getPlayers().forEach(p -> playerList.add(p.getName()));
		assertTrue(playerList.size() == 2);
		tester.assertListView("gameLobbyTab:playerContainer:playerList", playerList);

		freePlayerList = new ArrayList<>();
		for (int i = game.getPlayers().size() + 1; i <= game.getCapacity(); i++) {
			freePlayerList.add(i);
		}
		assertTrue(freePlayerList.size() == 2);
		tester.assertListView("gameLobbyTab:playerContainer:freePlayers", freePlayerList);
		gameLobby2.leaveGame(user2);
		TBIALApplication.getInstance().leaveGame(user1, game);

		assertTrue(game.getPlayers().isEmpty());
		assertFalse(TBIALApplication.getGames().contains(game));
	}

}
