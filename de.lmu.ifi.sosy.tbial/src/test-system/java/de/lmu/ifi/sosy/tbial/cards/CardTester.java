package de.lmu.ifi.sosy.tbial.cards;

import org.junit.Before;

import de.lmu.ifi.sosy.tbial.PageTestBase;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.visitor.PlayCardVisitor;

public abstract class CardTester extends PageTestBase {

	protected PlayCardVisitor visitor;
	protected MockGame game;
	protected Player player, player2, player3, player4, player5, player6, player7;

	@Before
	public void setUp() {
		setupApplication();
		User user = new User("", "");
		User user2 = new User("", "");
		User user3 = new User("", "");
		User user4 = new User("", "");
		User user5 = new User("", "");
		User user6 = new User("", "");
		User user7 = new User("", "");

		this.game = new MockGame("g1", (short) 7, false, user, true);
		game.join(user2);
		game.join(user3);
		game.join(user4);
		game.join(user5);
		game.join(user6);
		game.join(user7);

		this.player = game.getPlayer(user);
		this.player2 = game.getPlayer(user2);
		this.player3 = game.getPlayer(user3);
		this.player4 = game.getPlayer(user4);
		this.player5 = game.getPlayer(user5);
		this.player6 = game.getPlayer(user6);
		this.player7 = game.getPlayer(user7);

		game.init();
		visitor = new PlayCardVisitor(game);
	}
}