package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.behavior.AbstractAjaxBehavior;
import org.apache.wicket.extensions.ajax.markup.html.tabs.AjaxTabbedPanel;
import org.apache.wicket.extensions.markup.html.tabs.AbstractTab;
import org.apache.wicket.util.tester.FormTester;
import org.apache.wicket.util.tester.WicketTesterHelper;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.MockGame;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.pages.CreateGamePanel;
import de.lmu.ifi.sosy.tbial.pages.LobbyPanel;
import de.lmu.ifi.sosy.tbial.pages.TabPage;

public class CreateGameTest extends PageTestBase {

	private User user;

	private TabPage tabPage;

	@Before
	public void setUp() {
		setupApplication();

		TBIALSession session = getSession();
		assertNotNull(session);

		database.register("testuser", "1");
		session.signIn("testuser", "1");
		user = session.getUser();

		Game game = new MockGame("G1", (short) 4, false, user, true);
		TBIALApplication.getInstance().addGame(game);

		tabPage = tester.startPage(TabPage.class);
		tester.assertRenderedPage(TabPage.class);

		tester.assertComponent("tabs", AjaxTabbedPanel.class);

		AjaxTabbedPanel<AbstractTab> tabbedPanel = tabPage.getTabbedPanel();
		assertTrue(tabbedPanel.getSelectedTab() == 0);

		tester.assertComponent("tabs:panel:lobbyTab", LobbyPanel.class);

		FormTester form = tester.newFormTester("tabs:panel:lobbyTab:form5");
		form.submit("newGame");
	}

	@Test
	public void checkDeafult() {
		tester.assertComponent("tabs:panel:lobbyTab", CreateGamePanel.class);
	}

	@Test
	public void checkSetCapacity4() {
		FormTester form = tester.newFormTester("tabs:panel:lobbyTab:submitCapacity");
		form.submit("button4");
		tester.assertModelValue("tabs:panel:lobbyTab:submitCapacity:capacityView", (short) 4);
	}

	@Test
	public void checkSetCapacity5() {
		FormTester form = tester.newFormTester("tabs:panel:lobbyTab:submitCapacity");
		form.submit("button5");
		tester.assertModelValue("tabs:panel:lobbyTab:submitCapacity:capacityView", (short) 5);
	}

	@Test
	public void checkSetCapacity6() {
		FormTester form = tester.newFormTester("tabs:panel:lobbyTab:submitCapacity");
		form.submit("button6");
		tester.assertModelValue("tabs:panel:lobbyTab:submitCapacity:capacityView", (short) 6);
	}

	@Test
	public void checkSetCapacity7() {
		FormTester form = tester.newFormTester("tabs:panel:lobbyTab:submitCapacity");
		form.submit("button7");
		tester.assertModelValue("tabs:panel:lobbyTab:submitCapacity:capacityView", (short) 7);
	}

	@Test
	public void checkIsPrivateButton() {
		FormTester form = tester.newFormTester("tabs:panel:lobbyTab:submitIsPrivate");
		form.submit("isPrivatebutton");
		tester.assertModelValue("tabs:panel:lobbyTab:submitIsPrivate:isPrivateView", "private.");
	}

	@Test
	public void checkIsNotPrivateButton() {
		FormTester form = tester.newFormTester("tabs:panel:lobbyTab:submitIsPrivate");
		form.submit("isNotPrivatebutton");
		tester.assertModelValue("tabs:panel:lobbyTab:submitIsPrivate:isPrivateView", "public.");
	}

	@Test
	public void testCheckGameName() {
		// kontrolliert, ob nameCheck funktioniert
		testCheckGameName("Default");
	}

	public void testCheckGameName(String s) {
		if (s.equals("Default")) {
			TBIALApplication.getInstance().addGame(new MockGame(s, (short) 4, true, user, true));
		}

		FormTester form = tester.newFormTester("tabs:panel:lobbyTab:nameFieldForm");

		form.setValue("name", "");
		tester.assertModelValue("tabs:panel:lobbyTab:nameFieldForm:nameCheck", "");

		form.setValue("name", s);

		AbstractAjaxBehavior behavior = (AbstractAjaxBehavior) WicketTesterHelper.findBehavior(
				tester.getComponentFromLastRenderedPage("tabs:panel:lobbyTab:nameFieldForm:name"),
				OnChangeAjaxBehavior.class);
		tester.executeBehavior(behavior);

		if (s.equals("Default")) {
			tester.assertModelValue("tabs:panel:lobbyTab:nameFieldForm:nameCheck",
					"Game name already exists. Please take other name.");
		}
	}

	public void setPassword(String s) {
		FormTester form = tester.newFormTester("tabs:panel:lobbyTab:passwordForm");

		form.setValue("password", s);

		AbstractAjaxBehavior behavior = (AbstractAjaxBehavior) WicketTesterHelper.findBehavior(
				tester.getComponentFromLastRenderedPage("tabs:panel:lobbyTab:passwordForm:password"),
				OnChangeAjaxBehavior.class);
		tester.executeBehavior(behavior);
	}

	@Test
	public void testCreate() {
		testCheckGameName("testGame2");
		checkSetCapacity5();
		checkIsPrivateButton();
		setPassword("testPassword");

		TBIALApplication.getGames().clear();
		FormTester form = tester.newFormTester("tabs:panel:lobbyTab:CreateGame");
		form.submit("createbutton");

		// tester.assertRenderedPage(GameLobbyPage.class);
		Game game = TBIALApplication.getInstance().getGame("testGame2");
		assertTrue(game.getName().equals("testGame2"));
		assertTrue(game.getPassword().equals("testPassword"));
		assertTrue(game.getCapacity() == (short) 5);
		assertTrue(game.getIsPrivate());
	}

	@Test
	public void testCancel() {
		FormTester form = tester.newFormTester("tabs:panel:lobbyTab:CreateGame");
		form.submit("cancelbutton");

		tester.assertComponent("tabs:panel:lobbyTab", LobbyPanel.class);
	}
}
