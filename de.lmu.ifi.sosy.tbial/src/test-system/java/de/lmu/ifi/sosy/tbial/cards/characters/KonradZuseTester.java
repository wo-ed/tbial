package de.lmu.ifi.sosy.tbial.cards.characters;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.CharacterCardTester;
import de.lmu.ifi.sosy.tbial.model.characters.KonradZuse;

public class KonradZuseTester extends CharacterCardTester {

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void konradZuseHasIncrementedPrestige() {
		Assert.assertEquals(true, player5.getCharacter() instanceof KonradZuse);
		Assert.assertEquals(3, player5.getCharacter().getHp());

		Assert.assertEquals(0, player5.getCharacter().getPrestige());
		Assert.assertEquals(1, player5.getCharacter().getPrestigeSeenByCharacter(player.getCharacter()));
	}
}
