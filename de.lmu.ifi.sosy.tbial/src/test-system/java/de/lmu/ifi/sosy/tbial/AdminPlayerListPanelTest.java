package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.List;

import org.apache.derby.tools.sysinfo;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.util.tester.FormTester;
import org.apache.wicket.util.tester.TagTester;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.pages.AdminPlayerListPanel;
import de.lmu.ifi.sosy.tbial.pages.PlayerListPanel;
import de.lmu.ifi.sosy.tbial.pages.TabPage;

public class AdminPlayerListPanelTest extends PageTestBase {

	private TabPage tabPage;

	@Before
	public void setUp() {
		setupApplication();
		database.register("testuser", "1");
		database.register("admin", "admin");

		User ad = database.getUser("admin");
		try {
			database.updateAdminRights(ad, true);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		TBIALApplication.getUsers().clear();

		TBIALApplication.addUser(ad);
		TBIALApplication.addUser(database.getUser("testuser"));

	}

	@Test
	public void adminViewInVisibleForUser() {
		TBIALSession session = (TBIALSession) getSession();
		session.signIn("testuser", "1");
		tabPage = tester.startPage(TabPage.class);
		tabPage.selectPlayerListTab();
		tester.assertComponent("tabs:panel:playerListTab", PlayerListPanel.class);
	}

	@Test
	public void adminViewVisibleForAdmin() {
		TBIALSession session = (TBIALSession) getSession();
		session.signIn("admin", "admin");
		tabPage = tester.startPage(TabPage.class);
		tabPage.selectPlayerListTab();
		tester.assertComponent("tabs:panel:playerListTab", AdminPlayerListPanel.class);
	}

	@Test
	public void grantAdminRights() {
		TBIALSession session = (TBIALSession) getSession();
		session.signIn("admin", "admin");
		User admin = database.getUser("admin");
		assertTrue(admin.isAdmin());

		tabPage = tester.startPage(TabPage.class);
		tabPage.selectPlayerListTab();
		tester.assertComponent("tabs:panel:playerListTab", AdminPlayerListPanel.class);

		AdminPlayerListPanel playerListPanel = (AdminPlayerListPanel) tabPage.getCurrentPlayerList();
		tester.startComponentInPage(playerListPanel);

		List<TagTester> tagTesterName = tester.getTagsByWicketId("name");
		assertTrue(tagTesterName.size() == 2);

		List<TagTester> tagTesterState = tester.getTagsByWicketId("state");
		assertTrue(tagTesterState.size() == 2);

		List<TagTester> tagTesterAdminRights = tester.getTagsByWicketId("label");
		// Admin must not degrade himself.
		assertTrue(tagTesterAdminRights.size() == 1);
		assertEquals(tagTesterAdminRights.get(0).getValue(), "grant");

		// 2nd user
		User newUser = database.getUser("testuser");

		// Grant admin rights to new user(testuser2)
		// Click link would be better here...
		try {
			database.updateAdminRights(newUser, true);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		tester.executeAllTimerBehaviors(playerListPanel);
		assertTrue(newUser.isAdmin());

		// User can now be degraded.
		tagTesterAdminRights = tester.getTagsByWicketId("label");
		assertTrue(tagTesterAdminRights.size() == 1);
		assertEquals(tagTesterAdminRights.get(0).getValue(), "revoke");

	}

	@Test
	public void userSearchTest() {
		TBIALSession session = (TBIALSession) getSession();
		session.signIn("admin", "admin");
		User admin = database.getUser("admin");
		assertTrue(admin.isAdmin());

		tabPage = tester.startPage(TabPage.class);
		tabPage.selectPlayerListTab();
		tester.assertComponent("tabs:panel:playerListTab", AdminPlayerListPanel.class);
		AdminPlayerListPanel playerListPanel = (AdminPlayerListPanel) tabPage.getCurrentPlayerList();
		tester.startComponentInPage(playerListPanel);

		User qu = new User("qu", "qu");
		User q = new User("q", "q");
		User qU = new User("qU", "qU");
		TBIALApplication.addUser(qu);
		TBIALApplication.addUser(q);
		TBIALApplication.addUser(qU);
		tester.executeAllTimerBehaviors(playerListPanel);

		// All 5 users visible?
		List<TagTester> tagTesterName = tester.getTagsByWicketId("name");
		assertTrue(tagTesterName.size() == 5);

		List<TagTester> tagTesterState = tester.getTagsByWicketId("state");
		assertTrue(tagTesterState.size() == 5);

		List<TagTester> tagTesterAdminRights = tester.getTagsByWicketId("label");
		// Admin must not degrade himself.
		assertTrue(tagTesterAdminRights.size() == 4);
		assertEquals(tagTesterAdminRights.get(0).getValue(), "grant");

//		// Test User Search
//		FormTester formTester = tester.newFormTester("playerListTab:searchForm");
//		formTester.setValue("searchUser", "q");
//		tester.executeAllTimerBehaviors(playerListPanel);
//		// Update Behavior not triggered...
//		tagTesterName = tester.getTagsByWicketId("name");
//		System.out.println(tagTesterName.size());
//
//		assertTrue(tagTesterName.size() == 3);
//
//		tagTesterState = tester.getTagsByWicketId("state");
//		assertTrue(tagTesterState.size() == 3);
//
//		tagTesterAdminRights = tester.getTagsByWicketId("label");
//		// Admin must not degrade himself.
//		assertTrue(tagTesterAdminRights.size() == 3);
	}

}
