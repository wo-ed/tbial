package de.lmu.ifi.sosy.tbial.db;

import static java.util.Objects.requireNonNull;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import de.lmu.ifi.sosy.tbial.DatabaseException;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.util.VerificationToken;
import de.lmu.ifi.sosy.tbial.util.VisibleForTesting;
import edu.emory.mathcs.backport.java.util.Collections;

/**
 * A simple in-memory database using a list for managing users.
 * 
 * @author Andreas Schroeder, SWEP 2013 Team.
 * 
 */
public class InMemoryDatabase implements Database {

	private final Map<String, User> users;
	private final Map<Integer, VerificationToken> tokens;
	private final Map<Integer, GameStat> gameStats;
	private final Map<Integer, HealthPoints> healthPoints;

	@SuppressWarnings("unchecked")
	public InMemoryDatabase() {
		users = Collections.synchronizedMap(new HashMap<String, User>());
		tokens = Collections.synchronizedMap(new HashMap<Integer, VerificationToken>());
		gameStats = Collections.synchronizedMap(new HashMap<Integer, GameStat>());
		healthPoints = Collections.synchronizedMap(new HashMap<Integer, HealthPoints>());
	}

	@VisibleForTesting
	protected List<User> getUsers() {
		return new ArrayList<>(users.values());
	}

	@Override
	public User getUser(String name) {
		requireNonNull(name);
		synchronized (users) {

			return users.get(name);
		}
	}

	@Override
	public boolean nameTaken(String name) {
		return getUser(name) != null;
	}

	@Override
	public User register(String name, String password) {
		synchronized (users) {
			if (nameTaken(name)) {
				return null;
			}

			User user = new User(name, password);
			user.setId(users.size());
			user.setActive(true);
			users.put(name, user);
			storeToken(user.getId(), new VerificationToken(user).getToken());
			return user;
		}
	}

	@Override
	public boolean activateUser(User user) {
		user.setActive(true);
		if (tokens.containsKey(user.getId()))
			tokens.remove(user.getId());
		return user.isActive();
	}

	@Override
	public List<VerificationToken> getVerificationTokens() {
		return new ArrayList<>(tokens.values());
	}

	@Override
	public void storeToken(int id, String token) {
		users.values().stream().forEach(u -> {
			if (u.getId() == id) {
				tokens.put(id, new VerificationToken(token, u));
				return;
			}
		});
	}

	@Override
	public void updateUser(User user) {
		synchronized (users) {
			if (users.containsKey(user.getName())) {
				users.put(user.getName(), user);
			}
		}
	}

	@Override
	public void updateAdminRights(User user, boolean grant) throws SQLException {
		synchronized (users) {
			if (users.containsKey(user.getName())) {
				users.get(user.getName()).setAdmin(grant);
			}
		}
		
	}

	@Override
	public boolean storeGameStats(Game game) {
		synchronized (gameStats) {
			game.setId(gameStats.size());
			gameStats.put(game.getId(), new GameStat(game.getId(), game.getName(), 0, 0, 0, 0, 0, 0, "00:00", 0));
			return true;
		}
	}

	@Override
	public boolean updateGameStats(Game game) {
		synchronized (gameStats) {
			if (gameStats.containsKey(game.getId())) {
				gameStats.put(game.getId(),
						new GameStat(game.getId(), game.getName(), game.getBugsPlayed(), game.getExcusesPlayed(),
								game.getSolutionsPlayed(), game.getSpecialCardsPlayed(), game.getAbilityCardsPlayed(),
								game.getStumblingCardsPlayed(), game.getDurationInTime(), game.getTurnsCounter()));
				return true;
			}
		}
		return false;
	}

	@Override
	public GameStat getGameStat(Integer gameId) {
		synchronized (gameStats) {
			if (gameStats.containsKey(gameId)) {
				return gameStats.get(gameId);
			}
		}
		return null;
	}

	@Override
	public HealthPoints getHealthPoints(Integer gameId, Integer userId) {
		Integer combinedId = Integer.valueOf(String.valueOf(gameId) + String.valueOf(userId));
		synchronized (healthPoints) {
			if (healthPoints.containsKey(combinedId)) {
				return healthPoints.get(combinedId);
			}
		}
		return null;
	}
	
	@Override
	public synchronized void deleteUser(int id) {
		Optional<User> user = users.values().stream().filter(u -> u.getId() == id).findFirst();
		if(!user.isPresent()) {
			throw new DatabaseException("User not found");
		}
		
		tokens.remove(user.get().getId());
		users.remove(user.get().getName());
	}

	@Override
	public void deleteUser(User user) {
		users.remove(user.getName());
	}
}
