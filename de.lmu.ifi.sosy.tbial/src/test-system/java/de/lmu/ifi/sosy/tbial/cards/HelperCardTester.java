package de.lmu.ifi.sosy.tbial.cards;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.PersonalCoffeeMachine;
import de.lmu.ifi.sosy.tbial.model.actioncards.RedBullDispenser;

public class HelperCardTester extends CardTester {

	@Test
	public void testPlayPersonalCoffeeMachine() {

		// Test 1
		assertNotNull(player.getHandcards());
		int countCardsBefore1 = player.getHandcards().size();

		TurnCard card = game.drawCardofClass(player, PersonalCoffeeMachine.class);
		card.setTargetPlayer(player);
		card.play(this.visitor);

		assertEquals(player.getHandcards().size(), countCardsBefore1 + 2);

		// Test 2
		int countCardsBefore2 = player.getHandcards().size();

		card = game.drawCardofClass(player, PersonalCoffeeMachine.class);
		card.setTargetPlayer(player);
		card.play(this.visitor);

		assertEquals(player.getHandcards().size(), countCardsBefore2 + 2);

	}

	@Test
	public void testPlayRedBullDispenser() {

		// Test 1
		assertNotNull(player.getHandcards());
		int countCardsBefore3 = player.getHandcards().size();

		TurnCard card = game.drawCardofClass(player, RedBullDispenser.class);
		card.setTargetPlayer(player);
		card.play(this.visitor);

		assertEquals(player.getHandcards().size(), countCardsBefore3 + 3);

		// Test 2
		int countCardsBefore4 = player.getHandcards().size();
		game.drawCardofClass(player, RedBullDispenser.class).play(this.visitor);
		assertEquals(player.getHandcards().size(), countCardsBefore4 + 3);

	}

}
