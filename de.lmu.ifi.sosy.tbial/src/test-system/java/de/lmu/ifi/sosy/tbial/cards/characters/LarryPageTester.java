package de.lmu.ifi.sosy.tbial.cards.characters;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.CharacterCardTester;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.abilitycards.BugDelegation;
import de.lmu.ifi.sosy.tbial.model.abilitycards.WearsTieAtWork;
import de.lmu.ifi.sosy.tbial.model.stumblingblockcards.FortranMaintenance;

// player7 = LarryPage
public class LarryPageTester extends CharacterCardTester {

	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void checkLarryPageTakesAllCardsOfFiredIfHeKills() {
		game.setCurrentPlayer(player7);

		player.addBuffCard(new BugDelegation());
		player.addBuffCard(new WearsTieAtWork());
		player.addBuffCard(new BugDelegation());

		FortranMaintenance fm = new FortranMaintenance();
		player.addPenaltyCard(fm);

		List<TurnCard> playersCards = new ArrayList<>();
		playersCards.addAll(player.getBuffCards());
		playersCards.addAll(player.getHandcards());

		assertTrue(player.isAlive());
		while (player.isAlive()) {
			game.decHp(player, player7);
		}

		assertFalse(player.isAlive());

		for (TurnCard c : playersCards) {
			assertTrue(player7.getHandcards().contains(c));
		}
		assertFalse(player7.getHandcards().contains(fm));
		assertTrue(player7.getPenaltyCards().contains(fm));

	}

	@Test
	public void checkLarryPageTakesAllCardsOfFiredIfOtherKills() {
		game.setCurrentPlayer(player4);

		player2.addBuffCard(new BugDelegation());
		player2.addBuffCard(new WearsTieAtWork());
		player2.addBuffCard(new BugDelegation());

		FortranMaintenance fm = new FortranMaintenance();
		player2.addPenaltyCard(fm);

		List<TurnCard> playersCards = new ArrayList<>();
		playersCards.addAll(player2.getBuffCards());
		playersCards.addAll(player2.getHandcards());

		assertTrue(player2.isAlive());

		while (player2.isAlive()) {
			game.decHp(player2, player4);
		}

		assertFalse(player2.isAlive());

		for (TurnCard c : playersCards) {
			assertTrue(player7.getHandcards().contains(c));
		}
		assertFalse(player7.getHandcards().contains(fm));
		assertTrue(player7.getPenaltyCards().contains(fm));
	}

	@Test
	public void checkHeapGetsCardsOfFiredIfLarryPageIsDead() {
		while (player7.isAlive())
			game.decHp(player7, player7);

		game.setCurrentPlayer(player2);

		TurnCard buffCard = game.drawCardofClass(player2, BugDelegation.class);
		buffCard.setTargetPlayer(player2);
		game.playCard(buffCard);

		TurnCard penaltyCard = game.drawCardofClass(player2, FortranMaintenance.class);
		penaltyCard.setTargetPlayer(player2);
		game.playCard(penaltyCard);

		game.drawCards(player2, 3);

		List<TurnCard> playersCards = new ArrayList<>();
		playersCards.addAll(player2.getBuffCards());
		playersCards.addAll(player2.getHandcards());
		playersCards.addAll(player2.getPenaltyCards());

		assertTrue(player2.isAlive());

		while (player2.isAlive()) {
			game.decHp(player2, player4);
		}

		assertFalse(player2.isAlive());

		for (TurnCard c : playersCards) {
			assertTrue(game.getHeap().contains(c));
		}

		assertTrue(player2.getHandcards().isEmpty());
		assertTrue(player2.getBuffCards().isEmpty());
		assertTrue(player2.getPenaltyCards().isEmpty());

	}

}
