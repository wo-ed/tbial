package de.lmu.ifi.sosy.tbial.cards.characters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.CharacterCardTester;

//player4 == KentBeck
public class KentBeckTester extends CharacterCardTester {

	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void kentBeckCanDrop2CardsToGainHpOnTurn() {
		game.setCurrentPlayer(player4);
		int cardsToDraw = 6 - player4.getHandcards().size();
		int hpToLose = player4.getHp() + 2 - player4.getMaxHp();

		game.drawCards(player4, cardsToDraw);

		while (hpToLose > 0) {
			game.decHp(player4, player);
			hpToLose--;
		}

		int hpOld = player4.getHp();
		int cardSizeOld = player4.getHandcards().size();
		assertEquals(cardSizeOld, 6);

		game.attemptThrowCardToHeap(player4.getHandcards().get(0));
		game.attemptThrowCardToHeap(player4.getHandcards().get(0));
		// gain hp
		assertEquals(player4.getHp(), hpOld + 1);
		assertEquals(player4.getHandcards().size(), cardSizeOld - 2);

		game.attemptThrowCardToHeap(player4.getHandcards().get(0));
		game.attemptThrowCardToHeap(player4.getHandcards().get(0));
		assertEquals(player4.getHp(), hpOld + 2);
		assertEquals(player4.getHandcards().size(), cardSizeOld - 4);

		game.attemptThrowCardToHeap(player4.getHandcards().get(0));
		game.attemptThrowCardToHeap(player4.getHandcards().get(0));
		// cannot gain more hp than max
		assertTrue(player4.getHp() == hpOld + 2);
		assertTrue(player4.getHandcards().size() == cardSizeOld - 6);

		// cannot gain if to less cards but lose handcard
		game.cleanHandcards(player4);
		game.drawCard(player4);

		game.decHp(player4, player);
		hpOld = player4.getHp();
		game.attemptThrowCardToHeap(player4.getHandcards().get(0));
		assertTrue(player4.getHp() == hpOld);
		assertTrue(player4.getHandcards().size() == 0);

	}

	@Test
	public void kentBeckCanDrop2CardsToGainHpNotOnTurn() {
		game.setCurrentPlayer(player);
		int cardsToDraw = 6 - player4.getHandcards().size();
		int hpToLose = player4.getHp() + 2 - player4.getMaxHp();

		game.drawCards(player4, cardsToDraw);

		while (hpToLose > 0) {
			game.decHp(player4, player);
			hpToLose--;
		}

		int hpOld = player4.getHp();
		int cardSizeOld = player4.getHandcards().size();
		game.attemptThrowCardToHeap(player4.getHandcards().get(0));
		game.attemptThrowCardToHeap(player4.getHandcards().get(0));

		// gain hp
		assertEquals(player4.getHp(), hpOld + 1);
		assertEquals(player4.getHandcards().size(), cardSizeOld - 2);

		game.attemptThrowCardToHeap(player4.getHandcards().get(0));
		game.attemptThrowCardToHeap(player4.getHandcards().get(0));
		assertTrue(player4.getHp() == hpOld + 2);
		assertTrue(player4.getHandcards().size() == cardSizeOld - 4);

		game.attemptThrowCardToHeap(player4.getHandcards().get(0));
		game.attemptThrowCardToHeap(player4.getHandcards().get(0));
		// cannot gain more hp than max (dont loose cards)
		assertTrue(player4.getHp() == hpOld + 2);
		assertTrue(player4.getHandcards().size() == cardSizeOld - 4);

		// cannot gain if to less cards
		game.cleanHandcards(player4);
		game.drawCard(player4);

		game.decHp(player4, player);
		hpOld = player4.getHp();

		game.attemptThrowCardToHeap(player4.getHandcards().get(0));

		assertTrue(player4.getHp() == hpOld);
		assertTrue(player4.getHandcards().size() == 1);

	}

}
