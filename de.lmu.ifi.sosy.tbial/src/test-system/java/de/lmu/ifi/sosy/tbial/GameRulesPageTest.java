package de.lmu.ifi.sosy.tbial;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.pages.GameRulesPage;

public class GameRulesPageTest extends PageTestBase {
	@Before
	public void setUp(){
		setupApplication();
	}
	
	@Test
	public void notLoggedInTest (){
		tester.startPage(GameRulesPage.class);
		tester.assertRenderedPage(GameRulesPage.class);	
	}
	
	@Test
	public void loggedInPageTest (){
		database.register("user", "1");
		TBIALSession session = (TBIALSession) getSession();
		session.signIn("user", "1");
		tester.startPage(GameRulesPage.class);
		tester.assertRenderedPage(GameRulesPage.class);
	}
	
	@Test
	public void checkContents (){
		tester.startPage(GameRulesPage.class);
		tester.assertContains("Book of Rules");
		tester.assertContains("Introduction");
		tester.assertContains("About the Game");
		tester.assertContains("Cards involved");
		tester.assertContains("How to Play");
		tester.assertContains("Preparations");
		tester.assertContains("Playing The Bug is a Lie");
		tester.assertContains("Dealing with stumbling blocks");
		tester.assertContains("Popping cards from the stack");
		tester.assertContains("Playing Cards");
		tester.assertContains("Finishing the Turn");
		tester.assertContains("The Cards");
		tester.assertContains("Role Cards");
		tester.assertContains("Character Cards");
		tester.assertContains("Action Cards");
		tester.assertContains("Ability Cards");
		tester.assertContains("Stumbling Block Cards");
		tester.assertContains("Other Rules");	
	}
	
	@Test 
	public void notInContents (){
		tester.startPage(GameRulesPage.class);
		tester.assertContainsNot("another game");
	}
	
	
}
