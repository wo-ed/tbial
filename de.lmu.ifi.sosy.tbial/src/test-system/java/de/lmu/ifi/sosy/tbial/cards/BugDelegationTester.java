package de.lmu.ifi.sosy.tbial.cards;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.abilitycards.BugDelegation;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;

public class BugDelegationTester extends CardTester {
	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testBugDelegationCard() {

		game.setCurrentPlayer(player);
		game.cleanHandcards(player);
		game.resetCountBugDelegations();
		game.cleanHandcards(player2);
		TurnCard bDel = game.drawCardofClass(player, BugDelegation.class);
		bDel.setTargetPlayer(player);
		bDel.play(this.visitor);

		// Check BugDelegation card is in buffCards now
		assertTrue(player.getBuffCards().contains(bDel));
		assertTrue(player.hasBugDelegation());

		game.setCurrentPlayer(player2);
		game.cleanHandcards(player);
		game.cleanHandcards(player2);
		TurnCard bug = game.drawCardofClass(player2, BugReport.class);
		bug.setTargetPlayer(player);

		// test the case in which delegation was a success
		setHp(player, 3);
		game.playCard(bug);
		game.setBugAlreadyPlayedFalse();
		assertFalse(player2.getHandcards().contains(bug));
		assertEquals(player.getHp(), 3);
		bug = game.drawCardofClass(player2, BugReport.class);
		bug.setTargetPlayer(player);
		// test the case in which delegation fails
		setHp(player, 3);
		game.setBugAlreadyPlayedFalse();
		game.playCard(bug);
		assertFalse(player2.getHandcards().contains(bug));
		assertEquals(player.getHp(), 2);
	}

	public void setHp(Player p, int i) {
		while (p.getCharacter().getHp() < i) {
			p.getCharacter().incHp();
		}
		while (p.getCharacter().getHp() > i) {
			p.getCharacter().decHp();
		}
	}

}
