package de.lmu.ifi.sosy.tbial.cards.characters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.CharacterCardTester;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.abilitycards.BugDelegation;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;

//player12
public class TerryWeissmanTester extends CharacterCardTester {

	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void testBugDelegation() {
		game2.setCurrentPlayer(player13);
		game2.resetCountBugDelegations();
		// MockGame:first Delegation succeeds - each forth succeeds

		assertTrue(player12.getCharacter().hasBugDelegation());

		// First Deelgation succeeds
		TurnCard b = game2.drawCardofClass(player13, BugReport.class);
		b.setTargetPlayer(player12);

		game2.cleanHandcards(player12);
		int oldHp = player12.getHp();
		int oldCardSize = player12.getHandcards().size();

		game2.playCard(b);

		assertEquals(oldCardSize, player12.getHandcards().size());
		assertEquals(oldHp, player12.getHp());

		assertTrue(player12.getCharacter().hasBugDelegation());

		game2.setCurrentPlayer(player9);

		game2.cleanHandcards(player12);
		// Second Delegation fails
		TurnCard b2 = game2.drawCardofClass(player9, BugReport.class);
		b2.setTargetPlayer(player12);
		game2.playCard(b2);

		assertEquals(oldCardSize, player12.getHandcards().size());
		assertEquals(oldHp - 1, player12.getHp());

	}

	@Test
	public void checkBugDelegationNotAffectedbyBugDelegationCard() {
		game2.setCurrentPlayer(player12);
		assertTrue(player12.getCharacter().hasBugDelegation());

		// Check has Bug Delegation after playing BugDelegation card
		TurnCard bDel = game2.drawCardofClass(player12, BugDelegation.class);
		game2.playCard(bDel);
		assertTrue(player12.getCharacter().hasBugDelegation());

		// Check has Bug Delegation after losing BugDelegation card
		game2.throwCardToHeap(bDel);
		assertTrue(player12.getCharacter().hasBugDelegation());

	}

}
