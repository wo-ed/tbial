package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Arrays;

import org.apache.wicket.util.tester.FormTester;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.pages.DeleteAccountPage;

public class DeleteAccountTest extends PageTestBase {

	private String username = "testuser";
	private String password = "pw";
	private TBIALSession session;

	@Before
	public void setUp() {
		setupApplication();
		database.register(username, password);
		session = getSession();
		session.signIn(username, password);
	}

	@Test
	public void testDeleteAccount() {
		tester.startPage(DeleteAccountPage.class);
		tester.assertRenderedPage(DeleteAccountPage.class);
		assertNotNull(database.getUser(username));

		Arrays.asList(null, "", "wrongPassword").forEach(pw -> {
			submitDelete(pw);
			assertDeleteFailed();
		});

		submitDelete(password);
		assertDeleteSucceeded();
	}

	private void submitDelete(String password) {
		FormTester formTester = tester.newFormTester("deleteForm", false);
		formTester.setValue("passwordField", password);
		formTester.submit("deleteAccountBtn");
	}

	private void assertDeleteSucceeded() {
		assertNull(database.getUser(username));
		tester.assertRenderedPage(Login.class);
	}

	private void assertDeleteFailed() {
		assertNotNull(database.getUser(username));
		tester.assertRenderedPage(DeleteAccountPage.class);
	}

	@Test
	public void deleteAdminTest() {
		tester.startPage(DeleteAccountPage.class);
		tester.assertRenderedPage(DeleteAccountPage.class);
		assertNotNull(database.getUser(username));

		User user = session.getUser();
		user.setAdmin(true);

		submitDelete(password);
		assertDeleteFailed();
	}
}
