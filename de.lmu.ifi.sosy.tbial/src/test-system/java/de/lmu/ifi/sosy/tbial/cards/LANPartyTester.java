package de.lmu.ifi.sosy.tbial.cards;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.LANParty;

public class LANPartyTester extends CardTester {
	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void checkDeadPlayersDontReceiveHp() {
		List<Player> ps1 = createPlayerList(5);
		ps1.forEach(p -> {
			while (p.isAlive()) {
				game.decHp(p, p);
			}
		});

		game.drawCardofClass(player, LANParty.class).play(this.visitor);
		final List<Player> playersAlive = ps1.stream().filter(a -> a.getHp() > 0).collect(Collectors.toList());
		assertEquals(playersAlive.size(), 0);
	}

	@Test
	public void checkPlayersMaxHpReached() {
		List<Player> ps = createPlayerList(5);

		ps.forEach(p -> {
			while (p.getHp() < p.getMaxHp()) {
				game.incHp(p);
			}
		});
		List<Integer> oldHps = new ArrayList<>();
		ps.forEach(p -> oldHps.add(p.getHp()));

		game.drawCardofClass(player, LANParty.class).play(this.visitor);
		assertEquals(ps.size(), oldHps.size());
		Iterator<Player> iterPlayer = ps.iterator();
		Iterator<Integer> iterOldHps = oldHps.iterator();
		while (iterPlayer.hasNext() && iterOldHps.hasNext()) {
			int tmpHp = iterPlayer.next().getHp();
			int tmpOldHp = iterOldHps.next();
			assertEquals(tmpHp, tmpOldHp);
		}
	}

	@Test
	public void checkEachPlayerReceivesHp() {
		// Each Player alive receives 1 hp
		List<Player> ps = createPlayerList(5);
		randomInitHp(ps);
		List<Integer> oldHps = new ArrayList<>();
		ps.forEach(p -> oldHps.add(p.getHp()));

		TurnCard card = game.drawCardofClass(player, LANParty.class);
		card.setTargetPlayer(player);
		card.play(this.visitor);

		assertEquals(ps.size(), oldHps.size());
		Iterator<Player> iterPlayer = ps.iterator();
		Iterator<Integer> iterOldHps = oldHps.iterator();
		while (iterPlayer.hasNext() && iterOldHps.hasNext()) {
			Player playerTmp = iterPlayer.next();
			int tmpHp = playerTmp.getHp();
			int tmpOldHp = iterOldHps.next();
			if (tmpOldHp <= 0 || tmpOldHp == playerTmp.getMaxHp())
				assertEquals(tmpHp, tmpOldHp);
			else {
				assertEquals(tmpHp, tmpOldHp + 1);
			}
		}

	}

	private List<Player> createPlayerList(int size) {
		game.init();
		List<Player> players = new ArrayList<>(game.getAllPlayers());
		return players;
	}

	private void randomInitHp(List<Player> players) {
		Random r = new Random();
		players.forEach(p -> {
			for (int i = 0; i < r.nextInt(5); i++) {
				game.decHp(p, p);
			}
		});
	}

}
