package de.lmu.ifi.sosy.tbial.cards.characters;

import org.junit.Assert;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.CharacterCardTester;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.actioncards.LameExcuse;
import de.lmu.ifi.sosy.tbial.model.characters.LinusTorvalds;

public class LinusTorvaldsTester extends CharacterCardTester {

	@Test
	public void linusTorvaldsBugsCanNotBeDeflectedWithOneExcuse() {
		game2.setCurrentPlayer(player8);

		Assert.assertEquals(true, player8.getCharacter() instanceof LinusTorvalds);
		Assert.assertEquals(5, player8.getCharacter().getHp());

		player10.getHandcards().clear();
		game2.drawCardofClass(player10, LameExcuse.class);

		// Player 10 is Steve Ballmer and has 4 hp
		TurnCard card = game2.drawCardofClass(player8, BugReport.class);
		card.setTargetPlayer(player10);
		game2.playCard(card);

		Assert.assertEquals("Steve Ballmer wurde 1 Hp abgezogen weil er nicht kontern kann", 3, player10.getHp());
		Assert.assertEquals(PlayerState.WAIT, player10.getState());
	}

	@Test
	public void linusTorvaldsBugsCanBeDeflectedWithTwoExcuses() {
		game2.setCurrentPlayer(player8);

		Assert.assertEquals(true, player8.getCharacter() instanceof LinusTorvalds);
		Assert.assertEquals(5, player8.getCharacter().getHp());

		game2.drawCardofClass(player10, LameExcuse.class);
		game2.drawCardofClass(player10, LameExcuse.class);

		// Player 10 is Steve Ballmer and has 4 hp
		TurnCard card = game2.drawCardofClass(player8, BugReport.class);
		card.setTargetPlayer(player10);
		game2.playCard(card);

		Assert.assertEquals("Mark Zuckerburg wurde kein Hp abgezogen weil er kontern kann", 4, player10.getHp());
		Assert.assertEquals(PlayerState.DEFLECT_BUG, player10.getState());
	}

}
