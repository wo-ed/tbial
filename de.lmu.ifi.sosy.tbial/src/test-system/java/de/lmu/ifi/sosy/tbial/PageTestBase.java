package de.lmu.ifi.sosy.tbial;

import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.util.tester.WicketTester;

import de.lmu.ifi.sosy.tbial.db.Database;
import de.lmu.ifi.sosy.tbial.db.InMemoryDatabase;
import de.lmu.ifi.sosy.tbial.db.TestMailService;

public abstract class PageTestBase {

	protected TBIALApplication application;

	protected Database database;

	protected WicketTester tester;

	protected final void setupApplication() {
		setupApplication(RuntimeConfigurationType.DEVELOPMENT);
	}

	protected final void setupApplication(RuntimeConfigurationType configuration) {
		database = new InMemoryDatabase();
		application = new TBIALApplication(database);
		application.setConfigurationType(configuration);
		application.getUserService().setMailService(new TestMailService());
		tester = new WicketTester(application);
	}

	protected TBIALSession getSession() {
		return (TBIALSession) tester.getSession();
	}

}
