package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.wicket.extensions.ajax.markup.html.tabs.AjaxTabbedPanel;
import org.apache.wicket.extensions.markup.html.tabs.AbstractTab;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.pages.ConfirmationPanel;
import de.lmu.ifi.sosy.tbial.pages.GameLobbyPanel;
import de.lmu.ifi.sosy.tbial.pages.LobbyPanel;
import de.lmu.ifi.sosy.tbial.pages.PlayerListPanel;
import de.lmu.ifi.sosy.tbial.pages.TabPage;
import de.lmu.ifi.sosy.tbial.pages.UserStatPanel;

public class TabPageTest extends PageTestBase {

	TBIALSession session;

	@Before
	public void setUp() {
		setupApplication();
		database.register("testuser", "1");
		session = getSession();
		session.signIn("testuser", "1");
	}

	@Test
	public void tabbedPanelTest() {
		User user = session.getUser();

		Game game = new Game("G1", (short) 4, false, user);
		TBIALApplication.getInstance().addGame(game);
		TBIALApplication.getInstance().joinGame(user, game);
		TabPage tabPage = tester.startPage(TabPage.class);
		tester.assertRenderedPage(TabPage.class);

		tester.assertComponent("tabs", AjaxTabbedPanel.class);

		AjaxTabbedPanel<AbstractTab> tabbedPanel = tabPage.getTabbedPanel();
		assertTrue(tabbedPanel.getSelectedTab() == 0);
		tester.assertComponent("tabs:panel:lobbyTab", LobbyPanel.class);

		tabPage.selectPlayerListTab();
		assertTrue(tabbedPanel.getSelectedTab() == 1);
		tester.assertComponent("tabs:panel:playerListTab", PlayerListPanel.class);

		tabPage.selectUserStatTab();
		assertTrue(tabbedPanel.getSelectedTab() == 2);
		tester.assertComponent("tabs:panel:userStatTab", UserStatPanel.class);

		tabPage.setGameLobbyTab(game.getName());
		assertTrue(tabbedPanel.getSelectedTab() == 3);
		tester.assertComponent("tabs:panel:gameLobbyTab", GameLobbyPanel.class);

		tabPage.selectLobbyTab();
		assertTrue(tabbedPanel.getSelectedTab() == 0);
		tester.assertComponent("tabs:panel:lobbyTab", LobbyPanel.class);
	}

	@Test
	public void joinAnotherGameTest() {
		User user = session.getUser();

		Game game = new Game("G2", (short) 4, false, user);
		TBIALApplication.getInstance().addGame(game);
		TBIALApplication.getInstance().joinGame(user, game);
		TabPage tabPage = tester.startPage(TabPage.class);
		tester.assertRenderedPage(TabPage.class);

		tester.assertComponent("tabs", AjaxTabbedPanel.class);

		AjaxTabbedPanel<AbstractTab> tabbedPanel = tabPage.getTabbedPanel();
		assertTrue(tabbedPanel.getSelectedTab() == 0);
		tester.assertComponent("tabs:panel:lobbyTab", LobbyPanel.class);

		tabPage.setGameLobbyTab(game.getName());
		assertTrue(tabbedPanel.getSelectedTab() == 3);
		tester.assertComponent("tabs:panel:gameLobbyTab", GameLobbyPanel.class);
		tester.assertLabel("tabs:panel:gameLobbyTab:gameName", game.getName());

		tabPage.selectLobbyTab();
		assertTrue(tabbedPanel.getSelectedTab() == 0);
		tester.assertComponent("tabs:panel:lobbyTab", LobbyPanel.class);

		Game anotherGame = new Game("G4", (short) 5, false, new User("user2", "password"));
		TBIALApplication.getInstance().addGame(anotherGame);
		tabPage.tryToJoinGame(anotherGame);

		assertTrue(tabbedPanel.getSelectedTab() == 0);
		tester.assertComponent("tabs:panel:lobbyTab", ConfirmationPanel.class);

		String expectedMessage = String.format(
				"You are about to leave game %s and enter game %s. Do you want to continue?", game.getName(),
				anotherGame.getName());
		tester.assertLabel("tabs:panel:lobbyTab:message", expectedMessage);

		tester.clickLink("tabs:panel:lobbyTab:cancel");
		assertTrue(tabbedPanel.getSelectedTab() == 0);
		tester.assertComponent("tabs:panel:lobbyTab", LobbyPanel.class);

		tabPage.tryToJoinGame(anotherGame);

		assertTrue(tabbedPanel.getSelectedTab() == 0);
		tester.assertComponent("tabs:panel:lobbyTab", ConfirmationPanel.class);

		tester.assertLabel("tabs:panel:lobbyTab:message", expectedMessage);

		tester.clickLink("tabs:panel:lobbyTab:confirm");

		assertTrue(tabbedPanel.getSelectedTab() == 3);
		tester.assertComponent("tabs:panel:gameLobbyTab", GameLobbyPanel.class);
		tester.assertLabel("tabs:panel:gameLobbyTab:gameName", anotherGame.getName());

		assertFalse(game.getUsers().contains(user));
	}

}
