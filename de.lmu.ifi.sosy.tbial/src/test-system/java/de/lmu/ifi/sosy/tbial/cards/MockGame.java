package de.lmu.ifi.sosy.tbial.cards;

import java.util.ArrayList;

import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Game;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.RoleCard;
import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.characters.CharacterCard;

public class MockGame extends Game {

	private static final long serialVersionUID = 1L;

	private boolean randomFirst = true;

	private int countBugDelegations = 0;

	public MockGame(String name, short capacity, boolean isPrivate, User owner, boolean randomFirst) {
		super(name, capacity, isPrivate, owner);
		this.randomFirst = randomFirst;
	}

	public MockGame(String name, short capacity, boolean isPrivate, String password, User owner, boolean randomFirst) {
		super(name, capacity, isPrivate, password, owner);
		this.randomFirst = randomFirst;
	}

	@Override
	protected void initializePlayerAttributes(Player player, ArrayList<RoleCard> roles,
			ArrayList<CharacterCard> characters) {

		RoleCard role = roles.remove(randomFirst ? 0 : roles.size() - 1);
		player.setRole(role);

		CharacterCard character = characters.remove(randomFirst ? 0 : characters.size() - 1);
		character.initHp(role);
		player.setCharacter(character);

		player.getHandcards().clear();
		this.drawCards(player, player.getHp());
	}

	public void resetCountBugDelegations() {
		countBugDelegations = 0;
	}

	@Override
	public boolean tryBugDelegation(TurnCard bug) {
		if (bug.getTargetPlayer().hasBugDelegation()) {
			boolean res = this.countBugDelegations % 4 == 0;
			this.countBugDelegations++;

			if (res) {
				this.delegateBug(bug);
				return true;
			}
		}
		if (bug.getTargetPlayer().getCharacter().hasBugDelegation()) {
			boolean res = this.countBugDelegations % 4 == 0;
			this.countBugDelegations++;
			if (res) {
				this.delegateBug(bug);
				return true;
			}
		}
		return false;

	}

	public TurnCard drawCardofClass(Player p, Class<? extends TurnCard> toDraw) {

		for (TurnCard tc : this.getStack()) {
			if (tc.getClass() == toDraw) {
				getStack().remove(tc);
				tc.setPlayer(p);
				p.getHandcards().add(tc);
				tc.setTargetPlayer(p);
				return tc;
			}
		}
		for (TurnCard tc : this.getHeap()) {
			if (tc.getClass() == toDraw) {
				getHeap().remove(tc);
				tc.setPlayer(p);
				p.getHandcards().add(tc);
				tc.setTargetPlayer(p);
				return tc;
			}
		}

		try {
			TurnCard drawable = toDraw.newInstance();
			drawable.setPlayer(p);
			drawable.setTargetPlayer(p);
			p.getHandcards().add(drawable);
			return drawable;
		} catch (InstantiationException e) {

			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return null;

	}

	public void cleanHandcards(Player p) {
		this.getHeap().addAll(p.getHandcards());
		p.getHandcards().clear();
	}

	public void cleanAllHandcards() {
		getPlayers().stream().forEach(p -> cleanHandcards(p));
	}

	public void setCurrentPlayer(Player currentPlayer) {
		int i = 0;
		while (!currentPlayer.equals(this.getCurrentPlayer()) && i < 8) {
			if (!this.endTurn()) {
				this.cleanHandcards(this.getCurrentPlayer());
				this.endTurn();
			}
			i++;
		}
		if (!currentPlayer.equals(this.getCurrentPlayer()))
			throw new IllegalArgumentException("player not found in game");
	}

	public boolean endTurn() {
		return super.endTurn(getCurrentPlayer());
	}

	public void setBugAlreadyPlayedFalse() {
		setBugAlreadyPlayed(false);
	}

}
