package de.lmu.ifi.sosy.tbial.cards;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.model.TurnCard;
import de.lmu.ifi.sosy.tbial.model.actioncards.BugReport;
import de.lmu.ifi.sosy.tbial.model.stumblingblockcards.OffTheJobTraining;

public class OffTheJobTrainingTester extends CardTester {
	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void playCardAfterBug() {
		game.setCurrentPlayer(player);
		player.getHandcards().clear();
		game.drawCardofClass(player, BugReport.class);

		TurnCard card = player.getHandcards().get(0);
		card.setTargetPlayer(player2);
		Assert.assertEquals(true, card instanceof BugReport);
		game.playCard(card);

		player.getHandcards().clear();
		game.drawCardofClass(player, OffTheJobTraining.class);

		card = player.getHandcards().get(0);
		card.setTargetPlayer(player2);
		Assert.assertEquals(true, card instanceof OffTheJobTraining);
		game.playCard(card);

		Assert.assertEquals(1, player2.getPenaltyCards().size());
	}

}
