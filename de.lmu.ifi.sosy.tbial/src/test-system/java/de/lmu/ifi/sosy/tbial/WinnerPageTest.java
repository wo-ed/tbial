package de.lmu.ifi.sosy.tbial;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.wicket.extensions.ajax.markup.html.tabs.AjaxTabbedPanel;
import org.apache.wicket.extensions.markup.html.tabs.AbstractTab;
import org.junit.Before;
import org.junit.Test;

import de.lmu.ifi.sosy.tbial.cards.MockGame;
import de.lmu.ifi.sosy.tbial.db.User;
import de.lmu.ifi.sosy.tbial.model.Player;
import de.lmu.ifi.sosy.tbial.model.PlayerState;
import de.lmu.ifi.sosy.tbial.pages.GameStatPanel;
import de.lmu.ifi.sosy.tbial.pages.WinnerPage;
import de.lmu.ifi.sosy.tbial.pages.WinnerPanel;

public class WinnerPageTest extends PageTestBase {

	TBIALSession session;
	MockGame game;

	User user;

	@Before
	public void setUp() {
		setupApplication();
		database.register("testuser", "1");
		session = getSession();
		session.signIn("testuser", "1");

		user = session.getUser();

		game = new MockGame("G1", (short) 4, false, user, false);

		User u2 = new User("u2", "");
		User u3 = new User("u3", "");
		User u4 = new User("u4", "");

		game.join(u2);
		game.join(u3);
		game.join(u4);

		game.init();
	}

	@Test
	public void winnerTabbedPanelTest() {

		WinnerPage winnerPage = tester.startPage(new WinnerPage(game));
		tester.assertRenderedPage(winnerPage.getPageClass());

		tester.assertComponent("tabs", AjaxTabbedPanel.class);

		AjaxTabbedPanel<AbstractTab> tabbedPanel = winnerPage.getTabbedPanel();
		assertTrue(tabbedPanel.getSelectedTab() == 0);
		tester.assertComponent("tabs:panel:winnerTab", WinnerPanel.class);

		tabbedPanel.setSelectedTab(1);
		tester.assertComponent("tabs:panel:gameStatTab", GameStatPanel.class);

	}

	@Test
	public void winnerTabTest() {

		Player manager = game.getManager();
		Player consultant = game.getConsultant();
		List<Player> evilCodeMonkeys = game.getEvilCodeMonkeys();

		// manager is fired
		while (manager.getHp() > 0) {
			game.decHp(manager, consultant);
		}

		assertFalse(manager.isAlive());
		assertTrue(manager.getState() == PlayerState.DEAD);

		assertTrue(game.getWinners().size() == 2);

		assertFalse(game.getWinners().contains(manager));
		assertFalse(game.getWinners().contains(consultant));

		// evil code monkeys win
		assertTrue(game.getWinners().containsAll(evilCodeMonkeys));

		WinnerPage winnerPage = tester.startPage(new WinnerPage(game));
		tester.assertRenderedPage(winnerPage.getPageClass());

		AjaxTabbedPanel<AbstractTab> tabbedPanel = winnerPage.getTabbedPanel();
		assertTrue(tabbedPanel.getSelectedTab() == 0);

		tester.assertComponent("tabs:panel:winnerTab", WinnerPanel.class);

		tester.startComponentInPage(new WinnerPanel("winnerTab", game));

		List<Player> winnerList = game.getWinners();
		assertTrue(winnerList.size() == 2);
		tester.assertListView("winnerTab:winnerList", winnerList);

	}

}
