# Praktikum Software Engineering für Fortgeschrittene: The Bug is a Lie

Dieses Repository soll als Einstieg in das Praktikum dienen.
Es wird eine Beispielanwendung (Prototyp) bereitgestellt,
welche bereits einige der erforderlichen Technologien verwendet.

Weitere Informationen befinden sich auf der [SWEP Homepage](https://www.sosy-lab.org/~dbeyer/Teaching/2017-SS-SWEP/).

## Spielanleitung

Eine Spielanleitung ist [hier als PDF](TBIAL_Spielanleitung.pdf) verfügbar.

## Beispielanwendung ausführen

Dieses Repository stellt eine lauffähige Beispielanwendung zur Verfügung,
die bereits auf den notwendigen Technologien (Apache Tomcat, Apache Derby, Wicket) basiert.
Die Beispielanwendung ist als Projekt für Eclipse verfügbar.
Nachfolgend ist grob skiziert, wie das projekt in Eclipse ausgeführt wird.

### Vorbereitung

- *Eclipse IDE for Java EE Developers* (Paket [hier](https://www.eclipse.org/downloads/eclipse-packages/) verfügbar
  (Achtung: nicht das Standard-Paket wählen!)
- *Apache Tomcat 8* (oder neuer) in Eclipse einrichten
  (Window/Preferences/Server/Runtime Environment -> Add -> Apache Tomcat 8 (oder neuer))
- *Apache Derby* installieren und einrichten, Anleitung [hier](https://db.apache.org/derby/quick_start.html) verfügbar
- *Maven* installieren und einrichten, falls notwendig

### Projekt Setup und Ausführung

- Dieses Projekt-Repository auschecken und den Unterordner
  [de.lmu.ifi.sosy.tbial](de.lmu.ifi.sosy.tbial)
  als Eclipse-Projekt einrichten
- Apache Derby starten (als separaten Prozess!), ansonsten steht keine Datenbank zur Verfügung
- '*mvn exec:exec@create-development-db*' einmalig ausführen, um die Datenbank zu initialisieren
- '*mvn install*' ausführen, um das Projekt zu kompilieren
- '*Run on Server*', dann den eingerichteten Tomcat-Server wählen, um das Projekt zu deployen

Sollten bei der erstmaligen Ausführung Fehler wie *ClassNotFound* bzgl. Wicket auftreten,
kann es sein, dass Eclipse noch nicht alle benötigten Libraries erkannt hat.
Möglicherweise hilft dann '*maven clean*' oder ein Neustart von Eclipse,
gefolgt von den oben genannten Schritten.

### Screenshot

Die Webseite des gegebenen Prototyps sollte in etwa folgendermaßen aussehen:

---
![Screenshot des Prototyps border](Screenshot_Prototype.png)
---

### Tipps, Tools und Tutorials für Wicket:

- *[RepeatingView](https://ci.apache.org/projects/wicket/apidocs/6.x/org/apache/wicket/markup/repeater/RepeatingView.html),
[ListView](https://ci.apache.org/projects/wicket/apidocs/6.x/org/apache/wicket/markup/html/list/ListView.html) und
[GridViw](https://ci.apache.org/projects/wicket/apidocs/6.x/org/apache/wicket/markup/repeater/data/GridView.html)* (Für Listen, Tabellen und Grids.)
[Guide](https://ci.apache.org/projects/wicket/guide/6.x/guide/repeaters.html)
- *[Behavior](https://ci.apache.org/projects/wicket/apidocs/6.x/org/apache/wicket/behavior/Behavior.html)* (zb. um CSS Style Elemente zur Laufzeit hinzuzufügen).
```
target.add(new Behavior() {
			@Override
			public void onComponentTag(Component component, ComponentTag tag) {
				tag.put("style", "height: 100%");
			}
		});
```


Examples:

http://examples7x.wicket.apache.org/index.html

### Link zum Burndown-Chart
https://docs.google.com/spreadsheets/d/1VlYZQbbH56TanDnfMp2nUf_-XQSXdrhM-d6Q-5oUvzc/edit?usp=sharing

### Link Zur Übersicht aller Spielkarten
https://docs.google.com/spreadsheets/d/1ZnIFBxyYhlYvA0p1_g5hDNZ45pyTsZ5Bh-Kkrs-GNBc/edit?usp=sharing

### Libraries und Licensen

| Library                                               | Lizenz             |
|-------------------------------------------------------|--------------------------|
| Wicket Core                                           | Apache 2.0               |
| Wicket Extensions                                     | Apache 2.0               |
| Wicket Auth Roles                                     | Apache 2.0               |
| Java Servlet API                                      | CDDL2,GPL                |
| Apache Log4j SLF4J Binding                            | Apache 2.0               |
| Apache Log4j Core                                     | Apache 2.0               |
| Junit                                                 | EPL 1.0                  |
| Mockito                                               | MIT                      |
| Hamcrest All                                          | BSD 2-clause             |
| JaCoCo :: Maven Plugin                                | EPL 1.0                  |
| Apache Derby Database Engine And Embedded JDBC Driver | Apache 2.0               |
| Apache Derby Tools                                    | Apache 2.0               |
| Apache Derby Client JDBC Driver                       | Apache 2.0               |
| Guava: Google Core Libraries For Java                 | Apache 2.0               |
| PostgreSQL JDBC Driver JDBC 4.2                       | BSD 2-clause, POSTGRESQL |
| JavaMail API (compat)                                 | CDDL 1.0                 |
